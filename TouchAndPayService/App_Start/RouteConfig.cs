﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace TouchAndPayService
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
            routes.MapRoute(
               name: "Default1",
               url: "gulfbox/transaction/check",
               defaults: new { controller = "gulfbox", action = "transactioncheck" }
           );
            routes.MapRoute(
              name: "Default2",
              url: "gulfbox/transaction/confirm",
              defaults: new { controller = "gulfbox", action = "transactionconfirm" }
          );
        }
    }
}
