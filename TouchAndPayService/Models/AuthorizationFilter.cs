﻿using EntityLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http.Controllers;
using System.Net.Http;
using System.ServiceModel.Channels;

namespace TouchAndPayService.Models
{
    public class AuthorizationFilter : System.Web.Http.Filters.ActionFilterAttribute
    {
        protected TouchAndPayEntities _context = new TouchAndPayEntities();
        protected string salt = System.Configuration.ConfigurationManager.AppSettings["salt"];
        public override void OnActionExecuting(HttpActionContext actionContext)
        {

            string actionName = actionContext.ActionDescriptor.ActionName;
            string controllerName = actionContext.ControllerContext.ControllerDescriptor.ControllerName;

            var re = actionContext.Request;
            var headers = re.Headers;

            HttpRequestMessage request = actionContext.Request;
            var ip = "";
            if (request.Properties.ContainsKey("MS_HttpContext"))
            {
                ip = ((HttpContextWrapper)request.Properties["MS_HttpContext"]).Request.UserHostAddress;
            }
            else if (request.Properties.ContainsKey(RemoteEndpointMessageProperty.Name))
            {
                RemoteEndpointMessageProperty prop = (RemoteEndpointMessageProperty)request.Properties[RemoteEndpointMessageProperty.Name];
                ip = prop.Address;
            }
            else if (HttpContext.Current != null)
            {
                ip = HttpContext.Current.Request.UserHostAddress;
            }
            else
            {
                ip = null;
            }



            //string CustomerIP = "";
            //if (HttpContext.Current.Request.ServerVariables["HTTP_VIA"] != null)
            //{
            //    CustomerIP = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
            //}
            //else
            //{
            //    CustomerIP = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"].ToString();
            //}

            //object property;
            //request.Properties.TryGetValue(typeof(RemoteEndpointMessageProperty).FullName, out property);
            //RemoteEndpointMessageProperty remoteProperty = property as RemoteEndpointMessageProperty;

            //if (request.Properties.ContainsKey("MS_OwinContext"))
            //{
            //    dynamic owinContext = request.Properties["MS_OwinContext"];
            //    var ips= owinContext.Request.RemoteIpAddress;
            //}


            if (headers.Contains("x-token"))
            {




                string token = headers.GetValues("x-token").First();
                //string uToken = headers.GetValues("u-token").First();
                //var userIdString = Convert.FromBase64String(uToken);
                //var userId = Convert.ToInt64(userIdString);




                //try
                //{
                //    var rowIp = _context.tb_UserTransactionIp.Create();
                //    rowIp.NetworkIp = ip;
                //    rowIp.TimeStamp = System.DateTime.UtcNow;
                //    rowIp.UserId = userId;
                //    _context.tb_UserIp.Add(rowIp);
                //    _context.SaveChanges();
                //}
                //catch (Exception ex)
                //{

                //}




                try
                {
                    var validToken = _context.tAccessTokens.Where(z => z.Token == token).FirstOrDefault();
                    if (validToken == null)
                    {
                        //var currentIp = _context.tb_UserIp.FirstOrDefault(z => z.UserId == userId && z.NetworkIp == ip);
                        //if (currentIp == null)
                        //{
                        actionContext.Response = actionContext.Request.CreateResponse(
              HttpStatusCode.Unauthorized,
              new { status = false, message = "Invalid Token", ip = ip },
              actionContext.ControllerContext.Configuration.Formatters.JsonFormatter);
                        //}
                    }
                }
                catch (Exception ex)
                {
                    actionContext.Response = actionContext.Request.CreateResponse(
             HttpStatusCode.Unauthorized,
             new { status = false, message = ex.Message, ip = ip },
             actionContext.ControllerContext.Configuration.Formatters.JsonFormatter);
                }
            }
            else
            {
                actionContext.Response = actionContext.Request.CreateResponse(
               HttpStatusCode.Unauthorized,
               new { status = false, message = "Invalid Token", ip = ip },
               actionContext.ControllerContext.Configuration.Formatters.JsonFormatter);
            }
        }

    }
}