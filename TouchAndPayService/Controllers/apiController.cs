﻿using ClassLibrary.ServicePostModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace TouchAndPayService.Controllers
{
    public class apiController : baseController
    {
        [HttpPost]
        public IHttpActionResult prioritylist()
        {
            Tuple<ApiPriorityResponse> t = _apiRepository.prioritylist();
            return Ok(t.Item1);
        }


        [HttpPost]
        public IHttpActionResult fetchdeductedamount(FetchDeductedAmount model)
        {
            Tuple<FetchDeductedAmountResponse> t = _apiRepository.fetchdeductedamount(model);
            return Ok(t.Item1);
        }

        [HttpPost]
        public IHttpActionResult fetchdeductedamountinternational(FetchDeductedAmount model)
        {
            Tuple<FetchDeductedAmountResponse> t = _apiRepository.fetchdeductedamountinternational(model);
            return Ok(t.Item1);
        }

        [HttpGet]
        public IHttpActionResult getUiStatus()
        {
            Tuple<GetUiStatusResponse> t = _apiRepository.getUiStatus();
            return Ok(t.Item1);
        }


    }
}
