﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace TouchAndPayService.Controllers
{
    public class dingController : baseController
    {
        [TouchAndPayService.Models.AuthorizationFilter]
        [HttpPost]
        public IHttpActionResult GetAccountLookup(ClassLibrary.ServicePostModel.DingPostModel.GetAccountLookup model)
        {
            var result = _dingRepository.GetAccountLookup(model);
            return Ok(result);
        }

        [TouchAndPayService.Models.AuthorizationFilter]
        [HttpPost]
        public IHttpActionResult GetProviders(ClassLibrary.ServicePostModel.DingPostModel.GetProviders model)
        {
            var result = _dingRepository.GetProviders(model);
            return Ok(result);
        }

        [TouchAndPayService.Models.AuthorizationFilter]
        [HttpPost]
        public IHttpActionResult GetProducts(ClassLibrary.ServicePostModel.DingPostModel.GetProducts model)
        {
            var result = _dingRepository.GetProducts(model);
            return Ok(result);
        }

        [TouchAndPayService.Models.AuthorizationFilter]
        [HttpPost]
        public IHttpActionResult SendTransfer(ClassLibrary.ServicePostModel.DingPostModel.SendTransfer model)
        {
            try
            {
                model.ip = ip;
                var result = _dingRepository.SendTransfer(model);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return Ok(ex.Message);
            }
        }

        [TouchAndPayService.Models.AuthorizationFilter]
        [HttpPost]
        public IHttpActionResult SendTransferInternational(ClassLibrary.ServicePostModel.DingPostModel.SendTransfer model)
        {
            try
            {
                model.ip = ip;

                var result = _dingRepository.SendTransferInternational(model);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return Ok(ex.Message);
            }
        }

        [TouchAndPayService.Models.AuthorizationFilter]
        [HttpPost]
        public IHttpActionResult GetAccountLookupInternational(ClassLibrary.ServicePostModel.DingPostModel.GetAccountLookup model)
        {
            var result = _dingRepository.GetAccountLookupInternational(model);
            return Ok(result);
        }

        [TouchAndPayService.Models.AuthorizationFilter]
        [HttpPost]
        public IHttpActionResult GetProvidersInternational(ClassLibrary.ServicePostModel.DingPostModel.GetProviders model)
        {
            var result = _dingRepository.GetProvidersInternational(model);
            return Ok(result);
        }

        [TouchAndPayService.Models.AuthorizationFilter]
        [HttpPost]
        public IHttpActionResult GetProductsInternational(ClassLibrary.ServicePostModel.DingPostModel.GetProducts model)
        {
            var result = _dingRepository.GetProductsInternational(model);
            return Ok(result);
        }

        //
        /// <summary>
        /// Added by sibi
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>

        [TouchAndPayService.Models.AuthorizationFilter]
        [HttpPost]
        public IHttpActionResult SendTransferInternationalAED(ClassLibrary.ServicePostModel.DingPostModel.SendTransfer model) // Archana 26-03-2019
        {
            try
            {
                model.ip = ip;
                var result = _dingRepository.SendTransferInternationalAED(model);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return Ok(ex.Message);
            }
        }


    }
}
