﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace TouchAndPayService.Controllers
{
    public class pay2AllController : baseController
    {
        [TouchAndPayService.Models.AuthorizationFilter]
        [HttpPost]
        public IHttpActionResult prepaidrechargeANDmobilepostpaidbill(ClassLibrary.ServicePostModel.Pay2All.prepaidrechargeANDmobilepostpaidbill model)
        {
            model.ip = ip;
            var result = _pay2AllRepository.prepaidrechargeANDmobilepostpaidbill(model);
            return Ok(result);
        }
        //added by sibi

        [HttpGet]
        public object rechargeStatusUpdate()
        {
            try
            {
                var result = _pay2AllRepository.rechargeStatusUpdate();
                return result;
            }
            catch(Exception ex)
            {
                return null;
            }
        }
    }
}
