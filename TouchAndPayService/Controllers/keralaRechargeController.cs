﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace TouchAndPayService.Controllers
{
    public class keralaRechargeController : baseController
    {
        [TouchAndPayService.Models.AuthorizationFilter]
        [HttpPost]
        public IHttpActionResult findOperatorandCircle(ClassLibrary.ServicePostModel.KeralaRechargePostModel.FindOperatorAndCircle model)
        {
            var result = _keralaRechargeRepository.findOperatorandCircle(model);
            return Ok(result);
        }

        [TouchAndPayService.Models.AuthorizationFilter]
        [HttpPost]
        public IHttpActionResult rechargePlans(ClassLibrary.ServicePostModel.KeralaRechargePostModel.RechargePlans model)
        {
            var result = _keralaRechargeRepository.rechargePlans(model);
            return Ok(result);
        }

        [TouchAndPayService.Models.AuthorizationFilter]
        [HttpPost]
        public IHttpActionResult recharge(ClassLibrary.ServicePostModel.KeralaRechargePostModel.Recharge model)
        {
            model.ip = ip;
            var result = _keralaRechargeRepository.recharge(model);
            return Ok(result);
        }

        [TouchAndPayService.Models.AuthorizationFilter]
        [HttpPost]
        public IHttpActionResult ksebBillPayment(ClassLibrary.ServicePostModel.KeralaRechargePostModel.KsebBillPayment model)
        {
            model.ip = ip;
            var result = _keralaRechargeRepository.ksebBillPayment(model);
            return Ok(result);
        }


        //chronejob
        [HttpGet]
        public object rechargeStatusUpdate()
        {
            var result = _keralaRechargeRepository.rechargeStatusUpdate();
            return result;
        }


    }
}
