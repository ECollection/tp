﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace TouchAndPayService.Controllers
{
    public class TestController : ApiController
    {
        private static readonly HttpClient client = new HttpClient();

        [HttpPost]
        public IHttpActionResult test()
        {
            ClassLibrary.ServicePostModel.Test model = new ClassLibrary.ServicePostModel.Test();
            model.auth = new ClassLibrary.ServicePostModel.testClass();
            model.auth.id = 3018;
            model.auth.Key = 766348254658889;
            model.auth.hash = "3dd40b5a1bcf05c99cc0b016662e0bc4";

            var request = (HttpWebRequest)WebRequest.Create("http://api.gulfbox.ae/");

            string output = JsonConvert.SerializeObject(model);
            var data = System.Text.Encoding.ASCII.GetBytes(output);

            request.Method = "POST";
            request.ContentType = "application/json";
            request.ContentLength = data.Length;

            using (var stream = request.GetRequestStream())
            {
                stream.Write(data, 0, data.Length);
            }

            var response = (HttpWebResponse)request.GetResponse();

            var responseString = new System.IO.StreamReader(response.GetResponseStream()).ReadToEnd();
            return Ok(responseString);
        }

        public IHttpActionResult test1()
        {
            var requestString = "https://joloapi.com/api/recharge.php?userid="+"arakkal"+"&key="+"718127426963062"+"&type=json&mode=0&operator="+"IDX"+"&service="+"7558006175"+"&amount="+"50"+"&orderid="+"1234567"+"";
            var request = (HttpWebRequest)WebRequest.Create(requestString);
            var response = (HttpWebResponse)request.GetResponse();
            var responseString = new System.IO.StreamReader(response.GetResponseStream()).ReadToEnd();
            dynamic data = System.Web.Helpers.Json.Decode(responseString);
            //JObject joResponse = JObject.Parse(responseString);
            //JObject ojObject = (JObject)joResponse["response"];
            //JArray array = (JArray)ojObject["chats"];
            return Ok(data);
        }
    }
}
