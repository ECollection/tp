﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Http;
using System.Web.Http;

namespace TouchAndPayService.Controllers
{
    public class joloController : baseController
    {
        [TouchAndPayService.Models.AuthorizationFilter]
        [HttpPost]
        public IHttpActionResult operatorANDcircleFinder(ClassLibrary.ServicePostModel.JoloPostModel.operatorANDcircleFinder model)
        {
            var result = _joloRepository.operatorANDcircleFinder(model);
            return Ok(result);
        }

        [TouchAndPayService.Models.AuthorizationFilter]
        [HttpPost]
        public IHttpActionResult operatorplanANDofferFinder(ClassLibrary.ServicePostModel.JoloPostModel.operatorplanANDofferFinder model)
        {
            var result = _joloRepository.operatorplanANDofferFinder(model);
            return Ok(result);
        }

        [TouchAndPayService.Models.AuthorizationFilter]
        [HttpPost]
        public IHttpActionResult mobileAndDthRecharge(ClassLibrary.ServicePostModel.JoloPostModel.mobileAndDthRecharge model)
        {
            model.ip = ip;
            var result = _joloRepository.mobileAndDthRecharge(model);
            return Ok(result);
        }


        [TouchAndPayService.Models.AuthorizationFilter]
        [HttpPost]
        public IHttpActionResult FindDthOperator(ClassLibrary.ServicePostModel.JoloPostModel.FindDthOperator model)
        {
            var result = _joloRepository.FindDthOperator(model);
            return Ok(result);
        }

        [TouchAndPayService.Models.AuthorizationFilter]
        [HttpPost]
        public IHttpActionResult FindPlanDTH(ClassLibrary.ServicePostModel.JoloPostModel.FindPlanDTH model)
        {
            var result = _joloRepository.FindPlanDTH(model);
            return Ok(result);
        }

        [TouchAndPayService.Models.AuthorizationFilter]
        [HttpPost]
        public IHttpActionResult mobilePostpaidBill(ClassLibrary.ServicePostModel.JoloPostModel.MobilePostpaidBill model)
        {
            model.ip = ip;
            var result = _joloRepository.mobilePostpaidBill(model);
            return Ok(result);
        }

    }
}