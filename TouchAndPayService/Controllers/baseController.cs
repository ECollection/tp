﻿using ClassLibrary.ServicePostModel;
using EntityLibrary;
using EntityLibrary.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Web.Http;
using System.Web.Http.Controllers;

namespace TouchAndPayService.Controllers
{
    public class baseController : ApiController
    {
        protected TouchAndPayEntities _context = new TouchAndPayEntities();
        protected UserRepository _userRepository = new UserRepository();
        protected gulfboxRepository _gulfboxRepository = new gulfboxRepository();
        protected joloRepository _joloRepository = new joloRepository();
        protected magiceasyRepository _magiceasyRepository = new magiceasyRepository();
        protected apiRepository _apiRepository = new apiRepository();
        protected keralaRechargeRepository _keralaRechargeRepository = new keralaRechargeRepository();
        protected OnePrepayRepository _OnePrepayRepository = new OnePrepayRepository();
        protected dingRepository _dingRepository = new dingRepository();
        protected mobileAPIRepository _mobileAPIRepository = new mobileAPIRepository();
        protected databeatsRepository _databeatsRepository = new databeatsRepository();
        protected pay2allRepository _pay2AllRepository = new pay2allRepository();
        public DateTime currentTime = System.DateTime.UtcNow;
        protected string ip = "";


        public baseController()
        {
            try
            {
                HttpRequestMessage request = null;
                request = request ?? Request;
                if (request.Properties.ContainsKey("MS_HttpContext"))
                {
                    ip = ((System.Web.HttpContextWrapper)request.Properties["MS_HttpContext"]).Request.UserHostAddress;
                }
                else if (request.Properties.ContainsKey(System.ServiceModel.Channels.RemoteEndpointMessageProperty.Name))
                {
                    System.ServiceModel.Channels.RemoteEndpointMessageProperty prop = (System.ServiceModel.Channels.RemoteEndpointMessageProperty)request.Properties[System.ServiceModel.Channels.RemoteEndpointMessageProperty.Name];
                    ip = prop.Address;
                }
                else if (System.Web.HttpContext.Current != null)
                {
                    ip = System.Web.HttpContext.Current.Request.UserHostAddress;
                }
                else
                {
                    ip = null;
                }

            }
            catch (Exception ex)
            {
            }
        }


    }
}
