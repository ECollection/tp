﻿using ClassLibrary.ServicePostModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace TouchAndPayService.Controllers
{
    public class gulfboxController : baseController
    {
        [TouchAndPayService.Models.AuthorizationFilter]
        [HttpPost]
        public IHttpActionResult services()
        {
            var result = _gulfboxRepository.services();
            return Ok(result);
        }
        [TouchAndPayService.Models.AuthorizationFilter]
        [HttpPost]
        public IHttpActionResult providers(ClassLibrary.ServicePostModel.GulfboxPostModel.GulfboxProviderApi model)
        {
            //HttpRequestMessage request = null;
            //request = request ?? Request;
            //var ip = "";
            //if (request.Properties.ContainsKey("MS_HttpContext"))
            //{
            //    ip = ((System.Web.HttpContextWrapper)request.Properties["MS_HttpContext"]).Request.UserHostAddress;
            //}
            //else if (request.Properties.ContainsKey(System.ServiceModel.Channels.RemoteEndpointMessageProperty.Name))
            //{
            //    System.ServiceModel.Channels.RemoteEndpointMessageProperty prop = (System.ServiceModel.Channels.RemoteEndpointMessageProperty)request.Properties[System.ServiceModel.Channels.RemoteEndpointMessageProperty.Name];
            //    ip = prop.Address;
            //}
            //else if (System.Web.HttpContext.Current != null)
            //{
            //    ip = System.Web.HttpContext.Current.Request.UserHostAddress;
            //}
            //else
            //{
            //    ip = null;
            //}


            var result = _gulfboxRepository.providers(model);
            return Ok(result);
        }

        //[ActionName("transaction/check")]
        [TouchAndPayService.Models.AuthorizationFilter]
        [HttpPost]
        public IHttpActionResult transactioncheck(ClassLibrary.ServicePostModel.GulfboxPostModel.GulfboxTransactionApi model)
        {
            model.ip = ip;
            var result = _gulfboxRepository.transactioncheck(model);
            return Ok(result);
        }
        [TouchAndPayService.Models.AuthorizationFilter]
        [HttpPost]
        public IHttpActionResult transactionconfirm(ClassLibrary.ServicePostModel.GulfboxPostModel.GulfboxTransactionConfirmApi model)
        {
            model.ip = ip;
            var result = _gulfboxRepository.transactionconfirm(model);
            return Ok(result);
        }
        [TouchAndPayService.Models.AuthorizationFilter]
        [HttpPost]
        public IHttpActionResult balance(ClassLibrary.ServicePostModel.GulfboxPostModel.GulfboxBalanceApi model)
        {
            var result = _gulfboxRepository.balance(model);
            return Ok(result);
        }
        [TouchAndPayService.Models.AuthorizationFilter]
        [HttpPost]
        public IHttpActionResult servicetransactioncheck(ClassLibrary.ServicePostModel.GulfboxPostModel.GulfboxServiceTransactionApi model)
        {
            model.ip = ip;
            var result = _gulfboxRepository.servicetransactioncheck(model);
            return Ok(result);
        }
        [TouchAndPayService.Models.AuthorizationFilter]
        [HttpPost]
        public IHttpActionResult servicetransactionconfirm(ClassLibrary.ServicePostModel.GulfboxPostModel.GulfboxServiceTransactionConfirmApi model)
        {
            model.ip = ip;
            var result = _gulfboxRepository.servicetransactionconfirm(model);
            return Ok(result);
        }

        [TouchAndPayService.Models.AuthorizationFilter]
        [HttpPost]
        public IHttpActionResult vouchertransaction(ClassLibrary.ServicePostModel.GulfboxPostModel.GulfboxVoucherTransactionApi model)
        {
            model.ip = ip;
            var result = _gulfboxRepository.vouchertransaction(model);
            return Ok(result);
        }
        [TouchAndPayService.Models.AuthorizationFilter]
        [HttpPost]
        public IHttpActionResult vouchertransactionconfirm(ClassLibrary.ServicePostModel.GulfboxPostModel.GulfboxVoucherTransactionConfirmApi model)
        {
            model.ip = ip;
            var result = _gulfboxRepository.vouchertransactionconfirm(model);
            return Ok(result);
        }


        //chrone job
        [HttpGet]
        public object transactionStatusUpdate()
        {
            var result = _gulfboxRepository.transactionStatusUpdate();
            return result;
        }

        [HttpGet]
        public object voucherTransactionStatusUpdate()
        {
            var result = _gulfboxRepository.voucherTransactionStatusUpdate();
            return result;
        }
        //





    }
}
