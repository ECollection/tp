﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace TouchAndPayService.Controllers
{
    public class OnePrepayController : baseController
    {
        [TouchAndPayService.Models.AuthorizationFilter]
        [HttpPost]
        public IHttpActionResult pinlessrequest(ClassLibrary.ServicePostModel.OnePrepayPostModel.PinlessRequest model)
        {
            model.ip = ip;
            var result = _OnePrepayRepository.pinlessrequest(model);
            return Ok(result);
        }


        [TouchAndPayService.Models.AuthorizationFilter]
        [HttpPost]
        public IHttpActionResult pinlessrequestfirststep(ClassLibrary.ServicePostModel.OnePrepayPostModel.PinlessRequest model)
        {
            model.ip = ip;
            var result = _OnePrepayRepository.pinlessrequestfirststep(model);
            return Ok(result);
        }


        [TouchAndPayService.Models.AuthorizationFilter]
        [HttpPost]
        public IHttpActionResult pinrequest(ClassLibrary.ServicePostModel.OnePrepayPostModel.PinRequest model)
        {
            model.ip = ip;
            var result = _OnePrepayRepository.pinrequest(model);
            return Ok(result);
        }


        [HttpPost]
        public IHttpActionResult getbalancest()
        {
            var result = _OnePrepayRepository.getbalancest();
            return Ok(result);
        }

    }
}
