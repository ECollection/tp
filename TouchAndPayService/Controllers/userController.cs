﻿using ClassLibrary.ServicePostModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace TouchAndPayService.Controllers
{
    public class userController : baseController
    {

        [HttpPost]
        public IHttpActionResult register(UserRegisterModel model)
        {

            HttpRequestMessage request = null;
            request = request ?? Request;
            var ip = "";
            if (request.Properties.ContainsKey("MS_HttpContext"))
            {
                ip = ((System.Web.HttpContextWrapper)request.Properties["MS_HttpContext"]).Request.UserHostAddress;
            }
            else if (request.Properties.ContainsKey(System.ServiceModel.Channels.RemoteEndpointMessageProperty.Name))
            {
                System.ServiceModel.Channels.RemoteEndpointMessageProperty prop = (System.ServiceModel.Channels.RemoteEndpointMessageProperty)request.Properties[System.ServiceModel.Channels.RemoteEndpointMessageProperty.Name];
                ip = prop.Address;
            }
            else if (System.Web.HttpContext.Current != null)
            {
                ip = System.Web.HttpContext.Current.Request.UserHostAddress;
            }
            else
            {
                ip = null;
            }
            model.ip = ip;

            Tuple<UserRegisterModelReturn> t = _userRepository.register(model);
            return Ok(t.Item1);
        }

        [HttpPost]
        public IHttpActionResult login(UserLoginModel model)
        {
            HttpRequestMessage request = null;
            request = request ?? Request;
            var ip = "";
            if (request.Properties.ContainsKey("MS_HttpContext"))
            {
                ip = ((System.Web.HttpContextWrapper)request.Properties["MS_HttpContext"]).Request.UserHostAddress;
            }
            else if (request.Properties.ContainsKey(System.ServiceModel.Channels.RemoteEndpointMessageProperty.Name))
            {
                System.ServiceModel.Channels.RemoteEndpointMessageProperty prop = (System.ServiceModel.Channels.RemoteEndpointMessageProperty)request.Properties[System.ServiceModel.Channels.RemoteEndpointMessageProperty.Name];
                ip = prop.Address;
            }
            else if (System.Web.HttpContext.Current != null)
            {
                ip = System.Web.HttpContext.Current.Request.UserHostAddress;
            }
            else
            {
                ip = null;
            }
            model.ip = ip;
            Tuple<UserLoginModelReturn> t = _userRepository.login(model);
            return Ok(t.Item1);
        }

        [TouchAndPayService.Models.AuthorizationFilter]
        [HttpPost]
        public IHttpActionResult profileEdit()
        {

            ProfileEditModel model = new ProfileEditModel();

            var httpPostedFile = HttpContext.Current.Request.Files["profileImage"];
            model.profileImage = httpPostedFile;
            model.mobileNumber = HttpContext.Current.Request.Params["mobileNumber"];
            model.deviceType = Convert.ToInt32(HttpContext.Current.Request.Params["deviceType"]);
            model.username = HttpContext.Current.Request.Params["username"];
            model.userId = Convert.ToInt64(HttpContext.Current.Request.Params["userId"]);
            model.country = HttpContext.Current.Request.Params["country"];
            model.countryCode = HttpContext.Current.Request.Params["countryCode"];

            Tuple<ProfileEditModelReturn> t = _userRepository.profileEdit(model);
            return Ok(t.Item1);
        }

        [TouchAndPayService.Models.AuthorizationFilter]
        [HttpPost]
        public IHttpActionResult rechargeWallet(RechargeWalletModel model)
        {
            Tuple<RechargeWalletReturnModel> t = _userRepository.rechargeWallet(model);
            return Ok(t.Item1);
        }

        [TouchAndPayService.Models.AuthorizationFilter]
        [HttpPost]
        public IHttpActionResult assignCard(AssignCard model)
        {
            Tuple<AssignCardReturnModel> t = _userRepository.assignCard(model);
            return Ok(t.Item1);
        }

        [TouchAndPayService.Models.AuthorizationFilter]
        [HttpPost]
        public IHttpActionResult listAgents()
        {
            Tuple<ListAgentsReturnModel> t = _userRepository.listAgents();
            return Ok(t.Item1);
        }

        [TouchAndPayService.Models.AuthorizationFilter]
        [HttpPost]
        public IHttpActionResult resetPassword(ResetPassword model)
        {
            Tuple<ResetPasswordModelReturn> t = _userRepository.resetPassword(model);
            return Ok(t.Item1);
        }

        [HttpPost]
        public IHttpActionResult forgotPassword(ForgotPassword model)
        {
            Tuple<ForgotPasswordModelReturn> t = _userRepository.forgotPassword(model);
            return Ok(t.Item1);
        }

        [HttpPost]
        public IHttpActionResult getUserData(GetUserData model)
        {
            Tuple<GetUserDataReturn> t = _userRepository.getUserData(model);
            return Ok(t.Item1);
        }

        [HttpGet]
        public IHttpActionResult currencyConverter(decimal amount, string fromCurrency, string toCurrency)
        {
            var t = _userRepository.currencyConverter(amount, fromCurrency, toCurrency);
            return Ok(t);
        }

        [TouchAndPayService.Models.AuthorizationFilter]
        [HttpPost]
        public IHttpActionResult getTransaction(GetTransaction model)
        {
            Tuple<GetTransactionResponse> t = _userRepository.getTransaction(model);
            return Ok(t.Item1);
        }

        [HttpPost]
        public IHttpActionResult sendOtp(SendOtp model)
        {
            Tuple<SendOtpResponse> t = _userRepository.sendOtp(model);
            return Ok(t.Item1);
        }

        [HttpPost]
        public IHttpActionResult validateOtp(ValidateOtp model)
        {
            Tuple<ValidateOtpResponse> t = _userRepository.validateOtp(model);
            return Ok(t.Item1);
        }

        [HttpGet]
        public IHttpActionResult deleteOtp()
        {
            var t = _userRepository.deleteOtp();
            return Ok(t);
        }

        [HttpPost]
        public IHttpActionResult sendContactMail(SendContactMail model)
        {
            Tuple<SendContactMailResponse> t = _userRepository.sendContactMail(model);
            return Ok(t.Item1);
        }

        [TouchAndPayService.Models.AuthorizationFilter]
        [HttpPost]
        public IHttpActionResult logout(Logout model)
        {
            Tuple<LogoutResponse> t = _userRepository.logout(model);
            return Ok(t.Item1);
        }


        [HttpGet]
        public IHttpActionResult removeAccessToken()
        {
            var t = _userRepository.removeAccessToken();
            return Ok(t);
        }



        [TouchAndPayService.Models.AuthorizationFilter]
        [HttpPost]
        public IHttpActionResult getAllTotalAmount(GetAllTotalAmount model)
        {
            Tuple<GetAllTotalAmountResponse> t = _userRepository.getAllTotalAmount(model);
            return Ok(t.Item1);
        }



    }
}
