﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace TouchAndPayService.Controllers
{
    public class databeatsController  : baseController
    {
        [TouchAndPayService.Models.AuthorizationFilter]
        [HttpPost]
        public IHttpActionResult mnp(ClassLibrary.ServicePostModel.databeats.MNP model)
        {
            var result = _databeatsRepository.mnp(model);
            return Ok(result);
        }

        [TouchAndPayService.Models.AuthorizationFilter]
        [HttpPost]
        public IHttpActionResult mobileoffers(ClassLibrary.ServicePostModel.databeats.MobileOffers.MobileOffersRequest model)
        {
            var result = _databeatsRepository.mobileoffers(model);
            return Ok(result);
        }

    }
}
