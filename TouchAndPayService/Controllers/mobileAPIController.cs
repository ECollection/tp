﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace TouchAndPayService.Controllers
{
    public class mobileAPIController : baseController
    {
        [TouchAndPayService.Models.AuthorizationFilter]
        [HttpPost]
        public IHttpActionResult findProvider()
        {
            var result = _mobileAPIRepository.findProvider();
            return Ok(result);
        }

        [TouchAndPayService.Models.AuthorizationFilter]
        [HttpPost]
        public IHttpActionResult findCircle()
        {
            var result = _mobileAPIRepository.findCircle();
            return Ok(result);
        }

        [TouchAndPayService.Models.AuthorizationFilter]
        [HttpPost]
        public IHttpActionResult findPlansMobile(ClassLibrary.ServicePostModel.MobileApPostModel.PlansPostModel model)
        {
            var result = _mobileAPIRepository.findPlansMobile(model);
            return Ok(result);
        }

        [TouchAndPayService.Models.AuthorizationFilter]
        [HttpPost]
        public IHttpActionResult findPlansDTH(ClassLibrary.ServicePostModel.MobileApPostModel.PlansDthModel model)
        {
            var result = _mobileAPIRepository.findPlansDTH(model);
            return Ok(result);
        }

        [TouchAndPayService.Models.AuthorizationFilter]
        [HttpPost]
        public IHttpActionResult findROffer(ClassLibrary.ServicePostModel.MobileApPostModel.Roffer model)
        {
            var result = _mobileAPIRepository.findROffer(model);
            return Ok(result);
        }

    }
}
