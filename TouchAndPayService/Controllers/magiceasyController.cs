﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace TouchAndPayService.Controllers
{
    public class magiceasyController : baseController
    {
        [TouchAndPayService.Models.AuthorizationFilter]
        [HttpPost]
        public IHttpActionResult Get_Country()
        {
            var result = _magiceasyRepository.Get_Country();
            return Ok(result);
        }

        [TouchAndPayService.Models.AuthorizationFilter]
        [HttpPost]
        public IHttpActionResult Get_Opt_list(ClassLibrary.ServicePostModel.MagicEasyPostModel.Get_Opt_list model)
        {
            var result = _magiceasyRepository.Get_Opt_list(model);
            return Ok(result);
        }

        [TouchAndPayService.Models.AuthorizationFilter]
        [HttpPost]
        public IHttpActionResult Get_Oprator(ClassLibrary.ServicePostModel.MagicEasyPostModel.Get_Oprator model)
        {
            var result = _magiceasyRepository.Get_Oprator(model);
            return Ok(result);
        }

        [TouchAndPayService.Models.AuthorizationFilter]
        [HttpPost]
        public IHttpActionResult Get_Opt_offer(ClassLibrary.ServicePostModel.MagicEasyPostModel.Get_Opt_offer model)
        {
            var result = _magiceasyRepository.Get_Opt_offer(model);
            return Ok(result);
        }

        [TouchAndPayService.Models.AuthorizationFilter]
        [HttpPost]
        public IHttpActionResult Get_Opt_list_Mobile()
        {
            var result = _magiceasyRepository.Get_Opt_list_Mobile();
            return Ok(result);
        }

        [TouchAndPayService.Models.AuthorizationFilter]
        [HttpPost]
        public IHttpActionResult Get_Circle_list_Mobile()
        {
            var result = _magiceasyRepository.Get_Circle_list_Mobile();
            return Ok(result);
        }

        [TouchAndPayService.Models.AuthorizationFilter]
        [HttpPost]
        public IHttpActionResult Get_Oprator_Mobile(ClassLibrary.ServicePostModel.MagicEasyPostModel.Get_Oprator model)
        {
            var result = _magiceasyRepository.Get_Oprator_Mobile(model);
            return Ok(result);
        }

        [TouchAndPayService.Models.AuthorizationFilter]
        [HttpPost]
        public IHttpActionResult Get_Opt_list_DTH()
        {
            var result = _magiceasyRepository.Get_Opt_list_DTH();
            return Ok(result);
        }
    }
}
