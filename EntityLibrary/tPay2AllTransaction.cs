//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace EntityLibrary
{
    using System;
    using System.Collections.Generic;
    
    public partial class tPay2AllTransaction
    {
        public long Id { get; set; }
        public System.Guid Guid { get; set; }
        public long UserId { get; set; }
        public string Number { get; set; }
        public string ProviderId { get; set; }
        public string Amount { get; set; }
        public string ClientId { get; set; }
        public long Pay2AllPayId { get; set; }
        public string Pay2AllOperatorRef { get; set; }
        public string Pay2AllStatus { get; set; }
        public string Pay2AllTxstatusDesc { get; set; }
        public string Pay2AllMessage { get; set; }
        public System.DateTime Timestamp { get; set; }
        public string AmountDeducted { get; set; }
        public string AmountCurrency { get; set; }
        public string AmountDeductedCurrency { get; set; }
        public decimal Commission { get; set; }
    
        public virtual tb_User tb_User { get; set; }
    }
}
