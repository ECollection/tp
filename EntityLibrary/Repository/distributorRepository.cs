﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityLibrary.Repository
{
    public class distributorRepository : BaseReference
    {

        public Tuple<bool, string> addReseller(ClassLibrary.WebPostModel.AddReseller model)
        {
            bool status = false;
            string message = "Failed";
            string password = StringCipher.Encrypt(model.password, salt);
            string mobile = StringCipher.Encrypt(model.mobileNumber, salt);
            string userName = StringCipher.Encrypt(model.name, salt);
            string email = StringCipher.Encrypt(model.email, salt);
            TouchAndPayEntities o_ResellearOnly = new TouchAndPayEntities();
            var checkList = _context.tResellers.Where(z => (z.Mobile == mobile) && z.IsActive).ToList();
            if (checkList.Count > 0)
            {
                if (checkList.Any(z => z.Mobile == mobile))
                {
                    status = false;
                    message = "Mobile number already taken";
                }
                else if (checkList.Any(z => z.Email == email))
                {
                    status = false;
                    message = "Email already taken";
                }
                else
                {
                    status = false;
                    message = "Failed";
                }
            }
            else if (_context.tb_User.Any(z => z.Mobile == mobile))
            {
                status = false;
                message = "Mobile number already taken";
            }
            else if (_context.tDistributors.Any(z => z.Mobile == mobile))
            {
                status = false;
                message = "Mobile number already taken";
            }
            else
            {
                var newReseller = o_ResellearOnly.tResellers.Create();
                newReseller.Country = model.place ?? string.Empty;
                newReseller.CountryCode = model.countryCode ?? string.Empty;
                newReseller.IsActive = true;
                newReseller.Mobile = mobile ?? string.Empty;
                newReseller.Password = password ?? string.Empty;
                newReseller.Timestamp = currentTime;
                newReseller.LastUpdated = currentTime;
                newReseller.ResellerGuid = Guid.NewGuid();
                newReseller.ProfileImage = string.Empty;
                newReseller.Username = userName ?? string.Empty;
                newReseller.Address = model.address ?? string.Empty;
                newReseller.Place = model.place ?? string.Empty;
                newReseller.ShopName = model.shopName ?? string.Empty;
                newReseller.UserType = (int)ClassLibrary.Enum.userType.Reseller;
                newReseller.Wallet = Convert.ToDecimal(0);
                newReseller.IsBlocked = false;
                newReseller.Email = email ?? string.Empty;
                newReseller.DistributorId = model.distributorId;

                string currency = "AED";
                var Fromcurrency = _context.tb_AdminCurrencyRate.Where(z => z.FromCountryCode == newReseller.CountryCode && z.IsActive).FirstOrDefault();
                if (Fromcurrency != null)
                {
                    currency = Fromcurrency.FromCurrency;
                }
                else
                {
                    var Tocurrency = _context.tb_AdminCurrencyRate.Where(z => z.ToCountryCode == newReseller.CountryCode && z.IsActive).FirstOrDefault();
                    if (Tocurrency != null)
                    {
                        currency = Tocurrency.FromCurrency;
                    }
                }
                newReseller.Currency = currency ?? string.Empty;
                o_ResellearOnly.tResellers.Add(newReseller);
                o_ResellearOnly.SaveChanges();
                TouchAndPayEntities o_ResellerPermissionsOnly = new TouchAndPayEntities();
                //if (_context.SaveChanges() > 0)
                //{

                    if (model.permission_IndiaRegion)
                    {
                        var indiaPerm = o_ResellerPermissionsOnly.tResellerPermissions.Create();
                        indiaPerm.ResellerId = newReseller.ResellerId;
                        indiaPerm.Permissions = true;
                        indiaPerm.Timestamp = currentTime;
                        indiaPerm.Type = ClassLibrary.Enum.ResellerPermissions.India.ToString();
                        o_ResellerPermissionsOnly.tResellerPermissions.Add(indiaPerm);
                    }

                    if (model.permission_International)
                    {
                        var internationalPerm = o_ResellerPermissionsOnly.tResellerPermissions.Create();
                        internationalPerm.ResellerId = newReseller.ResellerId;
                        internationalPerm.Permissions = true;
                        internationalPerm.Timestamp = currentTime;
                        internationalPerm.Type = ClassLibrary.Enum.ResellerPermissions.International.ToString();
                        o_ResellerPermissionsOnly.tResellerPermissions.Add(internationalPerm);
                    }

                    if (model.permission_Uae)
                    {
                        var uaePerm = o_ResellerPermissionsOnly.tResellerPermissions.Create();
                        uaePerm.ResellerId = newReseller.ResellerId;
                        uaePerm.Permissions = true;
                        uaePerm.Timestamp = currentTime;
                        uaePerm.Type = ClassLibrary.Enum.ResellerPermissions.UAE.ToString();
                        o_ResellerPermissionsOnly.tResellerPermissions.Add(uaePerm);
                    }
                    o_ResellerPermissionsOnly.SaveChanges();

                    //Du Topup
                    var newEntry_Gulfbox_DuTopup = _context.tResellerCommisionRates.Create();
                    newEntry_Gulfbox_DuTopup.ResellerId = newReseller.ResellerId;
                    newEntry_Gulfbox_DuTopup.ApiProvider = (int)ClassLibrary.Enum.apiProvider.Gulfbox;
                    newEntry_Gulfbox_DuTopup.Commission = Convert.ToDouble(model.commision_DuTopup);
                    newEntry_Gulfbox_DuTopup.ProductType = (int)ClassLibrary.Enum.GulfboxProductTypes.Du;
                    newEntry_Gulfbox_DuTopup.IsActive = true;
                    newEntry_Gulfbox_DuTopup.Timestamp = currentTime;
                    _context.tResellerCommisionRates.Add(newEntry_Gulfbox_DuTopup);

                    var newEntry_OnePrepay_DuTopup = _context.tResellerCommisionRates.Create();
                    newEntry_OnePrepay_DuTopup.ResellerId = newReseller.ResellerId;
                    newEntry_OnePrepay_DuTopup.ApiProvider = (int)ClassLibrary.Enum.apiProvider.OnePrepay;
                    newEntry_OnePrepay_DuTopup.Commission = Convert.ToDouble(model.commision_DuTopup);
                    newEntry_OnePrepay_DuTopup.ProductType = (int)ClassLibrary.Enum.GulfboxProductTypes.Du;
                    newEntry_OnePrepay_DuTopup.IsActive = true;
                    newEntry_OnePrepay_DuTopup.Timestamp = currentTime;
                    _context.tResellerCommisionRates.Add(newEntry_OnePrepay_DuTopup);



                    //Du Voucher
                    var newEntry_Gulfbox_DuVoucher = _context.tResellerCommisionRates.Create();
                    newEntry_Gulfbox_DuVoucher.ResellerId = newReseller.ResellerId;
                    newEntry_Gulfbox_DuVoucher.ApiProvider = (int)ClassLibrary.Enum.apiProvider.Gulfbox;
                    newEntry_Gulfbox_DuVoucher.Commission = Convert.ToDouble(model.commision_DuVoucher);
                    newEntry_Gulfbox_DuVoucher.ProductType = (int)ClassLibrary.Enum.GulfboxProductTypes.DuVoucher;
                    newEntry_Gulfbox_DuVoucher.IsActive = true;
                    newEntry_Gulfbox_DuVoucher.Timestamp = currentTime;
                    _context.tResellerCommisionRates.Add(newEntry_Gulfbox_DuVoucher);

                    var newEntry_OnePrepay_DuVoucher = _context.tResellerCommisionRates.Create();
                    newEntry_OnePrepay_DuVoucher.ResellerId = newReseller.ResellerId;
                    newEntry_OnePrepay_DuVoucher.ApiProvider = (int)ClassLibrary.Enum.apiProvider.OnePrepay;
                    newEntry_OnePrepay_DuVoucher.Commission = Convert.ToDouble(model.commision_DuVoucher);
                    newEntry_OnePrepay_DuVoucher.ProductType = (int)ClassLibrary.Enum.GulfboxProductTypes.DuVoucher;
                    newEntry_OnePrepay_DuVoucher.IsActive = true;
                    newEntry_OnePrepay_DuVoucher.Timestamp = currentTime;
                    _context.tResellerCommisionRates.Add(newEntry_OnePrepay_DuVoucher);



                    //Etisalat
                    var newEntry_Gulfbox_Etisalat = _context.tResellerCommisionRates.Create();
                    newEntry_Gulfbox_Etisalat.ResellerId = newReseller.ResellerId;
                    newEntry_Gulfbox_Etisalat.ApiProvider = (int)ClassLibrary.Enum.apiProvider.Gulfbox;
                    newEntry_Gulfbox_Etisalat.Commission = Convert.ToDouble(model.commision_EtisalatTopup);
                    newEntry_Gulfbox_Etisalat.ProductType = (int)ClassLibrary.Enum.GulfboxProductTypes.Etisalat;
                    newEntry_Gulfbox_Etisalat.IsActive = true;
                    newEntry_Gulfbox_Etisalat.Timestamp = currentTime;
                    _context.tResellerCommisionRates.Add(newEntry_Gulfbox_Etisalat);

                    var newEntry_OnePrepay_Etisalat = _context.tResellerCommisionRates.Create();
                    newEntry_OnePrepay_Etisalat.ResellerId = newReseller.ResellerId;
                    newEntry_OnePrepay_Etisalat.ApiProvider = (int)ClassLibrary.Enum.apiProvider.OnePrepay;
                    newEntry_OnePrepay_Etisalat.Commission = Convert.ToDouble(model.commision_EtisalatTopup);
                    newEntry_OnePrepay_Etisalat.ProductType = (int)ClassLibrary.Enum.GulfboxProductTypes.Etisalat;
                    newEntry_OnePrepay_Etisalat.IsActive = true;
                    newEntry_OnePrepay_Etisalat.Timestamp = currentTime;
                    _context.tResellerCommisionRates.Add(newEntry_OnePrepay_Etisalat);

                    //Etisalat Voucher
                    var newEntry_Gulfbox_EtisalatVoucher = _context.tResellerCommisionRates.Create();
                    newEntry_Gulfbox_EtisalatVoucher.ResellerId = newReseller.ResellerId;
                    newEntry_Gulfbox_EtisalatVoucher.ApiProvider = (int)ClassLibrary.Enum.apiProvider.Gulfbox;
                    newEntry_Gulfbox_EtisalatVoucher.Commission = Convert.ToDouble(model.commision_EtisalatVoucher);
                    newEntry_Gulfbox_EtisalatVoucher.ProductType = (int)ClassLibrary.Enum.GulfboxProductTypes.EtisalatVoucher;
                    newEntry_Gulfbox_EtisalatVoucher.IsActive = true;
                    newEntry_Gulfbox_EtisalatVoucher.Timestamp = currentTime;
                    _context.tResellerCommisionRates.Add(newEntry_Gulfbox_EtisalatVoucher);

                    var newEntry_OnePrepay_EtisalatVoucher = _context.tResellerCommisionRates.Create();
                    newEntry_OnePrepay_EtisalatVoucher.ResellerId = newReseller.ResellerId;
                    newEntry_OnePrepay_EtisalatVoucher.ApiProvider = (int)ClassLibrary.Enum.apiProvider.OnePrepay;
                    newEntry_OnePrepay_EtisalatVoucher.Commission = Convert.ToDouble(model.commision_EtisalatVoucher);
                    newEntry_OnePrepay_EtisalatVoucher.ProductType = (int)ClassLibrary.Enum.GulfboxProductTypes.EtisalatVoucher;
                    newEntry_OnePrepay_EtisalatVoucher.IsActive = true;
                    newEntry_OnePrepay_EtisalatVoucher.Timestamp = currentTime;
                    _context.tResellerCommisionRates.Add(newEntry_OnePrepay_EtisalatVoucher);

                    //Five
                    var newEntry_Gulfbox_Five = _context.tResellerCommisionRates.Create();
                    newEntry_Gulfbox_Five.ResellerId = newReseller.ResellerId;
                    newEntry_Gulfbox_Five.ApiProvider = (int)ClassLibrary.Enum.apiProvider.Gulfbox;
                    newEntry_Gulfbox_Five.Commission = Convert.ToDouble(model.commision_Five);
                    newEntry_Gulfbox_Five.ProductType = (int)ClassLibrary.Enum.GulfboxProductTypes.Five;
                    newEntry_Gulfbox_Five.IsActive = true;
                    newEntry_Gulfbox_Five.Timestamp = currentTime;
                    _context.tResellerCommisionRates.Add(newEntry_Gulfbox_Five);

                    var newEntry_OnePrepay_Five = _context.tResellerCommisionRates.Create();
                    newEntry_OnePrepay_Five.ResellerId = newReseller.ResellerId;
                    newEntry_OnePrepay_Five.ApiProvider = (int)ClassLibrary.Enum.apiProvider.OnePrepay;
                    newEntry_OnePrepay_Five.Commission = Convert.ToDouble(model.commision_Five);
                    newEntry_OnePrepay_Five.ProductType = (int)ClassLibrary.Enum.GulfboxProductTypes.Five;
                    newEntry_OnePrepay_Five.IsActive = true;
                    newEntry_OnePrepay_Five.Timestamp = currentTime;
                    _context.tResellerCommisionRates.Add(newEntry_OnePrepay_Five);

                    //Hello

                    var newEntry_Gulfbox_Hello = _context.tResellerCommisionRates.Create();
                    newEntry_Gulfbox_Hello.ResellerId = newReseller.ResellerId;
                    newEntry_Gulfbox_Hello.ApiProvider = (int)ClassLibrary.Enum.apiProvider.Gulfbox;
                    newEntry_Gulfbox_Hello.Commission = Convert.ToDouble(model.commision_Hello);
                    newEntry_Gulfbox_Hello.ProductType = (int)ClassLibrary.Enum.GulfboxProductTypes.Hello;
                    newEntry_Gulfbox_Hello.IsActive = true;
                    newEntry_Gulfbox_Hello.Timestamp = currentTime;
                    _context.tResellerCommisionRates.Add(newEntry_Gulfbox_Hello);

                    var newEntry_OnePrepay_Hello = _context.tResellerCommisionRates.Create();
                    newEntry_OnePrepay_Hello.ResellerId = newReseller.ResellerId;
                    newEntry_OnePrepay_Hello.ApiProvider = (int)ClassLibrary.Enum.apiProvider.OnePrepay;
                    newEntry_OnePrepay_Hello.Commission = Convert.ToDouble(model.commision_Hello);
                    newEntry_OnePrepay_Hello.ProductType = (int)ClassLibrary.Enum.GulfboxProductTypes.Hello;
                    newEntry_OnePrepay_Hello.IsActive = true;
                    newEntry_OnePrepay_Hello.Timestamp = currentTime;
                    _context.tResellerCommisionRates.Add(newEntry_OnePrepay_Hello);

                    //salik

                    var newEntry_Gulfbox_salik = _context.tResellerCommisionRates.Create();
                    newEntry_Gulfbox_salik.ResellerId = newReseller.ResellerId;
                    newEntry_Gulfbox_salik.ApiProvider = (int)ClassLibrary.Enum.apiProvider.Gulfbox;
                    newEntry_Gulfbox_salik.Commission = Convert.ToDouble(model.commision_Salik);
                    newEntry_Gulfbox_salik.ProductType = (int)ClassLibrary.Enum.GulfboxProductTypes.Salik;
                    newEntry_Gulfbox_salik.IsActive = true;
                    newEntry_Gulfbox_salik.Timestamp = currentTime;
                    _context.tResellerCommisionRates.Add(newEntry_Gulfbox_salik);

                    var newEntry_OnePrepay_salik = _context.tResellerCommisionRates.Create();
                    newEntry_OnePrepay_salik.ResellerId = newReseller.ResellerId;
                    newEntry_OnePrepay_salik.ApiProvider = (int)ClassLibrary.Enum.apiProvider.OnePrepay;
                    newEntry_OnePrepay_salik.Commission = Convert.ToDouble(model.commision_Salik);
                    newEntry_OnePrepay_salik.ProductType = (int)ClassLibrary.Enum.GulfboxProductTypes.Salik;
                    newEntry_OnePrepay_salik.IsActive = true;
                    newEntry_OnePrepay_salik.Timestamp = currentTime;
                    _context.tResellerCommisionRates.Add(newEntry_OnePrepay_salik);



                    //India
                    var newEntry_Jolo_India = _context.tResellerCommisionRates.Create();
                    newEntry_Jolo_India.ResellerId = newReseller.ResellerId;
                    newEntry_Jolo_India.ApiProvider = (int)ClassLibrary.Enum.apiProvider.Jolo;
                    newEntry_Jolo_India.Commission = Convert.ToDouble(model.commision_India);
                    newEntry_Jolo_India.ProductType = (int)ClassLibrary.Enum.GulfboxProductTypes.NoProduct;
                    newEntry_Jolo_India.IsActive = true;
                    newEntry_Jolo_India.Timestamp = currentTime;
                    _context.tResellerCommisionRates.Add(newEntry_Jolo_India);

                    var newEntry_KeralaRecharge_India = _context.tResellerCommisionRates.Create();
                    newEntry_KeralaRecharge_India.ResellerId = newReseller.ResellerId;
                    newEntry_KeralaRecharge_India.ApiProvider = (int)ClassLibrary.Enum.apiProvider.KeralaRecharge;
                    newEntry_KeralaRecharge_India.Commission = Convert.ToDouble(model.commision_India);
                    newEntry_KeralaRecharge_India.ProductType = (int)ClassLibrary.Enum.GulfboxProductTypes.NoProduct;
                    newEntry_KeralaRecharge_India.IsActive = true;
                    newEntry_KeralaRecharge_India.Timestamp = currentTime;
                    _context.tResellerCommisionRates.Add(newEntry_KeralaRecharge_India);


                    var newEntry_Pay2All_India = _context.tResellerCommisionRates.Create();
                    newEntry_Pay2All_India.ResellerId = newReseller.ResellerId;
                    newEntry_Pay2All_India.ApiProvider = (int)ClassLibrary.Enum.apiProvider.Pay2All;
                    newEntry_Pay2All_India.Commission = Convert.ToDouble(model.commision_India);
                    newEntry_Pay2All_India.ProductType = (int)ClassLibrary.Enum.GulfboxProductTypes.NoProduct;
                    newEntry_Pay2All_India.IsActive = true;
                    newEntry_Pay2All_India.Timestamp = currentTime;
                    _context.tResellerCommisionRates.Add(newEntry_Pay2All_India);





                    //Other country
                    var newEntry_Gulfbox_Othercountry = _context.tResellerCommisionRates.Create();
                    newEntry_Gulfbox_Othercountry.ResellerId = newReseller.ResellerId;
                    newEntry_Gulfbox_Othercountry.ApiProvider = (int)ClassLibrary.Enum.apiProvider.Gulfbox;
                    newEntry_Gulfbox_Othercountry.Commission = Convert.ToDouble(model.commision_OtherCountry);
                    newEntry_Gulfbox_Othercountry.ProductType = (int)ClassLibrary.Enum.GulfboxProductTypes.Intl;
                    newEntry_Gulfbox_Othercountry.IsActive = true;
                    newEntry_Gulfbox_Othercountry.Timestamp = currentTime;
                    _context.tResellerCommisionRates.Add(newEntry_Gulfbox_Othercountry);

                    var newEntry_Ding_Othercountry = _context.tResellerCommisionRates.Create();
                    newEntry_Ding_Othercountry.ResellerId = newReseller.ResellerId;
                    newEntry_Ding_Othercountry.ApiProvider = (int)ClassLibrary.Enum.apiProvider.Ding;
                    newEntry_Ding_Othercountry.Commission = Convert.ToDouble(model.commision_OtherCountry);
                    newEntry_Ding_Othercountry.ProductType = (int)ClassLibrary.Enum.GulfboxProductTypes.Intl;
                    newEntry_Ding_Othercountry.IsActive = true;
                    newEntry_Ding_Othercountry.Timestamp = currentTime;
                    _context.tResellerCommisionRates.Add(newEntry_Ding_Othercountry);



                    //International
                    var newEntry_Ding_Intl = _context.tResellerCommisionRates.Create();
                    newEntry_Ding_Intl.ResellerId = newReseller.ResellerId;
                    newEntry_Ding_Intl.ApiProvider = (int)ClassLibrary.Enum.apiProvider.Ding;
                    newEntry_Ding_Intl.Commission = Convert.ToDouble(model.commision_International);
                    newEntry_Ding_Intl.ProductType = (int)ClassLibrary.Enum.GulfboxProductTypes.Intl;
                    newEntry_Ding_Intl.IsActive = true;
                    newEntry_Ding_Intl.Timestamp = currentTime;
                    _context.tResellerCommisionRates.Add(newEntry_Ding_Intl);




                    _context.SaveChanges();
                    status = true;
                    message = "Reseller added";
                //}
            }
            return new Tuple<bool, string>(status, message);
        }

        public Tuple<bool, string> deactivateReseller(string id)
        {
            var resellerId = Convert.ToInt64(id);
            var reseller = _context.tResellers.FirstOrDefault(z => z.ResellerId == resellerId && z.IsActive);
            if (reseller != null)
            {
                reseller.IsActive = false;
                if (_context.SaveChanges() > 0)
                {
                    return new Tuple<bool, string>(true, "success");
                }
                else
                {
                    return new Tuple<bool, string>(false, "failed");
                }
            }
            else
            {
                return new Tuple<bool, string>(false, "failed");
            }
        }

        public Tuple<bool, string> editReseller(ClassLibrary.WebPostModel.EditReseller model)
        {

            bool status = false;
            string message = "Failed";

            try
            {



                var Reseller = _context.tResellers.FirstOrDefault(z => z.ResellerId == model.resellerId);

                if (Reseller != null)
                {

                    Reseller.Country = model.place ?? string.Empty;
                    Reseller.LastUpdated = currentTime;
                    Reseller.Address = model.address ?? string.Empty;
                    Reseller.Place = model.place ?? string.Empty;
                    Reseller.ShopName = model.shopName ?? string.Empty;


                    foreach (var item in Reseller.tResellerPermissions.ToList())
                    {
                        _context.tResellerPermissions.Remove(item);
                    }

                    foreach (var item in Reseller.tResellerCommisionRates.ToList())
                    {
                        _context.tResellerCommisionRates.Remove(item);
                    }

                    if (model.permission_IndiaRegion)
                    {
                        var indiaPerm = _context.tResellerPermissions.Create();
                        indiaPerm.ResellerId = Reseller.ResellerId;
                        indiaPerm.Permissions = true;
                        indiaPerm.Timestamp = currentTime;
                        indiaPerm.Type = ClassLibrary.Enum.ResellerPermissions.India.ToString();
                        _context.tResellerPermissions.Add(indiaPerm);
                    }

                    if (model.permission_International)
                    {
                        var internationalPerm = _context.tResellerPermissions.Create();
                        internationalPerm.ResellerId = Reseller.ResellerId;
                        internationalPerm.Permissions = true;
                        internationalPerm.Timestamp = currentTime;
                        internationalPerm.Type = ClassLibrary.Enum.ResellerPermissions.International.ToString();
                        _context.tResellerPermissions.Add(internationalPerm);
                    }

                    if (model.permission_Uae)
                    {
                        var uaePerm = _context.tResellerPermissions.Create();
                        uaePerm.ResellerId = Reseller.ResellerId;
                        uaePerm.Permissions = true;
                        uaePerm.Timestamp = currentTime;
                        uaePerm.Type = ClassLibrary.Enum.ResellerPermissions.UAE.ToString();
                        _context.tResellerPermissions.Add(uaePerm);
                    }


                    //Du Topup
                    var newEntry_Gulfbox_DuTopup = _context.tResellerCommisionRates.Create();
                    newEntry_Gulfbox_DuTopup.ResellerId = Reseller.ResellerId;
                    newEntry_Gulfbox_DuTopup.ApiProvider = (int)ClassLibrary.Enum.apiProvider.Gulfbox;
                    newEntry_Gulfbox_DuTopup.Commission = Convert.ToDouble(model.commision_DuTopup);
                    newEntry_Gulfbox_DuTopup.ProductType = (int)ClassLibrary.Enum.GulfboxProductTypes.Du;
                    newEntry_Gulfbox_DuTopup.IsActive = true;
                    newEntry_Gulfbox_DuTopup.Timestamp = currentTime;
                    _context.tResellerCommisionRates.Add(newEntry_Gulfbox_DuTopup);

                    var newEntry_OnePrepay_DuTopup = _context.tResellerCommisionRates.Create();
                    newEntry_OnePrepay_DuTopup.ResellerId = Reseller.ResellerId;
                    newEntry_OnePrepay_DuTopup.ApiProvider = (int)ClassLibrary.Enum.apiProvider.OnePrepay;
                    newEntry_OnePrepay_DuTopup.Commission = Convert.ToDouble(model.commision_DuTopup);
                    newEntry_OnePrepay_DuTopup.ProductType = (int)ClassLibrary.Enum.GulfboxProductTypes.Du;
                    newEntry_OnePrepay_DuTopup.IsActive = true;
                    newEntry_OnePrepay_DuTopup.Timestamp = currentTime;
                    _context.tResellerCommisionRates.Add(newEntry_OnePrepay_DuTopup);



                    //Du Voucher
                    var newEntry_Gulfbox_DuVoucher = _context.tResellerCommisionRates.Create();
                    newEntry_Gulfbox_DuVoucher.ResellerId = Reseller.ResellerId;
                    newEntry_Gulfbox_DuVoucher.ApiProvider = (int)ClassLibrary.Enum.apiProvider.Gulfbox;
                    newEntry_Gulfbox_DuVoucher.Commission = Convert.ToDouble(model.commision_DuVoucher);
                    newEntry_Gulfbox_DuVoucher.ProductType = (int)ClassLibrary.Enum.GulfboxProductTypes.DuVoucher;
                    newEntry_Gulfbox_DuVoucher.IsActive = true;
                    newEntry_Gulfbox_DuVoucher.Timestamp = currentTime;
                    _context.tResellerCommisionRates.Add(newEntry_Gulfbox_DuVoucher);

                    var newEntry_OnePrepay_DuVoucher = _context.tResellerCommisionRates.Create();
                    newEntry_OnePrepay_DuVoucher.ResellerId = Reseller.ResellerId;
                    newEntry_OnePrepay_DuVoucher.ApiProvider = (int)ClassLibrary.Enum.apiProvider.OnePrepay;
                    newEntry_OnePrepay_DuVoucher.Commission = Convert.ToDouble(model.commision_DuVoucher);
                    newEntry_OnePrepay_DuVoucher.ProductType = (int)ClassLibrary.Enum.GulfboxProductTypes.DuVoucher;
                    newEntry_OnePrepay_DuVoucher.IsActive = true;
                    newEntry_OnePrepay_DuVoucher.Timestamp = currentTime;
                    _context.tResellerCommisionRates.Add(newEntry_OnePrepay_DuVoucher);



                    //Etisalat
                    var newEntry_Gulfbox_Etisalat = _context.tResellerCommisionRates.Create();
                    newEntry_Gulfbox_Etisalat.ResellerId = Reseller.ResellerId;
                    newEntry_Gulfbox_Etisalat.ApiProvider = (int)ClassLibrary.Enum.apiProvider.Gulfbox;
                    newEntry_Gulfbox_Etisalat.Commission = Convert.ToDouble(model.commision_EtisalatTopup);
                    newEntry_Gulfbox_Etisalat.ProductType = (int)ClassLibrary.Enum.GulfboxProductTypes.Etisalat;
                    newEntry_Gulfbox_Etisalat.IsActive = true;
                    newEntry_Gulfbox_Etisalat.Timestamp = currentTime;
                    _context.tResellerCommisionRates.Add(newEntry_Gulfbox_Etisalat);

                    var newEntry_OnePrepay_Etisalat = _context.tResellerCommisionRates.Create();
                    newEntry_OnePrepay_Etisalat.ResellerId = Reseller.ResellerId;
                    newEntry_OnePrepay_Etisalat.ApiProvider = (int)ClassLibrary.Enum.apiProvider.OnePrepay;
                    newEntry_OnePrepay_Etisalat.Commission = Convert.ToDouble(model.commision_EtisalatTopup);
                    newEntry_OnePrepay_Etisalat.ProductType = (int)ClassLibrary.Enum.GulfboxProductTypes.Etisalat;
                    newEntry_OnePrepay_Etisalat.IsActive = true;
                    newEntry_OnePrepay_Etisalat.Timestamp = currentTime;
                    _context.tResellerCommisionRates.Add(newEntry_OnePrepay_Etisalat);

                    //Etisalat Voucher
                    var newEntry_Gulfbox_EtisalatVoucher = _context.tResellerCommisionRates.Create();
                    newEntry_Gulfbox_EtisalatVoucher.ResellerId = Reseller.ResellerId;
                    newEntry_Gulfbox_EtisalatVoucher.ApiProvider = (int)ClassLibrary.Enum.apiProvider.Gulfbox;
                    newEntry_Gulfbox_EtisalatVoucher.Commission = Convert.ToDouble(model.commision_EtisalatVoucher);
                    newEntry_Gulfbox_EtisalatVoucher.ProductType = (int)ClassLibrary.Enum.GulfboxProductTypes.EtisalatVoucher;
                    newEntry_Gulfbox_EtisalatVoucher.IsActive = true;
                    newEntry_Gulfbox_EtisalatVoucher.Timestamp = currentTime;
                    _context.tResellerCommisionRates.Add(newEntry_Gulfbox_EtisalatVoucher);

                    var newEntry_OnePrepay_EtisalatVoucher = _context.tResellerCommisionRates.Create();
                    newEntry_OnePrepay_EtisalatVoucher.ResellerId = Reseller.ResellerId;
                    newEntry_OnePrepay_EtisalatVoucher.ApiProvider = (int)ClassLibrary.Enum.apiProvider.OnePrepay;
                    newEntry_OnePrepay_EtisalatVoucher.Commission = Convert.ToDouble(model.commision_EtisalatVoucher);
                    newEntry_OnePrepay_EtisalatVoucher.ProductType = (int)ClassLibrary.Enum.GulfboxProductTypes.EtisalatVoucher;
                    newEntry_OnePrepay_EtisalatVoucher.IsActive = true;
                    newEntry_OnePrepay_EtisalatVoucher.Timestamp = currentTime;
                    _context.tResellerCommisionRates.Add(newEntry_OnePrepay_EtisalatVoucher);

                    //Five
                    var newEntry_Gulfbox_Five = _context.tResellerCommisionRates.Create();
                    newEntry_Gulfbox_Five.ResellerId = Reseller.ResellerId;
                    newEntry_Gulfbox_Five.ApiProvider = (int)ClassLibrary.Enum.apiProvider.Gulfbox;
                    newEntry_Gulfbox_Five.Commission = Convert.ToDouble(model.commision_Five);
                    newEntry_Gulfbox_Five.ProductType = (int)ClassLibrary.Enum.GulfboxProductTypes.Five;
                    newEntry_Gulfbox_Five.IsActive = true;
                    newEntry_Gulfbox_Five.Timestamp = currentTime;
                    _context.tResellerCommisionRates.Add(newEntry_Gulfbox_Five);

                    var newEntry_OnePrepay_Five = _context.tResellerCommisionRates.Create();
                    newEntry_OnePrepay_Five.ResellerId = Reseller.ResellerId;
                    newEntry_OnePrepay_Five.ApiProvider = (int)ClassLibrary.Enum.apiProvider.OnePrepay;
                    newEntry_OnePrepay_Five.Commission = Convert.ToDouble(model.commision_Five);
                    newEntry_OnePrepay_Five.ProductType = (int)ClassLibrary.Enum.GulfboxProductTypes.Five;
                    newEntry_OnePrepay_Five.IsActive = true;
                    newEntry_OnePrepay_Five.Timestamp = currentTime;
                    _context.tResellerCommisionRates.Add(newEntry_OnePrepay_Five);

                    //Hello

                    var newEntry_Gulfbox_Hello = _context.tResellerCommisionRates.Create();
                    newEntry_Gulfbox_Hello.ResellerId = Reseller.ResellerId;
                    newEntry_Gulfbox_Hello.ApiProvider = (int)ClassLibrary.Enum.apiProvider.Gulfbox;
                    newEntry_Gulfbox_Hello.Commission = Convert.ToDouble(model.commision_Hello);
                    newEntry_Gulfbox_Hello.ProductType = (int)ClassLibrary.Enum.GulfboxProductTypes.Hello;
                    newEntry_Gulfbox_Hello.IsActive = true;
                    newEntry_Gulfbox_Hello.Timestamp = currentTime;
                    _context.tResellerCommisionRates.Add(newEntry_Gulfbox_Hello);

                    var newEntry_OnePrepay_Hello = _context.tResellerCommisionRates.Create();
                    newEntry_OnePrepay_Hello.ResellerId = Reseller.ResellerId;
                    newEntry_OnePrepay_Hello.ApiProvider = (int)ClassLibrary.Enum.apiProvider.OnePrepay;
                    newEntry_OnePrepay_Hello.Commission = Convert.ToDouble(model.commision_Hello);
                    newEntry_OnePrepay_Hello.ProductType = (int)ClassLibrary.Enum.GulfboxProductTypes.Hello;
                    newEntry_OnePrepay_Hello.IsActive = true;
                    newEntry_OnePrepay_Hello.Timestamp = currentTime;
                    _context.tResellerCommisionRates.Add(newEntry_OnePrepay_Hello);

                    //salik

                    var newEntry_Gulfbox_salik = _context.tResellerCommisionRates.Create();
                    newEntry_Gulfbox_salik.ResellerId = Reseller.ResellerId;
                    newEntry_Gulfbox_salik.ApiProvider = (int)ClassLibrary.Enum.apiProvider.Gulfbox;
                    newEntry_Gulfbox_salik.Commission = Convert.ToDouble(model.commision_Salik);
                    newEntry_Gulfbox_salik.ProductType = (int)ClassLibrary.Enum.GulfboxProductTypes.Salik;
                    newEntry_Gulfbox_salik.IsActive = true;
                    newEntry_Gulfbox_salik.Timestamp = currentTime;
                    _context.tResellerCommisionRates.Add(newEntry_Gulfbox_salik);

                    var newEntry_OnePrepay_salik = _context.tResellerCommisionRates.Create();
                    newEntry_OnePrepay_salik.ResellerId = Reseller.ResellerId;
                    newEntry_OnePrepay_salik.ApiProvider = (int)ClassLibrary.Enum.apiProvider.OnePrepay;
                    newEntry_OnePrepay_salik.Commission = Convert.ToDouble(model.commision_Salik);
                    newEntry_OnePrepay_salik.ProductType = (int)ClassLibrary.Enum.GulfboxProductTypes.Salik;
                    newEntry_OnePrepay_salik.IsActive = true;
                    newEntry_OnePrepay_salik.Timestamp = currentTime;
                    _context.tResellerCommisionRates.Add(newEntry_OnePrepay_salik);



                    //India
                    var newEntry_Jolo_India = _context.tResellerCommisionRates.Create();
                    newEntry_Jolo_India.ResellerId = Reseller.ResellerId;
                    newEntry_Jolo_India.ApiProvider = (int)ClassLibrary.Enum.apiProvider.Jolo;
                    newEntry_Jolo_India.Commission = Convert.ToDouble(model.commision_India);
                    newEntry_Jolo_India.ProductType = (int)ClassLibrary.Enum.GulfboxProductTypes.NoProduct;
                    newEntry_Jolo_India.IsActive = true;
                    newEntry_Jolo_India.Timestamp = currentTime;
                    _context.tResellerCommisionRates.Add(newEntry_Jolo_India);

                    var newEntry_KeralaRecharge_India = _context.tResellerCommisionRates.Create();
                    newEntry_KeralaRecharge_India.ResellerId = Reseller.ResellerId;
                    newEntry_KeralaRecharge_India.ApiProvider = (int)ClassLibrary.Enum.apiProvider.KeralaRecharge;
                    newEntry_KeralaRecharge_India.Commission = Convert.ToDouble(model.commision_India);
                    newEntry_KeralaRecharge_India.ProductType = (int)ClassLibrary.Enum.GulfboxProductTypes.NoProduct;
                    newEntry_KeralaRecharge_India.IsActive = true;
                    newEntry_KeralaRecharge_India.Timestamp = currentTime;
                    _context.tResellerCommisionRates.Add(newEntry_KeralaRecharge_India);


                    var newEntry_Pay2All_India = _context.tResellerCommisionRates.Create();
                    newEntry_Pay2All_India.ResellerId = Reseller.ResellerId;
                    newEntry_Pay2All_India.ApiProvider = (int)ClassLibrary.Enum.apiProvider.Pay2All;
                    newEntry_Pay2All_India.Commission = Convert.ToDouble(model.commision_India);
                    newEntry_Pay2All_India.ProductType = (int)ClassLibrary.Enum.GulfboxProductTypes.NoProduct;
                    newEntry_Pay2All_India.IsActive = true;
                    newEntry_Pay2All_India.Timestamp = currentTime;
                    _context.tResellerCommisionRates.Add(newEntry_Pay2All_India);





                    //Other country
                    var newEntry_Gulfbox_Othercountry = _context.tResellerCommisionRates.Create();
                    newEntry_Gulfbox_Othercountry.ResellerId = Reseller.ResellerId;
                    newEntry_Gulfbox_Othercountry.ApiProvider = (int)ClassLibrary.Enum.apiProvider.Gulfbox;
                    newEntry_Gulfbox_Othercountry.Commission = Convert.ToDouble(model.commision_OtherCountry);
                    newEntry_Gulfbox_Othercountry.ProductType = (int)ClassLibrary.Enum.GulfboxProductTypes.Intl;
                    newEntry_Gulfbox_Othercountry.IsActive = true;
                    newEntry_Gulfbox_Othercountry.Timestamp = currentTime;
                    _context.tResellerCommisionRates.Add(newEntry_Gulfbox_Othercountry);

                    var newEntry_Ding_Othercountry = _context.tResellerCommisionRates.Create();
                    newEntry_Ding_Othercountry.ResellerId = Reseller.ResellerId;
                    newEntry_Ding_Othercountry.ApiProvider = (int)ClassLibrary.Enum.apiProvider.Ding;
                    newEntry_Ding_Othercountry.Commission = Convert.ToDouble(model.commision_OtherCountry);
                    newEntry_Ding_Othercountry.ProductType = (int)ClassLibrary.Enum.GulfboxProductTypes.Intl;
                    newEntry_Ding_Othercountry.IsActive = true;
                    newEntry_Ding_Othercountry.Timestamp = currentTime;
                    _context.tResellerCommisionRates.Add(newEntry_Ding_Othercountry);



                    //International
                    var newEntry_Ding_Intl = _context.tResellerCommisionRates.Create();
                    newEntry_Ding_Intl.ResellerId = Reseller.ResellerId;
                    newEntry_Ding_Intl.ApiProvider = (int)ClassLibrary.Enum.apiProvider.Ding;
                    newEntry_Ding_Intl.Commission = Convert.ToDouble(model.commision_International);
                    newEntry_Ding_Intl.ProductType = (int)ClassLibrary.Enum.GulfboxProductTypes.Intl;
                    newEntry_Ding_Intl.IsActive = true;
                    newEntry_Ding_Intl.Timestamp = currentTime;
                    _context.tResellerCommisionRates.Add(newEntry_Ding_Intl);




                    status = _context.SaveChanges() > 0 ? true : false;
                    message = status == true ? "Reseller updated" : "Failed";

                }
                else
                {
                    status = false;
                    message = "Reseller not found";
                }
            }
            catch (Exception ex)
            {
                status = false;
                message = ex.Message;
            }
            return new Tuple<bool, string>(status, message);
        }

        public Tuple<bool, string> AddResellerCredit(ClassLibrary.WebPostModel.ResellerCredit model)
        {
            bool status = false;
            string message = "Failed";

            try
            {
                var amount = Convert.ToDecimal(model.amount);
                var reseller = _context.tResellers.FirstOrDefault(z => z.ResellerId == model.resellerId);
                if (reseller != null)
                {
                    reseller.Wallet = reseller.Wallet + amount;
                    if (_context.SaveChanges() > 0)
                    {
                        var res = _context.SP_UPDATE_RESELLER_CREDITHISTORY(model.resellerId, amount, "Add");
                        status = true;
                        message = "Success";
                    }
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return new Tuple<bool, string>(status, message);
        }

        public Tuple<bool, string, string> RefreshCreditSum(ClassLibrary.WebPostModel.RefreshCreditSumReseller model)
        {
            bool status = false;
            string message = "Failed";
            decimal value = 0;
            try
            {
                value = Convert.ToDecimal(_context.SP_RESELLER_CREDIT_SUM(model.resellerId, model.fromdate ?? string.Empty, model.todate ?? string.Empty).FirstOrDefault());
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return new Tuple<bool, string, string>(status, message, Convert.ToString(value));
        }
    }
}
