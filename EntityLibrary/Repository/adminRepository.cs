﻿using ClassLibrary.WebPostModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Web.Security;

namespace EntityLibrary.Repository
{
    public class adminRepository : BaseReference
    {
        public Tuple<bool, string, EntityLibrary.tb_Admin, EntityLibrary.tb_User, EntityLibrary.tDistributor, EntityLibrary.tReseller, EntityLibrary.tAgent> login(ClassLibrary.WebPostModel.LoginModel model)
        {
            string password = StringCipher.Encrypt(model.password, salt);
            string mobile = StringCipher.Encrypt(model.mobileNumber, salt);
            var checkDB = _context.tb_Admin.FirstOrDefault(z => z.Mobilenumber == mobile && z.Password == password && z.IsActive);
            if (checkDB != null)
            {
                //FormsAuthentication.SetAuthCookie(checkDB.AdminId.ToString(), true);
                return new Tuple<bool, string, EntityLibrary.tb_Admin, EntityLibrary.tb_User, EntityLibrary.tDistributor, EntityLibrary.tReseller, EntityLibrary.tAgent>(true, "success", checkDB, null, null, null, null);
            }
            else if (checkDB == null)
            {
                var reseller = _context.tResellers.FirstOrDefault(z => z.Mobile == mobile && z.Password == password && z.IsActive);
                if (reseller != null)
                {
                    FormsAuthentication.SetAuthCookie(reseller.ResellerId.ToString(), true);
                    return new Tuple<bool, string, EntityLibrary.tb_Admin, EntityLibrary.tb_User, EntityLibrary.tDistributor, EntityLibrary.tReseller, EntityLibrary.tAgent>(true, "success", null, null, null, reseller, null);
                }
                else
                {
                    var agent = _context.tb_User.Where(z => z.Mobile == mobile && z.Password == password && z.IsActive
                        && z.UserType == (int)ClassLibrary.Enum.userType.Agent).FirstOrDefault();
                    if (agent != null)
                    {
                        FormsAuthentication.SetAuthCookie(agent.UserID.ToString(), true);
                        return new Tuple<bool, string, EntityLibrary.tb_Admin, EntityLibrary.tb_User, EntityLibrary.tDistributor, EntityLibrary.tReseller, EntityLibrary.tAgent>(true, "success", null, agent, null, null, null);
                    }
                    else
                    {
                        var distributor = _context.tDistributors.FirstOrDefault(z => z.Mobile == mobile && z.Password == password && z.IsActive);
                        if (distributor != null)
                        {
                            FormsAuthentication.SetAuthCookie(distributor.DistributorID.ToString(), true);
                            return new Tuple<bool, string, EntityLibrary.tb_Admin, EntityLibrary.tb_User, EntityLibrary.tDistributor, EntityLibrary.tReseller, EntityLibrary.tAgent>(true, "success", null, null, distributor, null, null);
                        }
                        else
                        {
                            var agentNew = _context.tAgents.FirstOrDefault(z => z.Mobile == mobile && z.Password == password && z.IsActive);
                            if (agentNew != null)
                            {
                                FormsAuthentication.SetAuthCookie(agentNew.AgentID.ToString(), true);
                                return new Tuple<bool, string, EntityLibrary.tb_Admin, EntityLibrary.tb_User, EntityLibrary.tDistributor, EntityLibrary.tReseller, EntityLibrary.tAgent>(true, "success", null, null, null, null, agentNew);
                 
                            }
                            else{

                                return new Tuple<bool, string, EntityLibrary.tb_Admin, EntityLibrary.tb_User, EntityLibrary.tDistributor, EntityLibrary.tReseller, EntityLibrary.tAgent>(false, "invalid email / password", null, null, null, null, null);

                            }
                        }
                    }
                }

            }
            else
            {
                return new Tuple<bool, string, EntityLibrary.tb_Admin, EntityLibrary.tb_User, EntityLibrary.tDistributor, EntityLibrary.tReseller, EntityLibrary.tAgent>(false, "invalid email / password", null, null, null, null, null);
            }
            
        }

        public Tuple<bool, string> addagent(ClassLibrary.WebPostModel.AddAgent model)
        {
            bool status = false;
            string message = "Failed";
            string password = StringCipher.Encrypt(model.password, salt);
            string mobile = StringCipher.Encrypt(model.mobileNumber, salt);
            string userName = StringCipher.Encrypt(model.name, salt);
            var checkList = _context.tb_User.Where(z => (z.Mobile == mobile) && z.IsActive).ToList();
            //var checkList = _context.tb_User.Where(z => (z.Mobile == model.mobileNumber || z.Username.ToLower() == model.name.ToLower()) && z.IsActive).ToList();26-08-2018
            if (checkList.Count > 0)
            {
                if (checkList.Any(z => z.Mobile == mobile))
                {
                    status = false;
                    message = "Mobile number already taken";
                }
                //else if (checkList.Any(z => z.Username.ToLower() == model.name.ToLower()))
                //{
                //    status = false;
                //    message = "Name already taken";
                //}
                else
                {
                    status = false;
                    message = "Failed";
                }
            }
            else if (_context.tDistributors.Any(z => z.Mobile == mobile))
            {
                status = false;
                message = "Mobile number already taken";
            }
            else
            {
                var newagent = _context.tb_User.Create();
                newagent.Country = model.place ?? string.Empty;
                newagent.CountryCode = model.countryCode ?? string.Empty;
                newagent.IsActive = true;
                newagent.Mobile = mobile ?? string.Empty;
                newagent.Password = password ?? string.Empty;
                newagent.Timestamp = currentTime;
                newagent.UserGuid = Guid.NewGuid();
                newagent.ProfileImage = string.Empty;
                newagent.Username = userName ?? string.Empty;
                newagent.Address = model.address ?? string.Empty;
                newagent.Place = model.place ?? string.Empty;
                newagent.ShopName = model.shopName ?? string.Empty;
                newagent.UserType = (int)ClassLibrary.Enum.userType.Agent;
                newagent.Wallet = Convert.ToDecimal(0);
                newagent.IsBlocked = false;

                string currency = "AED";
                var Fromcurrency = _context.tb_AdminCurrencyRate.Where(z => z.FromCountryCode == newagent.CountryCode && z.IsActive).FirstOrDefault();
                if (Fromcurrency != null)
                {
                    currency = Fromcurrency.FromCurrency;
                }
                else
                {
                    var Tocurrency = _context.tb_AdminCurrencyRate.Where(z => z.ToCountryCode == newagent.CountryCode && z.IsActive).FirstOrDefault();
                    if (Tocurrency != null)
                    {
                        currency = Tocurrency.FromCurrency;
                    }
                }
                newagent.Currency = currency ?? string.Empty;
                _context.tb_User.Add(newagent);
                if (_context.SaveChanges() > 0)
                {
                    status = true;
                    message = "Agent added";
                }
            }
            return new Tuple<bool, string>(status, message);
        }

        public string BatchIdRandomString(int length)
        {
            Random random = new Random();
            const string chars = "abcdefghijklmnopqrstuvwxyz0123456789";
            var randomString = "";
            var check = 0;
            do
            {
                randomString = new string(Enumerable.Repeat(chars, length)
                 .Select(s => s[random.Next(s.Length)]).ToArray());
                check = _context.sp_check_recharge_batchid(randomString);
            } while (!Convert.ToBoolean(check));
            return randomString;
        }

        public Tuple<bool, string> addrechargecard(ClassLibrary.WebPostModel.Addrechargecard model)
        {
            bool status = false;
            string message = "Failed";
            var batchId = BatchIdRandomString(30);
            var returnValue = _context.sp_create_rechargecard(Convert.ToInt32(model.amount), Convert.ToInt32(model.count), batchId, model.currency ?? "AED").FirstOrDefault();
            if (Convert.ToInt32(returnValue) == 1)
            {
                status = false;
                message = "The operation completed with errors";
            }
            else
            {
                status = true;
                message = "Success";
            }
            return new Tuple<bool, string>(status, message);
        }

        public Tuple<bool, string> updateCard(string id)
        {
            try
            {
                var cardId = Convert.ToInt64(id);
                var cardData = _context.tb_RechargeCard.FirstOrDefault(z => z.CardId == cardId);
                if (cardData != null)
                {
                    cardData.IsDeactivated = !cardData.IsDeactivated;
                    if (_context.SaveChanges() > 0)
                    {
                        return new Tuple<bool, string>(true, "Success");
                    }
                    else
                    {
                        return new Tuple<bool, string>(false, "Failed");
                    }
                }
                else
                {
                    return new Tuple<bool, string>(false, "Card not found");
                }
            }
            catch (Exception ex)
            {
                return new Tuple<bool, string>(false, ex.Message);
            }
        }

        public Tuple<bool, string, tb_Admin> adminlogin(ClassLibrary.WebPostModel.AdminLoginModel model)
        {
            //string password = StringCipher.Decrypt(model.password, salt);
            //string mobile = StringCipher.Decrypt(model.mobileNumber, salt);
            var checkDB = _context.tb_Admin.FirstOrDefault(z => z.Mobilenumber == model.mobileNumber && z.Password == model.password && z.Pin == model.pin && z.IsActive);
            if (checkDB != null)
            {
                //FormsAuthentication.SetAuthCookie(checkDB.AdminId.ToString(), true);
                FormsAuthentication.SetAuthCookie(checkDB.AdminId.ToString(), false);
                return new Tuple<bool, string, tb_Admin>(true, "success", checkDB);
            }
            else
            {
                return new Tuple<bool, string, tb_Admin>(false, "Incorrect pin", null);
            }
        }

        public Tuple<bool, string> uploadprofileimage(ClassLibrary.WebPostModel.UploadProfileImage model)
        {
            string imageFilepath = model.imageFilepath;
            long adminId = Convert.ToInt64(model.adminId);
            var admin = _context.tb_Admin.FirstOrDefault(z => z.AdminId == adminId && z.IsActive);
            if (admin != null)
            {
                if (model.profileImage != null && model.profileImage.ContentLength > 0)
                {
                    //string folderPath = System.Web.HttpContext.Current.Server.MapPath("~/Files/ProfileImage/Admin/");
                    //string folderPath = System.Web.HttpContext.Current.Server.MapPath("~/upload/ProfileImage/Admin/");
                    string folderPath = System.Web.HttpContext.Current.Server.MapPath("~/upload/");
                    string fileExtension = Path.GetExtension(model.profileImage.FileName);
                    string fileName = Guid.NewGuid().ToString();
                    if (!Directory.Exists(folderPath))
                        Directory.CreateDirectory(folderPath);
                    var fileSavePath = Path.Combine(folderPath, fileName + fileExtension);
                    model.profileImage.SaveAs(fileSavePath);
                    //imageFilepath = Path.Combine("/Files/ProfileImage/Admin/", fileName + fileExtension);
                    //imageFilepath = Path.Combine("/upload/ProfileImage/Admin/", fileName + fileExtension);
                    imageFilepath = Path.Combine("/upload/", fileName + fileExtension);
                }
                admin.ProfilePic = imageFilepath;
                _context.SaveChanges();
            }

            return new Tuple<bool, string>(true, "success");
        }

        public Tuple<bool, string> editname(ClassLibrary.WebPostModel.EditName model)
        {
            long adminId = Convert.ToInt64(model.adminId);
            var admin = _context.tb_Admin.FirstOrDefault(z => z.AdminId == adminId && z.IsActive);
            if (admin != null)
            {
                admin.FulllName = model.name;
                _context.SaveChanges();
            }

            return new Tuple<bool, string>(true, "success");
        }

        public Tuple<bool, string> editcountry(ClassLibrary.WebPostModel.EditCountry model)
        {
            long adminId = Convert.ToInt64(model.adminId);
            var admin = _context.tb_Admin.FirstOrDefault(z => z.AdminId == adminId && z.IsActive);
            if (admin != null)
            {
                admin.Country = model.country;
                _context.SaveChanges();
            }

            return new Tuple<bool, string>(true, "success");
        }

        public Tuple<bool, string> editpassword(ClassLibrary.WebPostModel.EditPassword model)
        {
            long adminId = Convert.ToInt64(model.adminId);
            var admin = _context.tb_Admin.FirstOrDefault(z => z.AdminId == adminId && z.IsActive);
            if (admin != null)
            {
                string currentPassword = StringCipher.Encrypt(model.currentPassword, salt);
                string newPassword = StringCipher.Encrypt(model.newPassword, salt);
                if (admin.Password != currentPassword)
                {
                    return new Tuple<bool, string>(false, "Incorrect current password");
                }
                else
                {
                    if (admin.Password != newPassword)
                    {
                        admin.Password = newPassword;
                        if (_context.SaveChanges() > 0)
                        {
                            return new Tuple<bool, string>(true, "success");
                        }
                        else
                        {
                            return new Tuple<bool, string>(false, "failed");
                        }
                    }
                    else
                    {
                        return new Tuple<bool, string>(true, "success");
                    }
                }
            }
            else
            {
                return new Tuple<bool, string>(false, "failed");
            }
        }

        public Tuple<bool, string> editmobile(ClassLibrary.WebPostModel.EditMobile model)
        {
            long adminId = Convert.ToInt64(model.adminId);
            var admin = _context.tb_Admin.FirstOrDefault(z => z.AdminId == adminId && z.IsActive);
            if (admin != null)
            {
                string currentMobile = StringCipher.Encrypt(model.currentMobile, salt);
                string newMobile = StringCipher.Encrypt(model.newMobile, salt);
                if (admin.Mobilenumber != currentMobile)
                {
                    return new Tuple<bool, string>(false, "Incoreect current mobile number");
                }
                else if (_context.tb_Admin.Any(z => z.Mobilenumber == newMobile))
                {
                    return new Tuple<bool, string>(false, "Mobile number already taken");
                }
                else
                {
                    admin.Mobilenumber = newMobile;
                    if (_context.SaveChanges() > 0)
                    {
                        return new Tuple<bool, string>(true, "success");
                    }
                    else
                    {
                        return new Tuple<bool, string>(false, "failed");
                    }
                }
            }
            else
            {
                return new Tuple<bool, string>(false, "failed");
            }
        }

        public Tuple<bool, string, string, string> exportCardDataToExcel(string type)
        {
            bool status = false;
            string message = "failed";
            var filepath = "";
            var filename = "";
            try
            {
                string date = DateTime.UtcNow.ToString("dd_MM_yyyy_hh_mm_ss_tt");
                var cardList = new EntityLibrary.Data.AdminService().getAllRechargeCards(type);
                if (cardList.Count > 0)
                {
                    StringBuilder sb = new StringBuilder();
                    string folderPath = System.Web.HttpContext.Current.Server.MapPath("~/RechargeCardList/");
                    if (!Directory.Exists(folderPath))
                        Directory.CreateDirectory(folderPath);

                    sb.AppendLine("" + "," + "," + " Recharge Card List ");
                    sb.AppendLine("");
                    sb.AppendLine("" + "," + "Sl No" + "," + "Card Id" + "," + "Card Number" + "," + "Currency" + "," + "Amount" + "," + "Qr Code");
                    sb.AppendLine("");
                    int SLNo = 1;
                    foreach (var card in cardList.ToList().GroupBy(z => z.Currency).ToList())
                    {
                        foreach (var item in card)
                        {
                            sb.AppendLine("" + "," + SLNo.ToString() + "," + item.CardId + "," + item.CardNumber + "," + item.Currency + "," + item.Amount + "," + item.CardGuid.ToString() + '~' + item.CardId.ToString());
                            sb.AppendLine("");
                            SLNo = SLNo + 1;
                        }
                        sb.AppendLine("");
                    }
                    filename = "RechargeCardList_" + date + ".csv";
                    filepath = folderPath + @"\" + filename;
                    System.IO.File.WriteAllText(filepath, Convert.ToString(sb));
                    status = true;
                    message = "success";

                    var batchid = cardList.FirstOrDefault().BatchId ?? string.Empty;
                    if (batchid != null && batchid != string.Empty)
                    {
                        _context.sp_insert_batch_to_export_track(batchid);
                    }

                }
                else
                {
                    status = false;
                    message = "no card found";
                }

                //var couponData = new EntityLibrary.Data.Coupon(couponId);
                //if (couponData != null)
                //{
                //    string folderPath = HttpContext.Current.Server.MapPath("~/RechargeCardList/" + couponData.CouponGuid.ToString() + "/");
                //    if (!Directory.Exists(folderPath))
                //        Directory.CreateDirectory(folderPath);

                //    //List<PatientDetails> converterlist = (List<PatientDetails>)dgPatientStatusReport.ItemsSource;
                //    sb.AppendLine("" + "," + "," + "," + " Coupon Report ");
                //    sb.AppendLine("");
                //    sb.AppendLine("" + "Coupon Details");
                //    sb.AppendLine("");
                //    sb.AppendLine("" + "," + "Coupon Number" + "," + "Category" + "," + "From Date" + "," + "To Date" + "," + "No. of user's used");
                //    sb.AppendLine("");
                //    sb.AppendLine("" + "," + couponData.CouponNumber + "," + couponData.CategoryTypeString + "," + couponData.FromDate.ToString("dd/MM/yyyy") + "," + couponData.ToDate.ToString("dd/MM/yyyy") + "," + couponData.CouponUserList.Count.ToString());

                //    sb.AppendLine("");
                //    sb.AppendLine("" + "Used By");
                //    sb.AppendLine("");
                //    sb.AppendLine("" + "," + "Sl No" + "," + "Name" + "," + "Email" + "," + "Used on");
                //    sb.AppendLine("");
                //    int couponSLNo = 1;
                //    foreach (var user in couponData.CouponBookingList.Where(z => z.CouponStatus == (int)ClassLibrary.Enum.CouponStatus.Redeemed).ToList())
                //    {
                //        sb.AppendLine("" + "," + "," + "," + "," + couponSLNo.ToString() + "," + user.UserData.Name.ToString() + "," + user.UserData.Email + "," + user.UserData.Timestamp.ToString("dd/MM/yyyy"));
                //        couponSLNo = couponSLNo + 1;
                //    }

                //    string date = DateTime.UtcNow.ToString("dd_MM_yyyy_hh_mm_ss_tt");

                //    filename = "CouponReport_" + date + ".csv";
                //    filepath = folderPath + @"\" + filename;
                //    System.IO.File.WriteAllText(filepath, Convert.ToString(sb));
                //    status = true;
                //    message = "success";
                //}
            }
            catch (Exception ex)
            {
                status = false;
                message = ex.Message;
            }
            return new Tuple<bool, string, string, string>(status, message, filepath, filename);
        }


        //public Tuple<bool, string> editpriority(ClassLibrary.ServicePostModel.ApiPriority model)
        public Tuple<bool, string> editpriority(string data)
        {
            List<ClassLibrary.ServicePostModel.ApiPriorityData> bsObj = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ClassLibrary.ServicePostModel.ApiPriorityData>>(data);
            var serviceType = bsObj.FirstOrDefault().ServiceType;
            var prevList = _context.tb_ApiPriority.Where(z => z.IsActive && z.ServiceType == serviceType).ToList();
            if (prevList.Count > 0)
            {
                foreach (var item in prevList)
                {
                    _context.tb_ApiPriority.Remove(item);
                }
            }

            foreach (var item in bsObj)
            {
                var newRow = _context.tb_ApiPriority.Create();
                newRow.ApiProvider = item.ApiProvider;
                newRow.IsActive = true;
                newRow.Priority = Convert.ToInt32(item.Priority);
                newRow.ServiceArea = item.ServiceArea;
                newRow.ServiceType = item.ServiceType;
                _context.tb_ApiPriority.Add(newRow);
            }

            if (_context.SaveChanges() > 0)
            {
                return new Tuple<bool, string>(true, "success");
            }
            else
            {
                return new Tuple<bool, string>(false, "failed");
            }

        }

        public Tuple<bool, string> deactivateagent(string id)
        {
            var agentId = Convert.ToInt64(id);
            var agent = _context.tb_User.FirstOrDefault(z => z.UserID == agentId && z.IsActive);
            if (agent != null)
            {
                agent.IsActive = false;
                if (_context.SaveChanges() > 0)
                {
                    return new Tuple<bool, string>(true, "success");
                }
                else
                {
                    return new Tuple<bool, string>(false, "failed");
                }
            }
            else
            {
                return new Tuple<bool, string>(false, "failed");
            }
        }

        public Tuple<bool, string> editagent(ClassLibrary.WebPostModel.EditAgent model)
        {
            bool status = false;
            string message = "Failed";

            var agent = _context.tb_User.FirstOrDefault(z => z.UserID == model.agentId);
            if (agent != null)
            {
                if (agent.Address == model.address && agent.Place == model.place && agent.ShopName == model.shopName)
                {
                    status = true;
                    message = "Profile edited";
                }
                else
                {
                    agent.Address = model.address ?? string.Empty;
                    agent.Place = model.place ?? string.Empty;
                    agent.ShopName = model.shopName ?? string.Empty;
                    if (_context.SaveChanges() > 0)
                    {
                        status = true;
                        message = "Profile edited";
                    }
                }
            }
            return new Tuple<bool, string>(status, message);
        }

        public Tuple<bool, string> updatecurrencyrate(ClassLibrary.WebPostModel.UpdateCurrencyRate model)
        {
            bool status = false;
            string message = "Failed";

            var currencyRates = _context.tb_AdminCurrencyRate.ToList();
            foreach (var currency in currencyRates)
            {
                _context.tb_AdminCurrencyRate.Remove(currency);
            }


            var list = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ClassLibrary.WebPostModel.CurrencyData>>(model.data);
            foreach (var item in list)
            {
                var newEntry = _context.tb_AdminCurrencyRate.Create();
                newEntry.CurrencyRate = Convert.ToDouble(item.rate);
                newEntry.FromCountry = item.fromcountry;
                newEntry.FromCountryCode = item.fromcountrycode;
                newEntry.FromCurrency = item.fromcurrency;
                newEntry.IsActive = true;
                newEntry.ToCountry = item.tocountry;
                newEntry.ToCountryCode = item.tocountrycode;
                newEntry.ToCurrency = item.tocurrency;
                _context.tb_AdminCurrencyRate.Add(newEntry);
            }

            if (_context.SaveChanges() > 0)
            {
                status = true;
                message = "Currency rates updated";
            }
            else
            {
                status = false;
                message = "Failed";
            }

            return new Tuple<bool, string>(status, message);
        }

        public Tuple<bool, string> deleteuser(string id)
        {
            var userId = Convert.ToInt64(id);
            var user = _context.tb_User.FirstOrDefault(z => z.UserID == userId && z.IsActive);
            if (user != null)
            {
                user.IsActive = false;
                if (_context.SaveChanges() > 0)
                {
                    return new Tuple<bool, string>(true, "success");
                }
                else
                {
                    return new Tuple<bool, string>(false, "failed");
                }
            }
            else
            {
                return new Tuple<bool, string>(false, "failed");
            }
        }

        public Tuple<bool, string> updateagentcommissionrates(ClassLibrary.WebPostModel.UpdateCommission model)
        {
            bool status = false;
            string message = "Failed";

            var Rates = _context.tb_AgentCommisionRate.Where(z => z.AgentId == model.agentId).ToList();
            foreach (var item in Rates)
            {
                _context.tb_AgentCommisionRate.Remove(item);
            }


            var list = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ClassLibrary.WebPostModel.AddCommission>>(model.data);
            foreach (var item in list)
            {
                var newEntry = _context.tb_AgentCommisionRate.Create();
                newEntry.AgentId = item.agentId;
                newEntry.ApiProvider = item.apiprovider;
                newEntry.Commission = Convert.ToDouble(item.commission);
                newEntry.ProductType = item.producttype;
                newEntry.IsActive = true;
                newEntry.Timestamp = currentTime;
                newEntry.IsAdminCommission = Convert.ToBoolean(item.isadmincommission);
                _context.tb_AgentCommisionRate.Add(newEntry);
            }

            if (_context.SaveChanges() > 0)
            {
                status = true;
                message = "Commission rates updated";
            }
            else
            {
                status = false;
                message = "Failed";
            }

            return new Tuple<bool, string>(status, message);
        }

        public Tuple<bool, string> importdatafromexcel(ClassLibrary.WebPostModel.ImportDataFromExcel model)
        {
            string filePath = string.Empty;
            bool status = false;
            string message = "Failed";
            try
            {
                if (model.file != null && model.file.ContentLength > 0)
                {

                    //string folderPath = System.Web.HttpContext.Current.Server.MapPath("~/Files/Uploads/");
                    //string folderPath = System.Web.HttpContext.Current.Server.MapPath("~/upload/Uploads/");
                    string folderPath = System.Web.HttpContext.Current.Server.MapPath("~/upload/");
                    string fileExtension = Path.GetExtension(model.file.FileName);
                    string fileName = Guid.NewGuid().ToString();
                    if (!Directory.Exists(folderPath))
                        Directory.CreateDirectory(folderPath);
                    var fileSavePath = Path.Combine(folderPath, fileName + fileExtension);
                    filePath = fileSavePath;
                    model.file.SaveAs(fileSavePath);

                    string conString = string.Empty;
                    switch (fileExtension)
                    {
                        case ".xls": //Excel 97-03.
                            conString = System.Configuration.ConfigurationManager.ConnectionStrings["Excel03ConString"].ConnectionString;
                            break;
                        case ".xlsx": //Excel 07 and above.
                            conString = System.Configuration.ConfigurationManager.ConnectionStrings["Excel07ConString"].ConnectionString;
                            break;
                    }

                    DataTable dt = new DataTable();
                    conString = string.Format(conString, filePath);

                    using (OleDbConnection connExcel = new OleDbConnection(conString))
                    {
                        using (OleDbCommand cmdExcel = new OleDbCommand())
                        {
                            using (OleDbDataAdapter odaExcel = new OleDbDataAdapter())
                            {
                                cmdExcel.Connection = connExcel;

                                //Get the name of First Sheet.
                                connExcel.Open();
                                DataTable dtExcelSchema;
                                dtExcelSchema = connExcel.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                                string sheetName = dtExcelSchema.Rows[0]["TABLE_NAME"].ToString();
                                connExcel.Close();

                                //Read Data from First Sheet.
                                connExcel.Open();
                                //cmdExcel.CommandText = "SELECT * From [" + sheetName + "] where ([SerialNumber] is not null and [SerialNumber]!='') and ([PinNumber] is not null and [PinNumber]!='') and ([CardType] is not null and [CardType]!='') and ([Amount] is not null and [Amount]!='')";
                                cmdExcel.CommandText = "SELECT * From [" + sheetName + "]";
                                odaExcel.SelectCommand = cmdExcel;
                                odaExcel.Fill(dt);
                                connExcel.Close();
                            }
                        }
                    }

                    //DataTable dt1 = new DataTable();
                    //var rows = from row in dt.AsEnumerable()
                    //           where row.Field<double>("SerialNumber") != 0
                    //           //row.Field<string>("PinNumber") != null &&
                    //           //row.Field<string>("CardType") != null &&
                    //           //row.Field<double>("Amount") != 0
                    //           select row;
                    //foreach (DataRow dr in rows)
                    //{
                    //    dt1.Rows.Add(dr);
                    //}


                    conString = System.Configuration.ConfigurationManager.ConnectionStrings["TouchAndPayEntitiesExcel"].ConnectionString;
                    using (SqlConnection con = new SqlConnection(conString))
                    {
                        //29-08-2018 premjith
                        //using (SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(con))
                        //{
                        //    //Set the database table name.
                        //    sqlBulkCopy.DestinationTableName = "dbo.tb_AdminStoredCard";

                        //    //[OPTIONAL]: Map the Excel columns with that of the database table
                        //    sqlBulkCopy.ColumnMappings.Add("SerialNumber", "SerialNumber");
                        //    sqlBulkCopy.ColumnMappings.Add("PinNumber", "PinNumber");
                        //    sqlBulkCopy.ColumnMappings.Add("CardType", "CardType");
                        //    sqlBulkCopy.ColumnMappings.Add("Amount", "Amount");
                        //    con.Open();
                        //    //sqlBulkCopy.WriteToServer(dt1);
                        //    sqlBulkCopy.WriteToServer(dt);
                        //    con.Close();
                        //}


                        foreach (DataRow row in dt.Rows)
                        {
                            try
                            {
                                string SerialNumber = Convert.ToString(row.Field<double?>("SerialNumber"));
                                string PinNumber = Convert.ToString(row.Field<string>("PinNumber"));
                                string CardType = Convert.ToString(row.Field<string>("CardType"));
                                string Amount = Convert.ToString(row.Field<double?>("Amount"));

                                if (SerialNumber != null && SerialNumber != string.Empty &&
                                    PinNumber != null && PinNumber != string.Empty &&
                                    CardType != null && CardType != string.Empty &&
                                    Amount != null && Amount != string.Empty)
                                {
                                    try
                                    {
                                        var returenValue = _context.sp_add_importedcards(SerialNumber, PinNumber, CardType, Amount, "AED");
                                    }
                                    catch (Exception ex1)
                                    {

                                    }
                                }

                            }
                            catch (Exception ex)
                            {
                                status = false;
                                message = ex.Message;
                            }
                        }


                    }
                    status = true;
                    message = "Success";
                }
            }
            catch (Exception ex)
            {
                status = false;
                message = ex.Message;
            }
            return new Tuple<bool, string>(status, message);
        }

        public Tuple<bool, string> deleteimportedcards(string id)
        {
            bool status = false;
            string message = "Failed";

            try
            {
                var cardId = Convert.ToInt64(id);
                var card = _context.tb_AdminStoredCard.FirstOrDefault(z => z.Id == cardId && !z.IsDeleted);
                if (card != null)
                {
                    card.IsDeleted = true;
                    if (_context.SaveChanges() > 0)
                    {
                        status = true;
                        message = "Deleted";
                    }
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return new Tuple<bool, string>(status, message);
        }

        public Tuple<bool, string> AddAgentBalance(ClassLibrary.WebPostModel.AddAgentBalance model)
        {
            bool status = false;
            string message = "Failed";

            try
            {
                var amount = Convert.ToDecimal(model.amount);
                var agent = _context.tb_User.FirstOrDefault(z => z.UserID == model.agentId);
                if (agent != null)
                {
                    agent.Wallet = agent.Wallet + amount;
                    if (_context.SaveChanges() > 0)
                    {
                        var res = _context.SP_UPDATE_CREDITHISTORY(model.agentId, amount, "Add");
                        status = true;
                        message = "Success";
                    }
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return new Tuple<bool, string>(status, message);
        }

        public Tuple<bool, string> sendContactUsMail(ClassLibrary.WebPostModel.SendContactUsMail model)
        {
            bool status = false;
            string message = "Failed";

            try
            {
                var filePathdelr = System.Web.Hosting.HostingEnvironment.MapPath(@"~/EmailTemplate/ContactUsWeb.html");
                var emailTemplateCustomer = System.IO.File.ReadAllText(filePathdelr);
                var mailBodyDoctor = emailTemplateCustomer.Replace("{{Email}}", model.email)
                    .Replace("{{name}}", model.name)
                    .Replace("{{Subject}}", model.subject)
                    .Replace("{{message}}", model.message);
                var emailstatus = Send(mailBodyDoctor, "Mesage", new System.Collections.ArrayList { "info@touchpay.ae" });
                if (emailstatus)
                {
                    status = true;
                    message = "Mail sent";
                }
                else
                {
                    status = false;
                    message = "failed";
                }
            }
            catch (Exception ex)
            {
                status = false;
                message = ex.Message;
            }

            return new Tuple<bool, string>(status, message);
        }

        internal static bool Send(string mailbody, string receiverName, System.Collections.ArrayList list_emails)
        {
            {
                SmtpClient client = new SmtpClient();
                string userName = "touchandpay2k18@gmail.com";
                string password = "touchandpay";
                string fromName = "Touch&Pay";
                MailAddress address = new MailAddress(userName, fromName);

                foreach (string mailList in list_emails)
                {
                    MailMessage message = new MailMessage();
                    message.To.Add(new MailAddress(mailList, receiverName));
                    message.From = address;
                    message.Subject = "Touch&pay App";
                    message.IsBodyHtml = true;
                    message.Body = mailbody;
                    client.Host = "smtp.gmail.com";//ConfigurationManager.AppSettings["smptpserver"];
                    client.Port = 587;//Convert.ToInt32(ConfigurationManager.AppSettings["port"]);
                    // client.Host = "smtpmailer.hostek.net";
                    // client.Port =25;
                    client.EnableSsl = true;
                    client.UseDefaultCredentials = true;
                    //client.UseDefaultCredentials = true;
                    client.Credentials = new NetworkCredential(userName, password);
                    try
                    {
                        client.Send(message);
                    }
                    catch (Exception e)
                    {
                    }
                }
            }
            return true;
        }


        public Tuple<bool, string, List<EntityLibrary.Data.SP_FETCHALL_TRANSACTION_WITHOUTINDEX>> RefreshTotalSale(string startDate, string endDate, bool fetchall, string currency, string filterType)
        {

            var list = new EntityLibrary.Data.AdminService().getallTransactionwithoutIndex(startDate, endDate, fetchall, currency, filterType);
            return new Tuple<bool, string, List<EntityLibrary.Data.SP_FETCHALL_TRANSACTION_WITHOUTINDEX>>(true, "success", list);
        }

        public Tuple<bool, string, List<Data.SP_FETCHALL_TRANSACTION_WITHOUTINDEX>> RefreshActualAmountTotalSale(string startDate, string endDate, bool fetchall, string currency, string filterType)
        {
            var list = new EntityLibrary.Data.AdminService().getallTransactionwithoutIndex(startDate, endDate, fetchall, currency, filterType);
            return new Tuple<bool, string, List<EntityLibrary.Data.SP_FETCHALL_TRANSACTION_WITHOUTINDEX>>(true, "success", list);
        }

        public Tuple<bool, string, List<Data.SP_FETCHALL_TRANSACTION_WITHOUTINDEX>> RefreshWalletDeductionTotalSale(string startDate, string endDate, bool fetchall, string currency, string filterType)
        {
            var list = new EntityLibrary.Data.AdminService().getallTransactionwithoutIndex(startDate, endDate, fetchall, currency, filterType);
            return new Tuple<bool, string, List<EntityLibrary.Data.SP_FETCHALL_TRANSACTION_WITHOUTINDEX>>(true, "success", list);
        }

        public Tuple<bool, string> SubtractAgentBalance(ClassLibrary.WebPostModel.SubtractAgentBalance model)
        {
            bool status = false;
            string message = "Failed";

            try
            {
                var amount = Convert.ToDecimal(model.amount);
                var agent = _context.tb_User.FirstOrDefault(z => z.UserID == model.agentId);
                if (agent != null)
                {
                    agent.Wallet = agent.Wallet - amount;
                    if (_context.SaveChanges() > 0)
                    {
                        var res = _context.SP_UPDATE_CREDITHISTORY(model.agentId, amount, "Subtract");
                        status = true;
                        message = "Success";
                    }
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return new Tuple<bool, string>(status, message);
        }

        public Tuple<bool, string> forgotPasswordAgent(string id)
        {
            bool status = false;
            string message = "Failed";
            try
            {
                var userId = Convert.ToInt64(id);
                var account = _context.tb_User.Where(z => z.UserID == userId && z.IsActive).FirstOrDefault();
                if (account != null)
                {
                    string password = StringCipher.Decrypt(account.Password, salt);
                    string mobile = StringCipher.Decrypt(account.Mobile, salt);
                    var countryCode = account.CountryCode;
                    string MSG_message = "";
                    MSG_message = System.Web.HttpUtility.UrlEncode("Your Touch&Pay account Password is :" + password + "");
                    //MSG_message = System.Web.HttpUtility.UrlEncode("Test SMS");
                    string url = "https://smsapi.24x7sms.com/api_2.0/SendSMS.aspx?APIKEY=" + SMS_API_Key + "&MobileNo=" + countryCode + mobile + "&SenderID=" + SMS_SENDER_ID + "&Message=" + MSG_message + "&ServiceName=" + SMS_SERVICE + "";
                    //string url = "https://smsapi.24x7sms.com/api_2.0/SendSMS.aspx?APIKEY=" + SMS_API_Key + "&MobileNo=" + "971" + "507465039" + "&SenderID=" + SMS_SENDER_ID + "&Message=" + MSG_message + "&ServiceName=" + SMS_SERVICE + "";
                    string request = url;
                    string success = MakeWebRequestGET(request);
                    if (success.Contains("success"))
                    {
                        status = true;
                        message = "success";
                    }
                    else
                    {
                        status = false;
                        message = "failed";
                    }
                }
                else
                {
                    status = false;
                    message = "Invalid account";
                }
            }
            catch (Exception ex)
            {
                status = false;
                message = ex.Message;
            }

            return new Tuple<bool, string>(status, message);
        }
        public string MakeWebRequestGET(string url) //url is https API
        {
            string result = "";
            try
            {
                WebRequest WReq = WebRequest.Create(url);
                WebResponse wResp = WReq.GetResponse();
                StreamReader sr = new StreamReader(wResp.GetResponseStream());
                result = sr.ReadToEnd();
                sr.Close();
            }
            catch (Exception e)
            {
                result = "An error has occured";
            }
            finally
            {
            }
            return result; //result will provide you MsgID
        }

        public Tuple<bool, string> blockagent(string id)
        {
            var agentId = Convert.ToInt64(id);
            var agent = _context.tb_User.FirstOrDefault(z => z.UserID == agentId && z.IsActive);
            if (agent != null)
            {
                agent.IsBlocked = !agent.IsBlocked;
                if (_context.SaveChanges() > 0)
                {
                    return new Tuple<bool, string>(true, "success");
                }
                else
                {
                    return new Tuple<bool, string>(false, "failed");
                }
            }
            else
            {
                return new Tuple<bool, string>(false, "failed");
            }
        }

        public Tuple<bool, string> UpdateAndroidversion(ClassLibrary.WebPostModel.UpdateAndroidversion model)
        {
            long adminId = Convert.ToInt64(model.adminId);
            var version = _context.tAppVersions.FirstOrDefault(z => z.AppType == (int)ClassLibrary.Enum.AppType.Android);
            if (version != null)
            {
                version.Version = model.version;
                _context.SaveChanges();
            }
            else
            {
                var newVersion = _context.tAppVersions.Create();
                newVersion.AppType = (int)ClassLibrary.Enum.AppType.Android;
                newVersion.Timestamp = currentTime;
                newVersion.Version = model.version;
                _context.tAppVersions.Add(newVersion);
                _context.SaveChanges();
            }

            return new Tuple<bool, string>(true, "success");
        }

        public Tuple<bool, string> UpdateIosversion(ClassLibrary.WebPostModel.UpdateIosversion model)
        {
            long adminId = Convert.ToInt64(model.adminId);
            var version = _context.tAppVersions.FirstOrDefault(z => z.AppType == (int)ClassLibrary.Enum.AppType.Ios);
            if (version != null)
            {
                version.Version = model.version;
                _context.SaveChanges();
            }
            else
            {
                var newVersion = _context.tAppVersions.Create();
                newVersion.AppType = (int)ClassLibrary.Enum.AppType.Ios;
                newVersion.Timestamp = currentTime;
                newVersion.Version = model.version;
                _context.tAppVersions.Add(newVersion);
                _context.SaveChanges();
            }

            return new Tuple<bool, string>(true, "success");
        }


        public Tuple<bool, string> addDistributor(ClassLibrary.WebPostModel.AddDistributor model)
        {
            bool status = false;
            string message = "Failed";
            string password = StringCipher.Encrypt(model.password, salt);
            string mobile = StringCipher.Encrypt(model.mobileNumber, salt);
            string userName = StringCipher.Encrypt(model.name, salt);
            string email = StringCipher.Encrypt(model.email, salt);
            var checkList = _context.tDistributors.Where(z => (z.Mobile == mobile) && z.IsActive).ToList();
            if (checkList.Count > 0)
            {
                if (checkList.Any(z => z.Mobile == mobile))
                {
                    status = false;
                    message = "Mobile number already taken";
                }
                else if (checkList.Any(z => z.Email == email))
                {
                    status = false;
                    message = "Email already taken";
                }
                else
                {
                    status = false;
                    message = "Failed";
                }
            }
            else if (_context.tb_User.Any(z => z.Mobile == mobile))
            {
                status = false;
                message = "Mobile number already taken";
            }
            else if (_context.tResellers.Any(z => z.Mobile == mobile))
            {
                status = false;
                message = "Mobile number already taken";
            }
            else
            {
                var newDistributor = _context.tDistributors.Create();
                newDistributor.Country = model.place ?? string.Empty;
                newDistributor.CountryCode = model.countryCode ?? string.Empty;
                newDistributor.IsActive = true;
                newDistributor.Mobile = mobile ?? string.Empty;
                newDistributor.Password = password ?? string.Empty;
                newDistributor.Timestamp = currentTime;
                newDistributor.LastUpdated = currentTime;
                newDistributor.DistributorGuid = Guid.NewGuid();
                newDistributor.ProfileImage = string.Empty;
                newDistributor.Username = userName ?? string.Empty;
                newDistributor.Address = model.address ?? string.Empty;
                newDistributor.Place = model.place ?? string.Empty;
                newDistributor.ShopName = model.shopName ?? string.Empty;
                newDistributor.UserType = (int)ClassLibrary.Enum.userType.Distributor;
                newDistributor.Wallet = Convert.ToDecimal(0);
                newDistributor.IsBlocked = false;
                newDistributor.Email = email ?? string.Empty;

                string currency = "AED";
                var Fromcurrency = _context.tb_AdminCurrencyRate.Where(z => z.FromCountryCode == newDistributor.CountryCode && z.IsActive).FirstOrDefault();
                if (Fromcurrency != null)
                {
                    currency = Fromcurrency.FromCurrency;
                }
                else
                {
                    var Tocurrency = _context.tb_AdminCurrencyRate.Where(z => z.ToCountryCode == newDistributor.CountryCode && z.IsActive).FirstOrDefault();
                    if (Tocurrency != null)
                    {
                        currency = Tocurrency.FromCurrency;
                    }
                }
                newDistributor.Currency = currency ?? string.Empty;
                _context.tDistributors.Add(newDistributor);
                if (_context.SaveChanges() > 0)
                {

                    if (model.permission_IndiaRegion)
                    {
                        var indiaPerm = _context.tDistributorPermissions.Create();
                        indiaPerm.DistributorId = newDistributor.DistributorID;
                        indiaPerm.Permissions = true;
                        indiaPerm.Timestamp = currentTime;
                        indiaPerm.Type = ClassLibrary.Enum.DistributorPermissions.India.ToString();
                        _context.tDistributorPermissions.Add(indiaPerm);
                    }

                    if (model.permission_International)
                    {
                        var internationalPerm = _context.tDistributorPermissions.Create();
                        internationalPerm.DistributorId = newDistributor.DistributorID;
                        internationalPerm.Permissions = true;
                        internationalPerm.Timestamp = currentTime;
                        internationalPerm.Type = ClassLibrary.Enum.DistributorPermissions.International.ToString();
                        _context.tDistributorPermissions.Add(internationalPerm);
                    }

                    if (model.permission_Uae)
                    {
                        var uaePerm = _context.tDistributorPermissions.Create();
                        uaePerm.DistributorId = newDistributor.DistributorID;
                        uaePerm.Permissions = true;
                        uaePerm.Timestamp = currentTime;
                        uaePerm.Type = ClassLibrary.Enum.DistributorPermissions.UAE.ToString();
                        _context.tDistributorPermissions.Add(uaePerm);
                    }


                    //Du Topup
                    var newEntry_Gulfbox_DuTopup = _context.tDistributorCommisionRates.Create();
                    newEntry_Gulfbox_DuTopup.DistributorId = newDistributor.DistributorID;
                    newEntry_Gulfbox_DuTopup.ApiProvider = (int)ClassLibrary.Enum.apiProvider.Gulfbox;
                    newEntry_Gulfbox_DuTopup.Commission = Convert.ToDouble(model.commision_DuTopup);
                    newEntry_Gulfbox_DuTopup.ProductType = (int)ClassLibrary.Enum.GulfboxProductTypes.Du;
                    newEntry_Gulfbox_DuTopup.IsActive = true;
                    newEntry_Gulfbox_DuTopup.Timestamp = currentTime;
                    _context.tDistributorCommisionRates.Add(newEntry_Gulfbox_DuTopup);

                    var newEntry_OnePrepay_DuTopup = _context.tDistributorCommisionRates.Create();
                    newEntry_OnePrepay_DuTopup.DistributorId = newDistributor.DistributorID;
                    newEntry_OnePrepay_DuTopup.ApiProvider = (int)ClassLibrary.Enum.apiProvider.OnePrepay;
                    newEntry_OnePrepay_DuTopup.Commission = Convert.ToDouble(model.commision_DuTopup);
                    newEntry_OnePrepay_DuTopup.ProductType = (int)ClassLibrary.Enum.GulfboxProductTypes.Du;
                    newEntry_OnePrepay_DuTopup.IsActive = true;
                    newEntry_OnePrepay_DuTopup.Timestamp = currentTime;
                    _context.tDistributorCommisionRates.Add(newEntry_OnePrepay_DuTopup);



                    //Du Voucher
                    var newEntry_Gulfbox_DuVoucher = _context.tDistributorCommisionRates.Create();
                    newEntry_Gulfbox_DuVoucher.DistributorId = newDistributor.DistributorID;
                    newEntry_Gulfbox_DuVoucher.ApiProvider = (int)ClassLibrary.Enum.apiProvider.Gulfbox;
                    newEntry_Gulfbox_DuVoucher.Commission = Convert.ToDouble(model.commision_DuVoucher);
                    newEntry_Gulfbox_DuVoucher.ProductType = (int)ClassLibrary.Enum.GulfboxProductTypes.DuVoucher;
                    newEntry_Gulfbox_DuVoucher.IsActive = true;
                    newEntry_Gulfbox_DuVoucher.Timestamp = currentTime;
                    _context.tDistributorCommisionRates.Add(newEntry_Gulfbox_DuVoucher);

                    var newEntry_OnePrepay_DuVoucher = _context.tDistributorCommisionRates.Create();
                    newEntry_OnePrepay_DuVoucher.DistributorId = newDistributor.DistributorID;
                    newEntry_OnePrepay_DuVoucher.ApiProvider = (int)ClassLibrary.Enum.apiProvider.OnePrepay;
                    newEntry_OnePrepay_DuVoucher.Commission = Convert.ToDouble(model.commision_DuVoucher);
                    newEntry_OnePrepay_DuVoucher.ProductType = (int)ClassLibrary.Enum.GulfboxProductTypes.DuVoucher;
                    newEntry_OnePrepay_DuVoucher.IsActive = true;
                    newEntry_OnePrepay_DuVoucher.Timestamp = currentTime;
                    _context.tDistributorCommisionRates.Add(newEntry_OnePrepay_DuVoucher);



                    //Etisalat
                    var newEntry_Gulfbox_Etisalat = _context.tDistributorCommisionRates.Create();
                    newEntry_Gulfbox_Etisalat.DistributorId = newDistributor.DistributorID;
                    newEntry_Gulfbox_Etisalat.ApiProvider = (int)ClassLibrary.Enum.apiProvider.Gulfbox;
                    newEntry_Gulfbox_Etisalat.Commission = Convert.ToDouble(model.commision_EtisalatTopup);
                    newEntry_Gulfbox_Etisalat.ProductType = (int)ClassLibrary.Enum.GulfboxProductTypes.Etisalat;
                    newEntry_Gulfbox_Etisalat.IsActive = true;
                    newEntry_Gulfbox_Etisalat.Timestamp = currentTime;
                    _context.tDistributorCommisionRates.Add(newEntry_Gulfbox_Etisalat);

                    var newEntry_OnePrepay_Etisalat = _context.tDistributorCommisionRates.Create();
                    newEntry_OnePrepay_Etisalat.DistributorId = newDistributor.DistributorID;
                    newEntry_OnePrepay_Etisalat.ApiProvider = (int)ClassLibrary.Enum.apiProvider.OnePrepay;
                    newEntry_OnePrepay_Etisalat.Commission = Convert.ToDouble(model.commision_EtisalatTopup);
                    newEntry_OnePrepay_Etisalat.ProductType = (int)ClassLibrary.Enum.GulfboxProductTypes.Etisalat;
                    newEntry_OnePrepay_Etisalat.IsActive = true;
                    newEntry_OnePrepay_Etisalat.Timestamp = currentTime;
                    _context.tDistributorCommisionRates.Add(newEntry_OnePrepay_Etisalat);

                    //Etisalat Voucher
                    var newEntry_Gulfbox_EtisalatVoucher = _context.tDistributorCommisionRates.Create();
                    newEntry_Gulfbox_EtisalatVoucher.DistributorId = newDistributor.DistributorID;
                    newEntry_Gulfbox_EtisalatVoucher.ApiProvider = (int)ClassLibrary.Enum.apiProvider.Gulfbox;
                    newEntry_Gulfbox_EtisalatVoucher.Commission = Convert.ToDouble(model.commision_EtisalatVoucher);
                    newEntry_Gulfbox_EtisalatVoucher.ProductType = (int)ClassLibrary.Enum.GulfboxProductTypes.EtisalatVoucher;
                    newEntry_Gulfbox_EtisalatVoucher.IsActive = true;
                    newEntry_Gulfbox_EtisalatVoucher.Timestamp = currentTime;
                    _context.tDistributorCommisionRates.Add(newEntry_Gulfbox_EtisalatVoucher);

                    var newEntry_OnePrepay_EtisalatVoucher = _context.tDistributorCommisionRates.Create();
                    newEntry_OnePrepay_EtisalatVoucher.DistributorId = newDistributor.DistributorID;
                    newEntry_OnePrepay_EtisalatVoucher.ApiProvider = (int)ClassLibrary.Enum.apiProvider.OnePrepay;
                    newEntry_OnePrepay_EtisalatVoucher.Commission = Convert.ToDouble(model.commision_EtisalatVoucher);
                    newEntry_OnePrepay_EtisalatVoucher.ProductType = (int)ClassLibrary.Enum.GulfboxProductTypes.EtisalatVoucher;
                    newEntry_OnePrepay_EtisalatVoucher.IsActive = true;
                    newEntry_OnePrepay_EtisalatVoucher.Timestamp = currentTime;
                    _context.tDistributorCommisionRates.Add(newEntry_OnePrepay_EtisalatVoucher);

                    //Five
                    var newEntry_Gulfbox_Five = _context.tDistributorCommisionRates.Create();
                    newEntry_Gulfbox_Five.DistributorId = newDistributor.DistributorID;
                    newEntry_Gulfbox_Five.ApiProvider = (int)ClassLibrary.Enum.apiProvider.Gulfbox;
                    newEntry_Gulfbox_Five.Commission = Convert.ToDouble(model.commision_Five);
                    newEntry_Gulfbox_Five.ProductType = (int)ClassLibrary.Enum.GulfboxProductTypes.Five;
                    newEntry_Gulfbox_Five.IsActive = true;
                    newEntry_Gulfbox_Five.Timestamp = currentTime;
                    _context.tDistributorCommisionRates.Add(newEntry_Gulfbox_Five);

                    var newEntry_OnePrepay_Five = _context.tDistributorCommisionRates.Create();
                    newEntry_OnePrepay_Five.DistributorId = newDistributor.DistributorID;
                    newEntry_OnePrepay_Five.ApiProvider = (int)ClassLibrary.Enum.apiProvider.OnePrepay;
                    newEntry_OnePrepay_Five.Commission = Convert.ToDouble(model.commision_Five);
                    newEntry_OnePrepay_Five.ProductType = (int)ClassLibrary.Enum.GulfboxProductTypes.Five;
                    newEntry_OnePrepay_Five.IsActive = true;
                    newEntry_OnePrepay_Five.Timestamp = currentTime;
                    _context.tDistributorCommisionRates.Add(newEntry_OnePrepay_Five);

                    //Hello

                    var newEntry_Gulfbox_Hello = _context.tDistributorCommisionRates.Create();
                    newEntry_Gulfbox_Hello.DistributorId = newDistributor.DistributorID;
                    newEntry_Gulfbox_Hello.ApiProvider = (int)ClassLibrary.Enum.apiProvider.Gulfbox;
                    newEntry_Gulfbox_Hello.Commission = Convert.ToDouble(model.commision_Hello);
                    newEntry_Gulfbox_Hello.ProductType = (int)ClassLibrary.Enum.GulfboxProductTypes.Hello;
                    newEntry_Gulfbox_Hello.IsActive = true;
                    newEntry_Gulfbox_Hello.Timestamp = currentTime;
                    _context.tDistributorCommisionRates.Add(newEntry_Gulfbox_Hello);

                    var newEntry_OnePrepay_Hello = _context.tDistributorCommisionRates.Create();
                    newEntry_OnePrepay_Hello.DistributorId = newDistributor.DistributorID;
                    newEntry_OnePrepay_Hello.ApiProvider = (int)ClassLibrary.Enum.apiProvider.OnePrepay;
                    newEntry_OnePrepay_Hello.Commission = Convert.ToDouble(model.commision_Hello);
                    newEntry_OnePrepay_Hello.ProductType = (int)ClassLibrary.Enum.GulfboxProductTypes.Hello;
                    newEntry_OnePrepay_Hello.IsActive = true;
                    newEntry_OnePrepay_Hello.Timestamp = currentTime;
                    _context.tDistributorCommisionRates.Add(newEntry_OnePrepay_Hello);

                    //salik

                    var newEntry_Gulfbox_salik = _context.tDistributorCommisionRates.Create();
                    newEntry_Gulfbox_salik.DistributorId = newDistributor.DistributorID;
                    newEntry_Gulfbox_salik.ApiProvider = (int)ClassLibrary.Enum.apiProvider.Gulfbox;
                    newEntry_Gulfbox_salik.Commission = Convert.ToDouble(model.commision_Salik);
                    newEntry_Gulfbox_salik.ProductType = (int)ClassLibrary.Enum.GulfboxProductTypes.Salik;
                    newEntry_Gulfbox_salik.IsActive = true;
                    newEntry_Gulfbox_salik.Timestamp = currentTime;
                    _context.tDistributorCommisionRates.Add(newEntry_Gulfbox_salik);

                    var newEntry_OnePrepay_salik = _context.tDistributorCommisionRates.Create();
                    newEntry_OnePrepay_salik.DistributorId = newDistributor.DistributorID;
                    newEntry_OnePrepay_salik.ApiProvider = (int)ClassLibrary.Enum.apiProvider.OnePrepay;
                    newEntry_OnePrepay_salik.Commission = Convert.ToDouble(model.commision_Salik);
                    newEntry_OnePrepay_salik.ProductType = (int)ClassLibrary.Enum.GulfboxProductTypes.Salik;
                    newEntry_OnePrepay_salik.IsActive = true;
                    newEntry_OnePrepay_salik.Timestamp = currentTime;
                    _context.tDistributorCommisionRates.Add(newEntry_OnePrepay_salik);



                    //India
                    var newEntry_Jolo_India = _context.tDistributorCommisionRates.Create();
                    newEntry_Jolo_India.DistributorId = newDistributor.DistributorID;
                    newEntry_Jolo_India.ApiProvider = (int)ClassLibrary.Enum.apiProvider.Jolo;
                    newEntry_Jolo_India.Commission = Convert.ToDouble(model.commision_India);
                    newEntry_Jolo_India.ProductType = (int)ClassLibrary.Enum.GulfboxProductTypes.NoProduct;
                    newEntry_Jolo_India.IsActive = true;
                    newEntry_Jolo_India.Timestamp = currentTime;
                    _context.tDistributorCommisionRates.Add(newEntry_Jolo_India);

                    var newEntry_KeralaRecharge_India = _context.tDistributorCommisionRates.Create();
                    newEntry_KeralaRecharge_India.DistributorId = newDistributor.DistributorID;
                    newEntry_KeralaRecharge_India.ApiProvider = (int)ClassLibrary.Enum.apiProvider.KeralaRecharge;
                    newEntry_KeralaRecharge_India.Commission = Convert.ToDouble(model.commision_India);
                    newEntry_KeralaRecharge_India.ProductType = (int)ClassLibrary.Enum.GulfboxProductTypes.NoProduct;
                    newEntry_KeralaRecharge_India.IsActive = true;
                    newEntry_KeralaRecharge_India.Timestamp = currentTime;
                    _context.tDistributorCommisionRates.Add(newEntry_KeralaRecharge_India);



                    var newEntry_Pay2All_India = _context.tDistributorCommisionRates.Create();
                    newEntry_Pay2All_India.DistributorId = newDistributor.DistributorID;
                    newEntry_Pay2All_India.ApiProvider = (int)ClassLibrary.Enum.apiProvider.Pay2All;
                    newEntry_Pay2All_India.Commission = Convert.ToDouble(model.commision_India);
                    newEntry_Pay2All_India.ProductType = (int)ClassLibrary.Enum.GulfboxProductTypes.NoProduct;
                    newEntry_Pay2All_India.IsActive = true;
                    newEntry_Pay2All_India.Timestamp = currentTime;
                    _context.tDistributorCommisionRates.Add(newEntry_Pay2All_India);




                    //Other country
                    var newEntry_Gulfbox_Othercountry = _context.tDistributorCommisionRates.Create();
                    newEntry_Gulfbox_Othercountry.DistributorId = newDistributor.DistributorID;
                    newEntry_Gulfbox_Othercountry.ApiProvider = (int)ClassLibrary.Enum.apiProvider.Gulfbox;
                    newEntry_Gulfbox_Othercountry.Commission = Convert.ToDouble(model.commision_OtherCountry);
                    newEntry_Gulfbox_Othercountry.ProductType = (int)ClassLibrary.Enum.GulfboxProductTypes.Intl;
                    newEntry_Gulfbox_Othercountry.IsActive = true;
                    newEntry_Gulfbox_Othercountry.Timestamp = currentTime;
                    _context.tDistributorCommisionRates.Add(newEntry_Gulfbox_Othercountry);

                    var newEntry_Ding_Othercountry = _context.tDistributorCommisionRates.Create();
                    newEntry_Ding_Othercountry.DistributorId = newDistributor.DistributorID;
                    newEntry_Ding_Othercountry.ApiProvider = (int)ClassLibrary.Enum.apiProvider.Ding;
                    newEntry_Ding_Othercountry.Commission = Convert.ToDouble(model.commision_OtherCountry);
                    newEntry_Ding_Othercountry.ProductType = (int)ClassLibrary.Enum.GulfboxProductTypes.Intl;
                    newEntry_Ding_Othercountry.IsActive = true;
                    newEntry_Ding_Othercountry.Timestamp = currentTime;
                    _context.tDistributorCommisionRates.Add(newEntry_Ding_Othercountry);



                    //International
                    var newEntry_Ding_Intl = _context.tDistributorCommisionRates.Create();
                    newEntry_Ding_Intl.DistributorId = newDistributor.DistributorID;
                    newEntry_Ding_Intl.ApiProvider = (int)ClassLibrary.Enum.apiProvider.Ding;
                    newEntry_Ding_Intl.Commission = Convert.ToDouble(model.commision_International);
                    newEntry_Ding_Intl.ProductType = (int)ClassLibrary.Enum.GulfboxProductTypes.Intl;
                    newEntry_Ding_Intl.IsActive = true;
                    newEntry_Ding_Intl.Timestamp = currentTime;
                    _context.tDistributorCommisionRates.Add(newEntry_Ding_Intl);




                    _context.SaveChanges();
                    status = true;
                    message = "Distributor added";
                }
            }
            return new Tuple<bool, string>(status, message);
        }


        public Tuple<bool, string> editDistributor(ClassLibrary.WebPostModel.EditDistributor model)
        {
            bool status = false;
            string message = "Failed";

            try
            {
                var distributor = _context.tDistributors.FirstOrDefault(z => z.DistributorID == model.distributorId);
                if (distributor != null)
                {
                    distributor.Country = model.place ?? string.Empty;
                    distributor.Address = model.address ?? string.Empty;
                    distributor.Place = model.place ?? string.Empty;
                    distributor.ShopName = model.shopName ?? string.Empty;
                    distributor.LastUpdated = currentTime;

                    foreach (var item in distributor.tDistributorPermissions.ToList())
                    {
                        _context.tDistributorPermissions.Remove(item);
                    }

                    foreach (var item in distributor.tDistributorCommisionRates.ToList())
                    {
                        _context.tDistributorCommisionRates.Remove(item);
                    }



                    if (model.permission_IndiaRegion)
                    {
                        var indiaPerm = _context.tDistributorPermissions.Create();
                        indiaPerm.DistributorId = distributor.DistributorID;
                        indiaPerm.Permissions = true;
                        indiaPerm.Timestamp = currentTime;
                        indiaPerm.Type = ClassLibrary.Enum.DistributorPermissions.India.ToString();
                        _context.tDistributorPermissions.Add(indiaPerm);
                    }

                    if (model.permission_International)
                    {
                        var internationalPerm = _context.tDistributorPermissions.Create();
                        internationalPerm.DistributorId = distributor.DistributorID;
                        internationalPerm.Permissions = true;
                        internationalPerm.Timestamp = currentTime;
                        internationalPerm.Type = ClassLibrary.Enum.DistributorPermissions.International.ToString();
                        _context.tDistributorPermissions.Add(internationalPerm);
                    }

                    if (model.permission_Uae)
                    {
                        var uaePerm = _context.tDistributorPermissions.Create();
                        uaePerm.DistributorId = distributor.DistributorID;
                        uaePerm.Permissions = true;
                        uaePerm.Timestamp = currentTime;
                        uaePerm.Type = ClassLibrary.Enum.DistributorPermissions.UAE.ToString();
                        _context.tDistributorPermissions.Add(uaePerm);
                    }


                    //Du Topup
                    var newEntry_Gulfbox_DuTopup = _context.tDistributorCommisionRates.Create();
                    newEntry_Gulfbox_DuTopup.DistributorId = distributor.DistributorID;
                    newEntry_Gulfbox_DuTopup.ApiProvider = (int)ClassLibrary.Enum.apiProvider.Gulfbox;
                    newEntry_Gulfbox_DuTopup.Commission = Convert.ToDouble(model.commision_DuTopup);
                    newEntry_Gulfbox_DuTopup.ProductType = (int)ClassLibrary.Enum.GulfboxProductTypes.Du;
                    newEntry_Gulfbox_DuTopup.IsActive = true;
                    newEntry_Gulfbox_DuTopup.Timestamp = currentTime;
                    _context.tDistributorCommisionRates.Add(newEntry_Gulfbox_DuTopup);

                    var newEntry_OnePrepay_DuTopup = _context.tDistributorCommisionRates.Create();
                    newEntry_OnePrepay_DuTopup.DistributorId = distributor.DistributorID;
                    newEntry_OnePrepay_DuTopup.ApiProvider = (int)ClassLibrary.Enum.apiProvider.OnePrepay;
                    newEntry_OnePrepay_DuTopup.Commission = Convert.ToDouble(model.commision_DuTopup);
                    newEntry_OnePrepay_DuTopup.ProductType = (int)ClassLibrary.Enum.GulfboxProductTypes.Du;
                    newEntry_OnePrepay_DuTopup.IsActive = true;
                    newEntry_OnePrepay_DuTopup.Timestamp = currentTime;
                    _context.tDistributorCommisionRates.Add(newEntry_OnePrepay_DuTopup);



                    //Du Voucher
                    var newEntry_Gulfbox_DuVoucher = _context.tDistributorCommisionRates.Create();
                    newEntry_Gulfbox_DuVoucher.DistributorId = distributor.DistributorID;
                    newEntry_Gulfbox_DuVoucher.ApiProvider = (int)ClassLibrary.Enum.apiProvider.Gulfbox;
                    newEntry_Gulfbox_DuVoucher.Commission = Convert.ToDouble(model.commision_DuVoucher);
                    newEntry_Gulfbox_DuVoucher.ProductType = (int)ClassLibrary.Enum.GulfboxProductTypes.DuVoucher;
                    newEntry_Gulfbox_DuVoucher.IsActive = true;
                    newEntry_Gulfbox_DuVoucher.Timestamp = currentTime;
                    _context.tDistributorCommisionRates.Add(newEntry_Gulfbox_DuVoucher);

                    var newEntry_OnePrepay_DuVoucher = _context.tDistributorCommisionRates.Create();
                    newEntry_OnePrepay_DuVoucher.DistributorId = distributor.DistributorID;
                    newEntry_OnePrepay_DuVoucher.ApiProvider = (int)ClassLibrary.Enum.apiProvider.OnePrepay;
                    newEntry_OnePrepay_DuVoucher.Commission = Convert.ToDouble(model.commision_DuVoucher);
                    newEntry_OnePrepay_DuVoucher.ProductType = (int)ClassLibrary.Enum.GulfboxProductTypes.DuVoucher;
                    newEntry_OnePrepay_DuVoucher.IsActive = true;
                    newEntry_OnePrepay_DuVoucher.Timestamp = currentTime;
                    _context.tDistributorCommisionRates.Add(newEntry_OnePrepay_DuVoucher);



                    //Etisalat
                    var newEntry_Gulfbox_Etisalat = _context.tDistributorCommisionRates.Create();
                    newEntry_Gulfbox_Etisalat.DistributorId = distributor.DistributorID;
                    newEntry_Gulfbox_Etisalat.ApiProvider = (int)ClassLibrary.Enum.apiProvider.Gulfbox;
                    newEntry_Gulfbox_Etisalat.Commission = Convert.ToDouble(model.commision_EtisalatTopup);
                    newEntry_Gulfbox_Etisalat.ProductType = (int)ClassLibrary.Enum.GulfboxProductTypes.Etisalat;
                    newEntry_Gulfbox_Etisalat.IsActive = true;
                    newEntry_Gulfbox_Etisalat.Timestamp = currentTime;
                    _context.tDistributorCommisionRates.Add(newEntry_Gulfbox_Etisalat);

                    var newEntry_OnePrepay_Etisalat = _context.tDistributorCommisionRates.Create();
                    newEntry_OnePrepay_Etisalat.DistributorId = distributor.DistributorID;
                    newEntry_OnePrepay_Etisalat.ApiProvider = (int)ClassLibrary.Enum.apiProvider.OnePrepay;
                    newEntry_OnePrepay_Etisalat.Commission = Convert.ToDouble(model.commision_EtisalatTopup);
                    newEntry_OnePrepay_Etisalat.ProductType = (int)ClassLibrary.Enum.GulfboxProductTypes.Etisalat;
                    newEntry_OnePrepay_Etisalat.IsActive = true;
                    newEntry_OnePrepay_Etisalat.Timestamp = currentTime;
                    _context.tDistributorCommisionRates.Add(newEntry_OnePrepay_Etisalat);

                    //Etisalat Voucher
                    var newEntry_Gulfbox_EtisalatVoucher = _context.tDistributorCommisionRates.Create();
                    newEntry_Gulfbox_EtisalatVoucher.DistributorId = distributor.DistributorID;
                    newEntry_Gulfbox_EtisalatVoucher.ApiProvider = (int)ClassLibrary.Enum.apiProvider.Gulfbox;
                    newEntry_Gulfbox_EtisalatVoucher.Commission = Convert.ToDouble(model.commision_EtisalatVoucher);
                    newEntry_Gulfbox_EtisalatVoucher.ProductType = (int)ClassLibrary.Enum.GulfboxProductTypes.EtisalatVoucher;
                    newEntry_Gulfbox_EtisalatVoucher.IsActive = true;
                    newEntry_Gulfbox_EtisalatVoucher.Timestamp = currentTime;
                    _context.tDistributorCommisionRates.Add(newEntry_Gulfbox_EtisalatVoucher);

                    var newEntry_OnePrepay_EtisalatVoucher = _context.tDistributorCommisionRates.Create();
                    newEntry_OnePrepay_EtisalatVoucher.DistributorId = distributor.DistributorID;
                    newEntry_OnePrepay_EtisalatVoucher.ApiProvider = (int)ClassLibrary.Enum.apiProvider.OnePrepay;
                    newEntry_OnePrepay_EtisalatVoucher.Commission = Convert.ToDouble(model.commision_EtisalatVoucher);
                    newEntry_OnePrepay_EtisalatVoucher.ProductType = (int)ClassLibrary.Enum.GulfboxProductTypes.EtisalatVoucher;
                    newEntry_OnePrepay_EtisalatVoucher.IsActive = true;
                    newEntry_OnePrepay_EtisalatVoucher.Timestamp = currentTime;
                    _context.tDistributorCommisionRates.Add(newEntry_OnePrepay_EtisalatVoucher);

                    //Five
                    var newEntry_Gulfbox_Five = _context.tDistributorCommisionRates.Create();
                    newEntry_Gulfbox_Five.DistributorId = distributor.DistributorID;
                    newEntry_Gulfbox_Five.ApiProvider = (int)ClassLibrary.Enum.apiProvider.Gulfbox;
                    newEntry_Gulfbox_Five.Commission = Convert.ToDouble(model.commision_Five);
                    newEntry_Gulfbox_Five.ProductType = (int)ClassLibrary.Enum.GulfboxProductTypes.Five;
                    newEntry_Gulfbox_Five.IsActive = true;
                    newEntry_Gulfbox_Five.Timestamp = currentTime;
                    _context.tDistributorCommisionRates.Add(newEntry_Gulfbox_Five);

                    var newEntry_OnePrepay_Five = _context.tDistributorCommisionRates.Create();
                    newEntry_OnePrepay_Five.DistributorId = distributor.DistributorID;
                    newEntry_OnePrepay_Five.ApiProvider = (int)ClassLibrary.Enum.apiProvider.OnePrepay;
                    newEntry_OnePrepay_Five.Commission = Convert.ToDouble(model.commision_Five);
                    newEntry_OnePrepay_Five.ProductType = (int)ClassLibrary.Enum.GulfboxProductTypes.Five;
                    newEntry_OnePrepay_Five.IsActive = true;
                    newEntry_OnePrepay_Five.Timestamp = currentTime;
                    _context.tDistributorCommisionRates.Add(newEntry_OnePrepay_Five);

                    //Hello

                    var newEntry_Gulfbox_Hello = _context.tDistributorCommisionRates.Create();
                    newEntry_Gulfbox_Hello.DistributorId = distributor.DistributorID;
                    newEntry_Gulfbox_Hello.ApiProvider = (int)ClassLibrary.Enum.apiProvider.Gulfbox;
                    newEntry_Gulfbox_Hello.Commission = Convert.ToDouble(model.commision_Hello);
                    newEntry_Gulfbox_Hello.ProductType = (int)ClassLibrary.Enum.GulfboxProductTypes.Hello;
                    newEntry_Gulfbox_Hello.IsActive = true;
                    newEntry_Gulfbox_Hello.Timestamp = currentTime;
                    _context.tDistributorCommisionRates.Add(newEntry_Gulfbox_Hello);

                    var newEntry_OnePrepay_Hello = _context.tDistributorCommisionRates.Create();
                    newEntry_OnePrepay_Hello.DistributorId = distributor.DistributorID;
                    newEntry_OnePrepay_Hello.ApiProvider = (int)ClassLibrary.Enum.apiProvider.OnePrepay;
                    newEntry_OnePrepay_Hello.Commission = Convert.ToDouble(model.commision_Hello);
                    newEntry_OnePrepay_Hello.ProductType = (int)ClassLibrary.Enum.GulfboxProductTypes.Hello;
                    newEntry_OnePrepay_Hello.IsActive = true;
                    newEntry_OnePrepay_Hello.Timestamp = currentTime;
                    _context.tDistributorCommisionRates.Add(newEntry_OnePrepay_Hello);

                    //salik

                    var newEntry_Gulfbox_salik = _context.tDistributorCommisionRates.Create();
                    newEntry_Gulfbox_salik.DistributorId = distributor.DistributorID;
                    newEntry_Gulfbox_salik.ApiProvider = (int)ClassLibrary.Enum.apiProvider.Gulfbox;
                    newEntry_Gulfbox_salik.Commission = Convert.ToDouble(model.commision_Salik);
                    newEntry_Gulfbox_salik.ProductType = (int)ClassLibrary.Enum.GulfboxProductTypes.Salik;
                    newEntry_Gulfbox_salik.IsActive = true;
                    newEntry_Gulfbox_salik.Timestamp = currentTime;
                    _context.tDistributorCommisionRates.Add(newEntry_Gulfbox_salik);

                    var newEntry_OnePrepay_salik = _context.tDistributorCommisionRates.Create();
                    newEntry_OnePrepay_salik.DistributorId = distributor.DistributorID;
                    newEntry_OnePrepay_salik.ApiProvider = (int)ClassLibrary.Enum.apiProvider.OnePrepay;
                    newEntry_OnePrepay_salik.Commission = Convert.ToDouble(model.commision_Salik);
                    newEntry_OnePrepay_salik.ProductType = (int)ClassLibrary.Enum.GulfboxProductTypes.Salik;
                    newEntry_OnePrepay_salik.IsActive = true;
                    newEntry_OnePrepay_salik.Timestamp = currentTime;
                    _context.tDistributorCommisionRates.Add(newEntry_OnePrepay_salik);



                    //India
                    var newEntry_Jolo_India = _context.tDistributorCommisionRates.Create();
                    newEntry_Jolo_India.DistributorId = distributor.DistributorID;
                    newEntry_Jolo_India.ApiProvider = (int)ClassLibrary.Enum.apiProvider.Jolo;
                    newEntry_Jolo_India.Commission = Convert.ToDouble(model.commision_India);
                    newEntry_Jolo_India.ProductType = (int)ClassLibrary.Enum.GulfboxProductTypes.NoProduct;
                    newEntry_Jolo_India.IsActive = true;
                    newEntry_Jolo_India.Timestamp = currentTime;
                    _context.tDistributorCommisionRates.Add(newEntry_Jolo_India);

                    var newEntry_KeralaRecharge_India = _context.tDistributorCommisionRates.Create();
                    newEntry_KeralaRecharge_India.DistributorId = distributor.DistributorID;
                    newEntry_KeralaRecharge_India.ApiProvider = (int)ClassLibrary.Enum.apiProvider.KeralaRecharge;
                    newEntry_KeralaRecharge_India.Commission = Convert.ToDouble(model.commision_India);
                    newEntry_KeralaRecharge_India.ProductType = (int)ClassLibrary.Enum.GulfboxProductTypes.NoProduct;
                    newEntry_KeralaRecharge_India.IsActive = true;
                    newEntry_KeralaRecharge_India.Timestamp = currentTime;
                    _context.tDistributorCommisionRates.Add(newEntry_KeralaRecharge_India);



                    var newEntry_Pay2All_India = _context.tDistributorCommisionRates.Create();
                    newEntry_Pay2All_India.DistributorId = distributor.DistributorID;
                    newEntry_Pay2All_India.ApiProvider = (int)ClassLibrary.Enum.apiProvider.Pay2All;
                    newEntry_Pay2All_India.Commission = Convert.ToDouble(model.commision_India);
                    newEntry_Pay2All_India.ProductType = (int)ClassLibrary.Enum.GulfboxProductTypes.NoProduct;
                    newEntry_Pay2All_India.IsActive = true;
                    newEntry_Pay2All_India.Timestamp = currentTime;
                    _context.tDistributorCommisionRates.Add(newEntry_Pay2All_India);





                    //Other country
                    var newEntry_Gulfbox_Othercountry = _context.tDistributorCommisionRates.Create();
                    newEntry_Gulfbox_Othercountry.DistributorId = distributor.DistributorID;
                    newEntry_Gulfbox_Othercountry.ApiProvider = (int)ClassLibrary.Enum.apiProvider.Gulfbox;
                    newEntry_Gulfbox_Othercountry.Commission = Convert.ToDouble(model.commision_OtherCountry);
                    newEntry_Gulfbox_Othercountry.ProductType = (int)ClassLibrary.Enum.GulfboxProductTypes.Intl;
                    newEntry_Gulfbox_Othercountry.IsActive = true;
                    newEntry_Gulfbox_Othercountry.Timestamp = currentTime;
                    _context.tDistributorCommisionRates.Add(newEntry_Gulfbox_Othercountry);

                    var newEntry_Ding_Othercountry = _context.tDistributorCommisionRates.Create();
                    newEntry_Ding_Othercountry.DistributorId = distributor.DistributorID;
                    newEntry_Ding_Othercountry.ApiProvider = (int)ClassLibrary.Enum.apiProvider.Ding;
                    newEntry_Ding_Othercountry.Commission = Convert.ToDouble(model.commision_OtherCountry);
                    newEntry_Ding_Othercountry.ProductType = (int)ClassLibrary.Enum.GulfboxProductTypes.Intl;
                    newEntry_Ding_Othercountry.IsActive = true;
                    newEntry_Ding_Othercountry.Timestamp = currentTime;
                    _context.tDistributorCommisionRates.Add(newEntry_Ding_Othercountry);



                    //International
                    var newEntry_Ding_Intl = _context.tDistributorCommisionRates.Create();
                    newEntry_Ding_Intl.DistributorId = distributor.DistributorID;
                    newEntry_Ding_Intl.ApiProvider = (int)ClassLibrary.Enum.apiProvider.Ding;
                    newEntry_Ding_Intl.Commission = Convert.ToDouble(model.commision_International);
                    newEntry_Ding_Intl.ProductType = (int)ClassLibrary.Enum.GulfboxProductTypes.Intl;
                    newEntry_Ding_Intl.IsActive = true;
                    newEntry_Ding_Intl.Timestamp = currentTime;
                    _context.tDistributorCommisionRates.Add(newEntry_Ding_Intl);


                    status = _context.SaveChanges() > 0 ? true : false;
                    message = status == true ? "Distributor updated" : "Failed";


                }
                return new Tuple<bool, string>(status, message);
            }
            catch (Exception ex)
            {
                return new Tuple<bool, string>(false, ex.Message);
            }
        }

        public Tuple<bool, string> deactivateDistributor(string id)
        {
            var distributorId = Convert.ToInt64(id);
            var distributo = _context.tDistributors.FirstOrDefault(z => z.DistributorID == distributorId && z.IsActive);
            if (distributo != null)
            {
                distributo.IsActive = false;
                if (_context.SaveChanges() > 0)
                {
                    return new Tuple<bool, string>(true, "success");
                }
                else
                {
                    return new Tuple<bool, string>(false, "failed");
                }
            }
            else
            {
                return new Tuple<bool, string>(false, "failed");
            }
        }

        public Tuple<bool, string> AddDistributorCredit(ClassLibrary.WebPostModel.DistributorCredit model)
        {
            bool status = false;
            string message = "Failed";

            try
            {
                var amount = Convert.ToDecimal(model.amount);
                var distributor = _context.tDistributors.FirstOrDefault(z => z.DistributorID == model.distributorId);
                if (distributor != null)
                {
                    distributor.Wallet = distributor.Wallet + amount;
                    if (_context.SaveChanges() > 0)
                    {
                        var res = _context.SP_UPDATE_DISTRIBUTOR_CREDITHISTORY(model.distributorId, amount, "Add");
                        status = true;
                        message = "Success";
                    }
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return new Tuple<bool, string>(status, message);
        }


        public Tuple<bool, string, string> RefreshCreditSum(ClassLibrary.WebPostModel.RefreshCreditSum model)
        {
            bool status = false;
            string message = "Failed";
            decimal value = 0;
            try
            {
                value = Convert.ToDecimal(_context.SP_DISTRIBUTOR_CREDIT_SUM(model.distributorId, model.fromdate ?? string.Empty, model.todate ?? string.Empty).FirstOrDefault());
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return new Tuple<bool, string, string>(status, message, Convert.ToString(value));
        }

        public Tuple<bool, string, tDistributor> Distributorlogin(ClassLibrary.WebPostModel.LoginModel model)
        {
            string password = StringCipher.Encrypt(model.password, salt);
            string mobile = StringCipher.Encrypt(model.mobileNumber, salt);
            var checkDB = _context.tDistributors.FirstOrDefault(z => z.Mobile == mobile && z.Password == password && z.IsActive);
            if (checkDB != null)
            {
                FormsAuthentication.SetAuthCookie(checkDB.DistributorID.ToString(), true);
                return new Tuple<bool, string, tDistributor>(true, "success", checkDB);
            }
            else
            {
                return new Tuple<bool, string, tDistributor>(false, "invalid email / password", null);
            }
        }

        public List<Distributor> DistributorLists()
        {
            List<Distributor> AddDistributorList = new List<Distributor>();             
            var results = _context.tDistributors.ToList();
            foreach(var a1 in results){
                Distributor AddDistributor = new Distributor();
                AddDistributor.DistributorID = a1.DistributorID;
                AddDistributor.Username = StringCipher.Decrypt(a1.Username, salt);
                AddDistributorList.Add(AddDistributor);
            }
            return AddDistributorList;
        }
        public List<Reseller> ResellerLists(long id)
        {
            List<Reseller> ResellerList = new List<Reseller>();
            var results = _context.tResellers.Where(z => z.DistributorId == id).ToList();
            foreach (var a1 in results)
            {
                Reseller Reseller = new Reseller();
                Reseller.ResellerId = a1.ResellerId;
                Reseller.Username = StringCipher.Decrypt(a1.Username, salt);
                ResellerList.Add(Reseller);
            }
            return ResellerList;
        }

        public List<Agent> AgentLists(long id)
        {
            List<Agent> AgentList = new List<Agent>();
            var results = _context.tAgents.Where(z => z.ResellerId == id).ToList();
            foreach (var a1 in results)
            {
                Agent Agent = new Agent();
                Agent.AgentID = a1.ResellerId;
                Agent.Username = StringCipher.Decrypt(a1.Username, salt);
                AgentList.Add(Agent);
            }
            return AgentList;
        }

    }
}
