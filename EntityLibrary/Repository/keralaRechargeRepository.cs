﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace EntityLibrary.Repository
{
    public class keralaRechargeRepository : BaseReference
    {
        public object findOperatorandCircle(ClassLibrary.ServicePostModel.KeralaRechargePostModel.FindOperatorAndCircle model)
        {
            var returnModel = new ClassLibrary.ServicePostModel.KeralaRechargePostModel.FindOperatorAndCircleReturnModel();
            dynamic data = new System.Dynamic.ExpandoObject();
            try
            {
                var requestString = baseurl_keralaRecharge + "api_key=" + apikey_KeralaRecharge + "&module=mnp&number=" + model.mobileNumber + "&format=1";
                var request = (HttpWebRequest)WebRequest.Create(requestString);
                var response = (HttpWebResponse)request.GetResponse();
                var responseString = new System.IO.StreamReader(response.GetResponseStream()).ReadToEnd();
                data = Newtonsoft.Json.JsonConvert.DeserializeObject(responseString);
                returnModel.status = true;
                returnModel.message = "Success";
                returnModel.data = data;
            }
            catch (Exception ex)
            {
                returnModel.status = false;
                returnModel.message = "An error has occured";
            }
            return returnModel;
        }

        public object recharge(ClassLibrary.ServicePostModel.KeralaRechargePostModel.Recharge model)
        {
            var returnModel = new ClassLibrary.ServicePostModel.KeralaRechargePostModel.RechargeResponse();
            dynamic data = new System.Dynamic.ExpandoObject();
            dynamic data1 = new System.Dynamic.ExpandoObject();
            try
            {

                var user = _context.tb_User.FirstOrDefault(z => z.UserID == model.userId && z.IsActive);
                if (user != null)
                {


                    try
                    {
                        var rowIp = _context.tb_UserTransactionIp.Create();
                        rowIp.NetworkIp = model.ip;
                        rowIp.TimeStamp = System.DateTime.UtcNow;
                        rowIp.UserId = user.UserID;
                        _context.tb_UserTransactionIp.Add(rowIp);
                        _context.SaveChanges();
                    }
                    catch (Exception ex)
                    { }


                    var currency = _context.tb_AdminCurrencyRate.Where(z => z.ToCurrency == user.Currency && z.FromCurrency == model.amountCurrency).FirstOrDefault();
                    if (currency != null)
                    {
                        //var convertedAmount = Convert.ToDecimal(Convert.ToDouble(model.amount));
                        //if (user.Currency != "INR")
                        //{
                        //    convertedAmount = Convert.ToDecimal(Convert.ToDouble(model.amount) * currency.CurrencyRate);
                        //}


                        var newAmount = user.Wallet;
                        var convertedAmount = Convert.ToDecimal(Convert.ToDouble(model.amount) * currency.CurrencyRate);
                        var deductamt = convertedAmount;
                        decimal commissionAmount = Convert.ToDecimal(0);
                        if (user.UserType == (int)ClassLibrary.Enum.userType.Agent)
                        {
                            var commision = _context.tb_AgentCommisionRate.Where(z => z.AgentId == user.UserID && z.IsActive && z.ProductType == model.productType && z.ApiProvider == (int)ClassLibrary.Enum.apiProvider.KeralaRecharge).FirstOrDefault();
                            if (commision != null)
                            {
                                commissionAmount = ((Convert.ToDecimal(convertedAmount) * Convert.ToDecimal(commision.Commission)) / 100);
                                deductamt = Convert.ToDecimal(convertedAmount) - commissionAmount;
                                newAmount = user.Wallet - Convert.ToDecimal(deductamt);
                            }
                            else
                            {
                                newAmount = user.Wallet - Convert.ToDecimal(convertedAmount); ;
                            }
                        }
                        else
                        {
                            newAmount = user.Wallet - Convert.ToDecimal(convertedAmount);
                        }


                        if (user.Wallet >= deductamt && deductamt >= Convert.ToDecimal(0) && user.Wallet > Convert.ToDecimal(0))
                        {
                            var randomString = RandomKeralaRechargeRequestString(30);
                            var requestString = baseurl_keralaRecharge;
                            if (model.type == 1 || model.type == 2)//prepaid and postpaid mobile
                            {
                                requestString = baseurl_keralaRecharge + "api_key=" + apikey_KeralaRecharge + "&module=recharge&rid=" + randomString + "&type=" + model.type + "&number=" + model.mobileNumber + "&amount=" + model.amount + "&operator=" + model.Operator + "&circle=" + model.circle + "&format=1";
                            }
                            else
                            {
                                requestString = baseurl_keralaRecharge + "api_key=" + apikey_KeralaRecharge + "&module=recharge&rid=" + randomString + "&type=" + model.type + "&number=" + model.mobileNumber + "&amount=" + model.amount + "&operator=" + model.Operator + "&format=1";
                            }
                            var request = (HttpWebRequest)WebRequest.Create(requestString);
                            var response = (HttpWebResponse)request.GetResponse();
                            var responseString = new System.IO.StreamReader(response.GetResponseStream()).ReadToEnd();
                            data = Newtonsoft.Json.JsonConvert.DeserializeObject(responseString);

                            try
                            {
                                if (data != null)
                                {
                                    if (data.status != null)
                                    {
                                        if (data.status == "FAILED")
                                        {
                                            data1 = data;
                                            var transaction = _context.tb_KeralaRechargeTransaction.Create();
                                            transaction.Amount = Convert.ToString(model.amount);
                                            transaction.Ca = Convert.ToString(model.ca);
                                            transaction.Circle = Convert.ToString(model.circle);
                                            transaction.Message = data.message;
                                            transaction.Number = model.mobileNumber;
                                            transaction.Operator = Convert.ToString(model.Operator);
                                            transaction.OperatorRef = "";
                                            transaction.Rid = "";
                                            transaction.Status = data.status;
                                            transaction.Std = Convert.ToString(model.std);
                                            transaction.Timestamp = currentTime;
                                            transaction.Txnid = "";
                                            transaction.Type = model.type;
                                            transaction.UniqueIdentifier = Guid.NewGuid();
                                            transaction.UserId = model.userId;
                                            transaction.AmountCurrency = model.amountCurrency;
                                            transaction.AmountDeductedCurrency = user.Currency;
                                            transaction.AmountDeducted = String.Format("{0:0.00}", deductamt);
                                            transaction.Commission = commissionAmount;
                                            _context.tb_KeralaRechargeTransaction.Add(transaction);
                                        }
                                        else
                                        {
                                            data1 = data;
                                            var transaction = _context.tb_KeralaRechargeTransaction.Create();
                                            if (data.status == "PENDING")
                                                data1 = rechargeStatusUpdateBYTransaction(Convert.ToString(data.txnid), Convert.ToString(data.rid));
                                            transaction.Amount = Convert.ToString(model.amount);
                                            transaction.Ca = Convert.ToString(model.ca);
                                            transaction.Circle = Convert.ToString(model.circle);
                                            transaction.Message = data1.message;
                                            transaction.Number = model.mobileNumber;
                                            transaction.Operator = Convert.ToString(model.Operator);
                                            transaction.OperatorRef = data1.operator_ref;
                                            transaction.Rid = Convert.ToString(data.rid);
                                            transaction.Status = data1.status;
                                            transaction.Std = Convert.ToString(model.std);
                                            transaction.Timestamp = currentTime;
                                            transaction.Txnid = data1.txnid;
                                            transaction.Type = model.type;
                                            transaction.UniqueIdentifier = Guid.NewGuid();
                                            transaction.UserId = model.userId;
                                            transaction.AmountCurrency = model.amountCurrency;
                                            transaction.AmountDeductedCurrency = user.Currency;
                                            transaction.AmountDeducted = String.Format("{0:0.00}", deductamt);
                                            transaction.Commission = commissionAmount;
                                            _context.tb_KeralaRechargeTransaction.Add(transaction);



                                            user.Wallet = newAmount;
                                            //user.Wallet = user.Wallet - convertedAmount;

                                        }

                                        if (_context.SaveChanges() > 0)
                                        {
                                            returnModel.status = true;
                                            returnModel.message = "Success";
                                            returnModel.data = data1;
                                            returnModel.wallet = user.Wallet.ToString();
                                        }
                                        else
                                        {
                                            data1 = data;
                                            returnModel.status = false;
                                            returnModel.message = "Failed";
                                            returnModel.data = data1;
                                            returnModel.wallet = user.Wallet.ToString();
                                        }
                                    }
                                    else
                                    {
                                        data1 = data;
                                        returnModel.status = true;
                                        returnModel.message = "Success";
                                        returnModel.data = data1;
                                        returnModel.wallet = user.Wallet.ToString();
                                    }
                                }
                                else
                                {
                                    data1 = data;
                                    returnModel.status = false;
                                    returnModel.message = "Failed";
                                    returnModel.data = data1;
                                    returnModel.wallet = user.Wallet.ToString();
                                }
                            }
                            catch (Exception ex)
                            {
                                data1 = data;
                                returnModel.status = false;
                                returnModel.message = "Failed";
                                returnModel.data = data1;
                                returnModel.wallet = user.Wallet.ToString();
                            }
                        }
                        else
                        {
                            data1 = data;
                            returnModel.status = false;
                            returnModel.message = "Insufficient Amount";
                            returnModel.data = data1;
                            returnModel.wallet = user.Wallet.ToString();
                            return returnModel;
                        }
                    }
                    else
                    {
                        data1 = data;
                        returnModel.status = false;
                        returnModel.message = "Failed";
                        returnModel.data = data1;
                        returnModel.wallet = user.Wallet.ToString();
                        return returnModel;
                    }
                }
                else
                {
                    data1 = data;
                    returnModel.status = false;
                    returnModel.message = "Failed";
                    returnModel.data = data1;
                    returnModel.wallet = user.Wallet.ToString();
                    return returnModel;
                }

            }
            catch (Exception ex)
            {
                data1 = data;
                returnModel.status = false;
                returnModel.message = "An error has occured";
                returnModel.data = data1;
            }
            return returnModel;
        }

        public object rechargePlans(ClassLibrary.ServicePostModel.KeralaRechargePostModel.RechargePlans model)
        {
            var returnModel = new ClassLibrary.ServicePostModel.KeralaRechargePostModel.RechargePlansResponse();
            dynamic data = new System.Dynamic.ExpandoObject();
            try
            {
                var requestString = baseurl_keralaRecharge + "api_key=" + apikey_KeralaRecharge + "&module=rechargeoffers&circle=" + model.circle + "&operator=" + model.Operator + "&format=1";
                var request = (HttpWebRequest)WebRequest.Create(requestString);
                var response = (HttpWebResponse)request.GetResponse();
                var responseString = new System.IO.StreamReader(response.GetResponseStream()).ReadToEnd();
                data = Newtonsoft.Json.JsonConvert.DeserializeObject(responseString);
                if (data != null)
                {
                    returnModel.status = true;
                    returnModel.message = "Success";
                }
                else
                {
                    returnModel.status = false;
                    returnModel.message = "Failed";
                }
                returnModel.data = data;
            }
            catch (Exception ex)
            {
                returnModel.status = false;
                returnModel.message = "An error has occured";
            }
            return returnModel;
        }

        public object rechargeStatusUpdate()
        {
            dynamic data = new System.Dynamic.ExpandoObject();

            var list = _context.tb_KeralaRechargeTransaction.Where(z => z.Status == "PENDING").ToList();
            if (list.Count > 0)
            {
                foreach (var item in list)
                {
                    var requestString = baseurl_keralaRecharge + "api_key=" + apikey_KeralaRecharge + "&module=status&rid=" + item.Rid + "&format=1";
                    var request = (HttpWebRequest)WebRequest.Create(requestString);
                    var response = (HttpWebResponse)request.GetResponse();
                    var responseString = new System.IO.StreamReader(response.GetResponseStream()).ReadToEnd();
                    data = Newtonsoft.Json.JsonConvert.DeserializeObject(responseString);

                    try
                    {
                        if (data != null)
                        {
                            if (data.status != item.Status)
                            {
                                if (data.status == "FAILED")
                                {
                                    var user = _context.tb_User.FirstOrDefault(z => z.UserID == item.UserId && z.IsActive);
                                    if (user != null)
                                    {
                                        //var currency = _context.tb_AdminCurrencyRate.Where(z => z.ToCurrency == user.Currency && z.FromCurrency == "INR").FirstOrDefault();
                                        //if (currency != null)
                                        //{
                                        //    //var convertedAmount = Convert.ToDecimal(Convert.ToDouble(item.Amount));
                                        //    //if (user.Currency != "INR")
                                        //    //{
                                        //    //    convertedAmount = Convert.ToDecimal(Convert.ToDouble(item.Amount) * currency.CurrencyRate);
                                        //    //}
                                        //    //user.Wallet = user.Wallet + convertedAmount;
                                        //}
                                        user.Wallet = user.Wallet + Convert.ToDecimal(String.Format("{0:0.00}", Convert.ToDouble(item.AmountDeducted)));
                                    }
                                }
                                if (data.error == null || data.error == string.Empty)
                                {
                                    item.Status = data.status;
                                    item.Message = data.message;
                                }
                            }
                            _context.SaveChanges();
                        }
                    }
                    catch (Exception ex)
                    {
                    }
                }
            }
            return true;
        }

        public object ksebBillPayment(ClassLibrary.ServicePostModel.KeralaRechargePostModel.KsebBillPayment model)
        {
            var returnModel = new ClassLibrary.ServicePostModel.KeralaRechargePostModel.KsebBillPaymentResponse();
            dynamic data = new System.Dynamic.ExpandoObject();
            try
            {

                var user = _context.tb_User.FirstOrDefault(z => z.UserID == model.userId && z.IsActive);
                if (user != null)
                {

                    try
                    {
                        var rowIp = _context.tb_UserTransactionIp.Create();
                        rowIp.NetworkIp = model.ip;
                        rowIp.TimeStamp = System.DateTime.UtcNow;
                        rowIp.UserId = user.UserID;
                        _context.tb_UserTransactionIp.Add(rowIp);
                        _context.SaveChanges();
                    }
                    catch (Exception ex)
                    { }



                    var currency = _context.tb_AdminCurrencyRate.Where(z => z.ToCurrency == user.Currency && z.FromCurrency == model.amountCurrency).FirstOrDefault();
                    if (currency != null)
                    {
                        //var convertedAmount = Convert.ToDecimal(Convert.ToDouble(model.amount));
                        //if (user.Currency != "INR")
                        //{
                        //    convertedAmount = Convert.ToDecimal(Convert.ToDouble(model.amount) * currency.CurrencyRate);
                        //}


                        var newAmount = user.Wallet;
                        var convertedAmount = Convert.ToDecimal(Convert.ToDouble(model.amount) * currency.CurrencyRate);
                        var deductamt = convertedAmount;
                        decimal commissionAmount = Convert.ToDecimal(0);
                        if (user.UserType == (int)ClassLibrary.Enum.userType.Agent)
                        {
                            var commision = _context.tb_AgentCommisionRate.Where(z => z.AgentId == user.UserID && z.IsActive && z.ProductType == model.productType && z.ApiProvider == (int)ClassLibrary.Enum.apiProvider.KeralaRecharge).FirstOrDefault();
                            if (commision != null)
                            {
                                commissionAmount = ((Convert.ToDecimal(convertedAmount) * Convert.ToDecimal(commision.Commission)) / 100);
                                deductamt = Convert.ToDecimal(convertedAmount) - commissionAmount;
                                newAmount = user.Wallet - Convert.ToDecimal(deductamt);
                            }
                            else
                            {
                                newAmount = user.Wallet - Convert.ToDecimal(convertedAmount); ;
                            }
                        }
                        else
                        {
                            newAmount = user.Wallet - Convert.ToDecimal(convertedAmount);
                        }


                        if (user.Wallet >= deductamt && deductamt >= Convert.ToDecimal(0) && user.Wallet > Convert.ToDecimal(0))
                        {
                            var randomString = RandomKeralaRechargeKsebRequestString(30);
                            var requestString = baseurl_keralaRecharge;
                            requestString = baseurl_keralaRecharge + "api_key=" + apikey_KeralaRecharge + "&module=kseb&rid=" + randomString + "&section=" + model.section + "&customername=" + model.customerName + "&consumernumber=" + model.consumerNumber + "&customermobile=" + model.customerMobile + "&amount=" + model.amount + "&billnumber=" + model.billNumber + "&format=1";
                            var request = (HttpWebRequest)WebRequest.Create(requestString);
                            var response = (HttpWebResponse)request.GetResponse();
                            var responseString = new System.IO.StreamReader(response.GetResponseStream()).ReadToEnd();
                            data = Newtonsoft.Json.JsonConvert.DeserializeObject(responseString);

                            try
                            {
                                if (data != null)
                                {
                                    if (data.status != null)
                                    {
                                        if (data.status == "FAILED")
                                        {
                                            var transaction = _context.tb_KeralaRechargeKsebTransaction.Create();
                                            transaction.Amount = model.amount;
                                            transaction.Message = data.message;
                                            transaction.BillNumber = model.billNumber;
                                            transaction.Rid = randomString;
                                            transaction.Status = data.status;
                                            transaction.Timestamp = currentTime;
                                            transaction.UniqueIdentifier = Guid.NewGuid();
                                            transaction.UserId = model.userId;
                                            transaction.ConsumerNumber = model.consumerNumber;
                                            transaction.CustomerMobile = model.customerMobile;
                                            transaction.CustomerName = model.customerName;
                                            transaction.Section = model.section;
                                            transaction.AmountCurrency = model.amountCurrency;
                                            transaction.AmountDeductedCurrency = user.Currency;
                                            transaction.Commission = commissionAmount;
                                            transaction.AmountDeducted = String.Format("{0:0.00}", deductamt);
                                            _context.tb_KeralaRechargeKsebTransaction.Add(transaction);
                                        }
                                        else
                                        {
                                            var transaction = _context.tb_KeralaRechargeKsebTransaction.Create();
                                            transaction.Amount = model.amount;
                                            transaction.Message = data.message;
                                            transaction.BillNumber = model.billNumber;
                                            transaction.Rid = randomString;
                                            transaction.Status = data.status;
                                            transaction.Timestamp = currentTime;
                                            transaction.UniqueIdentifier = Guid.NewGuid();
                                            transaction.UserId = model.userId;
                                            transaction.ConsumerNumber = model.consumerNumber;
                                            transaction.CustomerMobile = model.customerMobile;
                                            transaction.CustomerName = model.customerName;
                                            transaction.Section = model.section;
                                            transaction.AmountCurrency = model.amountCurrency;
                                            transaction.AmountDeductedCurrency = user.Currency;
                                            transaction.AmountDeducted = String.Format("{0:0.00}", deductamt);
                                            transaction.Commission = commissionAmount;
                                            _context.tb_KeralaRechargeKsebTransaction.Add(transaction);

                                            user.Wallet = user.Wallet - convertedAmount;

                                        }

                                        if (_context.SaveChanges() > 0)
                                        {
                                            returnModel.status = true;
                                            returnModel.message = "Success";
                                            returnModel.data = data;
                                            returnModel.wallet = user.Wallet.ToString();
                                        }
                                        else
                                        {
                                            returnModel.status = false;
                                            returnModel.message = "Failed";
                                            returnModel.data = data;
                                            returnModel.wallet = user.Wallet.ToString();
                                        }
                                    }
                                    else
                                    {
                                        returnModel.status = true;
                                        returnModel.message = "Success";
                                        returnModel.wallet = user.Wallet.ToString(); returnModel.data = data;
                                    }
                                }
                                else
                                {
                                    returnModel.status = false;
                                    returnModel.message = "Failed";
                                    returnModel.wallet = user.Wallet.ToString(); returnModel.data = data;
                                }
                            }
                            catch (Exception ex)
                            {
                                returnModel.status = false;
                                returnModel.message = "Failed";
                                returnModel.data = data;
                                returnModel.wallet = user.Wallet.ToString();
                            }
                        }
                        else
                        {
                            returnModel.status = false;
                            returnModel.message = "Insufficient Amount";
                            returnModel.data = data;
                            returnModel.wallet = user.Wallet.ToString(); return returnModel;
                        }
                    }
                    else
                    {
                        returnModel.status = false;
                        returnModel.message = "Failed";
                        returnModel.data = data;
                        returnModel.wallet = user.Wallet.ToString(); return returnModel;
                    }
                }
                else
                {
                    returnModel.status = false;
                    returnModel.message = "Failed";
                    returnModel.data = data;
                    returnModel.wallet = user.Wallet.ToString(); return returnModel;
                }

            }
            catch (Exception ex)
            {
                returnModel.status = false;
                returnModel.message = "An error has occured";
                returnModel.data = data;
            }
            return returnModel;
        }



        public dynamic rechargeStatusUpdateBYTransaction(string txnd, string Rid)
        {
            dynamic data = new System.Dynamic.ExpandoObject();
            var requestString = baseurl_keralaRecharge + "api_key=" + apikey_KeralaRecharge + "&module=status&rid=" + Rid + "&format=1";
            var request = (HttpWebRequest)WebRequest.Create(requestString);
            var response = (HttpWebResponse)request.GetResponse();
            var responseString = new System.IO.StreamReader(response.GetResponseStream()).ReadToEnd();
            data = Newtonsoft.Json.JsonConvert.DeserializeObject(responseString);
            return data;
        }




    }
}
