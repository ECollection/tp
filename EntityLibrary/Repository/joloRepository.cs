﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace EntityLibrary.Repository
{
    public class joloRepository : BaseReference
    {
        public object operatorANDcircleFinder(ClassLibrary.ServicePostModel.JoloPostModel.operatorANDcircleFinder model)
        {
            var returnModel = new ClassLibrary.ServicePostModel.JoloPostModel.operatorANDcircleFinderReturnModel();
            try
            {
                //var requestString = "https://joloapi.com/api/recharge.php?userid=" + "arakkal" + "&key=" + "718127426963062" + "&type=json&mode=0&operator=" + "IDX" + "&service=" + "7558006175" + "&amount=" + "50" + "&orderid=" + "1234567" + "";
                var requestString = baseUrl + "findoperator.php?userid=" + userid + "&key=" + key + "&mob=" + model.mobileNumber + "&type=" + type + "";
                var request = (HttpWebRequest)WebRequest.Create(requestString);
                var response = (HttpWebResponse)request.GetResponse();
                var responseString = new System.IO.StreamReader(response.GetResponseStream()).ReadToEnd();
                dynamic data = Newtonsoft.Json.JsonConvert.DeserializeObject(responseString);
                returnModel.status = true;
                returnModel.message = "Success";
                returnModel.data = data;
            }
            catch (Exception ex)
            {
                returnModel.status = false;
                returnModel.message = "An error has occured";
            }
            return returnModel;
        }

        public object operatorplanANDofferFinder(ClassLibrary.ServicePostModel.JoloPostModel.operatorplanANDofferFinder model)
        {
            var returnModel = new ClassLibrary.ServicePostModel.JoloPostModel.operatorplanANDofferFinderReturnModel();
            try
            {
                var requestString = baseUrl + "findplan.php?userid=" + userid + "&key=" + key + "&opt=" + model.operator_code + "&cir=" + model.circle_code + "&typ=" + model.category_code + "&amt=" + model.amount + "&max=" + model.rowCount + "&type=" + type + "";
                var request = (HttpWebRequest)WebRequest.Create(requestString);
                var response = (HttpWebResponse)request.GetResponse();
                var responseString = new System.IO.StreamReader(response.GetResponseStream()).ReadToEnd();
                dynamic data = Newtonsoft.Json.JsonConvert.DeserializeObject(responseString);
                returnModel.status = true;
                returnModel.message = "Success";
                returnModel.data = data;
            }
            catch (Exception ex)
            {
                returnModel.status = false;
                returnModel.message = "An error has occured";
            }
            return returnModel;
        }

        public object mobileAndDthRecharge(ClassLibrary.ServicePostModel.JoloPostModel.mobileAndDthRecharge model)
        {
            var returnModel = new ClassLibrary.ServicePostModel.JoloPostModel.mobileAndDthRechargeReturnModel();
            dynamic data = new System.Dynamic.ExpandoObject();

            try
            {
                var user = _context.tb_User.FirstOrDefault(z => z.UserID == model.userId);
                if (user != null)
                {

                    try
                    {
                        var rowIp = _context.tb_UserTransactionIp.Create();
                        rowIp.NetworkIp = model.ip;
                        rowIp.TimeStamp = System.DateTime.UtcNow;
                        rowIp.UserId = user.UserID;
                        _context.tb_UserTransactionIp.Add(rowIp);
                        _context.SaveChanges();
                    }
                    catch (Exception ex)
                    { }





                    var currecyRate = _context.tb_AdminCurrencyRate.Where(z => z.ToCurrency == user.Currency && z.FromCurrency == model.amountCurrency).FirstOrDefault();
                    if (currecyRate != null)
                    {
                        var amount = Convert.ToDecimal(Convert.ToDouble(model.amount) * currecyRate.CurrencyRate);
                        var newAmount = user.Wallet;
                        var deductamt = amount;
                        decimal commissionAmount = Convert.ToDecimal(0);
                        if (user.UserType == (int)ClassLibrary.Enum.userType.Agent)
                        {
                            var commision = _context.tb_AgentCommisionRate.Where(z => z.AgentId == user.UserID && z.IsActive && z.ProductType == model.productType && z.ApiProvider == (int)ClassLibrary.Enum.apiProvider.Jolo).FirstOrDefault();
                            if (commision != null)
                            {
                                commissionAmount = ((Convert.ToDecimal(amount) * Convert.ToDecimal(commision.Commission)) / 100);
                                deductamt = Convert.ToDecimal(amount) - commissionAmount;
                                newAmount = user.Wallet - Convert.ToDecimal(deductamt);
                            }
                            else
                            {
                                newAmount = user.Wallet - Convert.ToDecimal(amount); ;
                            }
                        }
                        else
                        {
                            newAmount = user.Wallet - Convert.ToDecimal(amount);
                        }
                        if (user.Wallet >= deductamt && deductamt >= Convert.ToDecimal(0) && user.Wallet > Convert.ToDecimal(0))
                        {

                            var rechargeRequest = _context.tb_JoloRechargeRequest.Create();
                            rechargeRequest.Amount = model.amount;
                            rechargeRequest.Mode = model.mode;
                            rechargeRequest.Operator = model.operator_code;
                            rechargeRequest.OrderId = RandomJoloRequestString(30);
                            rechargeRequest.Service = model.mobileNumber;
                            rechargeRequest.Status = "";
                            rechargeRequest.Timestamp = currentTime;
                            rechargeRequest.UserId = model.userId;
                            rechargeRequest.Type = model.type;
                            rechargeRequest.AmountDeducted = Convert.ToString(deductamt);
                            rechargeRequest.AmountCurrency = model.amountCurrency;
                            rechargeRequest.AmountDeductedCurrency = user.Currency;
                            rechargeRequest.Commission = commissionAmount;
                            _context.tb_JoloRechargeRequest.Add(rechargeRequest);
                            if (_context.SaveChanges() > 0)
                            {
                                var requestString = baseUrl + "recharge.php?mode=" + model.mode + "&userid=" + userid + "&key=" + key + "&operator=" + model.operator_code + "&service=" + model.mobileNumber + "&amount=" + model.amount + "&orderid=" + rechargeRequest.OrderId + "&type=" + type + "";
                                var request = (HttpWebRequest)WebRequest.Create(requestString);
                                var response = (HttpWebResponse)request.GetResponse();
                                var responseString = new System.IO.StreamReader(response.GetResponseStream()).ReadToEnd();
                                data = Newtonsoft.Json.JsonConvert.DeserializeObject(responseString);

                                if (data != null)
                                {
                                    var rechargeResponse = _context.tb_JoloRechargeResponse.Create();
                                    if (data.status == "FAILED")
                                    {
                                        rechargeResponse.Timestamp = currentTime;
                                        rechargeResponse.Status = data.status;
                                        rechargeResponse.Service = "";
                                        rechargeResponse.RequestOrderID = rechargeRequest.OrderId;
                                        rechargeResponse.ReferenceId = rechargeRequest.Id;
                                        rechargeResponse.OperatorId = "";
                                        rechargeResponse.Operator = "";
                                        rechargeResponse.JoloOrderId = "";
                                        rechargeResponse.ErrorCode = data.error;
                                        rechargeResponse.Amount = "";

                                        rechargeRequest.Status = data.status;

                                        returnModel.status = false;
                                        returnModel.message = "Failed";
                                        returnModel.data = data;
                                    }
                                    else
                                    {
                                        rechargeResponse.Amount = data.amount;
                                        if (data.status == "FAILED")
                                        {
                                            rechargeResponse.ErrorCode = data.error;
                                        }
                                        else
                                        {
                                            rechargeResponse.ErrorCode = "";
                                        }
                                        rechargeResponse.JoloOrderId = data.txid;
                                        //rechanrgeResponse.Operator = data.operator;
                                        rechargeResponse.Operator = model.operator_code;
                                        rechargeResponse.OperatorId = data.operatorid;
                                        rechargeResponse.ReferenceId = rechargeRequest.Id;
                                        rechargeResponse.RequestOrderID = data.orderid;
                                        rechargeResponse.Service = data.service;
                                        rechargeResponse.Status = data.status;
                                        rechargeResponse.Timestamp = currentTime;

                                        rechargeRequest.Status = data.status;
                                        returnModel.status = true;
                                        returnModel.message = "Success";
                                        returnModel.data = data;



                                        user.Wallet = newAmount;


                                    }
                                    _context.tb_JoloRechargeResponse.Add(rechargeResponse);
                                    _context.SaveChanges();
                                    returnModel.wallet = user.Wallet.ToString();
                                }
                                else
                                {
                                    returnModel.status = false;
                                    returnModel.message = "Failed";
                                    returnModel.data = data;
                                    returnModel.wallet = user.Wallet.ToString();
                                }
                            }
                            else
                            {
                                returnModel.status = false;
                                returnModel.message = "Failed";
                                returnModel.data = data;
                                returnModel.wallet = "0";
                            }
                        }
                        else
                        {
                            returnModel.status = false;
                            returnModel.message = "Insufficient Amount";
                            returnModel.data = data;
                            returnModel.wallet = "0";
                        }
                    }
                    else
                    {
                        returnModel.status = false;
                        returnModel.message = "Failed";
                        returnModel.data = data;
                        returnModel.wallet = "0";
                    }
                }


            }
            catch (Exception ex)
            {
                returnModel.status = false;
                returnModel.message = "An error has occured";
                returnModel.data = data;
                returnModel.wallet = "0";
            }
            return returnModel;
        }

        public object FindDthOperator(ClassLibrary.ServicePostModel.JoloPostModel.FindDthOperator model)
        {
            var returnModel = new ClassLibrary.ServicePostModel.JoloPostModel.FindDthOperatorReturnModel();
            try
            {
                var requestString = baseUrl + "finddth.php?userid=" + userid + "&key=" + key + "&mob=" + model.subscriberid + "&type=" + type + "";
                var request = (HttpWebRequest)WebRequest.Create(requestString);
                var response = (HttpWebResponse)request.GetResponse();
                var responseString = new System.IO.StreamReader(response.GetResponseStream()).ReadToEnd();
                dynamic data = Newtonsoft.Json.JsonConvert.DeserializeObject(responseString);
                returnModel.status = true;
                returnModel.message = "Success";
                returnModel.data = data;
            }
            catch (Exception ex)
            {
                returnModel.status = false;
                returnModel.message = "An error has occured";
            }
            return returnModel;
        }

        public object FindPlanDTH(ClassLibrary.ServicePostModel.JoloPostModel.FindPlanDTH model)
        {
            var returnModel = new ClassLibrary.ServicePostModel.JoloPostModel.FindPlanDTHReturnModel();
            try
            {
                var requestString = baseUrl + "findplandth.php?userid=" + userid + "&key=" + key + "&opt=" + model.operator_code + "&amt=" + model.amount + "&max=" + model.rowCount + "&type=" + type + "";
                var request = (HttpWebRequest)WebRequest.Create(requestString);
                var response = (HttpWebResponse)request.GetResponse();
                var responseString = new System.IO.StreamReader(response.GetResponseStream()).ReadToEnd();
                dynamic data = Newtonsoft.Json.JsonConvert.DeserializeObject(responseString);
                returnModel.status = true;
                returnModel.message = "Success";
                returnModel.data = data;
            }
            catch (Exception ex)
            {
                returnModel.status = false;
                returnModel.message = "An error has occured";
            }
            return returnModel;
        }

        public object mobilePostpaidBill(ClassLibrary.ServicePostModel.JoloPostModel.MobilePostpaidBill model)
        {
            var returnModel = new ClassLibrary.ServicePostModel.JoloPostModel.mobileAndDthRechargeReturnModel();
            dynamic data = new System.Dynamic.ExpandoObject();

            try
            {
                var user = _context.tb_User.FirstOrDefault(z => z.UserID == model.userId);
                if (user != null)
                {

                    try
                    {
                        var rowIp = _context.tb_UserTransactionIp.Create();
                        rowIp.NetworkIp = model.ip;
                        rowIp.TimeStamp = System.DateTime.UtcNow;
                        rowIp.UserId = user.UserID;
                        _context.tb_UserTransactionIp.Add(rowIp);
                        _context.SaveChanges();
                    }
                    catch (Exception ex)
                    { }



                    var currecyRate = _context.tb_AdminCurrencyRate.Where(z => z.ToCurrency == user.Currency && z.FromCurrency == model.amountCurrency).FirstOrDefault();
                    if (currecyRate != null)
                    {
                        var amount = Convert.ToDecimal(Convert.ToDouble(model.amount) * currecyRate.CurrencyRate);
                        var deductamt = amount;
                        var newAmount = user.Wallet;
                        decimal commissionAmount = Convert.ToDecimal(0);
                        if (user.UserType == (int)ClassLibrary.Enum.userType.Agent)
                        {
                            var commision = _context.tb_AgentCommisionRate.Where(z => z.AgentId == user.UserID && z.IsActive && z.ProductType == model.productType && z.ApiProvider == (int)ClassLibrary.Enum.apiProvider.Jolo).FirstOrDefault();
                            if (commision != null)
                            {
                                commissionAmount = ((Convert.ToDecimal(amount) * Convert.ToDecimal(commision.Commission)) / 100);
                                deductamt = Convert.ToDecimal(amount) - commissionAmount;
                                newAmount = user.Wallet - Convert.ToDecimal(deductamt);
                            }
                            else
                            {
                                newAmount = user.Wallet - Convert.ToDecimal(amount); ;
                            }
                        }
                        else
                        {
                            newAmount = user.Wallet - Convert.ToDecimal(amount);
                        }
                        if (user.Wallet >= deductamt && deductamt >= Convert.ToDecimal(0) && user.Wallet > Convert.ToDecimal(0))
                        {

                            var rechargeRequest = _context.tb_JoloRechargeRequest.Create();
                            rechargeRequest.Amount = model.amount;
                            rechargeRequest.Mode = model.mode;
                            rechargeRequest.Operator = model.operator_code;
                            rechargeRequest.OrderId = RandomJoloRequestString(30);
                            rechargeRequest.Service = model.mobileNumber;
                            rechargeRequest.Status = "";
                            rechargeRequest.Timestamp = currentTime;
                            rechargeRequest.UserId = model.userId;
                            rechargeRequest.Type = model.type;
                            rechargeRequest.AmountDeducted = Convert.ToString(deductamt);
                            rechargeRequest.AmountCurrency = model.amountCurrency;
                            rechargeRequest.AmountDeductedCurrency = user.Currency;
                            rechargeRequest.Commission = commissionAmount;
                            _context.tb_JoloRechargeRequest.Add(rechargeRequest);
                            if (_context.SaveChanges() > 0)
                            {
                                var requestString = baseUrl + "cbill.php?mode=" + model.mode + "&userid=" + userid + "&key=" + key + "&operator=" + model.operator_code + "&service=" + model.mobileNumber + "&amount=" + model.amount + "&orderid=" + rechargeRequest.OrderId + "&type=" + type + "";
                                var request = (HttpWebRequest)WebRequest.Create(requestString);
                                var response = (HttpWebResponse)request.GetResponse();
                                var responseString = new System.IO.StreamReader(response.GetResponseStream()).ReadToEnd();
                                data = Newtonsoft.Json.JsonConvert.DeserializeObject(responseString);

                                if (data != null)
                                {
                                    var rechargeResponse = _context.tb_JoloRechargeResponse.Create();
                                    if (data.status == "FAILED")
                                    {
                                        rechargeResponse.Timestamp = currentTime;
                                        rechargeResponse.Status = data.status;
                                        rechargeResponse.Service = "";
                                        rechargeResponse.RequestOrderID = rechargeRequest.OrderId;
                                        rechargeResponse.ReferenceId = rechargeRequest.Id;
                                        rechargeResponse.OperatorId = "";
                                        rechargeResponse.Operator = "";
                                        rechargeResponse.JoloOrderId = "";
                                        rechargeResponse.ErrorCode = data.error;
                                        rechargeResponse.Amount = "";

                                        rechargeRequest.Status = data.status;

                                        returnModel.status = false;
                                        returnModel.message = "Failed";
                                        returnModel.data = data;
                                    }
                                    else
                                    {
                                        rechargeResponse.Amount = data.amount;
                                        if (data.status == "FAILED")
                                        {
                                            rechargeResponse.ErrorCode = data.error;
                                        }
                                        else
                                        {
                                            rechargeResponse.ErrorCode = "";
                                        }
                                        rechargeResponse.JoloOrderId = data.txid;
                                        //rechanrgeResponse.Operator = data.operator;
                                        rechargeResponse.Operator = model.operator_code;
                                        rechargeResponse.OperatorId = data.operatorid;
                                        rechargeResponse.ReferenceId = rechargeRequest.Id;
                                        rechargeResponse.RequestOrderID = data.orderid;
                                        rechargeResponse.Service = data.service;
                                        rechargeResponse.Status = data.status;
                                        rechargeResponse.Timestamp = currentTime;

                                        rechargeRequest.Status = data.status;
                                        returnModel.status = true;
                                        returnModel.message = "Success";
                                        returnModel.data = data;




                                        user.Wallet = newAmount;

                                    }
                                    _context.tb_JoloRechargeResponse.Add(rechargeResponse);
                                    _context.SaveChanges();
                                    returnModel.wallet = user.Wallet.ToString();
                                }
                                else
                                {
                                    returnModel.status = false;
                                    returnModel.message = "Failed";
                                    returnModel.data = data;
                                    returnModel.wallet = user.Wallet.ToString();
                                }
                            }
                            else
                            {
                                returnModel.status = false;
                                returnModel.message = "Failed";
                                returnModel.data = data;
                                returnModel.wallet = "0";
                            }
                        }
                        else
                        {
                            returnModel.status = false;
                            returnModel.message = "Insufficient Amount";
                            returnModel.data = data;
                            returnModel.wallet = "0";
                        }
                    }
                    else
                    {
                        returnModel.status = false;
                        returnModel.message = "Failed";
                        returnModel.data = data;
                        returnModel.wallet = "0";
                    }
                }


            }
            catch (Exception ex)
            {
                returnModel.status = false;
                returnModel.message = "An error has occured";
                returnModel.data = data;
                returnModel.wallet = "0";
            }
            return returnModel;
        }
    }
}
