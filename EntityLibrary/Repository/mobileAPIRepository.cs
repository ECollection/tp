﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace EntityLibrary.Repository
{
    public class mobileAPIRepository : BaseReference
    {

        public string fetchAccessToken()
        {
            try
            {

                string baseUrl = "https://mobileapi.in/api/token";
                WebRequest request = WebRequest.Create(baseUrl);
                // Set the Method property of the request to POST.  
                request.Method = "POST";

                var postModel = new ClassLibrary.ServicePostModel.MobileApPostModel.AccessTokenPostModel();
                postModel.username = username_mobileApi;
                postModel.password = password_mobileApi;

                var serializedPostModel = JsonConvert.SerializeObject(postModel);

                //request.Headers.Add("Authorization", "Bearer " + model.squareKey);
                ServicePointManager.Expect100Continue = true;
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                // Create POST data and convert it to a byte array.  
                string postData = serializedPostModel;
                byte[] byteArray = Encoding.UTF8.GetBytes(postData);
                // Set the ContentType property of the WebRequest.  
                //request.ContentType = "application/form-data";
                request.ContentType = "application/json";
                // Set the ContentLength property of the WebRequest.  
                request.ContentLength = byteArray.Length;
                // Get the request stream.  
                Stream dataStream = request.GetRequestStream();
                // Write the data to the request stream.  
                dataStream.Write(byteArray, 0, byteArray.Length);
                // Close the Stream object.  
                dataStream.Close();
                // Get the response.  
                WebResponse response = request.GetResponse();
                // Display the status.  
                //Console.WriteLine(((HttpWebResponse)response).StatusDescription);
                // Get the stream containing content returned by the server.  
                dataStream = response.GetResponseStream();
                // Open the stream using a StreamReader for easy access.  
                StreamReader reader = new StreamReader(dataStream);
                // Read the content.  
                string responseFromServer = reader.ReadToEnd();
                // Display the content.  
                //Console.WriteLine(responseFromServer);
                // Clean up the streams.  
                reader.Close();
                dataStream.Close();
                response.Close();
                return responseFromServer;
            }
            catch (Exception ex)
            {
                return string.Empty;
            }
        }


        public object findProvider()
        {
            try
            {
                var responseString = fetchAccessToken();
                //dynamic data = Newtonsoft.Json.JsonConvert.DeserializeObject(responseString);
                var data = JsonConvert.DeserializeObject<ClassLibrary.ServicePostModel.MobileApPostModel.AccessTokenResponse>(responseString);
                if (data != null)
                {
                    if (data.access_token != null && data.access_token != string.Empty)
                    {
                        try
                        {

                            string baseUrl = "https://mobileapi.in/api/v1/plan/providers";
                            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(baseUrl);
                            //WebRequest request = WebRequest.Create(baseUrl);
                            // Set the Method property of the request to POST.  
                            request.Method = "GET";
                            request.Accept = "application/json";

                            //var postModel = new ClassLibrary.ServicePostModel.MobileApPostModel.AccessTokenPostModel();
                            //postModel.username = username_mobileApi;
                            //postModel.password = password_mobileApi;

                            //var serializedPostModel = JsonConvert.SerializeObject(postModel);

                            request.Headers.Add("Authorization", "Bearer " + data.access_token);
                            //request.Headers.Add("Content-Type", "x-www-form-urlencoded");
                            //request.Headers.Add("Accept", "application/json");
                            //request.Accept("application/json");
                            //request.Headers.Add("Content-Type", "application/json");
                            ServicePointManager.Expect100Continue = true;
                            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                            // Create POST data and convert it to a byte array.  
                            //string postData = serializedPostModel;
                            //byte[] byteArray = Encoding.UTF8.GetBytes(postData);
                            // Set the ContentType property of the WebRequest.  
                            //request.ContentType = "application/form-data";
                            request.ContentType = "x-www-form-urlencoded";
                            // Set the ContentLength property of the WebRequest.  
                            //request.ContentLength = byteArray.Length;
                            // Get the request stream.  
                            //Stream dataStream = request.GetRequestStream();
                            // Write the data to the request stream.  
                            //dataStream.Write(byteArray, 0, byteArray.Length);
                            // Close the Stream object.  
                            //dataStream.Close();
                            // Get the response.  
                            WebResponse response = request.GetResponse();
                            // Display the status.  
                            //Console.WriteLine(((HttpWebResponse)response).StatusDescription);
                            // Get the stream containing content returned by the server.  
                            Stream dataStream = response.GetResponseStream();
                            // Open the stream using a StreamReader for easy access.  
                            StreamReader reader = new StreamReader(dataStream);
                            // Read the content.  
                            string responseFromServer = reader.ReadToEnd();
                            // Display the content.  
                            //Console.WriteLine(responseFromServer);
                            // Clean up the streams.  
                            reader.Close();
                            dataStream.Close();
                            response.Close();

                            var responseModel = new ClassLibrary.ServicePostModel.MobileApPostModel.ProviderResponse();
                            responseModel.status = true;
                            responseModel.message = "success";
                            responseModel.data = Newtonsoft.Json.JsonConvert.DeserializeObject(responseFromServer);
                            return responseModel;
                        }
                        catch (Exception ex)
                        {
                            var responseModel = new ClassLibrary.ServicePostModel.MobileApPostModel.ProviderResponse();
                            responseModel.status = false;
                            responseModel.message = ex.Message;
                            return responseModel;
                        }
                    }
                    else
                    {
                        var responseModel = new ClassLibrary.ServicePostModel.MobileApPostModel.ProviderResponse();
                        responseModel.status = false;
                        responseModel.message = "failed";
                        return responseModel;
                    }
                }
                else
                {
                    var responseModel = new ClassLibrary.ServicePostModel.MobileApPostModel.ProviderResponse();
                    responseModel.status = false;
                    responseModel.message = "failed";
                    return responseModel;
                }
                throw new NotImplementedException();
            }
            catch (Exception ex)
            {
                var responseModel = new ClassLibrary.ServicePostModel.MobileApPostModel.ProviderResponse();
                responseModel.status = false;
                responseModel.message = ex.Message;
                return responseModel;
            }
        }

        public object findCircle()
        {
            try
            {
                var responseString = fetchAccessToken();
                //dynamic data = Newtonsoft.Json.JsonConvert.DeserializeObject(responseString);
                var data = JsonConvert.DeserializeObject<ClassLibrary.ServicePostModel.MobileApPostModel.AccessTokenResponse>(responseString);
                if (data != null)
                {
                    if (data.access_token != null && data.access_token != string.Empty)
                    {
                        try
                        {

                            string baseUrl = "https://mobileapi.in/api/v1/plan/circle";
                            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(baseUrl);
                            //WebRequest request = WebRequest.Create(baseUrl);
                            // Set the Method property of the request to POST.  
                            request.Method = "GET";
                            request.Accept = "application/json";

                            //var postModel = new ClassLibrary.ServicePostModel.MobileApPostModel.AccessTokenPostModel();
                            //postModel.username = username_mobileApi;
                            //postModel.password = password_mobileApi;

                            //var serializedPostModel = JsonConvert.SerializeObject(postModel);

                            request.Headers.Add("Authorization", "Bearer " + data.access_token);
                            //request.Headers.Add("Content-Type", "x-www-form-urlencoded");
                            //request.Headers.Add("Accept", "application/json");
                            //request.Accept("application/json");
                            //request.Headers.Add("Content-Type", "application/json");
                            ServicePointManager.Expect100Continue = true;
                            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                            // Create POST data and convert it to a byte array.  
                            //string postData = serializedPostModel;
                            //byte[] byteArray = Encoding.UTF8.GetBytes(postData);
                            // Set the ContentType property of the WebRequest.  
                            //request.ContentType = "application/form-data";
                            request.ContentType = "x-www-form-urlencoded";
                            // Set the ContentLength property of the WebRequest.  
                            //request.ContentLength = byteArray.Length;
                            // Get the request stream.  
                            //Stream dataStream = request.GetRequestStream();
                            // Write the data to the request stream.  
                            //dataStream.Write(byteArray, 0, byteArray.Length);
                            // Close the Stream object.  
                            //dataStream.Close();
                            // Get the response.  
                            WebResponse response = request.GetResponse();
                            // Display the status.  
                            //Console.WriteLine(((HttpWebResponse)response).StatusDescription);
                            // Get the stream containing content returned by the server.  
                            Stream dataStream = response.GetResponseStream();
                            // Open the stream using a StreamReader for easy access.  
                            StreamReader reader = new StreamReader(dataStream);
                            // Read the content.  
                            string responseFromServer = reader.ReadToEnd();
                            // Display the content.  
                            //Console.WriteLine(responseFromServer);
                            // Clean up the streams.  
                            reader.Close();
                            dataStream.Close();
                            response.Close();

                            var responseModel = new ClassLibrary.ServicePostModel.MobileApPostModel.CircleResponse();
                            responseModel.status = true;
                            responseModel.message = "success";
                            responseModel.data = Newtonsoft.Json.JsonConvert.DeserializeObject(responseFromServer);
                            return responseModel;
                        }
                        catch (Exception ex)
                        {
                            var responseModel = new ClassLibrary.ServicePostModel.MobileApPostModel.CircleResponse();
                            responseModel.status = false;
                            responseModel.message = ex.Message;
                            return responseModel;
                        }
                    }
                    else
                    {
                        var responseModel = new ClassLibrary.ServicePostModel.MobileApPostModel.CircleResponse();
                        responseModel.status = false;
                        responseModel.message = "failed";
                        return responseModel;
                    }
                }
                else
                {
                    var responseModel = new ClassLibrary.ServicePostModel.MobileApPostModel.CircleResponse();
                    responseModel.status = false;
                    responseModel.message = "failed";
                    return responseModel;
                }
                throw new NotImplementedException();
            }
            catch (Exception ex)
            {
                var responseModel = new ClassLibrary.ServicePostModel.MobileApPostModel.CircleResponse();
                responseModel.status = false;
                responseModel.message = ex.Message;
                return responseModel;
            }
        }


        public object findPlansMobile(ClassLibrary.ServicePostModel.MobileApPostModel.PlansPostModel model)
        {
            try
            {
                var responseString = fetchAccessToken();
                //dynamic data = Newtonsoft.Json.JsonConvert.DeserializeObject(responseString);
                var data = JsonConvert.DeserializeObject<ClassLibrary.ServicePostModel.MobileApPostModel.AccessTokenResponse>(responseString);
                if (data != null)
                {
                    if (data.access_token != null && data.access_token != string.Empty)
                    {
                        try
                        {

                            string baseUrl = "https://mobileapi.in/api/v1/plan/mobile";
                            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(baseUrl);
                            //WebRequest request = WebRequest.Create(baseUrl);
                            // Set the Method property of the request to POST.  
                            request.Method = "POST";
                            request.Accept = "application/json";

                            //var postModel = new ClassLibrary.ServicePostModel.MobileApPostModel.AccessTokenPostModel();
                            //postModel.username = username_mobileApi;
                            //postModel.password = password_mobileApi;

                            var serializedPostModel = JsonConvert.SerializeObject(model);

                            request.Headers.Add("Authorization", "Bearer " + data.access_token);
                            //request.Headers.Add("Content-Type", "x-www-form-urlencoded");
                            //request.Headers.Add("Accept", "application/json");
                            //request.Accept("application/json");
                            //request.Headers.Add("Content-Type", "application/json");
                            ServicePointManager.Expect100Continue = true;
                            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                            // Create POST data and convert it to a byte array.  
                            string postData = serializedPostModel;
                            byte[] byteArray = Encoding.UTF8.GetBytes(postData);
                            // Set the ContentType property of the WebRequest.  
                            //request.ContentType = "application/form-data";
                            request.ContentType = "application/json";
                            // Set the ContentLength property of the WebRequest.  
                            request.ContentLength = byteArray.Length;
                            // Get the request stream.  
                            Stream dataStream = request.GetRequestStream();
                            // Write the data to the request stream.  
                            dataStream.Write(byteArray, 0, byteArray.Length);
                            // Close the Stream object.  
                            //dataStream.Close();
                            // Get the response.  
                            WebResponse response = request.GetResponse();
                            // Display the status.  
                            //Console.WriteLine(((HttpWebResponse)response).StatusDescription);
                            // Get the stream containing content returned by the server.  
                             dataStream = response.GetResponseStream();
                            // Open the stream using a StreamReader for easy access.  
                            StreamReader reader = new StreamReader(dataStream);
                            // Read the content.  
                            string responseFromServer = reader.ReadToEnd();
                            // Display the content.  
                            //Console.WriteLine(responseFromServer);
                            // Clean up the streams.  
                            reader.Close();
                            dataStream.Close();
                            response.Close();

                            var responseModel = new ClassLibrary.ServicePostModel.MobileApPostModel.PlansResponseModel();
                            responseModel.status = true;
                            responseModel.message = "success";
                            responseModel.data = Newtonsoft.Json.JsonConvert.DeserializeObject(responseFromServer);
                            return responseModel;
                        }
                        catch (Exception ex)
                        {
                            var responseModel = new ClassLibrary.ServicePostModel.MobileApPostModel.PlansResponseModel();
                            responseModel.status = false;
                            responseModel.message = ex.Message;
                            return responseModel;
                        }
                    }
                    else
                    {
                        var responseModel = new ClassLibrary.ServicePostModel.MobileApPostModel.PlansResponseModel();
                        responseModel.status = false;
                        responseModel.message = "failed";
                        return responseModel;
                    }
                }
                else
                {
                    var responseModel = new ClassLibrary.ServicePostModel.MobileApPostModel.PlansResponseModel();
                    responseModel.status = false;
                    responseModel.message = "failed";
                    return responseModel;
                }
                throw new NotImplementedException();
            }
            catch (Exception ex)
            {
                var responseModel = new ClassLibrary.ServicePostModel.MobileApPostModel.PlansResponseModel();
                responseModel.status = false;
                responseModel.message = ex.Message;
                return responseModel;
            }
        }


        public object findPlansDTH(ClassLibrary.ServicePostModel.MobileApPostModel.PlansDthModel model)
        {
            try
            {
                var responseString = fetchAccessToken();
                //dynamic data = Newtonsoft.Json.JsonConvert.DeserializeObject(responseString);
                var data = JsonConvert.DeserializeObject<ClassLibrary.ServicePostModel.MobileApPostModel.AccessTokenResponse>(responseString);
                if (data != null)
                {
                    if (data.access_token != null && data.access_token != string.Empty)
                    {
                        try
                        {

                            string baseUrl = "https://mobileapi.in/api/v1/plan/dth";
                            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(baseUrl);
                            //WebRequest request = WebRequest.Create(baseUrl);
                            // Set the Method property of the request to POST.  
                            request.Method = "POST";
                            request.Accept = "application/json";

                            //var postModel = new ClassLibrary.ServicePostModel.MobileApPostModel.AccessTokenPostModel();
                            //postModel.username = username_mobileApi;
                            //postModel.password = password_mobileApi;

                            var serializedPostModel = JsonConvert.SerializeObject(model);

                            request.Headers.Add("Authorization", "Bearer " + data.access_token);
                            //request.Headers.Add("Content-Type", "x-www-form-urlencoded");
                            //request.Headers.Add("Accept", "application/json");
                            //request.Accept("application/json");
                            //request.Headers.Add("Content-Type", "application/json");
                            ServicePointManager.Expect100Continue = true;
                            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                            // Create POST data and convert it to a byte array.  
                            string postData = serializedPostModel;
                            byte[] byteArray = Encoding.UTF8.GetBytes(postData);
                            // Set the ContentType property of the WebRequest.  
                            //request.ContentType = "application/form-data";
                            request.ContentType = "application/json";
                            // Set the ContentLength property of the WebRequest.  
                            request.ContentLength = byteArray.Length;
                            // Get the request stream.  
                            Stream dataStream = request.GetRequestStream();
                            // Write the data to the request stream.  
                            dataStream.Write(byteArray, 0, byteArray.Length);
                            // Close the Stream object.  
                            //dataStream.Close();
                            // Get the response.  
                            WebResponse response = request.GetResponse();
                            // Display the status.  
                            //Console.WriteLine(((HttpWebResponse)response).StatusDescription);
                            // Get the stream containing content returned by the server.  
                            dataStream = response.GetResponseStream();
                            // Open the stream using a StreamReader for easy access.  
                            StreamReader reader = new StreamReader(dataStream);
                            // Read the content.  
                            string responseFromServer = reader.ReadToEnd();
                            // Display the content.  
                            //Console.WriteLine(responseFromServer);
                            // Clean up the streams.  
                            reader.Close();
                            dataStream.Close();
                            response.Close();

                            var responseModel = new ClassLibrary.ServicePostModel.MobileApPostModel.PlansDthResponseModel();
                            responseModel.status = true;
                            responseModel.message = "success";
                            responseModel.data = Newtonsoft.Json.JsonConvert.DeserializeObject(responseFromServer);
                            return responseModel;
                        }
                        catch (Exception ex)
                        {
                            var responseModel = new ClassLibrary.ServicePostModel.MobileApPostModel.PlansDthResponseModel();
                            responseModel.status = false;
                            responseModel.message = ex.Message;
                            return responseModel;
                        }
                    }
                    else
                    {
                        var responseModel = new ClassLibrary.ServicePostModel.MobileApPostModel.PlansDthResponseModel();
                        responseModel.status = false;
                        responseModel.message = "failed";
                        return responseModel;
                    }
                }
                else
                {
                    var responseModel = new ClassLibrary.ServicePostModel.MobileApPostModel.PlansDthResponseModel();
                    responseModel.status = false;
                    responseModel.message = "failed";
                    return responseModel;
                }
                throw new NotImplementedException();
            }
            catch (Exception ex)
            {
                var responseModel = new ClassLibrary.ServicePostModel.MobileApPostModel.PlansDthResponseModel();
                responseModel.status = false;
                responseModel.message = ex.Message;
                return responseModel;
            }
        }


        public object findROffer(ClassLibrary.ServicePostModel.MobileApPostModel.Roffer model)
        {
            try
            {
                var responseString = fetchAccessToken();
                //dynamic data = Newtonsoft.Json.JsonConvert.DeserializeObject(responseString);
                var data = JsonConvert.DeserializeObject<ClassLibrary.ServicePostModel.MobileApPostModel.AccessTokenResponse>(responseString);
                if (data != null)
                {
                    if (data.access_token != null && data.access_token != string.Empty)
                    {
                        try
                        {

                            string baseUrl = "https://mobileapi.in/api/v1/plan/roffer";
                            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(baseUrl);
                            //WebRequest request = WebRequest.Create(baseUrl);
                            // Set the Method property of the request to POST.  
                            request.Method = "POST";
                            request.Accept = "application/json";

                            //var postModel = new ClassLibrary.ServicePostModel.MobileApPostModel.AccessTokenPostModel();
                            //postModel.username = username_mobileApi;
                            //postModel.password = password_mobileApi;

                            var serializedPostModel = JsonConvert.SerializeObject(model);

                            request.Headers.Add("Authorization", "Bearer " + data.access_token);
                            //request.Headers.Add("Content-Type", "x-www-form-urlencoded");
                            //request.Headers.Add("Accept", "application/json");
                            //request.Accept("application/json");
                            //request.Headers.Add("Content-Type", "application/json");
                            ServicePointManager.Expect100Continue = true;
                            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                            // Create POST data and convert it to a byte array.  
                            string postData = serializedPostModel;
                            byte[] byteArray = Encoding.UTF8.GetBytes(postData);
                            // Set the ContentType property of the WebRequest.  
                            //request.ContentType = "application/form-data";
                            request.ContentType = "application/json";
                            // Set the ContentLength property of the WebRequest.  
                            request.ContentLength = byteArray.Length;
                            // Get the request stream.  
                            Stream dataStream = request.GetRequestStream();
                            // Write the data to the request stream.  
                            dataStream.Write(byteArray, 0, byteArray.Length);
                            // Close the Stream object.  
                            //dataStream.Close();
                            // Get the response.  
                            WebResponse response = request.GetResponse();
                            // Display the status.  
                            //Console.WriteLine(((HttpWebResponse)response).StatusDescription);
                            // Get the stream containing content returned by the server.  
                            dataStream = response.GetResponseStream();
                            // Open the stream using a StreamReader for easy access.  
                            StreamReader reader = new StreamReader(dataStream);
                            // Read the content.  
                            string responseFromServer = reader.ReadToEnd();
                            // Display the content.  
                            //Console.WriteLine(responseFromServer);
                            // Clean up the streams.  
                            reader.Close();
                            dataStream.Close();
                            response.Close();

                            var responseModel = new ClassLibrary.ServicePostModel.MobileApPostModel.RofferResponseModel();
                            responseModel.status = true;
                            responseModel.message = "success";
                            responseModel.data = Newtonsoft.Json.JsonConvert.DeserializeObject(responseFromServer);
                            return responseModel;
                        }
                        catch (Exception ex)
                        {
                            var responseModel = new ClassLibrary.ServicePostModel.MobileApPostModel.RofferResponseModel();
                            responseModel.status = false;
                            responseModel.message = ex.Message;
                            return responseModel;
                        }
                    }
                    else
                    {
                        var responseModel = new ClassLibrary.ServicePostModel.MobileApPostModel.RofferResponseModel();
                        responseModel.status = false;
                        responseModel.message = "failed";
                        return responseModel;
                    }
                }
                else
                {
                    var responseModel = new ClassLibrary.ServicePostModel.MobileApPostModel.RofferResponseModel();
                    responseModel.status = false;
                    responseModel.message = "failed";
                    return responseModel;
                }
                throw new NotImplementedException();
            }
            catch (Exception ex)
            {
                var responseModel = new ClassLibrary.ServicePostModel.MobileApPostModel.RofferResponseModel();
                responseModel.status = false;
                responseModel.message = ex.Message;
                return responseModel;
            }
        }
    }
}
