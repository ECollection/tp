﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace EntityLibrary.Repository
{
    public class magiceasyRepository : BaseReference
    {
        public object Get_Country()
        {
            var returnModel = new ClassLibrary.ServicePostModel.MagicEasyPostModel.MagicEasy_Get_Country_Response();
            try
            {
                HttpWebRequest httpRequest =
    (HttpWebRequest)WebRequest.Create(baseUrlMagicEasy + "International/Get_Country");

                httpRequest.Method = "POST";
                httpRequest.ContentType = "application/x-www-form-urlencoded";
                string poststring = String.Format("clientid={0}&apikey={1}", clientid, apikey);
                byte[] bytedata = Encoding.UTF8.GetBytes(poststring);
                httpRequest.ContentLength = bytedata.Length;

                Stream requestStream = httpRequest.GetRequestStream();
                requestStream.Write(bytedata, 0, bytedata.Length);
                requestStream.Close();


                HttpWebResponse httpWebResponse =
                (HttpWebResponse)httpRequest.GetResponse();
                Stream responseStream = httpWebResponse.GetResponseStream();

                StringBuilder sb = new StringBuilder();

                using (StreamReader reader =
                new StreamReader(responseStream, System.Text.Encoding.UTF8))
                {
                    string line;
                    while ((line = reader.ReadLine()) != null)
                    {
                        sb.Append(line);
                    }
                }
                dynamic data = Newtonsoft.Json.JsonConvert.DeserializeObject(sb.ToString());
                returnModel.status = true;
                returnModel.message = "Success";
                returnModel.data = data;
                return returnModel;


            }
            catch (Exception ex)
            {
                returnModel.status = false;
                returnModel.message = "An error has occured";
                return returnModel;
            }
        }

        public object Get_Opt_list(ClassLibrary.ServicePostModel.MagicEasyPostModel.Get_Opt_list model)
        {
            var returnModel = new ClassLibrary.ServicePostModel.MagicEasyPostModel.MagicEasy_Response();
            try
            {
                HttpWebRequest httpRequest =
    (HttpWebRequest)WebRequest.Create(baseUrlMagicEasy + "International/Get_Opt_list");

                httpRequest.Method = "POST";
                httpRequest.ContentType = "application/x-www-form-urlencoded";
                string poststring = String.Format("clientid={0}&apikey={1}&ccode={2}", clientid, apikey, model.ccode);
                byte[] bytedata = Encoding.UTF8.GetBytes(poststring);
                httpRequest.ContentLength = bytedata.Length;

                Stream requestStream = httpRequest.GetRequestStream();
                requestStream.Write(bytedata, 0, bytedata.Length);
                requestStream.Close();


                HttpWebResponse httpWebResponse =
                (HttpWebResponse)httpRequest.GetResponse();
                Stream responseStream = httpWebResponse.GetResponseStream();

                StringBuilder sb = new StringBuilder();

                using (StreamReader reader =
                new StreamReader(responseStream, System.Text.Encoding.UTF8))
                {
                    string line;
                    while ((line = reader.ReadLine()) != null)
                    {
                        sb.Append(line);
                    }
                }
                dynamic data = Newtonsoft.Json.JsonConvert.DeserializeObject(sb.ToString());
                returnModel.status = true;
                returnModel.message = "Success";
                returnModel.data = data;
                return returnModel;


            }
            catch (Exception ex)
            {
                returnModel.status = false;
                returnModel.message = "An error has occured";
                return returnModel;
            }
        }

        public object Get_Oprator(ClassLibrary.ServicePostModel.MagicEasyPostModel.Get_Oprator model)
        {
            var returnModel = new ClassLibrary.ServicePostModel.MagicEasyPostModel.MagicEasy_Response();
            try
            {
                HttpWebRequest httpRequest =
    (HttpWebRequest)WebRequest.Create(baseUrlMagicEasy + "International/Get_Oprator");

                httpRequest.Method = "POST";
                httpRequest.ContentType = "application/x-www-form-urlencoded";
                string poststring = String.Format("clientid={0}&apikey={1}&ccode={2}&mobile={3}", clientid, apikey, model.ccode, model.mobile);
                byte[] bytedata = Encoding.UTF8.GetBytes(poststring);
                httpRequest.ContentLength = bytedata.Length;

                Stream requestStream = httpRequest.GetRequestStream();
                requestStream.Write(bytedata, 0, bytedata.Length);
                requestStream.Close();


                HttpWebResponse httpWebResponse =
                (HttpWebResponse)httpRequest.GetResponse();
                Stream responseStream = httpWebResponse.GetResponseStream();

                StringBuilder sb = new StringBuilder();

                using (StreamReader reader =
                new StreamReader(responseStream, System.Text.Encoding.UTF8))
                {
                    string line;
                    while ((line = reader.ReadLine()) != null)
                    {
                        sb.Append(line);
                    }
                }
                dynamic data = Newtonsoft.Json.JsonConvert.DeserializeObject(sb.ToString());
                returnModel.status = true;
                returnModel.message = "Success";
                returnModel.data = data;
                return returnModel;


            }
            catch (Exception ex)
            {
                returnModel.status = false;
                returnModel.message = "An error has occured";
                return returnModel;
            }
        }

        public object Get_Opt_offer(ClassLibrary.ServicePostModel.MagicEasyPostModel.Get_Opt_offer model)
        {
            var returnModel = new ClassLibrary.ServicePostModel.MagicEasyPostModel.MagicEasy_Response();
            try
            {
                HttpWebRequest httpRequest =
    (HttpWebRequest)WebRequest.Create(baseUrlMagicEasy + "International/Get_Opt_offer");

                httpRequest.Method = "POST";
                httpRequest.ContentType = "application/x-www-form-urlencoded";
                string poststring = String.Format("clientid={0}&apikey={1}&ccode={2}&mobile={3}&optr={4}", clientid, apikey, model.ccode, model.mobile,model.optr);
                byte[] bytedata = Encoding.UTF8.GetBytes(poststring);
                httpRequest.ContentLength = bytedata.Length;

                Stream requestStream = httpRequest.GetRequestStream();
                requestStream.Write(bytedata, 0, bytedata.Length);
                requestStream.Close();


                HttpWebResponse httpWebResponse =
                (HttpWebResponse)httpRequest.GetResponse();
                Stream responseStream = httpWebResponse.GetResponseStream();

                StringBuilder sb = new StringBuilder();

                using (StreamReader reader =
                new StreamReader(responseStream, System.Text.Encoding.UTF8))
                {
                    string line;
                    while ((line = reader.ReadLine()) != null)
                    {
                        sb.Append(line);
                    }
                }
                dynamic data = Newtonsoft.Json.JsonConvert.DeserializeObject(sb.ToString());
                returnModel.status = true;
                returnModel.message = "Success";
                returnModel.data = data;
                return returnModel;


            }
            catch (Exception ex)
            {
                returnModel.status = false;
                returnModel.message = "An error has occured";
                return returnModel;
            }
        }

        public object Get_Opt_list_Mobile()
        {
            var returnModel = new ClassLibrary.ServicePostModel.MagicEasyPostModel.MagicEasy_Response();
            try
            {
                HttpWebRequest httpRequest =
    (HttpWebRequest)WebRequest.Create(baseUrlMagicEasy + "Mobile/Get_Opt_list");

                httpRequest.Method = "POST";
                httpRequest.ContentType = "application/x-www-form-urlencoded";
                string poststring = String.Format("clientid={0}&apikey={1}", clientid, apikey);
                byte[] bytedata = Encoding.UTF8.GetBytes(poststring);
                httpRequest.ContentLength = bytedata.Length;

                Stream requestStream = httpRequest.GetRequestStream();
                requestStream.Write(bytedata, 0, bytedata.Length);
                requestStream.Close();


                HttpWebResponse httpWebResponse =
                (HttpWebResponse)httpRequest.GetResponse();
                Stream responseStream = httpWebResponse.GetResponseStream();

                StringBuilder sb = new StringBuilder();

                using (StreamReader reader =
                new StreamReader(responseStream, System.Text.Encoding.UTF8))
                {
                    string line;
                    while ((line = reader.ReadLine()) != null)
                    {
                        sb.Append(line);
                    }
                }
                dynamic data = Newtonsoft.Json.JsonConvert.DeserializeObject(sb.ToString());
                returnModel.status = true;
                returnModel.message = "Success";
                returnModel.data = data;
                return returnModel;


            }
            catch (Exception ex)
            {
                returnModel.status = false;
                returnModel.message = "An error has occured";
                return returnModel;
            }
        }

        public object Get_Circle_list_Mobile()
        {
            var returnModel = new ClassLibrary.ServicePostModel.MagicEasyPostModel.MagicEasy_Response();
            try
            {
                HttpWebRequest httpRequest =
    (HttpWebRequest)WebRequest.Create(baseUrlMagicEasy + "Mobile/Get_Circle_list");

                httpRequest.Method = "POST";
                httpRequest.ContentType = "application/x-www-form-urlencoded";
                string poststring = String.Format("clientid={0}&apikey={1}", clientid, apikey);
                byte[] bytedata = Encoding.UTF8.GetBytes(poststring);
                httpRequest.ContentLength = bytedata.Length;

                Stream requestStream = httpRequest.GetRequestStream();
                requestStream.Write(bytedata, 0, bytedata.Length);
                requestStream.Close();


                HttpWebResponse httpWebResponse =
                (HttpWebResponse)httpRequest.GetResponse();
                Stream responseStream = httpWebResponse.GetResponseStream();

                StringBuilder sb = new StringBuilder();

                using (StreamReader reader =
                new StreamReader(responseStream, System.Text.Encoding.UTF8))
                {
                    string line;
                    while ((line = reader.ReadLine()) != null)
                    {
                        sb.Append(line);
                    }
                }
                dynamic data = Newtonsoft.Json.JsonConvert.DeserializeObject(sb.ToString());
                returnModel.status = true;
                returnModel.message = "Success";
                returnModel.data = data;
                return returnModel;


            }
            catch (Exception ex)
            {
                returnModel.status = false;
                returnModel.message = "An error has occured";
                return returnModel;
            }
        }

        public object Get_Oprator_Mobile(ClassLibrary.ServicePostModel.MagicEasyPostModel.Get_Oprator model)
        {
            var returnModel = new ClassLibrary.ServicePostModel.MagicEasyPostModel.MagicEasy_Response();
            try
            {
                HttpWebRequest httpRequest =
    (HttpWebRequest)WebRequest.Create(baseUrlMagicEasy + "Mobile/Get_Oprator");

                httpRequest.Method = "POST";
                httpRequest.ContentType = "application/x-www-form-urlencoded";
                string poststring = String.Format("clientid={0}&apikey={1}&mobile={2}", clientid, apikey, model.mobile);
                byte[] bytedata = Encoding.UTF8.GetBytes(poststring);
                httpRequest.ContentLength = bytedata.Length;

                Stream requestStream = httpRequest.GetRequestStream();
                requestStream.Write(bytedata, 0, bytedata.Length);
                requestStream.Close();


                HttpWebResponse httpWebResponse =
                (HttpWebResponse)httpRequest.GetResponse();
                Stream responseStream = httpWebResponse.GetResponseStream();

                StringBuilder sb = new StringBuilder();

                using (StreamReader reader =
                new StreamReader(responseStream, System.Text.Encoding.UTF8))
                {
                    string line;
                    while ((line = reader.ReadLine()) != null)
                    {
                        sb.Append(line);
                    }
                }
                dynamic data = Newtonsoft.Json.JsonConvert.DeserializeObject(sb.ToString());
                returnModel.status = true;
                returnModel.message = "Success";
                returnModel.data = data;
                return returnModel;


            }
            catch (Exception ex)
            {
                returnModel.status = false;
                returnModel.message = "An error has occured";
                return returnModel;
            }
        }

        public object Get_Opt_list_DTH()
        {
            var returnModel = new ClassLibrary.ServicePostModel.MagicEasyPostModel.MagicEasy_Response();
            try
            {
                HttpWebRequest httpRequest =
    (HttpWebRequest)WebRequest.Create(baseUrlMagicEasy + "Dth/Get_Opt_list");

                httpRequest.Method = "POST";
                httpRequest.ContentType = "application/x-www-form-urlencoded";
                string poststring = String.Format("clientid={0}&apikey={1}", clientid, apikey);
                byte[] bytedata = Encoding.UTF8.GetBytes(poststring);
                httpRequest.ContentLength = bytedata.Length;

                Stream requestStream = httpRequest.GetRequestStream();
                requestStream.Write(bytedata, 0, bytedata.Length);
                requestStream.Close();


                HttpWebResponse httpWebResponse =
                (HttpWebResponse)httpRequest.GetResponse();
                Stream responseStream = httpWebResponse.GetResponseStream();

                StringBuilder sb = new StringBuilder();

                using (StreamReader reader =
                new StreamReader(responseStream, System.Text.Encoding.UTF8))
                {
                    string line;
                    while ((line = reader.ReadLine()) != null)
                    {
                        sb.Append(line);
                    }
                }
                dynamic data = Newtonsoft.Json.JsonConvert.DeserializeObject(sb.ToString());
                returnModel.status = true;
                returnModel.message = "Success";
                returnModel.data = data;
                return returnModel;


            }
            catch (Exception ex)
            {
                returnModel.status = false;
                returnModel.message = "An error has occured";
                return returnModel;
            }
        }
    }
}
