﻿using EntityLibrary.Data;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace EntityLibrary.Repository
{
    public class OnePrepayRepository : BaseReference
    {
        public object pinlessrequest(ClassLibrary.ServicePostModel.OnePrepayPostModel.PinlessRequest model)
        {
            var returnModel = new ClassLibrary.ServicePostModel.OnePrepayPostModel.PinlessRequestReturnModel();

            try
            {

                var user = _context.tb_User.FirstOrDefault(z => z.UserID == model.userId && z.IsActive);
                if (user != null)
                {

                    try
                    {
                        var rowIp = _context.tb_UserTransactionIp.Create();
                        rowIp.NetworkIp = model.ip;
                        rowIp.TimeStamp = System.DateTime.UtcNow;
                        rowIp.UserId = user.UserID;
                        _context.tb_UserTransactionIp.Add(rowIp);
                        _context.SaveChanges();
                    }
                    catch (Exception ex)
                    { }


                    var currecyRate = _context.tb_AdminCurrencyRate.Where(z => z.ToCurrency == user.Currency && z.FromCurrency == model.amountCurrency).FirstOrDefault();
                    if (currecyRate != null)
                    {
                        var amount = Convert.ToDecimal(Convert.ToDouble(model.amount) * currecyRate.CurrencyRate);
                        var deductamt = amount;
                        var newAmount = user.Wallet;
                        decimal commissionAmount = Convert.ToDecimal(0);
                        if (user.UserType == (int)ClassLibrary.Enum.userType.Agent)
                        {
                            var commision = _context.tb_AgentCommisionRate.Where(z => z.AgentId == user.UserID && z.IsActive && z.ProductType == model.productType && z.ApiProvider == (int)ClassLibrary.Enum.apiProvider.OnePrepay).FirstOrDefault();
                            if (commision != null)
                            {
                                commissionAmount = ((Convert.ToDecimal(amount) * Convert.ToDecimal(commision.Commission)) / 100);
                                deductamt = Convert.ToDecimal(amount) - commissionAmount;
                                newAmount = user.Wallet - Convert.ToDecimal(deductamt);
                            }
                            else
                            {
                                newAmount = user.Wallet - Convert.ToDecimal(amount);
                            }
                        }
                        else
                        {
                            newAmount = user.Wallet - Convert.ToDecimal(amount);
                        }


                        //if (user.Wallet >= Convert.ToDecimal(Convert.ToDouble(model.amount) * currecyRate.CurrencyRate))
                        if (user.Wallet >= deductamt && deductamt >= Convert.ToDecimal(0) && user.Wallet > Convert.ToDecimal(0))
                        {
                            dynamic data1 = new JObject();
                            dynamic data2 = new JObject();

                            var receiptNo = OnePrepayRandomString(20, ClassLibrary.Enum.OnePrepayType.PinlessRequest.ToString());

                            if (model.prodCode == "DUCTOP")
                            {
                                var RequestXml = "<RequestXml> <Type>PinlessRequest</Type> <TerminalId>" + TerminalID + "</TerminalId> <Password>" + Password + "</Password> <Language>eng</Language> <ReceiptNo>" + receiptNo + "</ReceiptNo> <AccountNo>" + model.account + "</AccountNo> <Amount>" + model.amount + "</Amount> <ClerkId>1</ClerkId> <ProdCode>" + model.prodCode + "</ProdCode> </RequestXml>";

                                HttpWebRequest webreq = (HttpWebRequest)WebRequest.Create(RequestUrlDuTopUp + RequestXml);
                                webreq.ContentType = "application/json";
                                //webreq.Accept = "text/xml";
                                webreq.Headers.Clear();
                                webreq.Method = "GET";
                                Encoding encode = Encoding.GetEncoding("utf-8");
                                HttpWebResponse webres = null;
                                webres = (HttpWebResponse)webreq.GetResponse();
                                Stream reader = null;
                                reader = webres.GetResponseStream();
                                StreamReader sreader = new StreamReader(reader, encode, true);
                                string result = sreader.ReadToEnd();


                                XmlDocument doc = new XmlDocument();
                                doc.LoadXml(result);
                                string jsonText = JsonConvert.SerializeXmlNode(doc, Newtonsoft.Json.Formatting.None, false);
                                dynamic data = JObject.Parse(jsonText);
                                var responsestring = "";

                                if (jsonText != null && jsonText != string.Empty)
                                {
                                    responsestring = data["string"];
                                    XmlDocument doc1 = new XmlDocument();
                                    doc1.LoadXml(responsestring);
                                    string jsonText1 = JsonConvert.SerializeXmlNode(doc1, Newtonsoft.Json.Formatting.Indented, true);
                                    data1 = JObject.Parse(jsonText1);


                                    if (data1 != null)
                                    {
                                        if (data1.StatusCode == "0")
                                        {
                                            var transaction = _context.tb_PinlessRequestTransaction.Create();
                                            transaction.AccountNo = model.account ?? string.Empty;
                                            transaction.Amount = model.amount ?? string.Empty;
                                            transaction.AuthorizationCode = data1.AuthorizationCode == null ? string.Empty : data1.AuthorizationCode;
                                            transaction.ProdCode = model.prodCode ?? string.Empty;
                                            //transaction.ReceiptInfo = data1.ReceiptInfo ?? string.Empty;
                                            //transaction.ReceiptInfo = data1.ReceiptInfo == null ? string.Empty : data1.ReceiptInfo;
                                            transaction.ReceiptInfo = "";                                            
                                            transaction.ReceiptNo = receiptNo ?? string.Empty;
                                            transaction.StatusCode = data1.StatusCode ?? string.Empty;
                                            transaction.StatusDescription = data1.StatusDescription ?? string.Empty;
                                            transaction.Timestamp = currentTime;
                                            transaction.TransDate = data1.TransDate ?? string.Empty;
                                            transaction.TransNo = data1.TransNo ?? string.Empty;
                                            transaction.TransTime = data1.TransTime ?? string.Empty;
                                            transaction.UserId = model.userId;
                                            transaction.AmountDeducted = deductamt;
                                            transaction.AmountCurrency = model.amountCurrency;
                                            transaction.AmountDeductedCurrency = user.Currency;
                                            transaction.Commission = commissionAmount;



                                            user.Wallet = newAmount;

                                            _context.tb_PinlessRequestTransaction.Add(transaction);



                                            if (_context.SaveChanges() > 0)
                                            {


                                                var RequestXml1 = "<RequestXml> <Type>PinlessRequest2ndStep</Type> <TerminalId>" + TerminalID + "</TerminalId> <Password>" + Password + "</Password> <Language>eng</Language> <ReceiptNo>" + receiptNo + "</ReceiptNo> <AccountNo>" + model.account + "</AccountNo> <Amount>" + model.amount + "</Amount> <ClerkId>1</ClerkId> <ProdCode>" + model.prodCode + "</ProdCode> </RequestXml>";

                                                HttpWebRequest webreq12 = (HttpWebRequest)WebRequest.Create(RequestUrlDuTopUp + RequestXml1);
                                                webreq12.ContentType = "application/json";
                                                //webreq.Accept = "text/xml";
                                                webreq12.Headers.Clear();
                                                webreq12.Method = "GET";
                                                Encoding encode12 = Encoding.GetEncoding("utf-8");
                                                HttpWebResponse webres12 = null;
                                                webres12 = (HttpWebResponse)webreq12.GetResponse();
                                                Stream reader21 = null;
                                                reader21 = webres12.GetResponseStream();
                                                StreamReader sreader123 = new StreamReader(reader21, encode12, true);
                                                string result12 = sreader123.ReadToEnd();

                                                XmlDocument doc12 = new XmlDocument();
                                                doc12.LoadXml(result12);
                                                string jsonText13 = JsonConvert.SerializeXmlNode(doc12, Newtonsoft.Json.Formatting.None, false);
                                                dynamic data3 = JObject.Parse(jsonText13);
                                                var responsestring1 = "";

                                                if (jsonText13 != null && jsonText13 != string.Empty)
                                                {
                                                    responsestring1 = data3["string"];
                                                    XmlDocument doc123 = new XmlDocument();
                                                    doc123.LoadXml(responsestring1);
                                                    string jsonText12 = JsonConvert.SerializeXmlNode(doc123, Newtonsoft.Json.Formatting.Indented, true);
                                                    data2 = JObject.Parse(jsonText12);
                                                    if (data2 != null)
                                                    {
                                                        if (data2.StatusCode == "0")
                                                        {
                                                            transaction.StatusCode = data2.StatusCode ?? string.Empty;
                                                            transaction.StatusDescription = data2.StatusDescription ?? string.Empty;
                                                            _context.SaveChanges();
                                                            returnModel.data = data2;
                                                            returnModel.status = true;
                                                            returnModel.wallet = user.Wallet.ToString();
                                                            returnModel.message = "Success";
                                                        }
                                                        else
                                                        {

                                                            if (data2.StatusCode == "1")
                                                            {
                                                                transaction.StatusCode = data2.StatusCode ?? string.Empty;
                                                                transaction.StatusDescription = data2.StatusDescription ?? string.Empty;
                                                                //user.Wallet = user.Wallet + amount;04-02-2019
                                                                user.Wallet = user.Wallet + deductamt
;
                                                                try
                                                                {
                                                                    _context.SaveChanges();
                                                                }
                                                                catch (Exception ex)
                                                                { }

                                                                returnModel.data = data2;
                                                                returnModel.status = false;
                                                                returnModel.wallet = user.Wallet.ToString();
                                                                returnModel.message = "Check your provider";
                                                            }
                                                            else
                                                            {
                                                                returnModel.data = data2;
                                                                returnModel.status = true;
                                                                returnModel.wallet = user.Wallet.ToString();
                                                                returnModel.message = "Success";
                                                            }
                                                        }
                                                    }
                                                    else
                                                    {
                                                        returnModel.data = data2;
                                                        returnModel.status = false;
                                                        returnModel.wallet = user.Wallet.ToString();
                                                        returnModel.message = "Failed";
                                                    }
                                                }
                                                else
                                                {
                                                    returnModel.data = data2;
                                                    returnModel.status = false;
                                                    returnModel.wallet = user.Wallet.ToString();
                                                    returnModel.message = "Failed";
                                                }
                                            }
                                            else
                                            {
                                                returnModel.data = data2;
                                                returnModel.status = false;
                                                returnModel.wallet = user.Wallet.ToString();
                                                returnModel.message = "Failed";
                                            }
                                        }
                                        else
                                        {
                                            returnModel.data = data2;
                                            returnModel.status = false;
                                            returnModel.wallet = user.Wallet.ToString();
                                            returnModel.message = "Failed";
                                        }
                                    }
                                    else
                                    {
                                        returnModel.data = data2;
                                        returnModel.status = false;
                                        returnModel.wallet = user.Wallet.ToString();
                                        returnModel.message = "Failed";
                                    }
                                }
                                else
                                {
                                    returnModel.data = data2;
                                    returnModel.status = false;
                                    returnModel.wallet = user.Wallet.ToString();
                                    returnModel.message = "Failed";
                                }
                            }
                            else
                            {
                                var RequestXml = "<RequestXml> <Type>PinlessRequest</Type> <TerminalId>" + TerminalID + "</TerminalId> <Password>" + Password + "</Password> <Language>eng</Language> <ReceiptNo>" + receiptNo + "</ReceiptNo> <AccountNo>" + model.account + "</AccountNo> <Amount>" + model.amount + "</Amount> <ClerkId>1</ClerkId> <ProdCode>" + model.prodCode + "</ProdCode> </RequestXml>";

                                HttpWebRequest webreq = (HttpWebRequest)WebRequest.Create(RequestUrl + RequestXml);
                                webreq.ContentType = "application/json";
                                //webreq.Accept = "text/xml";
                                webreq.Headers.Clear();
                                webreq.Method = "GET";
                                Encoding encode = Encoding.GetEncoding("utf-8");
                                HttpWebResponse webres = null;
                                webres = (HttpWebResponse)webreq.GetResponse();
                                Stream reader = null;
                                reader = webres.GetResponseStream();
                                StreamReader sreader = new StreamReader(reader, encode, true);
                                string result = sreader.ReadToEnd();


                                XmlDocument doc = new XmlDocument();
                                doc.LoadXml(result);
                                string jsonText = JsonConvert.SerializeXmlNode(doc, Newtonsoft.Json.Formatting.None, false);
                                dynamic data = JObject.Parse(jsonText);
                                var responsestring = "";

                                if (jsonText != null && jsonText != string.Empty)
                                {
                                    responsestring = data["string"];
                                    XmlDocument doc1 = new XmlDocument();
                                    doc1.LoadXml(responsestring);
                                    string jsonText1 = JsonConvert.SerializeXmlNode(doc1, Newtonsoft.Json.Formatting.Indented, true);
                                    data1 = JObject.Parse(jsonText1);


                                    if (data1 != null)
                                    {
                                        if (data1.StatusCode == "0")
                                        {
                                            var transaction = _context.tb_PinlessRequestTransaction.Create();
                                            transaction.AccountNo = model.account ?? string.Empty;
                                            transaction.Amount = model.amount ?? string.Empty;
                                            transaction.AuthorizationCode = data1.AuthorizationCode == null ? string.Empty : data1.AuthorizationCode;
                                            transaction.ProdCode = model.prodCode ?? string.Empty;
                                            //transaction.ReceiptInfo = data1.ReceiptInfo ?? string.Empty;
                                            //transaction.ReceiptInfo = data1.ReceiptInfo == null ? string.Empty : data1.ReceiptInfo;
                                            //if (data1.ReceiptInfo != null)
                                            //{
                                                transaction.ReceiptInfo = "";
                                            //}
                                            transaction.ReceiptNo = receiptNo ?? string.Empty;
                                            transaction.StatusCode = data1.StatusCode ?? string.Empty;
                                            transaction.StatusDescription = data1.StatusDescription ?? string.Empty;
                                            transaction.Timestamp = currentTime;
                                            transaction.TransDate = data1.TransDate ?? string.Empty;
                                            transaction.TransNo = data1.TransNo ?? string.Empty;
                                            transaction.TransTime = data1.TransTime ?? string.Empty;
                                            transaction.UserId = model.userId;
                                            transaction.AmountDeducted = deductamt;
                                            transaction.AmountCurrency = model.amountCurrency;
                                            transaction.AmountDeductedCurrency = user.Currency;
                                            transaction.Commission = commissionAmount;



                                            user.Wallet = newAmount;

                                            _context.tb_PinlessRequestTransaction.Add(transaction);



                                            if (_context.SaveChanges() > 0)
                                            {


                                                var RequestXml1 = "<RequestXml> <Type>PinlessRequest2ndStep</Type> <TerminalId>" + TerminalID + "</TerminalId> <Password>" + Password + "</Password> <Language>eng</Language> <ReceiptNo>" + receiptNo + "</ReceiptNo> <AccountNo>" + model.account + "</AccountNo> <Amount>" + model.amount + "</Amount> <ClerkId>1</ClerkId> <ProdCode>" + model.prodCode + "</ProdCode> </RequestXml>";

                                                HttpWebRequest webreq12 = (HttpWebRequest)WebRequest.Create(RequestUrl + RequestXml1);
                                                webreq12.ContentType = "application/json";
                                                //webreq.Accept = "text/xml";
                                                webreq12.Headers.Clear();
                                                webreq12.Method = "GET";
                                                Encoding encode12 = Encoding.GetEncoding("utf-8");
                                                HttpWebResponse webres12 = null;
                                                webres12 = (HttpWebResponse)webreq12.GetResponse();
                                                Stream reader21 = null;
                                                reader21 = webres12.GetResponseStream();
                                                StreamReader sreader123 = new StreamReader(reader21, encode12, true);
                                                string result12 = sreader123.ReadToEnd();

                                                XmlDocument doc12 = new XmlDocument();
                                                doc12.LoadXml(result12);
                                                string jsonText13 = JsonConvert.SerializeXmlNode(doc12, Newtonsoft.Json.Formatting.None, false);
                                                dynamic data3 = JObject.Parse(jsonText13);
                                                var responsestring1 = "";

                                                if (jsonText13 != null && jsonText13 != string.Empty)
                                                {
                                                    responsestring1 = data3["string"];
                                                    XmlDocument doc123 = new XmlDocument();
                                                    doc123.LoadXml(responsestring1);
                                                    string jsonText12 = JsonConvert.SerializeXmlNode(doc123, Newtonsoft.Json.Formatting.Indented, true);
                                                    data2 = JObject.Parse(jsonText12);
                                                    if (data2 != null)
                                                    {
                                                        if (data2.StatusCode == "0")
                                                        {
                                                            transaction.StatusCode = data2.StatusCode ?? string.Empty;
                                                            transaction.StatusDescription = data2.StatusDescription ?? string.Empty;
                                                            _context.SaveChanges();
                                                            returnModel.data = data2;
                                                            returnModel.status = true;
                                                            returnModel.wallet = user.Wallet.ToString();
                                                            returnModel.message = "Success";
                                                        }
                                                        else
                                                        {

                                                            if (data2.StatusCode == "1")
                                                            {
                                                                transaction.StatusCode = data2.StatusCode ?? string.Empty;
                                                                transaction.StatusDescription = data2.StatusDescription ?? string.Empty;
                                                                //user.Wallet = user.Wallet + amount;04-02-2019
                                                                user.Wallet = user.Wallet + deductamt;
                                                                try
                                                                {
                                                                    _context.SaveChanges();
                                                                }
                                                                catch (Exception ex)
                                                                { }

                                                                returnModel.data = data2;
                                                                returnModel.status = false;
                                                                returnModel.wallet = user.Wallet.ToString();
                                                                returnModel.message = "Check your provider";
                                                            }
                                                            else
                                                            {
                                                                returnModel.data = data2;
                                                                returnModel.status = true;
                                                                returnModel.wallet = user.Wallet.ToString();
                                                                returnModel.message = "Success";
                                                            }
                                                        }
                                                    }
                                                    else
                                                    {
                                                        returnModel.data = data2;
                                                        returnModel.status = false;
                                                        returnModel.wallet = user.Wallet.ToString();
                                                        returnModel.message = "Failed";
                                                    }
                                                }
                                                else
                                                {
                                                    returnModel.data = data2;
                                                    returnModel.status = false;
                                                    returnModel.wallet = user.Wallet.ToString();
                                                    returnModel.message = "Failed";
                                                }
                                            }
                                            else
                                            {
                                                returnModel.data = data2;
                                                returnModel.status = false;
                                                returnModel.wallet = user.Wallet.ToString();
                                                returnModel.message = "Failed";
                                            }
                                        }
                                        else
                                        {
                                            returnModel.data = data2;
                                            returnModel.status = false;
                                            returnModel.wallet = user.Wallet.ToString();
                                            returnModel.message = "Failed";
                                        }
                                    }
                                    else
                                    {
                                        returnModel.data = data2;
                                        returnModel.status = false;
                                        returnModel.wallet = user.Wallet.ToString();
                                        returnModel.message = "Failed";
                                    }
                                }
                                else
                                {
                                    returnModel.data = data2;
                                    returnModel.status = false;
                                    returnModel.wallet = user.Wallet.ToString();
                                    returnModel.message = "Failed";
                                }
                            }

                          
                        }
                        else
                        {
                            returnModel.status = false;
                            returnModel.message = "Insufficient Amount";
                            returnModel.wallet = user.Wallet.ToString();
                            return returnModel;
                        }
                    }
                    else
                    {
                        returnModel.status = false;
                        returnModel.message = "Failed";
                        returnModel.wallet = user.Wallet.ToString();
                        return returnModel;
                    }
                }
                else
                {
                    returnModel.status = false;
                    returnModel.message = "Failed";
                    returnModel.wallet = "0";
                    return returnModel;
                }
                return returnModel;
            }
            catch (Exception ex)
            {
                returnModel.status = false;
                returnModel.message = ex.Message;
                returnModel.wallet = "0";
                return returnModel;
            }
        }

        public object pinrequest(ClassLibrary.ServicePostModel.OnePrepayPostModel.PinRequest model)
        {
            var returnModel = new ClassLibrary.ServicePostModel.OnePrepayPostModel.PinRequestReturnModel();

            try
            {

                var fetchStoredCard = _context.SP_FETCH_STORED_VOUCHERS(model.type, model.ValueCode, model.userId).ToList()
               .Select(z => new SP_FETCH_STORED_VOUCHERS(z)).FirstOrDefault();
                if (fetchStoredCard != null)
                {
                    var user = _context.tb_User.FirstOrDefault(z => z.UserID == model.userId && z.IsActive);
                    if (user != null)
                    {

                        try
                        {
                            var rowIp = _context.tb_UserTransactionIp.Create();
                            rowIp.NetworkIp = model.ip;
                            rowIp.TimeStamp = System.DateTime.UtcNow;
                            rowIp.UserId = user.UserID;
                            _context.tb_UserTransactionIp.Add(rowIp);
                            _context.SaveChanges();
                        }
                        catch (Exception ex)
                        { }



                        var currecyRate = _context.tb_AdminCurrencyRate.Where(z => z.ToCurrency == user.Currency && z.FromCurrency == model.amountCurrency).FirstOrDefault();
                        if (currecyRate != null)
                        {
                            var amount = Convert.ToDecimal(Convert.ToDouble(model.ValueCode) * currecyRate.CurrencyRate);
                            var deductamt = amount;
                            var newAmount = user.Wallet;
                            decimal commissionAmount = Convert.ToDecimal(0);
                            if (user.UserType == (int)ClassLibrary.Enum.userType.Agent)
                            {
                                var commision = _context.tb_AgentCommisionRate.Where(z => z.AgentId == user.UserID && z.IsActive && z.ProductType == model.productType && z.ApiProvider == (int)ClassLibrary.Enum.apiProvider.OnePrepay).FirstOrDefault();
                                if (commision != null)
                                {
                                    commissionAmount = ((Convert.ToDecimal(amount) * Convert.ToDecimal(commision.Commission)) / 100);
                                    deductamt = Convert.ToDecimal(amount) - commissionAmount;
                                    newAmount = user.Wallet - Convert.ToDecimal(deductamt);
                                }
                                else
                                {
                                    newAmount = user.Wallet - Convert.ToDecimal(amount);
                                }
                            }
                            else
                            {
                                newAmount = user.Wallet - Convert.ToDecimal(amount);
                            }


                            if (user.Wallet >= deductamt && deductamt >= Convert.ToDecimal(0) && user.Wallet > Convert.ToDecimal(0))
                            {

                                try
                                {
                                    var response = _context.SP_INSERT_USED_STOREDCARD(model.userId, fetchStoredCard.PID, Convert.ToString(deductamt), user.Currency, commissionAmount).FirstOrDefault();
                                    if (Convert.ToInt64(response) > 0)
                                    {

                                        user.Wallet = newAmount;
                                        try
                                        {
                                            _context.SaveChanges();
                                            returnModel.wallet = user.Wallet.ToString();
                                            returnModel.hasImportedCards = true;
                                            returnModel.status = true;
                                            returnModel.message = "Success";
                                            returnModel.serialNumber = fetchStoredCard.SerialNumber;
                                            returnModel.pinNumber = fetchStoredCard.PinNumber;
                                            returnModel.data = null;
                                        }
                                        catch (Exception ex)
                                        {
                                            returnModel.status = false;
                                            returnModel.message = "Failed";
                                            returnModel.wallet = user.Wallet.ToString();
                                            returnModel.serialNumber = "";
                                            returnModel.pinNumber = "";
                                            returnModel.hasImportedCards = false;
                                            returnModel.data = null;
                                            return returnModel;
                                        }
                                    }
                                }
                                catch (Exception ex)
                                {
                                    returnModel.status = false;
                                    returnModel.message = "Failed";
                                    returnModel.wallet = user.Wallet.ToString();
                                    returnModel.serialNumber = "";
                                    returnModel.pinNumber = "";
                                    returnModel.hasImportedCards = false;
                                    returnModel.data = null;
                                    return returnModel;
                                }
                            }
                            else
                            {
                                returnModel.status = false;
                                returnModel.message = "Insufficient Amount";
                                returnModel.wallet = user.Wallet.ToString();
                                returnModel.serialNumber = "";
                                returnModel.pinNumber = "";
                                returnModel.hasImportedCards = false;
                                returnModel.data = null;
                                return returnModel;
                            }

                        }
                        else
                        {
                            returnModel.status = false;
                            returnModel.message = "Failed";
                            returnModel.serialNumber = "";
                            returnModel.pinNumber = "";
                            returnModel.hasImportedCards = false;
                            returnModel.data = null;
                            returnModel.wallet = user.Wallet.ToString();
                            return returnModel;
                        }
                    }
                    else
                    {
                        returnModel.status = false;
                        returnModel.message = "Failed";
                        returnModel.wallet = "0";
                        returnModel.serialNumber = "";
                        returnModel.pinNumber = "";
                        returnModel.hasImportedCards = false;
                        returnModel.data = null;
                        return returnModel;
                    }
                    return returnModel;
                }
                else
                {

                    var user = _context.tb_User.FirstOrDefault(z => z.UserID == model.userId && z.IsActive);
                    if (user != null)
                    {


                        try
                        {
                            var rowIp = _context.tb_UserTransactionIp.Create();
                            rowIp.NetworkIp = model.ip;
                            rowIp.TimeStamp = System.DateTime.UtcNow;
                            rowIp.UserId = user.UserID;
                            _context.tb_UserTransactionIp.Add(rowIp);
                            _context.SaveChanges();
                        }
                        catch (Exception ex)
                        { }



                        var currecyRate = _context.tb_AdminCurrencyRate.Where(z => z.ToCurrency == user.Currency && z.FromCurrency == model.amountCurrency).FirstOrDefault();
                        if (currecyRate != null)
                        {
                            var amount = Convert.ToDecimal(Convert.ToDouble(model.ValueCode) * currecyRate.CurrencyRate);
                            var deductamt = amount;
                            var newAmount = user.Wallet;
                            decimal commissionAmount = Convert.ToDecimal(0);
                            if (user.UserType == (int)ClassLibrary.Enum.userType.Agent)
                            {
                                var commision = _context.tb_AgentCommisionRate.Where(z => z.AgentId == user.UserID && z.IsActive && z.ProductType == model.productType && z.ApiProvider == (int)ClassLibrary.Enum.apiProvider.OnePrepay).FirstOrDefault();
                                if (commision != null)
                                {
                                    commissionAmount = ((Convert.ToDecimal(amount) * Convert.ToDecimal(commision.Commission)) / 100);
                                    deductamt = Convert.ToDecimal(amount) - commissionAmount;
                                    newAmount = user.Wallet - Convert.ToDecimal(deductamt);
                                }
                                else
                                {
                                    newAmount = user.Wallet - Convert.ToDecimal(amount);
                                }
                            }
                            else
                            {
                                newAmount = user.Wallet - Convert.ToDecimal(amount);
                            }

                            if (user.Wallet >= deductamt && deductamt >= Convert.ToDecimal(0) && user.Wallet > Convert.ToDecimal(0))
                            {

                                dynamic data1 = new JObject();
                                var receiptNo = OnePrepayRandomString(20, ClassLibrary.Enum.OnePrepayType.PinRequest.ToString());
                                var RequestXml3 = "<RequestXml><Type>PinRequest</Type><TerminalId>" + TerminalID + "</TerminalId><Password>" + Password + "</Password><Language>eng</Language><ReceiptNo>" + receiptNo + "</ReceiptNo><ValueCode>" + model.ValueCode + "</ValueCode><ClerkId>1</ClerkId><RefNo>" + receiptNo + "</RefNo><ProdCode>" + model.prodCode + "</ProdCode></RequestXml>";

                                HttpWebRequest webreq = (HttpWebRequest)WebRequest.Create(RequestUrl + RequestXml3);
                                webreq.ContentType = "application/json";
                                //webreq.Accept = "text/xml";
                                webreq.Headers.Clear();
                                webreq.Method = "GET";
                                Encoding encode = Encoding.GetEncoding("utf-8");
                                HttpWebResponse webres = null;
                                webres = (HttpWebResponse)webreq.GetResponse();
                                Stream reader = null;
                                reader = webres.GetResponseStream();
                                StreamReader sreader = new StreamReader(reader, encode, true);
                                string result = sreader.ReadToEnd();

                                XmlDocument doc = new XmlDocument();
                                doc.LoadXml(result);
                                string jsonText = JsonConvert.SerializeXmlNode(doc, Newtonsoft.Json.Formatting.None, false);
                                dynamic data = JObject.Parse(jsonText);
                                var responsestring = "";

                                if (jsonText != null && jsonText != string.Empty)
                                {
                                    responsestring = data["string"];
                                    XmlDocument doc1 = new XmlDocument();
                                    doc1.LoadXml(responsestring);
                                    string jsonText1 = JsonConvert.SerializeXmlNode(doc1, Newtonsoft.Json.Formatting.Indented, true);
                                    data1 = JObject.Parse(jsonText1);

                                    if (data1 != null)
                                    {
                                        if (data1.StatusCode == "0")
                                        {
                                            var transaction = _context.tb_PinRequestTransaction.Create();
                                            if (data1.PinData != null)
                                            {
                                                transaction.ExpiryDate = data1.PinData.ExpiryDate ?? string.Empty;
                                                transaction.Pin = data1.PinData.Pin ?? string.Empty;
                                                transaction.PinBatch = string.Empty;
                                                //transaction.PinBatch = data1.PinData.PinBatch ?? string.Empty;
                                                transaction.PinSerial = data1.PinData.PinSerial ?? string.Empty;
                                            }
                                            else
                                            {
                                                transaction.ExpiryDate = string.Empty;
                                                transaction.Pin = string.Empty;
                                                transaction.PinBatch = string.Empty;
                                                transaction.PinSerial = string.Empty;
                                            }
                                            transaction.ProdCode = model.prodCode ?? string.Empty;
                                            transaction.ReceiptNo = receiptNo ?? string.Empty;
                                            transaction.RefNo = receiptNo ?? string.Empty;
                                            transaction.StatusCode = data1.StatusCode ?? string.Empty;
                                            transaction.StatusDescription = data1.StatusDescription ?? string.Empty;
                                            transaction.Timestamp = currentTime;
                                            transaction.TransDate = data1.TransDate ?? string.Empty;
                                            transaction.TransNo = data1.TransNo ?? string.Empty;
                                            transaction.TransTime = data1.TransTime ?? string.Empty;
                                            transaction.UserId = model.userId;
                                            transaction.ValueCode = model.ValueCode ?? string.Empty;
                                            transaction.AmountCurrency = model.amountCurrency;
                                            transaction.AmountDeductedCurrency = user.Currency;
                                            transaction.AmountDeducted = deductamt;
                                            transaction.Commission = commissionAmount; 

                                            user.Wallet = newAmount;

                                            _context.tb_PinRequestTransaction.Add(transaction);


                                            if (_context.SaveChanges() > 0)
                                            {
                                                returnModel.data = data1;
                                                returnModel.status = true;
                                                returnModel.wallet = user.Wallet.ToString();
                                                returnModel.message = "Success";
                                                returnModel.serialNumber = "";
                                                returnModel.pinNumber = "";
                                                returnModel.hasImportedCards = false;
                                            }
                                            else
                                            {
                                                returnModel.data = data1;
                                                returnModel.status = false;
                                                returnModel.wallet = user.Wallet.ToString();
                                                returnModel.message = "Failed";
                                                returnModel.serialNumber = "";
                                                returnModel.pinNumber = "";
                                                returnModel.hasImportedCards = false;
                                            }
                                        }
                                        else
                                        {
                                            returnModel.data = data1;
                                            returnModel.status = false;
                                            returnModel.message = "Failed";
                                            returnModel.wallet = user.Wallet.ToString();
                                            returnModel.serialNumber = "";
                                            returnModel.pinNumber = "";
                                            returnModel.hasImportedCards = false;
                                        }
                                    }
                                    else
                                    {
                                        returnModel.data = data1;
                                        returnModel.status = false;
                                        returnModel.wallet = user.Wallet.ToString();
                                        returnModel.message = "Failed";
                                        returnModel.serialNumber = "";
                                        returnModel.pinNumber = "";
                                        returnModel.hasImportedCards = false;
                                    }
                                }
                                else
                                {
                                    returnModel.data = data1;
                                    returnModel.status = false;
                                    returnModel.wallet = user.Wallet.ToString();
                                    returnModel.message = "Failed";
                                    returnModel.serialNumber = "";
                                    returnModel.pinNumber = "";
                                    returnModel.hasImportedCards = false;
                                }
                            }
                            else
                            {
                                returnModel.status = false;
                                returnModel.message = "Insufficient Amount";
                                returnModel.wallet = user.Wallet.ToString();
                                returnModel.serialNumber = "";
                                returnModel.pinNumber = "";
                                returnModel.hasImportedCards = false;
                                return returnModel;
                            }
                        }
                        else
                        {
                            returnModel.status = false;
                            returnModel.message = "Failed";
                            returnModel.wallet = user.Wallet.ToString();
                            returnModel.serialNumber = "";
                            returnModel.pinNumber = "";
                            returnModel.hasImportedCards = false;
                            return returnModel;
                        }
                    }
                    else
                    {
                        returnModel.status = false;
                        returnModel.message = "Failed";
                        returnModel.wallet = "0";
                        returnModel.serialNumber = "";
                        returnModel.pinNumber = "";
                        returnModel.hasImportedCards = false;
                        return returnModel;
                    }
                }
            }
            catch (Exception ex)
            {
                returnModel.status = false;
                returnModel.message = ex.Message;
                returnModel.wallet = "0";
                returnModel.serialNumber = "";
                returnModel.pinNumber = "";
                returnModel.hasImportedCards = false;
                return returnModel;
            }
            return returnModel;
        }

        public object pinlessrequestfirststep(ClassLibrary.ServicePostModel.OnePrepayPostModel.PinlessRequest model)
        {
            var returnModel = new ClassLibrary.ServicePostModel.OnePrepayPostModel.PinlessRequestReturnModel();

            try
            {

                var user = _context.tb_User.FirstOrDefault(z => z.UserID == model.userId && z.IsActive);
                if (user != null)
                {

                    try
                    {
                        var rowIp = _context.tb_UserTransactionIp.Create();
                        rowIp.NetworkIp = model.ip;
                        rowIp.TimeStamp = System.DateTime.UtcNow;
                        rowIp.UserId = user.UserID;
                        _context.tb_UserTransactionIp.Add(rowIp);
                        _context.SaveChanges();
                    }
                    catch (Exception ex)
                    { }



                    var currecyRate = _context.tb_AdminCurrencyRate.Where(z => z.ToCurrency == user.Currency && z.FromCurrency == model.amountCurrency).FirstOrDefault();
                    if (currecyRate != null)
                    {
                        var amount = Convert.ToDecimal(Convert.ToDouble(model.amount) * currecyRate.CurrencyRate);
                        if (user.Wallet >= Convert.ToDecimal(Convert.ToDouble(model.amount) * currecyRate.CurrencyRate))
                        {
                            dynamic data1 = new JObject();
                            dynamic data2 = new JObject();

                            var receiptNo = OnePrepayRandomString(20, ClassLibrary.Enum.OnePrepayType.PinlessRequest.ToString());

                            var RequestXml = "<RequestXml> <Type>PinlessRequest</Type> <TerminalId>" + TerminalID + "</TerminalId> <Password>" + Password + "</Password> <Language>eng</Language> <ReceiptNo>" + receiptNo + "</ReceiptNo> <AccountNo>" + model.account + "</AccountNo> <Amount>" + model.amount + "</Amount> <ClerkId>1</ClerkId> <ProdCode>" + model.prodCode + "</ProdCode> </RequestXml>";

                            HttpWebRequest webreq = (HttpWebRequest)WebRequest.Create(RequestUrl + RequestXml);
                            webreq.ContentType = "application/json";
                            //webreq.Accept = "text/xml";
                            webreq.Headers.Clear();
                            webreq.Method = "GET";
                            Encoding encode = Encoding.GetEncoding("utf-8");
                            HttpWebResponse webres = null;
                            webres = (HttpWebResponse)webreq.GetResponse();
                            Stream reader = null;
                            reader = webres.GetResponseStream();
                            StreamReader sreader = new StreamReader(reader, encode, true);
                            string result = sreader.ReadToEnd();


                            XmlDocument doc = new XmlDocument();
                            doc.LoadXml(result);
                            string jsonText = JsonConvert.SerializeXmlNode(doc, Newtonsoft.Json.Formatting.None, false);
                            dynamic data = JObject.Parse(jsonText);
                            var responsestring = "";

                            if (jsonText != null && jsonText != string.Empty)
                            {
                                responsestring = data["string"];
                                XmlDocument doc1 = new XmlDocument();
                                doc1.LoadXml(responsestring);
                                string jsonText1 = JsonConvert.SerializeXmlNode(doc1, Newtonsoft.Json.Formatting.Indented, true);
                                data1 = JObject.Parse(jsonText1);


                                if (data1 != null)
                                {
                                    if (data1.StatusCode == "0")
                                    {
                                        //var transaction = _context.tb_PinlessRequestTransaction.Create();
                                        //transaction.AccountNo = model.account ?? string.Empty;
                                        //transaction.Amount = model.amount ?? string.Empty;
                                        //transaction.AuthorizationCode = data1.AuthorizationCode == null ? string.Empty : data1.AuthorizationCode;
                                        //transaction.ProdCode = model.prodCode ?? string.Empty;
                                        ////transaction.ReceiptInfo = data1.ReceiptInfo ?? string.Empty;
                                        //transaction.ReceiptInfo = data1.ReceiptInfo == null ? string.Empty : data1.ReceiptInfo;
                                        //transaction.ReceiptNo = receiptNo ?? string.Empty;
                                        //transaction.StatusCode = data1.StatusCode ?? string.Empty;
                                        //transaction.StatusDescription = data1.StatusDescription ?? string.Empty;
                                        //transaction.Timestamp = currentTime;
                                        //transaction.TransDate = data1.TransDate ?? string.Empty;
                                        //transaction.TransNo = data1.TransNo ?? string.Empty;
                                        //transaction.TransTime = data1.TransTime ?? string.Empty;
                                        //transaction.UserId = model.userId;


                                        //var newAmount = user.Wallet;
                                        //if (user.UserType == (int)ClassLibrary.Enum.userType.Agent)
                                        //{
                                        //    var commision = _context.tb_AgentCommisionRate.Where(z => z.AgentId == user.UserID && z.IsActive && z.ProductType == model.productType && z.ApiProvider == (int)ClassLibrary.Enum.apiProvider.OnePrepay).FirstOrDefault();
                                        //    if (commision != null)
                                        //    {
                                        //        var deductamt = Convert.ToDecimal(amount) - ((Convert.ToDecimal(amount) * Convert.ToDecimal(commision.Commission)) / 100);
                                        //        newAmount = user.Wallet - Convert.ToDecimal(deductamt);
                                        //        transaction.AmountDeducted = deductamt;
                                        //    }
                                        //    else
                                        //    {
                                        //        newAmount = user.Wallet - Convert.ToDecimal(amount);
                                        //        transaction.AmountDeducted = amount;
                                        //    }
                                        //}
                                        //else
                                        //{
                                        //    newAmount = user.Wallet - Convert.ToDecimal(amount);
                                        //    transaction.AmountDeducted = amount;
                                        //}

                                        //user.Wallet = newAmount;

                                        //_context.tb_PinlessRequestTransaction.Add(transaction);



                                        //if (_context.SaveChanges() > 0)
                                        //{
                                        //    returnModel.data = data1;
                                        //    returnModel.status = true;
                                        //    returnModel.wallet = user.Wallet.ToString();
                                        //    returnModel.message = "Success";
                                        //}
                                        //else
                                        //{
                                        //    returnModel.data = data1;
                                        //    returnModel.status = false;
                                        //    returnModel.wallet = user.Wallet.ToString();
                                        //    returnModel.message = "Failed";
                                        //}
                                        returnModel.data = data1;
                                        returnModel.status = true;
                                        returnModel.wallet = user.Wallet.ToString();
                                        returnModel.message = "Success";
                                    }
                                    else
                                    {
                                        returnModel.data = data1;
                                        returnModel.status = false;
                                        returnModel.wallet = user.Wallet.ToString();
                                        returnModel.message = "Failed";
                                    }
                                }
                                else
                                {
                                    returnModel.data = data1;
                                    returnModel.status = false;
                                    returnModel.wallet = user.Wallet.ToString();
                                    returnModel.message = "Failed";
                                }
                            }
                            else
                            {
                                returnModel.data = data1;
                                returnModel.status = false;
                                returnModel.wallet = user.Wallet.ToString();
                                returnModel.message = "Failed";
                            }
                        }
                        else
                        {
                            returnModel.status = false;
                            returnModel.message = "Insufficient Amount";
                            returnModel.wallet = user.Wallet.ToString();
                            return returnModel;
                        }
                    }
                    else
                    {
                        returnModel.status = false;
                        returnModel.message = "Failed";
                        returnModel.wallet = user.Wallet.ToString();
                        return returnModel;
                    }
                }
                else
                {
                    returnModel.status = false;
                    returnModel.message = "Failed";
                    returnModel.wallet = "0";
                    return returnModel;
                }
                return returnModel;
            }
            catch (Exception ex)
            {
                returnModel.status = false;
                returnModel.message = ex.Message;
                returnModel.wallet = "0";
                return returnModel;
            }
        }

        public object getbalancest()
        {
            var returnModel = new ClassLibrary.ServicePostModel.OnePrepayPostModel.GetbalancestReturnModel();

            try
            {

                dynamic data1 = new JObject();
                var receiptNo = OnePrepayRandomString(20, ClassLibrary.Enum.OnePrepayType.PinRequest.ToString());
                var RequestXml3 = "<RequestXml><Type>GetBalanceSt</Type><TerminalId>" + TerminalID + "</TerminalId><Password>" + Password + "</Password><Language>eng</Language></RequestXml>";

                HttpWebRequest webreq = (HttpWebRequest)WebRequest.Create(RequestUrl + RequestXml3);
                webreq.ContentType = "application/json";
                //webreq.Accept = "text/xml";
                webreq.Headers.Clear();
                webreq.Method = "GET";
                Encoding encode = Encoding.GetEncoding("utf-8");
                HttpWebResponse webres = null;
                webres = (HttpWebResponse)webreq.GetResponse();
                Stream reader = null;
                reader = webres.GetResponseStream();
                StreamReader sreader = new StreamReader(reader, encode, true);
                string result = sreader.ReadToEnd();

                XmlDocument doc = new XmlDocument();
                doc.LoadXml(result);
                string jsonText = JsonConvert.SerializeXmlNode(doc, Newtonsoft.Json.Formatting.None, false);
                dynamic data = JObject.Parse(jsonText);
                var responsestring = "";

                if (jsonText != null && jsonText != string.Empty)
                {
                    responsestring = data["string"];
                    XmlDocument doc1 = new XmlDocument();
                    doc1.LoadXml(responsestring);
                    string jsonText1 = JsonConvert.SerializeXmlNode(doc1, Newtonsoft.Json.Formatting.Indented, true);
                    data1 = JObject.Parse(jsonText1);

                    if (data1 != null)
                    {
                        returnModel.data = data1;
                        returnModel.status = true;
                        returnModel.message = "Success";
                    }
                    else
                    {
                        returnModel.data = data1;
                        returnModel.status = false;
                        returnModel.message = "Failed";
                    }
                }
                else
                {
                    returnModel.data = data1;
                    returnModel.status = false;
                    returnModel.message = "Failed";
                }

            }
            catch (Exception ex)
            {
                returnModel.status = false;
                returnModel.message = ex.Message;
                return returnModel;
            }
            return returnModel;
        }
    }
}
