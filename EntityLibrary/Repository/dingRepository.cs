﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace EntityLibrary.Repository
{
    public class dingRepository : BaseReference
    {
        public object GetAccountLookup(ClassLibrary.ServicePostModel.DingPostModel.GetAccountLookup model)
        {
            var returnModel = new ClassLibrary.ServicePostModel.DingPostModel.GetAccountLookupResponse();
            dynamic data = new System.Dynamic.ExpandoObject();

            try
            {
                //using (var client = new HttpClient())
                //{
                //    client.BaseAddress = new Uri(dingUrl);
                //    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json")); 
                //    var response = client.PostAsJsonAsync("/api/V1/GetAccountLookup", model).Result;
                //    if (response.IsSuccessStatusCode)
                //    {
                //        using (HttpContent content = response.Content)
                //        {
                //            // ... Read the string.
                //            Task<string> result = content.ReadAsStringAsync();
                //            data = JObject.Parse(result.Result);
                //        }
                //    }
                //}
                //ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                //var httpWebRequest = (HttpWebRequest)WebRequest.Create(dingUrl + "/api/V1/GetAccountLookup");
                //httpWebRequest.ContentType = "application/json";
                //httpWebRequest.Method = "POST";
                //httpWebRequest.Headers.Add("api_key", "87b9PEIh1FU6evRJS2gcij");

                //Encoding encoding = new UTF8Encoding();
                //var datamodel = JsonConvert.SerializeObject(model);
                //byte[] dataToPost = encoding.GetBytes(datamodel);

                //httpWebRequest.ContentLength = dataToPost.Length;  


                //Stream stream = httpWebRequest.GetRequestStream();
                //stream.Write(dataToPost, 0, dataToPost.Length);
                //stream.Close();

                //var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                //using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                //{
                //    var result = streamReader.ReadToEnd();
                //}


                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                var requestString = dingUrl + "/api/V1/GetAccountLookup?accountNumber=" + model.accountNumber;
                var request = (HttpWebRequest)WebRequest.Create(requestString);
                request.Method = "GET";
                request.ContentType = "application/json";
                request.Headers.Add("api_key", ding_apiKey);
                var response = (HttpWebResponse)request.GetResponse();
                var responseString = new System.IO.StreamReader(response.GetResponseStream()).ReadToEnd();
                data = Newtonsoft.Json.JsonConvert.DeserializeObject(responseString);






                returnModel.status = true;
                returnModel.message = "Success";
                returnModel.data = data;
            }
            catch (Exception ex)
            {
                returnModel.status = false;
                returnModel.message = ex.Message;
                returnModel.data = data;
            }
            return returnModel;
            //var requestString = dingUrl + "/api/V1/GetAccountLookup";
            //var request = (HttpWebRequest)WebRequest.Create(requestString);
            //var response = (HttpWebResponse)request.GetResponse();
            //var responseString = new System.IO.StreamReader(response.GetResponseStream()).ReadToEnd();
            //data = Newtonsoft.Json.JsonConvert.DeserializeObject(responseString);
        }

        public object GetProviders(ClassLibrary.ServicePostModel.DingPostModel.GetProviders model)
        {
            var returnModel = new ClassLibrary.ServicePostModel.DingPostModel.GetProvidersResponse();
            dynamic data = new System.Dynamic.ExpandoObject();

            try
            {
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                var requestString = dingUrl + "/api/V1/GetProviders?accountNumber=" + model.accountNumber;

                if (model.providerCodes != null && model.providerCodes != string.Empty)
                {
                    requestString = requestString + "&providerCodes=" + model.providerCodes;
                }

                if (model.countryIsos != null && model.countryIsos != string.Empty)
                {
                    requestString = requestString + "&countryIsos=" + model.countryIsos;
                }

                if (model.regionCodes != null && model.regionCodes != string.Empty)
                {
                    requestString = requestString + "&regionCodes=" + model.regionCodes;
                }

                var request = (HttpWebRequest)WebRequest.Create(requestString);
                request.Method = "GET";
                request.ContentType = "application/json";
                request.Headers.Add("api_key", ding_apiKey);
                var response = (HttpWebResponse)request.GetResponse();
                var responseString = new System.IO.StreamReader(response.GetResponseStream()).ReadToEnd();
                data = Newtonsoft.Json.JsonConvert.DeserializeObject(responseString);

                returnModel.status = true;
                returnModel.message = "Success";
                returnModel.data = data;
            }
            catch (Exception ex)
            {
                returnModel.status = false;
                returnModel.message = ex.Message;
                returnModel.data = data;
            }
            return returnModel;
        }

        public object GetProducts(ClassLibrary.ServicePostModel.DingPostModel.GetProducts model)
        {
            var returnModel = new ClassLibrary.ServicePostModel.DingPostModel.GetProductsResponse();
            dynamic data = new System.Dynamic.ExpandoObject();

            try
            {
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                var requestString = dingUrl + "/api/V1/GetProducts?accountNumber=" + model.accountNumber;

                if (model.providerCodes != null && model.providerCodes != string.Empty)
                {
                    requestString = requestString + "&providerCodes=" + model.providerCodes;
                }

                if (model.countryIsos != null && model.countryIsos != string.Empty)
                {
                    requestString = requestString + "&countryIsos=" + model.countryIsos;
                }

                if (model.regionCodes != null && model.regionCodes != string.Empty)
                {
                    requestString = requestString + "&regionCodes=" + model.regionCodes;
                }

                if (model.skuCodes != null && model.skuCodes != string.Empty)
                {
                    requestString = requestString + "&skuCodes=" + model.skuCodes;
                }

                if (model.benefits != null && model.benefits != string.Empty)
                {
                    requestString = requestString + "&benefits=" + model.benefits;
                }


                var request = (HttpWebRequest)WebRequest.Create(requestString);
                request.Method = "GET";
                request.ContentType = "application/json";
                request.Headers.Add("api_key", ding_apiKey);
                var response = (HttpWebResponse)request.GetResponse();
                var responseString = new System.IO.StreamReader(response.GetResponseStream()).ReadToEnd();
                data = Newtonsoft.Json.JsonConvert.DeserializeObject(responseString);

                returnModel.status = true;
                returnModel.message = "Success";
                returnModel.data = data;
            }
            catch (Exception ex)
            {
                returnModel.status = false;
                returnModel.message = ex.Message;
                returnModel.data = data;
            }
            return returnModel;
        }

        public object SendTransfer(ClassLibrary.ServicePostModel.DingPostModel.SendTransfer model)
        {
            var returnModel = new ClassLibrary.ServicePostModel.DingPostModel.SendTransferResponse();

            var user = _context.tb_User.FirstOrDefault(z => z.UserID == model.userId && z.IsActive);
            if (user != null)
            {

                try
                {
                    var rowIp = _context.tb_UserTransactionIp.Create();
                    rowIp.NetworkIp = model.ip;
                    rowIp.TimeStamp = System.DateTime.UtcNow;
                    rowIp.UserId = user.UserID;
                    _context.tb_UserTransactionIp.Add(rowIp);
                    _context.SaveChanges();
                }
                catch (Exception ex)
                {

                }


                //var currecyRate = _context.tb_AdminCurrencyRate.Where(z => z.Currency == model.currency).FirstOrDefault();
                var currecyRate = new tb_AdminCurrencyRate();
                if (model.switchCurrency)
                {
                    var deductamt = Convert.ToDecimal(model.amount);
                    var newSendValue = Convert.ToString(model.amount);
                    var newAmount = user.Wallet;
                    decimal commissionAmount = Convert.ToDecimal(0);
                    var commision = _context.tb_AgentCommisionRate.Where(z => z.AgentId == user.UserID && z.IsActive && z.ProductType == model.productType && z.ApiProvider == (int)ClassLibrary.Enum.apiProvider.Ding).ToList();
                    if (user.UserType == (int)ClassLibrary.Enum.userType.Agent)
                    {
                        var agentCommission = commision.Where(z => z.IsAdminCommission == false).FirstOrDefault();
                        if (agentCommission != null)
                        {
                            commissionAmount = ((Convert.ToDecimal(model.amount) * Convert.ToDecimal(agentCommission.Commission)) / 100);
                            deductamt = Convert.ToDecimal(model.amount) - commissionAmount;
                            newSendValue = Convert.ToString(deductamt);
                            //newAmount = user.Wallet - Convert.ToDecimal(deductamt);
                        }
                        //else
                        //{
                        //    newAmount = user.Wallet - Convert.ToDecimal(amount); ;
                        //}
                    }
                    //else
                    //{
                    //    newAmount = user.Wallet - Convert.ToDecimal(amount);
                    //}
                    var admincommissionAmount = Convert.ToDecimal(0);
                    var adminCommission = commision.Where(z => z.IsAdminCommission == true).FirstOrDefault();
                    if (adminCommission != null)
                    {
                        admincommissionAmount = ((Convert.ToDecimal(newSendValue) * Convert.ToDecimal(adminCommission.Commission)) / 100);
                        var admindeductamt = Convert.ToDecimal(newSendValue) - admincommissionAmount;
                        newSendValue = Convert.ToString(admindeductamt);
                    }


                    //currecyRate = _context.tb_AdminCurrencyRate.Where(z => z.FromCurrency == user.Currency && z.ToCurrency == model.amountCurrency).FirstOrDefault();14-11-2018
                    currecyRate = _context.tb_AdminCurrencyRate.Where(z => z.FromCurrency == model.amountCurrency && z.ToCurrency == user.Currency).FirstOrDefault();//ask rahul
                    if (currecyRate != null)
                    {
                        var newDeductamtAmount = Convert.ToDecimal(Convert.ToDouble(deductamt) * currecyRate.CurrencyRate);
                        //if (model.switchCurrency)
                        //{
                        //    var newCurrecyRate = _context.tb_AdminCurrencyRate.Where(z => z.ToCurrency == user.Currency && z.FromCurrency == model.amountCurrency).FirstOrDefault();//ask rahul
                        //    newDeductamtAmount = Convert.ToDecimal(Convert.ToDouble(newDeductamtAmount) * newCurrecyRate.CurrencyRate);
                        //}

                        newAmount = user.Wallet - Convert.ToDecimal(newDeductamtAmount);

                        if (user.Wallet >= newDeductamtAmount && user.Wallet > Convert.ToDecimal(0) && newDeductamtAmount >= Convert.ToDecimal(0))
                        //if (user.Wallet >= newDeductamtAmount)
                        {
                            var postModel = new ClassLibrary.ServicePostModel.DingPostModel.PostModel();
                            postModel.request = new ClassLibrary.ServicePostModel.DingPostModel.Request();
                            postModel.request.AccountNumber = model.AccountNumber;
                            postModel.request.DistributorRef = DingRandomString(20);
                            postModel.request.SendCurrencyIso = model.SendCurrencyIso;
                            postModel.request.SkuCode = model.SkuCode;
                            postModel.request.ValidateOnly = model.ValidateOnly;
                            //postModel.request.SendValue = Convert.ToDouble(model.SendValue);//14-11-2018
                            postModel.request.SendValue = Convert.ToDouble(newSendValue);


                            //Uri requestUri = new Uri(dingUrl+"/api/V1/SendTransfer"); //replace your Url  
                            //string json = "";
                            //json = Newtonsoft.Json.JsonConvert.SerializeObject(postModel.request);
                            //var objClint = new System.Net.Http.HttpClient();
                            //var respon = objClint.PostAsJsonAsync(requestUri, new StringContent(json, System.Text.Encoding.UTF8, "application/json")).Result;
                            ////string responJsonText =  respon.Content.ReadAsStringAsync();  
                            //Task<string> result1 =respon.Content.ReadAsStringAsync();
                            //dynamic data1 = JObject.Parse(result1.Result);

                            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                            using (var client = new HttpClient())
                            {
                                client.BaseAddress = new Uri(dingUrl);
                                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));//ACCEPT header
                                client.DefaultRequestHeaders.Add("api_key", ding_apiKey);
                                var response = client.PostAsJsonAsync("/api/V1/SendTransfer", postModel.request).Result;
                                if (response.IsSuccessStatusCode)
                                {
                                    using (HttpContent content = response.Content)
                                    {
                                        // ... Read the string.
                                        Task<string> result = content.ReadAsStringAsync();
                                        dynamic data = JObject.Parse(result.Result);

                                        returnModel.status = true;
                                        returnModel.message = "Success";
                                        returnModel.data = data;
                                        returnModel.wallet = user.Wallet.ToString();


                                        if (data != null)
                                        {
                                            var transaction = _context.tbDingTransactions.Create();
                                            transaction.AccountNumber = model.AccountNumber;
                                            transaction.DistributorRef = postModel.request.DistributorRef;

                                            if (data.ErrorCodes.Count > 0)
                                            {
                                                transaction.ErrorCode = data.ErrorCodes.Code;
                                                transaction.ErrorContext = data.ErrorCodes.Context;
                                            }
                                            else
                                            {
                                                transaction.ErrorCode = string.Empty;
                                                transaction.ErrorContext = string.Empty;
                                            }

                                            transaction.Guid = Guid.NewGuid();
                                            transaction.ResultCode = data.ResultCode;
                                            transaction.SendCurrencyIso = model.SendCurrencyIso;
                                            transaction.SendValue = model.SendValue;
                                            transaction.SkuCode = model.SkuCode;
                                            //transaction.Timestamp = System.DateTime.UtcNow;
                                            transaction.AmountCurrency = model.amountCurrency;
                                            transaction.Amount = Convert.ToString(model.amount);
                                            if (data.TransferRecord != null)
                                            {
                                                //transaction.Timestamp = DateTimeOffset.Parse(data.TransferRecord.CompletedUtc);
                                                if (data.TransferRecord.TransferId != null)
                                                {
                                                    transaction.TransferRef = data.TransferRecord.TransferId.TransferRef;
                                                }
                                                else
                                                {
                                                    transaction.TransferRef = string.Empty;
                                                }
                                                transaction.ProcessingState = data.TransferRecord.ProcessingState;

                                                if (data.TransferRecord.Price != null)
                                                {
                                                    if (data.TransferRecord.Price.ReceiveValue != null)
                                                    {
                                                        //transaction.Amount = data.TransferRecord.Price.ReceiveValue;
                                                        //transaction.AmountCurrency = data.TransferRecord.Price.ReceiveCurrencyIso;
                                                    }
                                                }

                                            }
                                            else
                                            {
                                                transaction.ProcessingState = string.Empty;
                                                transaction.TransferRef = string.Empty;
                                            }
                                            transaction.Timestamp = DateTime.UtcNow;
                                            transaction.UserId = model.userId;
                                            transaction.ValidateOnly = model.ValidateOnly;
                                            transaction.AmountDedudcted = Convert.ToString(0);
                                            transaction.Commission = commissionAmount;
                                            transaction.AdminCommissionAmount = admincommissionAmount;


                                            transaction.AmountDeductedCurrency = user.Currency;
                                            transaction.DingProductType = model.dingProductType;
                                            _context.tbDingTransactions.Add(transaction);
                                            if (_context.SaveChanges() > 0)
                                            {
                                                if (data.ResultCode == 1 || data.ResultCode == 2)
                                                {

                                                    //transaction.AmountDedudcted = Convert.ToString(deductamt);14-11-2018
                                                    transaction.AmountDedudcted = Convert.ToString(newDeductamtAmount);
                                                    user.Wallet = newAmount;

                                                    try
                                                    {
                                                        _context.SaveChanges();
                                                        returnModel.wallet = user.Wallet.ToString();
                                                    }
                                                    catch (Exception ex)
                                                    {
                                                        returnModel.status = true;
                                                        returnModel.message = "Success";
                                                        returnModel.data = data;
                                                        returnModel.wallet = user.Wallet.ToString();
                                                    }

                                                }
                                            }
                                            else
                                            {
                                                returnModel.status = false;
                                                returnModel.message = "Failed";
                                                returnModel.data = data;
                                                returnModel.wallet = user.Wallet.ToString();
                                            }

                                        }
                                        return returnModel;
                                    }
                                }
                                else
                                {
                                    returnModel.status = false;
                                    returnModel.message = "Failed";
                                    returnModel.data = null;
                                    returnModel.wallet = user.Wallet.ToString();
                                    return returnModel;
                                }
                            }
                        }
                        else
                        {
                            returnModel.status = false;
                            returnModel.message = "Insufficient Amount";
                            returnModel.data = null;
                            returnModel.wallet = user.Wallet.ToString();
                            return returnModel;
                        }

                    }
                    else
                    {
                        returnModel.status = false;
                        returnModel.message = "Failed";
                        returnModel.data = null;
                        returnModel.wallet = user.Wallet.ToString();
                        return returnModel;
                    }
                }
                else
                {
                    currecyRate = _context.tb_AdminCurrencyRate.Where(z => z.ToCurrency == user.Currency && z.FromCurrency == model.amountCurrency).FirstOrDefault();//ask rahul
                    if (currecyRate != null)
                    {
                        var amount = Convert.ToDecimal(Convert.ToDouble(model.amount) * currecyRate.CurrencyRate);
                        if (model.switchCurrency)
                        {
                            var newCurrecyRate = _context.tb_AdminCurrencyRate.Where(z => z.ToCurrency == user.Currency && z.FromCurrency == model.amountCurrency).FirstOrDefault();//ask rahul
                            amount = Convert.ToDecimal(Convert.ToDouble(amount) * newCurrecyRate.CurrencyRate);
                        }
                        var deductamt = amount;
                        var newAmount = user.Wallet;
                        decimal commissionAmount = Convert.ToDecimal(0);
                        if (user.UserType == (int)ClassLibrary.Enum.userType.Agent)
                        {
                            var commision = _context.tb_AgentCommisionRate.Where(z => z.AgentId == user.UserID && z.IsActive && z.ProductType == model.productType && z.ApiProvider == (int)ClassLibrary.Enum.apiProvider.Ding).FirstOrDefault();
                            if (commision != null)
                            {
                                commissionAmount = ((Convert.ToDecimal(amount) * Convert.ToDecimal(commision.Commission)) / 100);
                                deductamt = Convert.ToDecimal(amount) - commissionAmount;
                                newAmount = user.Wallet - Convert.ToDecimal(deductamt);
                            }
                            else
                            {
                                newAmount = user.Wallet - Convert.ToDecimal(amount); ;
                            }
                        }
                        else
                        {
                            newAmount = user.Wallet - Convert.ToDecimal(amount);
                        }

                        if (user.Wallet >= deductamt && user.Wallet > Convert.ToDecimal(0) && deductamt >= Convert.ToDecimal(0))
                        //if (user.Wallet >= deductamt)
                        {


                            var postModel = new ClassLibrary.ServicePostModel.DingPostModel.PostModel();
                            postModel.request = new ClassLibrary.ServicePostModel.DingPostModel.Request();
                            postModel.request.AccountNumber = model.AccountNumber;
                            postModel.request.DistributorRef = DingRandomString(20);
                            postModel.request.SendCurrencyIso = model.SendCurrencyIso;
                            postModel.request.SkuCode = model.SkuCode;
                            postModel.request.ValidateOnly = model.ValidateOnly;
                            postModel.request.SendValue = Convert.ToDouble(model.SendValue);


                            //Uri requestUri = new Uri(dingUrl+"/api/V1/SendTransfer"); //replace your Url  
                            //string json = "";
                            //json = Newtonsoft.Json.JsonConvert.SerializeObject(postModel.request);
                            //var objClint = new System.Net.Http.HttpClient();
                            //var respon = objClint.PostAsJsonAsync(requestUri, new StringContent(json, System.Text.Encoding.UTF8, "application/json")).Result;
                            ////string responJsonText =  respon.Content.ReadAsStringAsync();  
                            //Task<string> result1 =respon.Content.ReadAsStringAsync();
                            //dynamic data1 = JObject.Parse(result1.Result);

                            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                            using (var client = new HttpClient())
                            {
                                client.BaseAddress = new Uri(dingUrl);
                                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));//ACCEPT header
                                client.DefaultRequestHeaders.Add("api_key", ding_apiKey);
                                var response = client.PostAsJsonAsync("/api/V1/SendTransfer", postModel.request).Result;
                                if (response.IsSuccessStatusCode)
                                {
                                    using (HttpContent content = response.Content)
                                    {
                                        // ... Read the string.
                                        Task<string> result = content.ReadAsStringAsync();
                                        dynamic data = JObject.Parse(result.Result);

                                        returnModel.status = true;
                                        returnModel.message = "Success";
                                        returnModel.data = data;
                                        returnModel.wallet = user.Wallet.ToString();


                                        if (data != null)
                                        {
                                            var transaction = _context.tbDingTransactions.Create();
                                            transaction.AccountNumber = model.AccountNumber;
                                            transaction.DistributorRef = postModel.request.DistributorRef;

                                            if (data.ErrorCodes.Count > 0)
                                            {
                                                transaction.ErrorCode = data.ErrorCodes.Code;
                                                transaction.ErrorContext = data.ErrorCodes.Context;
                                            }
                                            else
                                            {
                                                transaction.ErrorCode = string.Empty;
                                                transaction.ErrorContext = string.Empty;
                                            }

                                            transaction.Guid = Guid.NewGuid();
                                            transaction.ResultCode = data.ResultCode;
                                            transaction.SendCurrencyIso = model.SendCurrencyIso;
                                            transaction.SendValue = model.SendValue;
                                            transaction.SkuCode = model.SkuCode;
                                            //transaction.Timestamp = System.DateTime.UtcNow;
                                            transaction.AmountCurrency = model.amountCurrency;
                                            transaction.Amount = Convert.ToString(model.amount);
                                            if (data.TransferRecord != null)
                                            {
                                                //transaction.Timestamp = DateTimeOffset.Parse(data.TransferRecord.CompletedUtc);
                                                if (data.TransferRecord.TransferId != null)
                                                {
                                                    transaction.TransferRef = data.TransferRecord.TransferId.TransferRef;
                                                }
                                                else
                                                {
                                                    transaction.TransferRef = string.Empty;
                                                }
                                                transaction.ProcessingState = data.TransferRecord.ProcessingState;

                                                if (data.TransferRecord.Price != null)
                                                {
                                                    if (data.TransferRecord.Price.ReceiveValue != null)
                                                    {
                                                        //transaction.Amount = data.TransferRecord.Price.ReceiveValue;
                                                        //transaction.AmountCurrency = data.TransferRecord.Price.ReceiveCurrencyIso;
                                                    }
                                                }

                                            }
                                            else
                                            {
                                                transaction.ProcessingState = string.Empty;
                                                transaction.TransferRef = string.Empty;
                                            }
                                            transaction.Timestamp = DateTime.UtcNow;
                                            transaction.UserId = model.userId;
                                            transaction.ValidateOnly = model.ValidateOnly;
                                            transaction.AmountDedudcted = Convert.ToString(0);
                                            transaction.Commission = commissionAmount;
                                            transaction.AdminCommissionAmount = Convert.ToDecimal(0);


                                            transaction.AmountDeductedCurrency = user.Currency;
                                            transaction.DingProductType = model.dingProductType;
                                            _context.tbDingTransactions.Add(transaction);
                                            if (_context.SaveChanges() > 0)
                                            {
                                                if (data.ResultCode == 1 || data.ResultCode == 2)
                                                {

                                                    transaction.AmountDedudcted = Convert.ToString(deductamt);
                                                    user.Wallet = newAmount;

                                                    try
                                                    {
                                                        _context.SaveChanges();
                                                        returnModel.wallet = user.Wallet.ToString();
                                                    }
                                                    catch (Exception ex)
                                                    {
                                                        returnModel.status = true;
                                                        returnModel.message = "Success";
                                                        returnModel.data = data;
                                                        returnModel.wallet = user.Wallet.ToString();
                                                    }

                                                }
                                            }
                                            else
                                            {
                                                returnModel.status = false;
                                                returnModel.message = "Failed";
                                                returnModel.data = data;
                                                returnModel.wallet = user.Wallet.ToString();
                                            }

                                        }
                                        return returnModel;
                                    }
                                }
                                else
                                {
                                    returnModel.status = false;
                                    returnModel.message = "Failed";
                                    returnModel.data = null;
                                    returnModel.wallet = user.Wallet.ToString();
                                    return returnModel;
                                }
                            }
                        }
                        else
                        {
                            returnModel.status = false;
                            returnModel.message = "Insufficient Amount";
                            returnModel.data = null;
                            returnModel.wallet = user.Wallet.ToString();
                            return returnModel;
                        }

                    }
                    else
                    {
                        returnModel.status = false;
                        returnModel.message = "Failed";
                        returnModel.data = null;
                        returnModel.wallet = user.Wallet.ToString();
                        return returnModel;
                    }
                }

            }
            else
            {
                returnModel.status = false;
                returnModel.message = "Failed";
                returnModel.data = null;
                returnModel.wallet = "";
                return returnModel;
            }
        }

        public object SendTransferInternational(ClassLibrary.ServicePostModel.DingPostModel.SendTransfer model)
        {
            var returnModel = new ClassLibrary.ServicePostModel.DingPostModel.SendTransferResponse();

            var user = _context.tb_User.FirstOrDefault(z => z.UserID == model.userId && z.IsActive);
            if (user != null)
            {


                try
                {
                    var rowIp = _context.tb_UserTransactionIp.Create();
                    rowIp.NetworkIp = model.ip;
                    rowIp.TimeStamp = System.DateTime.UtcNow;
                    rowIp.UserId = user.UserID;
                    _context.tb_UserTransactionIp.Add(rowIp);
                    _context.SaveChanges();
                }
                catch (Exception ex)
                {

                }



                var currecyRate = Convert.ToDouble(0);


                if (model.switchCurrency)
                {

                    var deductamt = Convert.ToDecimal(model.amount);
                    var newSendValue = Convert.ToString(model.amount);
                    var newAmount = user.Wallet;
                    decimal commissionAmount = Convert.ToDecimal(0);

                    var commision = _context.tb_AgentCommisionRate.Where(z => z.AgentId == user.UserID && z.IsActive && z.ProductType == model.productType && z.ApiProvider == (int)ClassLibrary.Enum.apiProvider.Ding).ToList();
                    if (user.UserType == (int)ClassLibrary.Enum.userType.Agent)
                    {
                        var agentCommission = commision.Where(z => z.IsAdminCommission == false).FirstOrDefault();
                        if (agentCommission != null)
                        {
                            commissionAmount = ((Convert.ToDecimal(model.amount) * Convert.ToDecimal(agentCommission.Commission)) / 100);
                            deductamt = Convert.ToDecimal(model.amount) - commissionAmount;
                            //newAmount = user.Wallet - Convert.ToDecimal(deductamt);
                            newSendValue = Convert.ToString(deductamt);
                        }
                        //else
                        //{
                        //    newAmount = user.Wallet - Convert.ToDecimal(amount); ;
                        //}
                    }
                    //else
                    //{
                    //    newAmount = user.Wallet - Convert.ToDecimal(amount);
                    //}
                    var admincommissionAmount = Convert.ToDecimal(0);
                    var adminCommission = commision.Where(z => z.IsAdminCommission == true).FirstOrDefault();
                    if (adminCommission != null)
                    {
                        admincommissionAmount = ((Convert.ToDecimal(newSendValue) * Convert.ToDecimal(adminCommission.Commission)) / 100);
                        var admindeductamt = Convert.ToDecimal(newSendValue) - admincommissionAmount;
                        newSendValue = Convert.ToString(admindeductamt);
                    }


                    //var currencyToConvert = user.Currency + "_" + model.amountCurrency;14-11-2018
                    var currencyToConvert = model.amountCurrency + "_" + user.Currency;
                    WebClient web = new WebClient();
                    string url = string.Format("http://free.currencyconverterapi.com/api/v5/convert?q=" + currencyToConvert + "&compact=y&apiKey=" + converterApiKey);
                    string responseString = web.DownloadString(url);
                    dynamic d = JObject.Parse(responseString);
                    if (d != null)
                    {
                        dynamic blogPost = d[currencyToConvert];
                        dynamic blogPost1 = blogPost["val"];
                        if (blogPost1 != null)
                        {
                            currecyRate = blogPost1.Value;
                        }
                    }

                    if (currecyRate != 0)
                    {
                        var newDeductamtAmount = Convert.ToDecimal(Convert.ToDouble(deductamt) * currecyRate);
                        //if (model.switchCurrency)
                        //{
                        //    var currencyToConvert1 = model.amountCurrency + "_" + user.Currency;
                        //    WebClient web1 = new WebClient();
                        //    string url1 = string.Format("http://free.currencyconverterapi.com/api/v5/convert?q=" + currencyToConvert1 + "&compact=y");
                        //    string responseString1 = web1.DownloadString(url1);
                        //    dynamic d1 = JObject.Parse(responseString1);
                        //    var newcurrecyRate = Convert.ToDouble(0);
                        //    if (d1 != null)
                        //    {
                        //        dynamic blogPost = d1[currencyToConvert1];
                        //        dynamic blogPost1 = blogPost["val"];
                        //        if (blogPost1 != null)
                        //        {
                        //            newcurrecyRate = blogPost1.Value;
                        //        }
                        //    }
                        //    newDeductamtAmount = Convert.ToDecimal(Convert.ToDouble(newDeductamtAmount) * newcurrecyRate);
                        //}

                        newAmount = user.Wallet - Convert.ToDecimal(newDeductamtAmount);


                        if (user.Wallet >= newDeductamtAmount && newDeductamtAmount >= Convert.ToDecimal(0) && user.Wallet > Convert.ToDecimal(0))
                        //if (user.Wallet >= newDeductamtAmount)
                        {


                            var postModel = new ClassLibrary.ServicePostModel.DingPostModel.PostModel();
                            postModel.request = new ClassLibrary.ServicePostModel.DingPostModel.Request();
                            postModel.request.AccountNumber = model.AccountNumber;
                            postModel.request.DistributorRef = DingRandomString(20);
                            postModel.request.SendCurrencyIso = model.SendCurrencyIso;
                            postModel.request.SkuCode = model.SkuCode;
                            postModel.request.ValidateOnly = model.ValidateOnly;
                            //postModel.request.SendValue = Convert.ToDouble(model.SendValue);//14-11-2018
                            postModel.request.SendValue = Convert.ToDouble(newSendValue);


                            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                            using (var client = new HttpClient())
                            {
                                client.BaseAddress = new Uri(dingUrl);
                                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));//ACCEPT header
                                client.DefaultRequestHeaders.Add("api_key", ding_apiKey_International);
                                var response = client.PostAsJsonAsync("/api/V1/SendTransfer", postModel.request).Result;
                                if (response.IsSuccessStatusCode)
                                {
                                    using (HttpContent content = response.Content)
                                    {
                                        // ... Read the string.
                                        Task<string> result = content.ReadAsStringAsync();
                                        dynamic data = JObject.Parse(result.Result);

                                        returnModel.status = true;
                                        returnModel.message = "Success";
                                        returnModel.data = data;
                                        returnModel.wallet = user.Wallet.ToString();


                                        if (data != null)
                                        {
                                            var transaction = _context.tbDingTransactions.Create();
                                            transaction.AccountNumber = model.AccountNumber;
                                            transaction.DistributorRef = postModel.request.DistributorRef;

                                            if (data.ErrorCodes.Count > 0)
                                            {
                                                transaction.ErrorCode = data.ErrorCodes.Code;
                                                transaction.ErrorContext = data.ErrorCodes.Context;
                                            }
                                            else
                                            {
                                                transaction.ErrorCode = string.Empty;
                                                transaction.ErrorContext = string.Empty;
                                            }

                                            transaction.Guid = Guid.NewGuid();
                                            transaction.ResultCode = data.ResultCode;
                                            transaction.SendCurrencyIso = model.SendCurrencyIso;
                                            transaction.SendValue = model.SendValue;
                                            transaction.SkuCode = model.SkuCode;
                                            //transaction.Timestamp = System.DateTime.UtcNow;
                                            transaction.Amount = Convert.ToString(model.amount);
                                            transaction.AmountCurrency = model.amountCurrency;
                                            if (data.TransferRecord != null)
                                            {
                                                //transaction.Timestamp = DateTimeOffset.Parse(data.TransferRecord.CompletedUtc);
                                                if (data.TransferRecord.TransferId != null)
                                                {
                                                    transaction.TransferRef = data.TransferRecord.TransferId.TransferRef;
                                                }
                                                else
                                                {
                                                    transaction.TransferRef = string.Empty;
                                                }
                                                transaction.ProcessingState = data.TransferRecord.ProcessingState;

                                                if (data.TransferRecord.Price != null)
                                                {
                                                    if (data.TransferRecord.Price.ReceiveValue != null)
                                                    {
                                                        //transaction.Amount = data.TransferRecord.Price.ReceiveValue;
                                                        //transaction.AmountCurrency = data.TransferRecord.Price.ReceiveCurrencyIso;
                                                    }
                                                }

                                            }
                                            else
                                            {
                                                transaction.ProcessingState = string.Empty;
                                                transaction.TransferRef = string.Empty;
                                            }
                                            transaction.Timestamp = DateTime.UtcNow;
                                            transaction.UserId = model.userId;
                                            transaction.ValidateOnly = model.ValidateOnly;
                                            transaction.AmountDedudcted = Convert.ToString(0);
                                            transaction.Commission = commissionAmount;


                                            transaction.AmountDeductedCurrency = user.Currency;
                                            transaction.DingProductType = model.dingProductType;
                                            _context.tbDingTransactions.Add(transaction);
                                            if (_context.SaveChanges() > 0)
                                            {
                                                if (data.ResultCode == 1 || data.ResultCode == 2)
                                                {

                                                    //transaction.AmountDedudcted = Convert.ToString(deductamt);14-11-2018
                                                    transaction.AmountDedudcted = Convert.ToString(newDeductamtAmount);
                                                    user.Wallet = newAmount;

                                                    try
                                                    {
                                                        _context.SaveChanges();
                                                        returnModel.wallet = user.Wallet.ToString();
                                                    }
                                                    catch (Exception ex)
                                                    {
                                                        returnModel.status = true;
                                                        returnModel.message = "Success";
                                                        returnModel.data = data;
                                                        returnModel.wallet = user.Wallet.ToString();
                                                    }

                                                }
                                            }
                                            else
                                            {
                                                returnModel.status = false;
                                                returnModel.message = "Failed";
                                                returnModel.data = data;
                                                returnModel.wallet = user.Wallet.ToString();
                                            }

                                        }
                                        return returnModel;
                                    }
                                }
                                else
                                {
                                    returnModel.status = false;
                                    returnModel.message = "Failed";
                                    returnModel.data = null;
                                    returnModel.wallet = user.Wallet.ToString();
                                    return returnModel;
                                }
                            }
                        }
                        else
                        {
                            returnModel.status = false;
                            returnModel.message = "Insufficient Amount";
                            returnModel.data = null;
                            returnModel.wallet = user.Wallet.ToString();
                            return returnModel;
                        }

                    }
                    else
                    {
                        returnModel.status = false;
                        returnModel.message = "Failed";
                        returnModel.data = null;
                        returnModel.wallet = user.Wallet.ToString();
                        return returnModel;
                    }


                }
                else
                {
                    var currencyToConvert = model.amountCurrency + "_" + user.Currency;
                    WebClient web = new WebClient();
                    string url = string.Format("http://free.currencyconverterapi.com/api/v5/convert?q=" + currencyToConvert + "&compact=y&apiKey=" + converterApiKey);
                    string responseString = web.DownloadString(url);
                    dynamic d = JObject.Parse(responseString);
                    if (d != null)
                    {
                        dynamic blogPost = d[currencyToConvert];
                        dynamic blogPost1 = blogPost["val"];
                        if (blogPost1 != null)
                        {
                            currecyRate = blogPost1.Value;
                        }
                    }


                    if (currecyRate != 0)
                    {
                        var amount = Convert.ToDecimal(Convert.ToDouble(model.amount) * currecyRate);
                        if (model.switchCurrency)
                        {
                            var currencyToConvert1 = model.amountCurrency + "_" + user.Currency;
                            WebClient web1 = new WebClient();
                            string url1 = string.Format("http://free.currencyconverterapi.com/api/v5/convert?q=" + currencyToConvert1 + "&compact=y&apiKey=" + converterApiKey);
                            string responseString1 = web1.DownloadString(url1);
                            dynamic d1 = JObject.Parse(responseString1);
                            var newcurrecyRate = Convert.ToDouble(0);
                            if (d1 != null)
                            {
                                dynamic blogPost = d1[currencyToConvert1];
                                dynamic blogPost1 = blogPost["val"];
                                if (blogPost1 != null)
                                {
                                    newcurrecyRate = blogPost1.Value;
                                }
                            }
                            amount = Convert.ToDecimal(Convert.ToDouble(model.amount) * newcurrecyRate);
                        }

                        var deductamt = amount;
                        var newAmount = user.Wallet;
                        decimal commissionAmount = Convert.ToDecimal(0);
                        if (user.UserType == (int)ClassLibrary.Enum.userType.Agent)
                        {
                            var commision = _context.tb_AgentCommisionRate.Where(z => z.AgentId == user.UserID && z.IsActive && z.ProductType == model.productType && z.ApiProvider == (int)ClassLibrary.Enum.apiProvider.Ding).FirstOrDefault();
                            if (commision != null)
                            {
                                commissionAmount = ((Convert.ToDecimal(amount) * Convert.ToDecimal(commision.Commission)) / 100);
                                deductamt = Convert.ToDecimal(amount) - commissionAmount;
                                newAmount = user.Wallet - Convert.ToDecimal(deductamt);
                            }
                            else
                            {
                                newAmount = user.Wallet - Convert.ToDecimal(amount); ;
                            }
                        }
                        else
                        {
                            newAmount = user.Wallet - Convert.ToDecimal(amount);
                        }

                        if (user.Wallet >= deductamt && deductamt >= Convert.ToDecimal(0) && user.Wallet > Convert.ToDecimal(0))
                        //if (user.Wallet >= deductamt)
                        {


                            var postModel = new ClassLibrary.ServicePostModel.DingPostModel.PostModel();
                            postModel.request = new ClassLibrary.ServicePostModel.DingPostModel.Request();
                            postModel.request.AccountNumber = model.AccountNumber;
                            postModel.request.DistributorRef = DingRandomString(20);
                            postModel.request.SendCurrencyIso = model.SendCurrencyIso;
                            postModel.request.SkuCode = model.SkuCode;
                            postModel.request.ValidateOnly = model.ValidateOnly;
                            postModel.request.SendValue = Convert.ToDouble(model.SendValue);


                            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                            using (var client = new HttpClient())
                            {
                                client.BaseAddress = new Uri(dingUrl);
                                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));//ACCEPT header
                                client.DefaultRequestHeaders.Add("api_key", ding_apiKey_International);
                                var response = client.PostAsJsonAsync("/api/V1/SendTransfer", postModel.request).Result;
                                if (response.IsSuccessStatusCode)
                                {
                                    using (HttpContent content = response.Content)
                                    {
                                        // ... Read the string.
                                        Task<string> result = content.ReadAsStringAsync();
                                        dynamic data = JObject.Parse(result.Result);

                                        returnModel.status = true;
                                        returnModel.message = "Success";
                                        returnModel.data = data;
                                        returnModel.wallet = user.Wallet.ToString();


                                        if (data != null)
                                        {
                                            var transaction = _context.tbDingTransactions.Create();
                                            transaction.AccountNumber = model.AccountNumber;
                                            transaction.DistributorRef = postModel.request.DistributorRef;

                                            if (data.ErrorCodes.Count > 0)
                                            {
                                                transaction.ErrorCode = data.ErrorCodes.Code;
                                                transaction.ErrorContext = data.ErrorCodes.Context;
                                            }
                                            else
                                            {
                                                transaction.ErrorCode = string.Empty;
                                                transaction.ErrorContext = string.Empty;
                                            }

                                            transaction.Guid = Guid.NewGuid();
                                            transaction.ResultCode = data.ResultCode;
                                            transaction.SendCurrencyIso = model.SendCurrencyIso;
                                            transaction.SendValue = model.SendValue;
                                            transaction.SkuCode = model.SkuCode;
                                            //transaction.Timestamp = System.DateTime.UtcNow;
                                            transaction.Amount = Convert.ToString(model.amount);
                                            transaction.AmountCurrency = model.amountCurrency;
                                            if (data.TransferRecord != null)
                                            {
                                                //transaction.Timestamp = DateTimeOffset.Parse(data.TransferRecord.CompletedUtc);
                                                if (data.TransferRecord.TransferId != null)
                                                {
                                                    transaction.TransferRef = data.TransferRecord.TransferId.TransferRef;
                                                }
                                                else
                                                {
                                                    transaction.TransferRef = string.Empty;
                                                }
                                                transaction.ProcessingState = data.TransferRecord.ProcessingState;

                                                if (data.TransferRecord.Price != null)
                                                {
                                                    if (data.TransferRecord.Price.ReceiveValue != null)
                                                    {
                                                        //transaction.Amount = data.TransferRecord.Price.ReceiveValue;
                                                        //transaction.AmountCurrency = data.TransferRecord.Price.ReceiveCurrencyIso;
                                                    }
                                                }

                                            }
                                            else
                                            {
                                                transaction.ProcessingState = string.Empty;
                                                transaction.TransferRef = string.Empty;
                                            }
                                            transaction.Timestamp = DateTime.UtcNow;
                                            transaction.UserId = model.userId;
                                            transaction.ValidateOnly = model.ValidateOnly;
                                            transaction.AmountDedudcted = Convert.ToString(0);
                                            transaction.Commission = commissionAmount;


                                            transaction.AmountDeductedCurrency = user.Currency;
                                            transaction.DingProductType = model.dingProductType;
                                            _context.tbDingTransactions.Add(transaction);
                                            if (_context.SaveChanges() > 0)
                                            {
                                                if (data.ResultCode == 1 || data.ResultCode == 2)
                                                {

                                                    transaction.AmountDedudcted = Convert.ToString(deductamt);
                                                    user.Wallet = newAmount;

                                                    try
                                                    {
                                                        _context.SaveChanges();
                                                        returnModel.wallet = user.Wallet.ToString();
                                                    }
                                                    catch (Exception ex)
                                                    {
                                                        returnModel.status = true;
                                                        returnModel.message = "Success";
                                                        returnModel.data = data;
                                                        returnModel.wallet = user.Wallet.ToString();
                                                    }

                                                }
                                            }
                                            else
                                            {
                                                returnModel.status = false;
                                                returnModel.message = "Failed";
                                                returnModel.data = data;
                                                returnModel.wallet = user.Wallet.ToString();
                                            }

                                        }
                                        return returnModel;
                                    }
                                }
                                else
                                {
                                    returnModel.status = false;
                                    returnModel.message = "Failed";
                                    returnModel.data = null;
                                    returnModel.wallet = user.Wallet.ToString();
                                    return returnModel;
                                }
                            }
                        }
                        else
                        {
                            returnModel.status = false;
                            returnModel.message = "Insufficient Amount";
                            returnModel.data = null;
                            returnModel.wallet = user.Wallet.ToString();
                            return returnModel;
                        }

                    }
                    else
                    {
                        returnModel.status = false;
                        returnModel.message = "Failed";
                        returnModel.data = null;
                        returnModel.wallet = user.Wallet.ToString();
                        return returnModel;
                    }



                }




            }
            else
            {
                returnModel.status = false;
                returnModel.message = "Failed";
                returnModel.data = null;
                returnModel.wallet = "";
                return returnModel;
            }
        }




        public object GetAccountLookupInternational(ClassLibrary.ServicePostModel.DingPostModel.GetAccountLookup model)
        {
            var returnModel = new ClassLibrary.ServicePostModel.DingPostModel.GetAccountLookupResponse();
            dynamic data = new System.Dynamic.ExpandoObject();

            try
            {
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                var requestString = dingUrl + "/api/V1/GetAccountLookup?accountNumber=" + model.accountNumber;
                var request = (HttpWebRequest)WebRequest.Create(requestString);
                request.Method = "GET";
                request.ContentType = "application/json";
                request.Headers.Add("api_key", ding_apiKey_International);
                var response = (HttpWebResponse)request.GetResponse();
                var responseString = new System.IO.StreamReader(response.GetResponseStream()).ReadToEnd();
                data = Newtonsoft.Json.JsonConvert.DeserializeObject(responseString);

                returnModel.status = true;
                returnModel.message = "Success";
                returnModel.data = data;
            }
            catch (Exception ex)
            {
                returnModel.status = false;
                returnModel.message = ex.Message;
                returnModel.data = data;
            }
            return returnModel;
        }

        public object GetProvidersInternational(ClassLibrary.ServicePostModel.DingPostModel.GetProviders model)
        {
            var returnModel = new ClassLibrary.ServicePostModel.DingPostModel.GetProvidersResponse();
            dynamic data = new System.Dynamic.ExpandoObject();

            try
            {
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                var requestString = dingUrl + "/api/V1/GetProviders?accountNumber=" + model.accountNumber;

                if (model.providerCodes != null && model.providerCodes != string.Empty)
                {
                    requestString = requestString + "&providerCodes=" + model.providerCodes;
                }

                if (model.countryIsos != null && model.countryIsos != string.Empty)
                {
                    requestString = requestString + "&countryIsos=" + model.countryIsos;
                }

                if (model.regionCodes != null && model.regionCodes != string.Empty)
                {
                    requestString = requestString + "&regionCodes=" + model.regionCodes;
                }

                var request = (HttpWebRequest)WebRequest.Create(requestString);
                request.Method = "GET";
                request.ContentType = "application/json";
                request.Headers.Add("api_key", ding_apiKey_International);
                var response = (HttpWebResponse)request.GetResponse();
                var responseString = new System.IO.StreamReader(response.GetResponseStream()).ReadToEnd();
                data = Newtonsoft.Json.JsonConvert.DeserializeObject(responseString);

                returnModel.status = true;
                returnModel.message = "Success";
                returnModel.data = data;
            }
            catch (Exception ex)
            {
                returnModel.status = false;
                returnModel.message = ex.Message;
                returnModel.data = data;
            }
            return returnModel;
        }

        public object GetProductsInternational(ClassLibrary.ServicePostModel.DingPostModel.GetProducts model)
        {
            var returnModel = new ClassLibrary.ServicePostModel.DingPostModel.GetProductsResponse();
            dynamic data = new System.Dynamic.ExpandoObject();

            try
            {
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                var requestString = dingUrl + "/api/V1/GetProducts?accountNumber=" + model.accountNumber;

                if (model.providerCodes != null && model.providerCodes != string.Empty)
                {
                    requestString = requestString + "&providerCodes=" + model.providerCodes;
                }

                if (model.countryIsos != null && model.countryIsos != string.Empty)
                {
                    requestString = requestString + "&countryIsos=" + model.countryIsos;
                }

                if (model.regionCodes != null && model.regionCodes != string.Empty)
                {
                    requestString = requestString + "&regionCodes=" + model.regionCodes;
                }

                if (model.skuCodes != null && model.skuCodes != string.Empty)
                {
                    requestString = requestString + "&skuCodes=" + model.skuCodes;
                }

                if (model.benefits != null && model.benefits != string.Empty)
                {
                    requestString = requestString + "&benefits=" + model.benefits;
                }


                var request = (HttpWebRequest)WebRequest.Create(requestString);
                request.Method = "GET";
                request.ContentType = "application/json";
                request.Headers.Add("api_key", ding_apiKey_International);
                var response = (HttpWebResponse)request.GetResponse();
                var responseString = new System.IO.StreamReader(response.GetResponseStream()).ReadToEnd();
                data = Newtonsoft.Json.JsonConvert.DeserializeObject(responseString);

                returnModel.status = true;
                returnModel.message = "Success";
                returnModel.data = data;
            }
            catch (Exception ex)
            {
                returnModel.status = false;
                returnModel.message = ex.Message;
                returnModel.data = data;
            }
            return returnModel;
        }

        ////
        //////
        ///Add by sibi
        ///
        public object SendTransferInternationalAED(ClassLibrary.ServicePostModel.DingPostModel.SendTransfer model) // Archana 26-03-2019
        {
            var returnModel = new ClassLibrary.ServicePostModel.DingPostModel.SendTransferResponse();
            var user = _context.tb_User.FirstOrDefault(z => z.UserID == model.userId && z.IsActive);
            if (user != null)
            {
                try
                {
                    var rowIp = _context.tb_UserTransactionIp.Create();
                    rowIp.NetworkIp = model.ip;
                    rowIp.TimeStamp = System.DateTime.UtcNow;
                    rowIp.UserId = user.UserID;
                    _context.tb_UserTransactionIp.Add(rowIp);
                    _context.SaveChanges();
                }
                catch (Exception ex)
                {

                }
                var currecyRate = Convert.ToDouble(0);
                #region Swich Case
                if (model.switchCurrency)
                {
                    var deductamt = Convert.ToDecimal(model.amount);
                    var newSendValue = Convert.ToString(model.amount);
                    var newAmount = user.Wallet;
                    decimal commissionAmount = Convert.ToDecimal(0);
                    var commision = _context.tb_AgentCommisionRate.Where(z => z.AgentId == user.UserID && z.IsActive && z.ProductType == model.productType && z.ApiProvider == (int)ClassLibrary.Enum.apiProvider.Ding).ToList();
                    if (user.UserType == (int)ClassLibrary.Enum.userType.Agent)
                    {
                        var agentCommission = commision.Where(z => z.IsAdminCommission == false).FirstOrDefault();
                        if (agentCommission != null)
                        {
                            commissionAmount = ((Convert.ToDecimal(model.amount) * Convert.ToDecimal(agentCommission.Commission)) / 100);
                            deductamt = Convert.ToDecimal(model.amount) - commissionAmount;
                            newSendValue = Convert.ToString(deductamt);
                        }
                    }
                    var admincommissionAmount = Convert.ToDecimal(0);
                    var adminCommission = commision.Where(z => z.IsAdminCommission == true).FirstOrDefault();
                    if (adminCommission != null)
                    {
                        admincommissionAmount = ((Convert.ToDecimal(newSendValue) * Convert.ToDecimal(adminCommission.Commission)) / 100);
                        var admindeductamt = Convert.ToDecimal(newSendValue) - admincommissionAmount;
                        newSendValue = Convert.ToString(admindeductamt);
                    }
                    var currencyToConvert = model.amountCurrency + "_" + user.Currency;
                    WebClient web = new WebClient();
                    string url = string.Format("http://free.currencyconverterapi.com/api/v5/convert?q=" + currencyToConvert + "&compact=y&apiKey=" + converterApiKey);
                    string responseString = web.DownloadString(url);
                    dynamic d = JObject.Parse(responseString);
                    if (d != null)
                    {
                        dynamic blogPost = d[currencyToConvert];
                        dynamic blogPost1 = blogPost["val"];
                        if (blogPost1 != null)
                        {
                            currecyRate = blogPost1.Value;
                        }
                    }

                    if (currecyRate != 0)
                    {
                        var newDeductamtAmount = Convert.ToDecimal(Convert.ToDouble(deductamt) * currecyRate);
                        newAmount = user.Wallet - Convert.ToDecimal(newDeductamtAmount);
                        if (user.Wallet >= newDeductamtAmount && newDeductamtAmount >= Convert.ToDecimal(0) && user.Wallet > Convert.ToDecimal(0))
                        {
                            var postModel = new ClassLibrary.ServicePostModel.DingPostModel.PostModel();
                            postModel.request = new ClassLibrary.ServicePostModel.DingPostModel.Request();
                            postModel.request.AccountNumber = model.AccountNumber;
                            postModel.request.DistributorRef = DingRandomString(20);
                            postModel.request.SendCurrencyIso = model.SendCurrencyIso;
                            postModel.request.SkuCode = model.SkuCode;
                            postModel.request.ValidateOnly = model.ValidateOnly;
                            postModel.request.SendValue = Convert.ToDouble(newSendValue);
                            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                            using (var client = new HttpClient())
                            {
                                client.BaseAddress = new Uri(dingUrl);
                                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));//ACCEPT header
                                client.DefaultRequestHeaders.Add("api_key", ding_apiKey_International);
                                var response = client.PostAsJsonAsync("/api/V1/SendTransfer", postModel.request).Result;
                                if (response.IsSuccessStatusCode)
                                {
                                    using (HttpContent content = response.Content)
                                    {
                                        Task<string> result = content.ReadAsStringAsync();
                                        dynamic data = JObject.Parse(result.Result);
                                        returnModel.status = true;
                                        returnModel.message = "Success";
                                        returnModel.data = data;
                                        returnModel.wallet = user.Wallet.ToString();
                                        if (data != null)
                                        {
                                            var transaction = _context.tbDingTransactions.Create();
                                            transaction.AccountNumber = model.AccountNumber;
                                            transaction.DistributorRef = postModel.request.DistributorRef;
                                            if (data.ErrorCodes.Count > 0)
                                            {
                                                transaction.ErrorCode = data.ErrorCodes.Code;
                                                transaction.ErrorContext = data.ErrorCodes.Context;
                                            }
                                            else
                                            {
                                                transaction.ErrorCode = string.Empty;
                                                transaction.ErrorContext = string.Empty;
                                            }
                                            transaction.Guid = Guid.NewGuid();
                                            transaction.ResultCode = data.ResultCode;
                                            transaction.SendCurrencyIso = model.SendCurrencyIso;
                                            transaction.SendValue = model.SendValue;
                                            transaction.SkuCode = model.SkuCode;
                                            transaction.Amount = Convert.ToString(model.amount);
                                            transaction.AmountCurrency = model.amountCurrency;
                                            if (data.TransferRecord != null)
                                            {
                                                if (data.TransferRecord.TransferId != null)
                                                {
                                                    transaction.TransferRef = data.TransferRecord.TransferId.TransferRef;
                                                }
                                                else
                                                {
                                                    transaction.TransferRef = string.Empty;
                                                }
                                                transaction.ProcessingState = data.TransferRecord.ProcessingState;

                                                if (data.TransferRecord.Price != null)
                                                {
                                                    if (data.TransferRecord.Price.ReceiveValue != null)
                                                    {
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                transaction.ProcessingState = string.Empty;
                                                transaction.TransferRef = string.Empty;
                                            }
                                            transaction.Timestamp = DateTime.UtcNow;
                                            transaction.UserId = model.userId;
                                            transaction.ValidateOnly = model.ValidateOnly;
                                            transaction.AmountDedudcted = Convert.ToString(0);
                                            transaction.Commission = commissionAmount;
                                            transaction.AmountDeductedCurrency = user.Currency;
                                            transaction.DingProductType = model.dingProductType;
                                            _context.tbDingTransactions.Add(transaction);
                                            if (_context.SaveChanges() > 0)
                                            {
                                                if (data.ResultCode == 1 || data.ResultCode == 2)
                                                {
                                                    transaction.AmountDedudcted = Convert.ToString(newDeductamtAmount);
                                                    user.Wallet = newAmount;
                                                    try
                                                    {
                                                        _context.SaveChanges();
                                                        returnModel.wallet = user.Wallet.ToString();
                                                    }
                                                    catch (Exception ex)
                                                    {
                                                        returnModel.status = true;
                                                        returnModel.message = "Success";
                                                        returnModel.data = data;
                                                        returnModel.wallet = user.Wallet.ToString();
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                returnModel.status = false;
                                                returnModel.message = "Failed";
                                                returnModel.data = data;
                                                returnModel.wallet = user.Wallet.ToString();
                                            }
                                        }
                                        return returnModel;
                                    }
                                }
                                else
                                {
                                    returnModel.status = false;
                                    returnModel.message = "Failed";
                                    returnModel.data = null;
                                    returnModel.wallet = user.Wallet.ToString();
                                    return returnModel;
                                }
                            }
                        }
                        else
                        {
                            returnModel.status = false;
                            returnModel.message = "Insufficient Amount";
                            returnModel.data = null;
                            returnModel.wallet = user.Wallet.ToString();
                            return returnModel;
                        }
                    }
                    else
                    {
                        returnModel.status = false;
                        returnModel.message = "Failed";
                        returnModel.data = null;
                        returnModel.wallet = user.Wallet.ToString();
                        return returnModel;
                    }
                }
                #endregion Swich Case
                else
                {

                    var amount = Convert.ToDecimal(Convert.ToDouble(model.SendValue));


                    var deductamt = amount;
                    var newAmount = user.Wallet;
                    decimal agentCommisionOnly = 0;
                    decimal commissionAmount = Convert.ToDecimal(0);
                    if (user.UserType == (int)ClassLibrary.Enum.userType.Agent)
                    {

                        var adminComm = new EntityLibrary.Data.User(user.UserID);
                        if (adminComm != null)
                        {
                            var com = adminComm.commissions.ToList();
                            var adminCommision = com.Where(x => x.ApiProvider == (int)ClassLibrary.Enum.apiProvider.Ding && x.ProductType == (int)ClassLibrary.Enum.GulfboxProductTypes.Intl && x.IsAdminCommission == true).FirstOrDefault();
                            if (adminCommision != null)
                            {
                                commissionAmount = ((Convert.ToDecimal(amount) * Convert.ToDecimal(adminCommision.Commission)) / 100);
                            }
                            else
                            {
                                commissionAmount = (Convert.ToDecimal(amount));
                            }
                            deductamt = Convert.ToDecimal(amount) + commissionAmount;
                            newAmount = user.Wallet - Convert.ToDecimal(deductamt);
                            var agentCommision = _context.tb_AgentCommisionRate.Where(z => z.AgentId == user.UserID && z.IsActive && z.ProductType == model.productType && z.ApiProvider == (int)ClassLibrary.Enum.apiProvider.Ding).FirstOrDefault();
                            if (agentCommision != null)
                            {
                                agentCommisionOnly = ((Convert.ToDecimal(amount) * Convert.ToDecimal(agentCommision.Commission)) / 100);
                            }
                        }
                        else
                        {
                            newAmount = user.Wallet - Convert.ToDecimal(amount);
                        }
                    }
                    else
                    {
                        newAmount = user.Wallet - Convert.ToDecimal(amount);
                    }

                    if (user.Wallet >= deductamt && deductamt >= Convert.ToDecimal(0) && user.Wallet > Convert.ToDecimal(0))
                    {
                        var postModel = new ClassLibrary.ServicePostModel.DingPostModel.PostModel();
                        postModel.request = new ClassLibrary.ServicePostModel.DingPostModel.Request();
                        postModel.request.AccountNumber = model.AccountNumber;
                        postModel.request.DistributorRef = DingRandomString(20);
                        postModel.request.SendCurrencyIso = model.SendCurrencyIso;
                        postModel.request.SkuCode = model.SkuCode;
                        postModel.request.ValidateOnly = model.ValidateOnly;
                        postModel.request.SendValue = Convert.ToDouble(model.SendValue);
                        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                        using (var client = new HttpClient())
                        {
                            client.BaseAddress = new Uri(dingUrl);
                            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                            client.DefaultRequestHeaders.Add("api_key", ding_apiKey_International);
                            //client.DefaultRequestHeaders.Add("api_key", ding_apiKey);
                            var response = client.PostAsJsonAsync("/api/V1/SendTransfer", postModel.request).Result;
                            if (response.IsSuccessStatusCode)
                            {
                                using (HttpContent content = response.Content)
                                {
                                    Task<string> result = content.ReadAsStringAsync();
                                    dynamic data = JObject.Parse(result.Result);
                                    returnModel.status = true;
                                    returnModel.message = "Success";
                                    returnModel.data = data;
                                    returnModel.wallet = user.Wallet.ToString();
                                    if (data != null)
                                    {
                                        var transaction = _context.tbDingTransactions.Create();
                                        transaction.AccountNumber = model.AccountNumber;
                                        transaction.DistributorRef = postModel.request.DistributorRef;
                                        if (data.ErrorCodes.Count > 0)
                                        {
                                            transaction.ErrorCode = data.ErrorCodes.Code;
                                            transaction.ErrorContext = data.ErrorCodes.Context;
                                        }
                                        else
                                        {
                                            transaction.ErrorCode = string.Empty;
                                            transaction.ErrorContext = string.Empty;
                                        }
                                        transaction.Guid = Guid.NewGuid();
                                        transaction.ResultCode = data.ResultCode;
                                        transaction.SendCurrencyIso = model.SendCurrencyIso;
                                        transaction.SendValue = model.SendValue;
                                        transaction.SkuCode = model.SkuCode;
                                        //transaction.Amount = Convert.ToString(model.amount);
                                        var transAmt = agentCommisionOnly + deductamt;
                                        transaction.Amount = Convert.ToString(transAmt);
                                        //transaction.AmountCurrency = model.amountCurrency;
                                        transaction.AmountCurrency = model.SendCurrencyIso;
                                        if (data.TransferRecord != null)
                                        {
                                            if (data.TransferRecord.TransferId != null)
                                            {
                                                transaction.TransferRef = data.TransferRecord.TransferId.TransferRef;
                                            }
                                            else
                                            {
                                                transaction.TransferRef = string.Empty;
                                            }
                                            transaction.ProcessingState = data.TransferRecord.ProcessingState;

                                            if (data.TransferRecord.Price != null)
                                            {
                                                if (data.TransferRecord.Price.ReceiveValue != null)
                                                {
                                                }
                                            }
                                        }
                                        else
                                        {
                                            transaction.ProcessingState = string.Empty;
                                            transaction.TransferRef = string.Empty;
                                        }
                                        transaction.Timestamp = DateTime.UtcNow;
                                        transaction.UserId = model.userId;
                                        transaction.ValidateOnly = model.ValidateOnly;
                                        transaction.AmountDedudcted = Convert.ToString(0);
                                        transaction.Commission = commissionAmount;
                                        transaction.AmountDeductedCurrency = user.Currency;
                                        transaction.DingProductType = model.dingProductType;
                                        _context.tbDingTransactions.Add(transaction);
                                        if (_context.SaveChanges() > 0)
                                        {
                                            if (data.ResultCode == 1 || data.ResultCode == 2)
                                            {
                                                transaction.AmountDedudcted = Convert.ToString(deductamt);
                                                user.Wallet = newAmount;
                                                try
                                                {
                                                    _context.SaveChanges();
                                                    returnModel.wallet = user.Wallet.ToString();
                                                }
                                                catch (Exception ex)
                                                {
                                                    returnModel.status = true;
                                                    returnModel.message = "Success";
                                                    returnModel.data = data;
                                                    returnModel.wallet = user.Wallet.ToString();
                                                }
                                            }
                                        }
                                        else
                                        {
                                            returnModel.status = false;
                                            returnModel.message = "Failed";
                                            returnModel.data = data;
                                            returnModel.wallet = user.Wallet.ToString();
                                        }
                                    }
                                    return returnModel;
                                }
                            }
                            else
                            {
                                returnModel.status = false;
                                returnModel.message = "Failed";
                                returnModel.data = null;
                                returnModel.wallet = user.Wallet.ToString();
                                return returnModel;
                            }
                        }
                    }
                    else
                    {
                        returnModel.status = false;
                        returnModel.message = "Insufficient Amount";
                        returnModel.data = null;
                        returnModel.wallet = user.Wallet.ToString();
                        return returnModel;
                    }

                }
            }
            else
            {
                returnModel.status = false;
                returnModel.message = "Failed";
                returnModel.data = null;
                returnModel.wallet = "";
                return returnModel;
            }
        }


    }
}
