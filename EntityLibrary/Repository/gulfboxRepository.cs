﻿using EntityLibrary.Data;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace EntityLibrary.Repository
{
    public class gulfboxRepository : BaseReference
    {
        public object services()
        {
            ClassLibrary.ServicePostModel.GulfboxPostModel.GulfboxServiceApi model = new ClassLibrary.ServicePostModel.GulfboxPostModel.GulfboxServiceApi();
            model.auth = new ClassLibrary.ServicePostModel.GulfboxAuthModel();
            model.auth = getAuthData();
            var returnModel = new ClassLibrary.ServicePostModel.GulfboxPostModel.GulfboxServiceApiReturnModel();
            //var request = (HttpWebRequest)WebRequest.Create("http://api.gulfbox.ae/services");

            //string output = JsonConvert.SerializeObject(model);
            //var data = System.Text.Encoding.ASCII.GetBytes(output);

            //request.Method = "POST";
            //request.ContentType = "application/json";
            //request.ContentLength = data.Length;

            //using (var stream = request.GetRequestStream())
            //{
            //    stream.Write(data, 0, data.Length);
            //}

            //var response = (HttpWebResponse)request.GetResponse();

            //var responseString = new System.IO.StreamReader(response.GetResponseStream()).ReadToEnd();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://api.gulfbox.ae/");
                var response = client.PostAsJsonAsync("services", model).Result;
                if (response.IsSuccessStatusCode)
                {
                    using (HttpContent content = response.Content)
                    {
                        // ... Read the string.
                        Task<string> result = content.ReadAsStringAsync();
                        dynamic data = JObject.Parse(result.Result);
                        returnModel.status = true;
                        returnModel.message = "Success";
                        returnModel.data = data;
                        return returnModel;
                    }
                }
                else
                {
                    returnModel.status = false;
                    returnModel.message = "No data found";
                    return returnModel;
                }
            }
        }

        public object providers(ClassLibrary.ServicePostModel.GulfboxPostModel.GulfboxProviderApi model)
        {
            model.auth = new ClassLibrary.ServicePostModel.GulfboxAuthModel();
            model.auth = getAuthData();
            var returnModel = new ClassLibrary.ServicePostModel.GulfboxPostModel.GulfboxProviderApiReturnModel();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://api.gulfbox.ae/");
                var response = client.PostAsJsonAsync("providers", model).Result;
                if (response.IsSuccessStatusCode)
                {
                    using (HttpContent content = response.Content)
                    {
                        // ... Read the string.
                        Task<string> result = content.ReadAsStringAsync();
                        dynamic data = JObject.Parse(result.Result);
                        returnModel.status = true;
                        returnModel.message = "Success";
                        returnModel.data = data;
                        return returnModel;
                    }
                }
                else
                {
                    returnModel.status = false;
                    returnModel.message = "No data found";
                    return returnModel;
                }
            }

        }

        public object transactioncheck(ClassLibrary.ServicePostModel.GulfboxPostModel.GulfboxTransactionApi model)
        {
            model.auth = new ClassLibrary.ServicePostModel.GulfboxAuthModel();
            model.auth = getAuthData();
            model.external_transaction_id = RandomString(30);
            var returnModel = new ClassLibrary.ServicePostModel.GulfboxPostModel.GulfboxTransactionApiReturnModel();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://api.gulfbox.ae/");
                var response = client.PostAsJsonAsync("transaction/check", model).Result;
                if (response.IsSuccessStatusCode)
                {
                    using (HttpContent content = response.Content)
                    {
                        // ... Read the string.
                        Task<string> result = content.ReadAsStringAsync();
                        dynamic data = JObject.Parse(result.Result);

                        returnModel.data = data;
                        if (data != null)
                        {
                            returnModel.status = true;
                            returnModel.message = "Success";
                            if (data.transaction != null)
                            {
                                var transactionCheck = _context.tb_Transaction.Create();
                                transactionCheck.Account = model.fields.account;
                                transactionCheck.ExternalTransactionId = model.external_transaction_id;
                                transactionCheck.GulfBoxTransactionId = data.transaction.id;
                                transactionCheck.State = data.transaction.state;
                                transactionCheck.Error = "";
                                transactionCheck.Amount = Convert.ToDecimal(0);
                                transactionCheck.AmountDeducted = Convert.ToDecimal(0);
                                DateTime NewDate = DateTime.ParseExact(Convert.ToString(data.transaction.time), "yyyy-MM-dd HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);
                                NewDate = DateTime.SpecifyKind(NewDate,
                 DateTimeKind.Unspecified);
                                TimeZoneInfo userTimeZone = TimeZoneInfo.FindSystemTimeZoneById("Arabian Standard Time");
                                DateTime convertedTime = TimeZoneInfo.ConvertTimeToUtc(NewDate, userTimeZone);
                                transactionCheck.Time = convertedTime;
                                transactionCheck.UserId = model.userId;
                                transactionCheck.AmountCurrency = string.Empty;
                                transactionCheck.AmountDeductedCurrency = string.Empty;
                                transactionCheck.Commission = Convert.ToDecimal(0);
                                _context.tb_Transaction.Add(transactionCheck);
                                try
                                {
                                    returnModel.external_transaction_id = model.external_transaction_id;
                                    returnModel.transaction_id = data.transaction.id;
                                    _context.SaveChanges();
                                }
                                catch (Exception ex)
                                {

                                }

                            }
                        }
                        else
                        {
                            returnModel.status = false;
                            returnModel.message = "No data found";
                            return returnModel;
                        }
                        return returnModel;
                    }
                }
                else
                {
                    returnModel.status = false;
                    returnModel.message = "Failed";
                    return returnModel;
                }
            }

        }

        public object transactionconfirm(ClassLibrary.ServicePostModel.GulfboxPostModel.GulfboxTransactionConfirmApi model)
        {
            model.auth = new ClassLibrary.ServicePostModel.GulfboxAuthModel();
            model.auth = getAuthData();
            var returnModel = new ClassLibrary.ServicePostModel.GulfboxPostModel.GulfboxTransactionConfirmApiReturnModel();

            var user = _context.tb_User.FirstOrDefault(z => z.UserID == model.userId);
            if (user != null)
            {

                try
                {
                    var rowIp = _context.tb_UserTransactionIp.Create();
                    rowIp.NetworkIp = model.ip;
                    rowIp.TimeStamp = System.DateTime.UtcNow;
                    rowIp.UserId = user.UserID;
                    _context.tb_UserTransactionIp.Add(rowIp);
                    _context.SaveChanges();
                }
                catch (Exception ex)
                {

                }

                //var currecyRate = _context.tb_AdminCurrencyRate.Where(z => z.Currency == model.currency).FirstOrDefault();
                var currecyRate = _context.tb_AdminCurrencyRate.Where(z => z.ToCurrency == user.Currency && z.FromCurrency == model.amountCurrency).FirstOrDefault();
                if (currecyRate != null)
                {
                    //var amount = Convert.ToDecimal(Convert.ToDouble(model.amountAED) * currecyRate.CurrencyRate);
                    var amount = Convert.ToDecimal(Convert.ToDouble(model.fields.amount) * currecyRate.CurrencyRate);
                    var deductamt = amount;
                    decimal commisionAmount = Convert.ToDecimal(0);
                    var newAmount = user.Wallet;
                    if (user.UserType == (int)ClassLibrary.Enum.userType.Agent)
                    {
                        var commision = _context.tb_AgentCommisionRate.Where(z => z.AgentId == user.UserID && z.IsActive && z.ProductType == model.productType).FirstOrDefault();
                        if (commision != null)
                        {
                            commisionAmount = ((Convert.ToDecimal(amount) * Convert.ToDecimal(commision.Commission)) / 100);
                            deductamt = Convert.ToDecimal(amount) - commisionAmount;
                            newAmount = user.Wallet - Convert.ToDecimal(deductamt);
                        }
                        else
                        {
                            newAmount = user.Wallet - Convert.ToDecimal(amount); ;
                        }
                    }
                    else
                    {
                        newAmount = user.Wallet - Convert.ToDecimal(amount);
                    }

                    if (user.Wallet >= deductamt && deductamt >= Convert.ToDecimal(0) && user.Wallet > Convert.ToDecimal(0))
                    {

                        using (var client = new HttpClient())
                        {
                            client.BaseAddress = new Uri("http://api.gulfbox.ae/");
                            var response = client.PostAsJsonAsync("transaction/confirm", model).Result;
                            if (response.IsSuccessStatusCode)
                            {
                                using (HttpContent content = response.Content)
                                {
                                    // ... Read the string.
                                    Task<string> result = content.ReadAsStringAsync();
                                    dynamic data = JObject.Parse(result.Result);

                                    returnModel.status = true;
                                    returnModel.message = "Success";
                                    returnModel.data = data;
                                    returnModel.wallet = user.Wallet.ToString();


                                    if (data.transaction != null)
                                    {
                                        var transaction = _context.tb_Transaction.Where(z => z.ExternalTransactionId == model.external_transaction_id && z.GulfBoxTransactionId == model.transaction_id).FirstOrDefault();
                                        if (transaction != null)
                                        {


                                            transaction.AmountDeducted = deductamt;
                                            transaction.AmountDeductedCurrency = user.Currency;
                                            transaction.AmountCurrency = model.amountCurrency ?? string.Empty;
                                            transaction.Commission = commisionAmount;

                                            transaction.Amount = Convert.ToDecimal(model.fields.amount);
                                            transaction.AmountAED = Convert.ToDecimal(model.amountAED);
                                            transaction.State = data.transaction.state;
                                            if (transaction.State == 2)
                                            {
                                                transaction.Error = data.transaction.error ?? string.Empty;
                                            }
                                            else
                                            {
                                                user.Wallet = newAmount;
                                            }
                                        }
                                        try
                                        {
                                            _context.SaveChanges();
                                            returnModel.wallet = user.Wallet.ToString();
                                        }
                                        catch (Exception ex)
                                        {
                                            returnModel.status = false;
                                            returnModel.message = "Failed";
                                            returnModel.wallet = user.Wallet.ToString();
                                        }

                                    }
                                    return returnModel;
                                }
                            }
                            else
                            {
                                returnModel.status = false;
                                returnModel.message = "Failed";
                                returnModel.wallet = user.Wallet.ToString();
                                return returnModel;
                            }
                        }
                    }
                    else
                    {
                        returnModel.status = false;
                        returnModel.message = "Insufficient Amount";
                        returnModel.wallet = user.Wallet.ToString();
                        return returnModel;
                    }

                }
                else
                {
                    returnModel.status = false;
                    returnModel.message = "Failed";
                    returnModel.wallet = user.Wallet.ToString();
                    return returnModel;
                }
            }
            else
            {
                returnModel.status = false;
                returnModel.message = "Failed";
                returnModel.wallet = user.Wallet.ToString();
                return returnModel;
            }
        }

        public object balance(ClassLibrary.ServicePostModel.GulfboxPostModel.GulfboxBalanceApi model)
        {
            model.auth = new ClassLibrary.ServicePostModel.GulfboxAuthModel();
            model.auth = getAuthData();
            var returnModel = new ClassLibrary.ServicePostModel.GulfboxPostModel.GulfboxBalanceReturnModel();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://api.gulfbox.ae/");
                var response = client.PostAsJsonAsync("balance", model).Result;
                if (response.IsSuccessStatusCode)
                {
                    using (HttpContent content = response.Content)
                    {
                        // ... Read the string.
                        Task<string> result = content.ReadAsStringAsync();
                        dynamic data = JObject.Parse(result.Result);

                        returnModel.status = true;
                        returnModel.message = "Success";
                        returnModel.data = data;
                        return returnModel;
                    }
                }
                else
                {
                    returnModel.status = false;
                    returnModel.message = "No data found";
                    return returnModel;
                }
            }
        }

        public object servicetransactioncheck(ClassLibrary.ServicePostModel.GulfboxPostModel.GulfboxServiceTransactionApi model)
        {
            model.auth = new ClassLibrary.ServicePostModel.GulfboxAuthModel();
            model.auth = getAuthData();
            model.external_transaction_id = RandomString(30);
            var returnModel = new ClassLibrary.ServicePostModel.GulfboxPostModel.GulfboxServiceTransactionApiReturnModel();

            var user = _context.tb_User.FirstOrDefault(z => z.UserID == model.userId);
            if (user != null)
            {

                try
                {
                    var rowIp = _context.tb_UserTransactionIp.Create();
                    rowIp.NetworkIp = model.ip;
                    rowIp.TimeStamp = System.DateTime.UtcNow;
                    rowIp.UserId = user.UserID;
                    _context.tb_UserTransactionIp.Add(rowIp);
                    _context.SaveChanges();
                }
                catch (Exception ex)
                {

                }


                var currecyRate = _context.tb_AdminCurrencyRate.Where(z => z.ToCurrency == user.Currency && z.FromCurrency == "AED").FirstOrDefault();
                if (currecyRate != null)
                {
                    if (user.Wallet >= Convert.ToDecimal(Convert.ToDouble(model.amountAED) * currecyRate.CurrencyRate))
                    {
                        using (var client = new HttpClient())
                        {
                            client.BaseAddress = new Uri("http://api.gulfbox.ae/");
                            var response = client.PostAsJsonAsync("transaction/check", model).Result;
                            if (response.IsSuccessStatusCode)
                            {
                                using (HttpContent content = response.Content)
                                {
                                    // ... Read the string.
                                    Task<string> result = content.ReadAsStringAsync();
                                    dynamic data = JObject.Parse(result.Result);

                                    returnModel.data = data;
                                    if (data != null)
                                    {
                                        returnModel.status = true;
                                        returnModel.message = "Success";
                                        if (data.transaction != null)
                                        {
                                            var transactionCheck = _context.tb_Transaction.Create();
                                            transactionCheck.Account = model.fields.account;
                                            transactionCheck.ExternalTransactionId = model.external_transaction_id;
                                            transactionCheck.GulfBoxTransactionId = data.transaction.id;
                                            transactionCheck.State = data.transaction.state;
                                            DateTime NewDate = DateTime.ParseExact(Convert.ToString(data.transaction.time), "yyyy-MM-dd HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);
                                            NewDate = DateTime.SpecifyKind(NewDate,
                             DateTimeKind.Unspecified);
                                            TimeZoneInfo userTimeZone = TimeZoneInfo.FindSystemTimeZoneById("Arabian Standard Time");
                                            DateTime convertedTime = TimeZoneInfo.ConvertTimeToUtc(NewDate, userTimeZone);
                                            transactionCheck.Time = convertedTime;
                                            transactionCheck.UserId = model.userId;
                                            _context.tb_Transaction.Add(transactionCheck);
                                            try
                                            {
                                                returnModel.external_transaction_id = model.external_transaction_id;
                                                returnModel.transaction_id = data.transaction.id;
                                                _context.SaveChanges();
                                            }
                                            catch (Exception ex)
                                            {

                                            }

                                        }
                                    }
                                    return returnModel;
                                }
                            }
                            else
                            {
                                returnModel.status = false;
                                returnModel.message = "No data found";
                                return returnModel;
                            }
                        }
                    }
                    else
                    {
                        returnModel.status = false;
                        returnModel.message = "Insufficient Amount";
                        return returnModel;

                    }


                }
                else
                {
                    returnModel.status = false;
                    returnModel.message = "Failed";
                    return returnModel;
                }
            }
            else
            {
                returnModel.status = false;
                returnModel.message = "Failed";
                return returnModel;
            }


        }

        public object servicetransactionconfirm(ClassLibrary.ServicePostModel.GulfboxPostModel.GulfboxServiceTransactionConfirmApi model)
        {
            model.auth = new ClassLibrary.ServicePostModel.GulfboxAuthModel();
            model.auth = getAuthData();
            var returnModel = new ClassLibrary.ServicePostModel.GulfboxPostModel.GulfboxServiceTransactionConfirmApiReturnModel();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://api.gulfbox.ae/");
                var response = client.PostAsJsonAsync("transaction/confirm", model).Result;
                if (response.IsSuccessStatusCode)
                {
                    using (HttpContent content = response.Content)
                    {
                        // ... Read the string.
                        Task<string> result = content.ReadAsStringAsync();
                        dynamic data = JObject.Parse(result.Result);

                        returnModel.status = true;
                        returnModel.message = "Success";
                        returnModel.data = data;
                        return returnModel;
                    }
                }
                else
                {
                    returnModel.status = false;
                    returnModel.message = "No data found";
                    return returnModel;
                }
            }
        }

        public object vouchertransaction(ClassLibrary.ServicePostModel.GulfboxPostModel.GulfboxVoucherTransactionApi model)
        {
            var returnModel = new ClassLibrary.ServicePostModel.GulfboxPostModel.GulfboxVoucherTransactionApiReturnModel();
            var fetchStoredCard = _context.SP_FETCH_STORED_VOUCHERS(model.type, model.amount, model.userId).ToList()
                .Select(z => new SP_FETCH_STORED_VOUCHERS(z)).FirstOrDefault();
            if (fetchStoredCard != null)
            {
                var user = _context.tb_User.FirstOrDefault(z => z.UserID == model.userId);
                if (user != null)
                {

                    try
                    {
                        var rowIp = _context.tb_UserTransactionIp.Create();
                        rowIp.NetworkIp = model.ip;
                        rowIp.TimeStamp = System.DateTime.UtcNow;
                        rowIp.UserId = user.UserID;
                        _context.tb_UserTransactionIp.Add(rowIp);
                        _context.SaveChanges();
                    }
                    catch (Exception ex)
                    {

                    }



                    var currecyRate = _context.tb_AdminCurrencyRate.Where(z => z.ToCurrency == user.Currency && z.FromCurrency == "AED").FirstOrDefault();
                    if (currecyRate != null)
                    {
                        var amount = Convert.ToDecimal(Convert.ToDouble(model.amount) * currecyRate.CurrencyRate);
                        var newAmount = user.Wallet;
                        decimal commisionAmount = Convert.ToDecimal(0);
                        var deductamt = amount;
                        if (user.UserType == (int)ClassLibrary.Enum.userType.Agent)
                        {
                            var commision = _context.tb_AgentCommisionRate.Where(z => z.AgentId == user.UserID && z.IsActive && z.ProductType == model.productType && z.ApiProvider == (int)ClassLibrary.Enum.apiProvider.Gulfbox).FirstOrDefault();
                            if (commision != null)
                            {
                                commisionAmount = ((Convert.ToDecimal(amount) * Convert.ToDecimal(commision.Commission)) / 100);
                                deductamt = Convert.ToDecimal(amount) - commisionAmount;
                                newAmount = user.Wallet - Convert.ToDecimal(deductamt);
                            }
                            else
                            {
                                newAmount = user.Wallet - Convert.ToDecimal(amount);
                            }
                        }
                        else
                        {
                            newAmount = user.Wallet - Convert.ToDecimal(amount);
                        }
                        if (user.Wallet >= deductamt && deductamt >= Convert.ToDecimal(0) && user.Wallet > Convert.ToDecimal(0))
                        {

                            try
                            {
                                var response = _context.SP_INSERT_USED_STOREDCARD(model.userId, fetchStoredCard.PID, Convert.ToString(deductamt), user.Currency, commisionAmount).FirstOrDefault();
                                if (Convert.ToInt64(response) > 0)
                                {

                                    user.Wallet = newAmount;
                                    try
                                    {
                                        _context.SaveChanges();
                                        returnModel.wallet = user.Wallet.ToString();
                                        returnModel.hasImportedCards = true;
                                        returnModel.status = true;
                                        returnModel.message = "Success";
                                        returnModel.serialNumber = fetchStoredCard.SerialNumber;
                                        returnModel.pinNumber = fetchStoredCard.PinNumber;
                                        returnModel.transaction_id = "";
                                        returnModel.external_transaction_id = "";
                                        returnModel.data = null;
                                    }
                                    catch (Exception ex)
                                    {
                                        returnModel.status = false;
                                        returnModel.message = "Failed";
                                        returnModel.wallet = user.Wallet.ToString();
                                        returnModel.serialNumber = "";
                                        returnModel.pinNumber = "";
                                        returnModel.data = null;
                                        return returnModel;
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                returnModel.status = false;
                                returnModel.message = "Failed";
                                returnModel.wallet = user.Wallet.ToString();
                                returnModel.serialNumber = "";
                                returnModel.pinNumber = "";
                                returnModel.data = null;
                                return returnModel;
                            }
                        }
                        else
                        {
                            returnModel.status = false;
                            returnModel.message = "Insufficient Amount";
                            returnModel.wallet = user.Wallet.ToString();
                            returnModel.serialNumber = "";
                            returnModel.pinNumber = "";
                            returnModel.data = null;
                            return returnModel;
                        }

                    }
                    else
                    {
                        returnModel.status = false;
                        returnModel.message = "Failed";
                        returnModel.serialNumber = "";
                        returnModel.pinNumber = "";
                        returnModel.data = null;
                        returnModel.wallet = user.Wallet.ToString();
                        return returnModel;
                    }
                }
                else
                {
                    returnModel.status = false;
                    returnModel.message = "Failed";
                    returnModel.wallet = "0";
                    returnModel.serialNumber = "";
                    returnModel.pinNumber = "";
                    returnModel.data = null;
                    return returnModel;
                }
                return returnModel;
            }
            else
            {


                model.auth = new ClassLibrary.ServicePostModel.GulfboxAuthModel();
                model.auth = getAuthData();
                model.external_transaction_id = RandomString(30);
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri("http://api.gulfbox.ae/");
                    var response = client.PostAsJsonAsync("transaction/check", model).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        using (HttpContent content = response.Content)
                        {
                            // ... Read the string.
                            Task<string> result = content.ReadAsStringAsync();
                            dynamic data = JObject.Parse(result.Result);

                            returnModel.data = data;
                            if (data != null)
                            {
                                returnModel.status = true;
                                returnModel.message = "Success";
                                if (data.transaction != null)
                                {
                                    var transactionCheck = _context.tb_GulfboxVoucherTransaction.Create();
                                    transactionCheck.ExternalTransactionId = model.external_transaction_id;
                                    transactionCheck.GulfBoxTransactionId = data.transaction.id;
                                    transactionCheck.State = Convert.ToInt32(-1);
                                    transactionCheck.Error = "";
                                    transactionCheck.SerialNumber = "";
                                    transactionCheck.CardNumber = "";
                                    transactionCheck.Amount = Convert.ToDecimal(0);
                                    transactionCheck.AmountDeducted = Convert.ToDecimal(0);
                                    transactionCheck.Commission = Convert.ToDecimal(0);
                                    transactionCheck.ProductType = string.Empty;
                                    DateTime NewDate = DateTime.ParseExact(Convert.ToString(data.transaction.time), "yyyy-MM-dd HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);
                                    NewDate = DateTime.SpecifyKind(NewDate,
                     DateTimeKind.Unspecified);
                                    TimeZoneInfo userTimeZone = TimeZoneInfo.FindSystemTimeZoneById("Arabian Standard Time");
                                    DateTime convertedTime = TimeZoneInfo.ConvertTimeToUtc(NewDate, userTimeZone);
                                    transactionCheck.Time = convertedTime;
                                    transactionCheck.UserId = model.userId;
                                    transactionCheck.AmountCurrency = string.Empty;
                                    transactionCheck.AmountDeductedCurrency = string.Empty;
                                    _context.tb_GulfboxVoucherTransaction.Add(transactionCheck);
                                    try
                                    {
                                        returnModel.external_transaction_id = model.external_transaction_id;
                                        returnModel.transaction_id = data.transaction.id;
                                        _context.SaveChanges();
                                    }
                                    catch (Exception ex)
                                    {

                                    }

                                }
                            }
                            else
                            {
                                returnModel.status = false;
                                returnModel.message = "No data found";
                                return returnModel;
                            }
                            return returnModel;
                        }
                    }
                    else
                    {
                        returnModel.status = false;
                        returnModel.message = "Failed";
                        return returnModel;
                    }
                }
            }
        }

        public object vouchertransactionconfirm(ClassLibrary.ServicePostModel.GulfboxPostModel.GulfboxVoucherTransactionConfirmApi model)
        {

            model.auth = new ClassLibrary.ServicePostModel.GulfboxAuthModel();
            model.auth = getAuthData();
            var returnModel = new ClassLibrary.ServicePostModel.GulfboxPostModel.GulfboxVoucherTransactionConfirmApiReturnModel();

            var user = _context.tb_User.FirstOrDefault(z => z.UserID == model.userId);
            if (user != null)
            {


                try
                {
                    var rowIp = _context.tb_UserTransactionIp.Create();
                    rowIp.NetworkIp = model.ip;
                    rowIp.TimeStamp = System.DateTime.UtcNow;
                    rowIp.UserId = user.UserID;
                    _context.tb_UserTransactionIp.Add(rowIp);
                    _context.SaveChanges();
                }
                catch (Exception ex)
                {

                }





                var currecyRate = _context.tb_AdminCurrencyRate.Where(z => z.ToCurrency == user.Currency && z.FromCurrency == "AED").FirstOrDefault();
                if (currecyRate != null)
                {
                    var amount = Convert.ToDecimal(Convert.ToDouble(model.amountAED) * currecyRate.CurrencyRate);
                    var newAmount = user.Wallet;
                    decimal commisionAmount = Convert.ToDecimal(0);
                    var deductamt = amount;
                    if (user.UserType == (int)ClassLibrary.Enum.userType.Agent)
                    {
                        var commision = _context.tb_AgentCommisionRate.Where(z => z.AgentId == user.UserID && z.IsActive && z.ProductType == model.productType && z.ApiProvider == (int)ClassLibrary.Enum.apiProvider.Gulfbox).FirstOrDefault();
                        if (commision != null)
                        {
                            commisionAmount = ((Convert.ToDecimal(amount) * Convert.ToDecimal(commision.Commission)) / 100);
                            deductamt = Convert.ToDecimal(amount) - commisionAmount;
                            newAmount = user.Wallet - Convert.ToDecimal(deductamt);

                        }
                        else
                        {
                            newAmount = user.Wallet - Convert.ToDecimal(amount);
                        }
                    }
                    else
                    {
                        newAmount = user.Wallet - Convert.ToDecimal(amount);
                    }

                    if (user.Wallet >= deductamt && deductamt >= Convert.ToDecimal(0) && user.Wallet > Convert.ToDecimal(0))
                    {

                        using (var client = new HttpClient())
                        {
                            client.BaseAddress = new Uri("http://api.gulfbox.ae/");
                            var response = client.PostAsJsonAsync("transaction/confirm", model).Result;
                            if (response.IsSuccessStatusCode)
                            {
                                using (HttpContent content = response.Content)
                                {
                                    // ... Read the string.
                                    Task<string> result = content.ReadAsStringAsync();
                                    dynamic data = JObject.Parse(result.Result);

                                    returnModel.status = true;
                                    returnModel.message = "Success";
                                    returnModel.data = data;
                                    returnModel.wallet = user.Wallet.ToString();

                                    if (data.transaction != null)
                                    {
                                        var transaction = _context.tb_GulfboxVoucherTransaction.Where(z => z.ExternalTransactionId == model.external_transaction_id && z.GulfBoxTransactionId == model.transaction_id).FirstOrDefault();
                                        if (transaction != null)
                                        {
                                            user.Wallet = newAmount;
                                            transaction.AmountDeducted = deductamt;
                                            transaction.AmountDeductedCurrency = user.Currency;
                                            transaction.AmountCurrency = model.amountCurrency;
                                            transaction.Commission = commisionAmount;
                                            transaction.Amount = Convert.ToDecimal(model.amountAED);
                                            transaction.State = data.transaction.state;
                                            if (data.info != null)
                                            {
                                                transaction.SerialNumber = data.info.serial ?? string.Empty;
                                                transaction.CardNumber = data.info.number ?? string.Empty;
                                            }
                                            else
                                            {
                                                transaction.SerialNumber = "";
                                                transaction.CardNumber = "";
                                            }
                                            if (transaction.State == 2)
                                            {
                                                transaction.Error = data.transaction.error;
                                            }

                                            if (model.productType == (int)ClassLibrary.Enum.GulfboxProductTypes.Du)
                                                transaction.ProductType = "Du";
                                            else if (model.productType == (int)ClassLibrary.Enum.GulfboxProductTypes.Etisalat)
                                                transaction.ProductType = "Etisalat";
                                            else if (model.productType == (int)ClassLibrary.Enum.GulfboxProductTypes.Hello)
                                                transaction.ProductType = "Hello";
                                            else if (model.productType == (int)ClassLibrary.Enum.GulfboxProductTypes.Five)
                                                transaction.ProductType = "Five";
                                            else if (model.productType == (int)ClassLibrary.Enum.GulfboxProductTypes.Salik)
                                                transaction.ProductType = "Salik";
                                        }
                                        try
                                        {
                                            _context.SaveChanges();
                                            returnModel.wallet = user.Wallet.ToString();
                                        }
                                        catch (Exception ex)
                                        {
                                            returnModel.status = false;
                                            returnModel.message = "Failed";
                                            returnModel.wallet = user.Wallet.ToString();
                                            return returnModel;
                                        }

                                    }
                                    return returnModel;
                                }
                            }
                            else
                            {
                                returnModel.status = false;
                                returnModel.message = "Failed";
                                returnModel.wallet = user.Wallet.ToString();
                                return returnModel;
                            }
                        }
                    }
                    else
                    {
                        returnModel.status = false;
                        returnModel.message = "Insufficient Amount";
                        returnModel.wallet = user.Wallet.ToString();
                        return returnModel;
                    }

                }
                else
                {
                    returnModel.status = false;
                    returnModel.message = "Failed";
                    returnModel.wallet = user.Wallet.ToString();
                    return returnModel;
                }
            }
            else
            {
                returnModel.status = false;
                returnModel.message = "Failed";
                returnModel.wallet = "0";
                return returnModel;
            }
        }

        public object transactionStatusUpdate()
        {
            var model = new ClassLibrary.ServicePostModel.GulfboxPostModel.GulfboxFindApi();
            model.auth = new ClassLibrary.ServicePostModel.GulfboxAuthModel();


            var list = _context.tb_Transaction.Where(z => z.State == 0).ToList();
            if (list.Count > 0)
            {
                foreach (var item in list)
                {
                    try
                    {
                        model.transaction_id = item.GulfBoxTransactionId;
                        model.auth = getAuthData();

                        using (var client = new HttpClient())
                        {
                            client.BaseAddress = new Uri("http://api.gulfbox.ae/");
                            var response = client.PostAsJsonAsync("transaction/find", model).Result;
                            if (response.IsSuccessStatusCode)
                            {
                                using (HttpContent content = response.Content)
                                {
                                    // ... Read the string.
                                    Task<string> result = content.ReadAsStringAsync();
                                    dynamic data = JObject.Parse(result.Result);

                                    if (data.transaction != null)
                                    {
                                        if (item.State != Convert.ToInt32(data.transaction.state))
                                        {
                                            item.State = Convert.ToInt32(data.transaction.state);
                                            if (item.State == 2)
                                            {
                                                var user = _context.tb_User.FirstOrDefault(z => z.UserID == item.UserId);
                                                if (user != null)
                                                {
                                                    user.Wallet = user.Wallet + item.AmountDeducted;
                                                }
                                                item.Error = data.transaction.error ?? string.Empty;

                                            }
                                            try
                                            {
                                                _context.SaveChanges();
                                            }
                                            catch (Exception ex)
                                            {

                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {

                    }
                }

            }
            return true;
        }

        public object voucherTransactionStatusUpdate()
        {
            var model = new ClassLibrary.ServicePostModel.GulfboxPostModel.GulfboxFindApi();
            model.auth = new ClassLibrary.ServicePostModel.GulfboxAuthModel();


            var list = _context.tb_GulfboxVoucherTransaction.Where(z => z.State == 0).ToList();
            if (list.Count > 0)
            {
                foreach (var item in list)
                {
                    try
                    {
                        model.transaction_id = item.GulfBoxTransactionId;
                        model.auth = getAuthData();

                        using (var client = new HttpClient())
                        {
                            client.BaseAddress = new Uri("http://api.gulfbox.ae/");
                            var response = client.PostAsJsonAsync("transaction/find", model).Result;
                            if (response.IsSuccessStatusCode)
                            {
                                using (HttpContent content = response.Content)
                                {
                                    // ... Read the string.
                                    Task<string> result = content.ReadAsStringAsync();
                                    dynamic data = JObject.Parse(result.Result);

                                    if (data.transaction != null)
                                    {
                                        if (item.State != Convert.ToInt32(data.transaction.state))
                                        {
                                            if (data.info != null)
                                            {
                                                item.SerialNumber = data.info.serial ?? string.Empty;
                                                item.CardNumber = data.info.number ?? string.Empty;
                                            }
                                            else
                                            {
                                                item.SerialNumber = "";
                                                item.CardNumber = "";
                                            }
                                            item.State = Convert.ToInt32(data.transaction.state);
                                            if (item.State == 2)
                                            {
                                                var user = _context.tb_User.FirstOrDefault(z => z.UserID == item.UserId);
                                                if (user != null)
                                                {
                                                    user.Wallet = user.Wallet + item.AmountDeducted;
                                                }
                                                item.Error = data.transaction.error ?? string.Empty;

                                            }
                                            try
                                            {
                                                _context.SaveChanges();
                                            }
                                            catch (Exception ex)
                                            {

                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {

                    }
                }

            }
            return true;
        }
    }
}
