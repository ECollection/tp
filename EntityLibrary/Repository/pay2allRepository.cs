﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace EntityLibrary.Repository
{
    public class pay2allRepository : BaseReference
    {
        public object prepaidrechargeANDmobilepostpaidbill(ClassLibrary.ServicePostModel.Pay2All.prepaidrechargeANDmobilepostpaidbill model)
        {
            var responseModel = new ClassLibrary.ServicePostModel.Pay2All.prepaidrechargeANDmobilepostpaidbillReturnModel();
            responseModel.data = new ClassLibrary.ServicePostModel.Pay2All.prepaidrechargeANDmobilepostpaidbillApiResponse();
            try
            {
                var user = _context.tb_User.FirstOrDefault(z => z.UserID == model.userId && z.IsActive);
                if (user != null)
                {

                    try
                    {
                        var rowIp = _context.tb_UserTransactionIp.Create();
                        rowIp.NetworkIp = model.ip;
                        rowIp.TimeStamp = System.DateTime.UtcNow;
                        rowIp.UserId = user.UserID;
                        _context.tb_UserTransactionIp.Add(rowIp);
                        _context.SaveChanges();
                    }
                    catch (Exception ex)
                    { }


                    var currency = _context.tb_AdminCurrencyRate.Where(z => z.ToCurrency == user.Currency && z.FromCurrency == model.amountCurrency).FirstOrDefault();
                    if (currency != null)
                    {
                        var newAmount = user.Wallet;
                        var convertedAmount = Convert.ToDecimal(Convert.ToDouble(model.amount) * currency.CurrencyRate);
                        var deductamt = convertedAmount;
                        decimal commissionAmount = Convert.ToDecimal(0);
                        if (user.UserType == (int)ClassLibrary.Enum.userType.Agent)
                        {
                            var commision = _context.tb_AgentCommisionRate.Where(z => z.AgentId == user.UserID && z.IsActive && z.ProductType == model.productType && z.ApiProvider == (int)ClassLibrary.Enum.apiProvider.Pay2All).FirstOrDefault();
                            if (commision != null)
                            {
                                commissionAmount = ((Convert.ToDecimal(convertedAmount) * Convert.ToDecimal(commision.Commission)) / 100);
                                deductamt = Convert.ToDecimal(convertedAmount) - commissionAmount;
                                newAmount = user.Wallet - Convert.ToDecimal(deductamt);
                            }
                            else
                            {
                                newAmount = user.Wallet - Convert.ToDecimal(convertedAmount); ;
                            }
                        }
                        else
                        {
                            newAmount = user.Wallet - Convert.ToDecimal(convertedAmount);
                        }


                        if (user.Wallet >= deductamt && deductamt >= Convert.ToDecimal(0) && user.Wallet > Convert.ToDecimal(0))
                        {
                            var clientId = RandomPay2AllClientId(15);


                            var newTransaction = _context.tPay2AllTransaction.Create();
                            newTransaction.Amount = model.amount ?? string.Empty;
                            newTransaction.ClientId = clientId;
                            newTransaction.Guid = Guid.NewGuid();
                            newTransaction.Number = model.number;
                            newTransaction.UserId = model.userId;
                            newTransaction.ProviderId = model.providerId;
                            newTransaction.Pay2AllMessage = string.Empty;
                            newTransaction.Pay2AllOperatorRef = string.Empty;
                            newTransaction.Pay2AllPayId = Convert.ToInt64(0);
                            newTransaction.Pay2AllStatus = string.Empty;
                            newTransaction.Pay2AllTxstatusDesc = "Pending";
                            newTransaction.Timestamp = System.DateTime.UtcNow;
                            newTransaction.AmountCurrency = model.amountCurrency;
                            newTransaction.AmountDeductedCurrency = user.Currency;
                            newTransaction.AmountDeducted = String.Format("{0:0.00}", deductamt);
                            newTransaction.Commission = commissionAmount;


                            _context.tPay2AllTransaction.Add(newTransaction);

                            if (_context.SaveChanges() > 0)
                            {

                                string Url = "https://www.pay2all.in/web-api/paynow?";
                                Url = Url + "api_token=" + apiToken_pay2All + "&number=" + model.number + "&provider_id=" + model.providerId + "&amount=" + model.amount + "&client_id=" + clientId + "&live=" + pay2AllLive;
                                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(Url);
                                //WebRequest request = WebRequest.Create(baseUrl);
                                // Set the Method property of the request to POST.  
                                request.Method = "GET";
                                request.Accept = "application/json";

                                //var postModel = new ClassLibrary.ServicePostModel.MobileApPostModel.AccessTokenPostModel();
                                //postModel.username = username_mobileApi;
                                //postModel.password = password_mobileApi;

                                //var serializedPostModel = JsonConvert.SerializeObject(postModel);

                                //request.Headers.Add("Authorization", "Bearer " + data.access_token);
                                //request.Headers.Add("Content-Type", "x-www-form-urlencoded");
                                //request.Headers.Add("Accept", "application/json");
                                //request.Accept("application/json");
                                //request.Headers.Add("Content-Type", "application/json");
                                ServicePointManager.Expect100Continue = true;
                                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                                // Create POST data and convert it to a byte array.  
                                //string postData = serializedPostModel;
                                //byte[] byteArray = Encoding.UTF8.GetBytes(postData);
                                // Set the ContentType property of the WebRequest.  
                                //request.ContentType = "application/form-data";
                                request.ContentType = "x-www-form-urlencoded";
                                // Set the ContentLength property of the WebRequest.  
                                //request.ContentLength = byteArray.Length;
                                // Get the request stream.  
                                //Stream dataStream = request.GetRequestStream();
                                // Write the data to the request stream.  
                                //dataStream.Write(byteArray, 0, byteArray.Length);
                                // Close the Stream object.  
                                //dataStream.Close();
                                // Get the response.  
                                WebResponse response = request.GetResponse();
                                // Display the status.  
                                //Console.WriteLine(((HttpWebResponse)response).StatusDescription);
                                // Get the stream containing content returned by the server.  
                                Stream dataStream = response.GetResponseStream();
                                // Open the stream using a StreamReader for easy access.  
                                StreamReader reader = new StreamReader(dataStream);
                                // Read the content.  
                                string responseFromServer = reader.ReadToEnd();
                                // Display the content.  
                                //Console.WriteLine(responseFromServer);
                                // Clean up the streams.  
                                reader.Close();
                                dataStream.Close();
                                response.Close();

                                try
                                {
                                    //responseModel.Success = Newtonsoft.Json.JsonConvert.DeserializeObject(responseFromServer);
                                    dynamic ress = new System.Dynamic.ExpandoObject();
                                    //var ress = Newtonsoft.Json.JsonConvert.DeserializeObject<ClassLibrary.ServicePostModel.Pay2All.prepaidrechargeANDmobilepostpaidbillApiResponse>(responseFromServer);
                                    ress = Newtonsoft.Json.JsonConvert.DeserializeObject(responseFromServer);
                                    if (ress != null)
                                    {
                                        string a1 = ress.payid; 
                                        if (a1 != "0")
                                        {
                                            newTransaction.Pay2AllMessage = ress.message ?? string.Empty;
                                            newTransaction.Pay2AllOperatorRef = ress.operator_ref ?? string.Empty;

                                            try
                                            {
                                                newTransaction.Pay2AllPayId = Convert.ToInt64(ress.payid);
                                            }
                                            catch (Exception ex)
                                            {
                                                newTransaction.Pay2AllPayId = Convert.ToInt64(0);

                                            }
                                            newTransaction.Pay2AllStatus = ress.status ?? string.Empty;
                                            newTransaction.Pay2AllTxstatusDesc = ress.txstatus_desc ?? string.Empty;
                                            newTransaction.Timestamp = System.DateTime.UtcNow;

                                            if (ress.status == "success" && ress.txstatus_desc != "Failure")
                                                user.Wallet = newAmount;

                                            if (_context.SaveChanges() > 0)
                                            {
                                                responseModel.status = true;
                                                responseModel.message = "Success";
                                                responseModel.wallet = user.Wallet.ToString();
                                            }
                                            else
                                            {
                                                responseModel.status = false;
                                                responseModel.message = "Failed";
                                                responseModel.wallet = user.Wallet.ToString();
                                            }



                                            responseModel.data.message = ress.message ?? string.Empty;
                                            responseModel.data.operator_ref = ress.operator_ref ?? string.Empty;
                                            responseModel.data.payid = Convert.ToString(ress.payid);
                                            responseModel.data.status = ress.status ?? string.Empty;
                                            responseModel.data.txstatus_desc = ress.txstatus_desc ?? string.Empty;

                                            //responseModel.status = true;
                                            //responseModel.message = "success";
                                            return responseModel;
                                        }
                                        else
                                        {
                                            responseModel.status = false;
                                            responseModel.message = "failed";
                                            responseModel.wallet = user.Wallet.ToString();
                                            return responseModel;
                                        }
                                        //////////
                                    }
                                    else
                                    {
                                        responseModel.status = false;
                                        responseModel.message = "failed";
                                        responseModel.wallet = user.Wallet.ToString();
                                        return responseModel;
                                    }
                                }
                                catch (Exception ex)
                                {
                                    //responseModel.Success = Newtonsoft.Json.JsonConvert.DeserializeObject(responseFromServer);
                                    //var ress = Newtonsoft.Json.JsonConvert.DeserializeObject<ClassLibrary.ServicePostModel.databeats.MNPResponse.Failure>(responseFromServer);
                                    responseModel.status = false;
                                    responseModel.message = ex.Message;
                                    responseModel.wallet = user.Wallet.ToString();
                                    return responseModel;
                                }
                            }
                            else
                            {
                                responseModel.status = false;
                                responseModel.message = "Failed";
                                responseModel.wallet = user.Wallet.ToString();
                                return responseModel;
                            }

                        }
                        else
                        {
                            responseModel.status = false;
                            responseModel.message = "Insufficient Amount";
                            responseModel.wallet = user.Wallet.ToString();
                            return responseModel;
                        }
                    }
                    else
                    {
                        responseModel.status = false;
                        responseModel.message = "Failed";
                        responseModel.wallet = user.Wallet.ToString();
                        return responseModel;
                    }
                }
                else
                {
                    responseModel.status = false;
                    responseModel.message = "User not found";
                    responseModel.wallet = "0";
                    return responseModel;
                }
            }
            catch (Exception ex)
            {
                responseModel.status = false;
                responseModel.message = ex.Message;
                return responseModel;
            }
        }

        ///added by sibi
        ///

        public object rechargeStatusUpdate()
        {
            dynamic data = new System.Dynamic.ExpandoObject();

            var list = _context.tPay2AllTransaction.Where(z => z.Pay2AllTxstatusDesc == "PENDING").ToList();
            try
            {
                if (list.Count > 0)
                {
                    foreach (var item in list)
                    {
                        var requestString = baseurl_pay2all + "api_token=" + apiToken_pay2All + "&client_id=" + item.ClientId;
                        var request = (HttpWebRequest)WebRequest.Create(requestString);
                        var response = (HttpWebResponse)request.GetResponse();
                        var responseString = new System.IO.StreamReader(response.GetResponseStream()).ReadToEnd();
                        if (responseString != "Record Not found")
                        {
                            data = Newtonsoft.Json.JsonConvert.DeserializeObject(responseString);

                            try
                            {
                                if (data != null)
                                {
                                    if (data.status != item.Pay2AllStatus)
                                    {
                                        if (data.status == "FAILED")
                                        {
                                            var user = _context.tb_User.FirstOrDefault(z => z.UserID == item.UserId && z.IsActive);
                                            if (user != null)
                                            {
                                                user.Wallet = user.Wallet + Convert.ToDecimal(String.Format("{0:0.00}", Convert.ToDouble(item.AmountDeducted)));
                                            }
                                        }
                                        if (data.error == null || data.error == string.Empty)
                                        {
                                            item.Pay2AllStatus = data.status;
                                            item.Pay2AllTxstatusDesc = data.status;
                                            item.Pay2AllMessage = data.message;
                                        }
                                    }
                                    _context.SaveChanges();
                                }
                            }
                            catch (Exception ex)
                            {
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return true;
        }


    }
}
