﻿using ClassLibrary.ServicePostModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace EntityLibrary.Repository
{
    public class BaseReference
    {
        protected TouchAndPayEntities _context = new TouchAndPayEntities();
        public DateTime currentTime = System.DateTime.UtcNow;
        protected string salt = System.Configuration.ConfigurationManager.AppSettings["salt"];
        //Gulfbox
        private string Token = "/1rvVKgNoQWD3JNx";
        private string Id = "3018";

        //jolo
        protected string baseUrl = "https://joloapi.com/api/";
        protected string userid = "arakkal";
        protected string key = "718127426963062";
        protected string type = "json";
        protected string mode = "0";//test


        //currency converter api key
        public string converterApiKey = "a39752cea68bce149a72";



        //magiceasy
        protected string baseUrlMagicEasy = "http://magiceasy.in/appi/";
        protected string clientid = "1700";
        protected string apikey = "hT7xk5baAofy#rnESwlA";

        //KeralaRecharge
        protected string apikey_KeralaRecharge = "196f0d8e793bf2bd65aa7e70d3bb07c6";
        protected string baseurl_keralaRecharge = "http://www.keralarecharge.com/api/?";

        protected GulfboxAuthPostModel model = new GulfboxAuthPostModel();


        //MobileApi credential
        protected string username_mobileApi = "9567500003";
        protected string password_mobileApi = "12345";



        //databeats
        protected string clientId_databeats = "1012";
        protected string key_databeats = "pqfScPMczOffqg5";
        protected string ip_databeats = "148.72.208.174";


        //pay2all
        //protected string apiToken_pay2All = "lPx2rpQeS5ixkydNhPa5r68Z1J212NtV3BeXLBl2ha3CLMqrB21GxuJqDpKs";
        protected string apiToken_pay2All = "12MlDIU4MMGk0vyM6987XFVDrBXbqVgPOZKWvdVKa4TKndeU7A7dnw9sOvOI";
        protected string baseurl_pay2all = "https://www.pay2all.in/web-api/get-status-client_id?";
        protected string pay2AllLive = "1";//live
        //protected string pay2AllLive = "2";//sandbox


        protected GulfboxAuthModel getAuthData()
        {
            var model = new GulfboxAuthModel();
            model.id = Id;
            Random rnd = new Random();
            var value = (int)(DateTime.UtcNow - new DateTime(1970, 1, 1)).TotalSeconds;
            var key = Convert.ToString(value) + rnd.Next(100, 999);
            model.key = key;
            model.hash = CalculateMD5Hash(Id + Token + Convert.ToString(key));
            return model;
        }
        protected string CalculateMD5Hash(string input)
        {
            StringBuilder hash = new StringBuilder();
            MD5CryptoServiceProvider md5provider = new MD5CryptoServiceProvider();
            byte[] bytes = md5provider.ComputeHash(new UTF8Encoding().GetBytes(input));

            for (int i = 0; i < bytes.Length; i++)
            {
                hash.Append(bytes[i].ToString("x2"));
            }
            return hash.ToString();
        }
        private static Random random = new Random();

        //24*7 SMS
        protected string SMS_API_Key = "wQRJTfvil5l";
        protected string SMS_SENDER_ID = "TESTIN";
        protected string SMS_SERVICE = "INTERNATIONAL";


        ////OnePrepay demo
        //protected string TerminalID = "CRATIS-1";
        //protected string Password = "CRATIS123";
        //protected string MerchantID = "CRATIS";
        //protected string Username = "CRATIS";
        //protected string RequestUrl = "https://test.nexu.cloud:444/Web/Get?RequestXml=";

        //OnePrepay live
        //protected string TerminalID = "1103200-1";17-12-2018
        //protected string Password = "2FC96JE78Q";17-12-2018
        protected string TerminalID = "1103200-2";//21-12-2018
        protected string Password = "3BH68AL54C";//21-12-2018
        protected string MerchantID = "1103200";
        protected string Username = "CRATIS";
        protected string RequestUrl = "https://paynet.ae:550/Web/Get?RequestXml=";
        protected string RequestUrlDuTopUp = "https://paynet.ae:444/Web/Get?RequestXml=";


        //Ding 

        protected string dingUrl = "https://api.dingconnect.com";
        protected string ding_apiKey = "87b9PEIh1FU6evRJS2gcij";
        protected string ding_apiKey_International = "34Y6yTsKniH5b7hIkWlalp";


        public string RandomString(int length)
        {
            const string chars = "abcdefghijklmnopqrstuvwxyz0123456789";
            var randomString = "";
            var check = 0;
            do
            {
                randomString = new string(Enumerable.Repeat(chars, length)
                 .Select(s => s[random.Next(s.Length)]).ToArray());
                check = _context.sp_check_external_identifier_transactionCheck(randomString);
            } while (!Convert.ToBoolean(check));
            return randomString;
        }
        public string RandomJoloRequestString(int length)
        {
            const string chars = "abcdefghijklmnopqrstuvwxyz0123456789";
            var randomString = "";
            var check = 0;
            do
            {
                randomString = new string(Enumerable.Repeat(chars, length)
                 .Select(s => s[random.Next(s.Length)]).ToArray());
                check = _context.sp_check_orderid_jolorecharge(randomString);
            } while (!Convert.ToBoolean(check));
            return randomString;
        }

        public string RandomKeralaRechargeRequestString(int length)
        {
            const string chars = "abcdefghijklmnopqrstuvwxyz0123456789";
            var randomString = "";
            var check = 0;
            do
            {
                randomString = new string(Enumerable.Repeat(chars, length)
                 .Select(s => s[random.Next(s.Length)]).ToArray());
                check = _context.sp_check_keralaRecharge_RequestId(randomString);
            } while (!Convert.ToBoolean(check));
            return randomString;
        }

        public string RandomKeralaRechargeKsebRequestString(int length)
        {
            const string chars = "abcdefghijklmnopqrstuvwxyz0123456789";
            var randomString = "";
            var check = 0;
            do
            {
                randomString = new string(Enumerable.Repeat(chars, length)
                 .Select(s => s[random.Next(s.Length)]).ToArray());
                check = _context.sp_check_keralaKsebPayment_RequestId(randomString);
            } while (!Convert.ToBoolean(check));
            return randomString;
        }


        public string OnePrepayRandomString(int length, string type)
        {
            const string chars = "0123456789";
            var randomString = "";
            var check = 0;
            do
            {
                randomString = new string(Enumerable.Repeat(chars, length)
                 .Select(s => s[random.Next(s.Length)]).ToArray());
                check = _context.sp_check_ReceiptNo_OnePrepay(randomString, type);
            } while (!Convert.ToBoolean(check));
            return randomString;
        }

        public string DingRandomString(int length)
        {
            const string chars = "abcdefghijklmnopqrstuvwxyz0123456789";
            var randomString = "";
            var UniqueValue = "";
            var check = 0;
            do
            {
                randomString = new string(Enumerable.Repeat(chars, length)
                 .Select(s => s[random.Next(s.Length)]).ToArray());

                UniqueValue = randomString;
                try
                {
                    UniqueValue = ReturnUniqueValue(DateTime.UtcNow, randomString);
                }
                catch (Exception ex)
                {

                }

                check = _context.SP_CHECK_DING_DISTRIBUTORREF(UniqueValue);
            } while (!Convert.ToBoolean(check));
            return UniqueValue;
        }

        public string ReturnUniqueValue(DateTime date, string randomString)
        {
            var result = default(byte[]);

            using (var stream = new MemoryStream())
            {
                using (var writer = new BinaryWriter(stream, Encoding.UTF8, true))
                {
                    writer.Write(date.Ticks);
                    writer.Write(randomString);
                }

                stream.Position = 0;

                using (var hash = SHA256.Create())
                {
                    result = hash.ComputeHash(stream);
                }
            }

            var text = new string[20];

            for (var i = 0; i < text.Length; i++)
            {
                text[i] = result[i].ToString("x2");
            }

            return string.Concat(text);
        }



        public string RandomPay2AllClientId(int length)
        {
            const string chars = "abcdefghijklmnopqrstuvwxyz0123456789";
            var randomString = "";
            var check = 0;
            do
            {
                randomString = new string(Enumerable.Repeat(chars, length)
                 .Select(s => s[random.Next(s.Length)]).ToArray());
                check = _context.SP_CHECK_PAY2ALL_CLIENTID(randomString);
            } while (!Convert.ToBoolean(check));
            return randomString;
        }




        protected static class StringCipher
        {
            // This constant is used to determine the keysize of the encryption algorithm in bits.
            // We divide this by 8 within the code below to get the equivalent number of bytes.
            private const int Keysize = 256;

            // This constant determines the number of iterations for the password bytes generation function.
            private const int DerivationIterations = 1000;

            public static string Encrypt(string plainText, string passPhrase)
            {
                // Salt and IV is randomly generated each time, but is preprended to encrypted cipher text
                // so that the same Salt and IV values can be used when decrypting.  
                var saltStringBytes = Encoding.ASCII.GetBytes("touchPay@&2018Securitycredential");
                //var saltStringBytes = Generate256BitsOfRandomEntropy();
                //var ivStringBytes = Generate256BitsOfRandomEntropy();
                var ivStringBytes = Encoding.ASCII.GetBytes("touchPay@&2018Securitycredential");
                var plainTextBytes = Encoding.UTF8.GetBytes(plainText);
                using (var password = new Rfc2898DeriveBytes(passPhrase, saltStringBytes, DerivationIterations))
                {
                    var keyBytes = password.GetBytes(Keysize / 8);
                    using (var symmetricKey = new RijndaelManaged())
                    {
                        symmetricKey.BlockSize = 256;
                        symmetricKey.Mode = CipherMode.CBC;
                        symmetricKey.Padding = PaddingMode.PKCS7;
                        using (var encryptor = symmetricKey.CreateEncryptor(keyBytes, ivStringBytes))
                        {
                            using (var memoryStream = new MemoryStream())
                            {
                                using (var cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write))
                                {
                                    cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);
                                    cryptoStream.FlushFinalBlock();
                                    // Create the final bytes as a concatenation of the random salt bytes, the random iv bytes and the cipher bytes.
                                    var cipherTextBytes = saltStringBytes;
                                    cipherTextBytes = cipherTextBytes.Concat(ivStringBytes).ToArray();
                                    cipherTextBytes = cipherTextBytes.Concat(memoryStream.ToArray()).ToArray();
                                    memoryStream.Close();
                                    cryptoStream.Close();
                                    return Convert.ToBase64String(cipherTextBytes);
                                }
                            }
                        }
                    }
                }
            }

            public static string Decrypt(string cipherText, string passPhrase)
            {
                // Get the complete stream of bytes that represent:
                // [32 bytes of Salt] + [32 bytes of IV] + [n bytes of CipherText]
                var cipherTextBytesWithSaltAndIv = Convert.FromBase64String(cipherText);
                // Get the saltbytes by extracting the first 32 bytes from the supplied cipherText bytes.
                var saltStringBytes = cipherTextBytesWithSaltAndIv.Take(Keysize / 8).ToArray();
                // Get the IV bytes by extracting the next 32 bytes from the supplied cipherText bytes.
                var ivStringBytes = cipherTextBytesWithSaltAndIv.Skip(Keysize / 8).Take(Keysize / 8).ToArray();
                // Get the actual cipher text bytes by removing the first 64 bytes from the cipherText string.
                var cipherTextBytes = cipherTextBytesWithSaltAndIv.Skip((Keysize / 8) * 2).Take(cipherTextBytesWithSaltAndIv.Length - ((Keysize / 8) * 2)).ToArray();

                using (var password = new Rfc2898DeriveBytes(passPhrase, saltStringBytes, DerivationIterations))
                {
                    var keyBytes = password.GetBytes(Keysize / 8);
                    using (var symmetricKey = new RijndaelManaged())
                    {
                        symmetricKey.BlockSize = 256;
                        symmetricKey.Mode = CipherMode.CBC;
                        symmetricKey.Padding = PaddingMode.PKCS7;
                        using (var decryptor = symmetricKey.CreateDecryptor(keyBytes, ivStringBytes))
                        {
                            using (var memoryStream = new MemoryStream(cipherTextBytes))
                            {
                                using (var cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read))
                                {
                                    var plainTextBytes = new byte[cipherTextBytes.Length];
                                    var decryptedByteCount = cryptoStream.Read(plainTextBytes, 0, plainTextBytes.Length);
                                    memoryStream.Close();
                                    cryptoStream.Close();
                                    return Encoding.UTF8.GetString(plainTextBytes, 0, decryptedByteCount);
                                }
                            }
                        }
                    }
                }
            }

            private static byte[] Generate256BitsOfRandomEntropy()
            {
                //var randomBytes = Encoding.ASCII.GetBytes(someString);
                var randomBytes = new byte[32]; // 32 Bytes will give us 256 bits.
                using (var rngCsp = new RNGCryptoServiceProvider())
                {
                    // Fill the array with cryptographically secure random bytes.
                    rngCsp.GetBytes(randomBytes);
                }
                return randomBytes;
            }
        }


    }
}
