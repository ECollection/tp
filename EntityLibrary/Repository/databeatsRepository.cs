﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace EntityLibrary.Repository
{
    public class databeatsRepository : BaseReference
    {
        public object mnp(ClassLibrary.ServicePostModel.databeats.MNP model)
        {
            try
            {

                string baseUrl = "http://api.databeats.in/mnp/" + model.mobile + "?clientId=" + clientId_databeats + "&apiKey=" + key_databeats;
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(baseUrl);
                //WebRequest request = WebRequest.Create(baseUrl);
                // Set the Method property of the request to POST.  
                request.Method = "GET";
                request.Accept = "application/json";

                //var postModel = new ClassLibrary.ServicePostModel.MobileApPostModel.AccessTokenPostModel();
                //postModel.username = username_mobileApi;
                //postModel.password = password_mobileApi;

                //var serializedPostModel = JsonConvert.SerializeObject(postModel);

                //request.Headers.Add("Authorization", "Bearer " + data.access_token);
                //request.Headers.Add("Content-Type", "x-www-form-urlencoded");
                //request.Headers.Add("Accept", "application/json");
                //request.Accept("application/json");
                //request.Headers.Add("Content-Type", "application/json");
                ServicePointManager.Expect100Continue = true;
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                // Create POST data and convert it to a byte array.  
                //string postData = serializedPostModel;
                //byte[] byteArray = Encoding.UTF8.GetBytes(postData);
                // Set the ContentType property of the WebRequest.  
                //request.ContentType = "application/form-data";
                request.ContentType = "x-www-form-urlencoded";
                // Set the ContentLength property of the WebRequest.  
                //request.ContentLength = byteArray.Length;
                // Get the request stream.  
                //Stream dataStream = request.GetRequestStream();
                // Write the data to the request stream.  
                //dataStream.Write(byteArray, 0, byteArray.Length);
                // Close the Stream object.  
                //dataStream.Close();
                // Get the response.  
                WebResponse response = request.GetResponse();
                // Display the status.  
                //Console.WriteLine(((HttpWebResponse)response).StatusDescription);
                // Get the stream containing content returned by the server.  
                Stream dataStream = response.GetResponseStream();
                // Open the stream using a StreamReader for easy access.  
                StreamReader reader = new StreamReader(dataStream);
                // Read the content.  
                string responseFromServer = reader.ReadToEnd();
                // Display the content.  
                //Console.WriteLine(responseFromServer);
                // Clean up the streams.  
                reader.Close();
                dataStream.Close();
                response.Close();

                var responseModel = new ClassLibrary.ServicePostModel.databeats.MNPReturnModel();
                responseModel.Success = new ClassLibrary.ServicePostModel.databeats.MNPResponse.Success();
                responseModel.Failure = new ClassLibrary.ServicePostModel.databeats.MNPResponse.Failure();
                try
                {
                    //responseModel.Success = Newtonsoft.Json.JsonConvert.DeserializeObject(responseFromServer);
                    var ress = Newtonsoft.Json.JsonConvert.DeserializeObject<ClassLibrary.ServicePostModel.databeats.MNPResponse.Success>(responseFromServer);
                    responseModel.Success = ress;
                    responseModel.status = true;
                    responseModel.message = "success";
                    return responseModel;
                }
                catch (Exception ex)
                {
                    //responseModel.Success = Newtonsoft.Json.JsonConvert.DeserializeObject(responseFromServer);
                    var ress = Newtonsoft.Json.JsonConvert.DeserializeObject<ClassLibrary.ServicePostModel.databeats.MNPResponse.Failure>(responseFromServer);
                    responseModel.Failure = ress;
                    responseModel.status = false;
                    responseModel.message = "failed";
                    return responseModel;
                }
            }
            catch (Exception ex)
            {
                var responseModel = new ClassLibrary.ServicePostModel.databeats.MNPReturnModel();
                responseModel.Success = new ClassLibrary.ServicePostModel.databeats.MNPResponse.Success();
                responseModel.Failure = new ClassLibrary.ServicePostModel.databeats.MNPResponse.Failure();
                responseModel.status = false;
                responseModel.message = ex.Message;
                return responseModel;
            }
        }

        public object mobileoffers(ClassLibrary.ServicePostModel.databeats.MobileOffers.MobileOffersRequest model)
        {
            try
            {

                string baseUrl = "http://api.databeats.in/offers/mobile";

                if (model.operators != null && model.operators != string.Empty)
                    baseUrl = baseUrl + "/" + model.operators;
                if (model.circle != null && model.circle != string.Empty)
                    baseUrl = baseUrl + "/" + model.circle;
                if (model.type != null && model.type != string.Empty)
                    baseUrl = baseUrl + "/" + model.type;

                baseUrl = baseUrl + "?clientId=" + clientId_databeats + "&apiKey=" + key_databeats;

                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(baseUrl);
                //WebRequest request = WebRequest.Create(baseUrl);
                // Set the Method property of the request to POST.  
                request.Method = "GET";
                request.Accept = "application/json";

                //var postModel = new ClassLibrary.ServicePostModel.MobileApPostModel.AccessTokenPostModel();
                //postModel.username = username_mobileApi;
                //postModel.password = password_mobileApi;

                //var serializedPostModel = JsonConvert.SerializeObject(postModel);

                //request.Headers.Add("Authorization", "Bearer " + data.access_token);
                //request.Headers.Add("Content-Type", "x-www-form-urlencoded");
                //request.Headers.Add("Accept", "application/json");
                //request.Accept("application/json");
                //request.Headers.Add("Content-Type", "application/json");
                ServicePointManager.Expect100Continue = true;
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                // Create POST data and convert it to a byte array.  
                //string postData = serializedPostModel;
                //byte[] byteArray = Encoding.UTF8.GetBytes(postData);
                // Set the ContentType property of the WebRequest.  
                //request.ContentType = "application/form-data";
                request.ContentType = "x-www-form-urlencoded";
                // Set the ContentLength property of the WebRequest.  
                //request.ContentLength = byteArray.Length;
                // Get the request stream.  
                //Stream dataStream = request.GetRequestStream();
                // Write the data to the request stream.  
                //dataStream.Write(byteArray, 0, byteArray.Length);
                // Close the Stream object.  
                //dataStream.Close();
                // Get the response.  
                WebResponse response = request.GetResponse();
                // Display the status.  
                //Console.WriteLine(((HttpWebResponse)response).StatusDescription);
                // Get the stream containing content returned by the server.  
                Stream dataStream = response.GetResponseStream();
                // Open the stream using a StreamReader for easy access.  
                StreamReader reader = new StreamReader(dataStream);
                // Read the content.  
                string responseFromServer = reader.ReadToEnd();
                // Display the content.  
                //Console.WriteLine(responseFromServer);
                // Clean up the streams.  
                reader.Close();
                dataStream.Close();
                response.Close();

                var responseModel = new ClassLibrary.ServicePostModel.databeats.MobileOffers.MobileOffersReturnModel();
                responseModel.Success = new List<ClassLibrary.ServicePostModel.databeats.MobileOffers.MobileOfferApiResponse.Success>();
                responseModel.Failure = new ClassLibrary.ServicePostModel.databeats.MobileOffers.MobileOfferApiResponse.Failure();
                try
                {
                    //responseModel.Success = Newtonsoft.Json.JsonConvert.DeserializeObject(responseFromServer);
                    var ress = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ClassLibrary.ServicePostModel.databeats.MobileOffers.MobileOfferApiResponse.Success>>(responseFromServer);
                    responseModel.Success = ress;
                    responseModel.status = true;
                    responseModel.message = "success";
                    return responseModel;
                }
                catch (Exception ex)
                {
                    //responseModel.Success = Newtonsoft.Json.JsonConvert.DeserializeObject(responseFromServer);
                    var ress = Newtonsoft.Json.JsonConvert.DeserializeObject<ClassLibrary.ServicePostModel.databeats.MobileOffers.MobileOfferApiResponse.Failure>(responseFromServer);
                    responseModel.Failure = ress;
                    responseModel.status = false;
                    responseModel.message = "failed";
                    return responseModel;
                }
            }
            catch (Exception ex)
            {
                var responseModel = new ClassLibrary.ServicePostModel.databeats.MobileOffers.MobileOffersReturnModel();
                responseModel.Success = new List<ClassLibrary.ServicePostModel.databeats.MobileOffers.MobileOfferApiResponse.Success>();
                responseModel.Failure = new ClassLibrary.ServicePostModel.databeats.MobileOffers.MobileOfferApiResponse.Failure();
                responseModel.status = false;
                responseModel.message = ex.Message;
                return responseModel;
            }
        }
    }
}
