﻿using ClassLibrary.ServicePostModel;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace EntityLibrary.Repository
{
    public class UserRepository : BaseReference
    {
        public bool SaveAll()
        {
            return _context.SaveChanges() > 0;
        }

        public Tuple<UserRegisterModelReturn> register(UserRegisterModel model)
        {
            UserRegisterModelReturn returnModel = new UserRegisterModelReturn();
            returnModel.postModel = new UserRegisterModel();
            returnModel.user = new UserData();
            returnModel.postModel = model;
            string mobile = StringCipher.Encrypt(model.mobileNumber, salt);
            string userName = StringCipher.Encrypt(model.userName, salt);
            try
            {
                var checkList = _context.tb_User.Where(z => (z.Mobile == mobile || z.Username == userName) && z.IsActive).ToList();
                if (checkList.Count > 0)
                {
                    if (checkList.Any(z => z.Mobile == mobile))
                    {
                        returnModel.status = false;
                        returnModel.message = "Mobile number already taken";
                    }
                    //else if (checkList.Any(z => z.Username.ToLower() == model.userName.ToLower()))
                    //{
                    //    returnModel.status = false;
                    //    returnModel.message = "Username already taken";
                    //}
                    else
                    {
                        returnModel.status = false;
                        returnModel.message = "Failed";
                    }
                }
                else
                {
                    var newUser = _context.tb_User.Create();
                    newUser.Country = model.country ?? string.Empty;
                    newUser.CountryCode = model.countryCode ?? string.Empty;
                    newUser.IsActive = true;
                    newUser.Mobile = mobile;
                    //newUser.Password = model.password ?? string.Empty;3-12-2018
                    //newUser.Username = model.userName ?? string.Empty;3-12-2018
                    newUser.Password = StringCipher.Encrypt(model.password, salt);
                    newUser.Username = userName;
                    newUser.Timestamp = currentTime;
                    newUser.UserGuid = Guid.NewGuid();
                    newUser.ProfileImage = string.Empty;
                    newUser.Address = string.Empty;
                    newUser.Place = string.Empty;
                    newUser.ShopName = string.Empty;
                    newUser.UserType = (int)ClassLibrary.Enum.userType.User;
                    newUser.Wallet = Convert.ToDecimal(0);
                    newUser.IsBlocked = false;

                    var currency = _context.tb_AdminCurrencyRate.Where(z => z.FromCountryCode == model.countryCode && z.ToCountryCode == model.countryCode).FirstOrDefault();
                    if (currency != null)
                    {
                        newUser.Currency = currency.FromCurrency;
                    }
                    else
                    {
                        newUser.Currency = "";
                    }
                    _context.tb_User.Add(newUser);

                    returnModel.status = SaveAll();
                    returnModel.message = returnModel.status == true ? "User Registered" : "Failed";
                    if (returnModel.status)
                    {
                        _context.SP_REMOVE_DEVICETOKEN(model.deviceToken);
                        _context.SP_ADD_DEVICETOKEN(newUser.UserID, model.deviceToken, model.deviceType);

                        var accessToken = StringCipher.Encrypt(Guid.NewGuid().ToString(), salt);
                        _context.SP_ADD_ACCESSTOKEN(newUser.UserID, accessToken);


                        returnModel.user.country = newUser.Country ?? string.Empty;
                        returnModel.user.countryCode = newUser.CountryCode ?? string.Empty;
                        returnModel.user.mobileNumber = StringCipher.Decrypt(newUser.Mobile, salt);
                        returnModel.user.uniqueIdentifier = newUser.UserGuid.ToString();
                        returnModel.user.userId = newUser.UserID;
                        //returnModel.user.userName = newUser.Username ?? string.Empty;3-12-2018
                        returnModel.user.userName = StringCipher.Decrypt(newUser.Username, salt);
                        returnModel.user.profileImage = newUser.ProfileImage ?? string.Empty;
                        returnModel.user.currency = newUser.Currency ?? string.Empty;
                        returnModel.user.wallet = String.Format("{0:#}", newUser.Wallet);
                        returnModel.user.isBlocked = newUser.IsBlocked;
                        returnModel.user.appVersion = "";
                        var appVersion = _context.SP_FETCH_APPVERSION(model.deviceType).FirstOrDefault();
                        if (appVersion != null)
                        {
                            returnModel.user.appVersion = appVersion.ToString();
                        }
                        returnModel.accessToken = accessToken;


                        try
                        {
                            var rowIp = _context.tb_UserIp.Create();
                            rowIp.NetworkIp = model.ip;
                            rowIp.TimeStamp = System.DateTime.UtcNow;
                            rowIp.UserId = newUser.UserID;
                            _context.tb_UserIp.Add(rowIp);
                            _context.SaveChanges();
                        }
                        catch (Exception ex)
                        {

                        }


                    }
                }
            }
            catch (Exception ex)
            {
                returnModel.status = false;
                returnModel.message = "An error has occured";
            }
            return new Tuple<UserRegisterModelReturn>(returnModel);
        }

        public Tuple<UserLoginModelReturn> login(UserLoginModel model)
        {
            UserLoginModelReturn returnModel = new UserLoginModelReturn();
            returnModel.postModel = new UserLoginModel();
            returnModel.user = new UserData();
            returnModel.postModel = model;
            string password = StringCipher.Encrypt(model.password, salt);
            string mobile = StringCipher.Encrypt(model.mobileNumber, salt);
            try
            {
                if (Convert.ToBoolean(_context.SP_CHECK_IF_ALREADY_LOGGEDIN(mobile, model.countryCode, password).FirstOrDefault()))
                {
                    returnModel.status = true;
                    returnModel.message = "Logout from other devices";
                    var getData = _context.tb_User.FirstOrDefault(z => z.Mobile == mobile && z.Password == password && z.CountryCode == model.countryCode && z.IsActive);
                    if (getData != null)
                    {
                        returnModel.prevDeviceToken = "";
                        //var prevDeviceToken = _context.tUserDeviceTokens.Where(z => z.UserId == getData.UserID && z.DeviceType == model.deviceType).FirstOrDefault();
                        var prevDeviceToken = _context.tUserDeviceTokens.Where(z => z.UserId == getData.UserID).FirstOrDefault();
                        if (prevDeviceToken != null)
                        {
                            returnModel.prevDeviceToken = prevDeviceToken.DeviceToken;
                        }

                        _context.SP_REMOVE_DEVICETOKEN(model.deviceToken);
                        _context.SP_ADD_DEVICETOKEN(getData.UserID, model.deviceToken, model.deviceType);

                        var accessToken = StringCipher.Encrypt(Guid.NewGuid().ToString(), salt);
                        _context.SP_ADD_ACCESSTOKEN(getData.UserID, accessToken);

                        returnModel.user.country = getData.Country ?? string.Empty;
                        returnModel.user.countryCode = getData.CountryCode ?? string.Empty;
                        returnModel.user.mobileNumber = StringCipher.Decrypt(getData.Mobile, salt);
                        returnModel.user.uniqueIdentifier = getData.UserGuid.ToString();
                        returnModel.user.userId = getData.UserID;
                        returnModel.user.userName = StringCipher.Decrypt(getData.Username, salt);
                        //returnModel.user.userName = getData.Username ?? string.Empty;3-12-2018
                        returnModel.user.profileImage = getData.ProfileImage ?? string.Empty;
                        //returnModel.user.wallet = String.Format("{0:#}", getData.Wallet);
                        returnModel.user.wallet = getData.Wallet.ToString();
                        returnModel.user.currency = getData.Currency ?? string.Empty;
                        returnModel.user.isBlocked = getData.IsBlocked;
                        returnModel.user.appVersion = "";
                        var appVersion = _context.SP_FETCH_APPVERSION(model.deviceType).FirstOrDefault();
                        if (appVersion != null)
                        {
                            returnModel.user.appVersion = appVersion.ToString();
                        }

                        returnModel.accessToken = accessToken;

                        returnModel.status = true;
                        returnModel.message = "Success";
                        returnModel.ip = model.ip;


                        try
                        {
                            var rowIp = _context.tb_UserIp.Create();
                            rowIp.NetworkIp = model.ip;
                            rowIp.TimeStamp = System.DateTime.UtcNow;
                            rowIp.UserId = getData.UserID;
                            _context.tb_UserIp.Add(rowIp);
                            _context.SaveChanges();
                        }
                        catch (Exception ex)
                        {

                        }



                    }
                    else
                    {
                        returnModel.status = false;
                        returnModel.message = "Incorrect Mobile number / password";
                    }
                }
                else
                {
                    returnModel.prevDeviceToken = "";
                    var getData = _context.tb_User.FirstOrDefault(z => z.Mobile == mobile && z.Password == password && z.CountryCode == model.countryCode && z.IsActive);
                    if (getData != null)
                    {
                        _context.SP_REMOVE_DEVICETOKEN(model.deviceToken);
                        _context.SP_ADD_DEVICETOKEN(getData.UserID, model.deviceToken, model.deviceType);


                        try
                        {
                            var rowIp = _context.tb_UserIp.Create();
                            rowIp.NetworkIp = model.ip;
                            rowIp.TimeStamp = System.DateTime.UtcNow;
                            rowIp.UserId = getData.UserID;
                            _context.tb_UserIp.Add(rowIp);
                            _context.SaveChanges();
                        }
                        catch (Exception ex)
                        {

                        }


                        var accessToken = StringCipher.Encrypt(Guid.NewGuid().ToString(), salt);
                        _context.SP_ADD_ACCESSTOKEN(getData.UserID, accessToken);

                        returnModel.user.country = getData.Country ?? string.Empty;
                        returnModel.user.countryCode = getData.CountryCode ?? string.Empty;
                        returnModel.user.mobileNumber = StringCipher.Decrypt(getData.Mobile, salt);
                        returnModel.user.uniqueIdentifier = getData.UserGuid.ToString();
                        returnModel.user.userId = getData.UserID;
                        returnModel.user.userName = StringCipher.Decrypt(getData.Username, salt);
                        //returnModel.user.userName = getData.Username ?? string.Empty;3-12-2018
                        returnModel.user.profileImage = getData.ProfileImage ?? string.Empty;
                        //returnModel.user.wallet = String.Format("{0:#}", getData.Wallet);
                        returnModel.user.wallet = getData.Wallet.ToString();
                        returnModel.user.currency = getData.Currency ?? string.Empty;
                        returnModel.user.isBlocked = getData.IsBlocked;
                        returnModel.user.appVersion = "";
                        var appVersion = _context.SP_FETCH_APPVERSION(model.deviceType).FirstOrDefault();
                        if (appVersion != null)
                        {
                            returnModel.user.appVersion = appVersion.ToString();
                        }

                        returnModel.accessToken = accessToken;

                        returnModel.status = true;
                        returnModel.message = "Success";
                        returnModel.ip = model.ip;
                    }
                    else
                    {
                        returnModel.status = false;
                        returnModel.message = "Incorrect Mobile number / password";
                    }
                }

            }
            catch (Exception ex)
            {
                returnModel.status = false;
                returnModel.message = "An error has occured";
            }
            return new Tuple<UserLoginModelReturn>(returnModel);
        }

        public Tuple<ProfileEditModelReturn> profileEdit(ProfileEditModel model)
        {
            ProfileEditModelReturn returnModel = new ProfileEditModelReturn();
            returnModel.user = new UserData();

            try
            {
                var user = _context.tb_User.FirstOrDefault(z => z.UserID == model.userId);
                if (user != null)
                {
                    var wallet = user.Wallet;
                    string filepath = user.ProfileImage;
                    if (model.profileImage != null || user.Username != model.username || user.Mobile != model.mobileNumber || user.Country != model.country || user.CountryCode != model.countryCode)
                    {
                        var checkList = _context.tb_User.Where(z => (z.Mobile == model.mobileNumber || z.Username.ToLower() == model.username.ToLower()) && z.IsActive && z.UserID != model.userId).ToList();
                        if (checkList.Count > 0)
                        {
                            if (checkList.Any(z => z.Mobile == model.mobileNumber))
                            {
                                returnModel.status = false;
                                returnModel.message = "Mobile number already taken";
                            }
                            else if (checkList.Any(z => z.Username.ToLower() == model.username.ToLower()))
                            {
                                returnModel.status = false;
                                returnModel.message = "Username already taken";
                            }
                            else
                            {
                                returnModel.status = false;
                                returnModel.message = "Failed";
                            }
                        }
                        else
                        {

                            if (model.countryCode != user.CountryCode)
                            {
                                var currency = _context.tb_AdminCurrencyRate.Where(z => z.ToCountryCode == model.countryCode && z.FromCountryCode == user.CountryCode && z.IsActive).FirstOrDefault();
                                if (currency != null)
                                {
                                    try
                                    {
                                        var basicAmount = "0";
                                        WebClient web = new WebClient();
                                        var conversion = currency.FromCurrency + "_" + currency.ToCurrency;
                                        string url = string.Format("http://free.currencyconverterapi.com/api/v5/convert?q=" + conversion + "&compact=y&apiKey=" + converterApiKey);
                                        string response = web.DownloadString(url);
                                        dynamic d = JObject.Parse(response);
                                        if (d != null)
                                        {
                                            foreach (Newtonsoft.Json.Linq.JProperty row in d)
                                            {
                                                basicAmount = Convert.ToString(row.Values().ToList().FirstOrDefault().Last);
                                            }
                                        }

                                        user.Wallet = Convert.ToDecimal(Convert.ToDecimal(basicAmount) * wallet);
                                        user.Currency = currency.ToCurrency;
                                    }
                                    catch (Exception ex1)
                                    {

                                    }
                                }
                            }


                            if (model.profileImage != null)
                            {
                                //string folderPath = System.Web.HttpContext.Current.Server.MapPath("~/Files/ProfileImage/");
                                //string folderPath = System.Web.HttpContext.Current.Server.MapPath("~/upload/ProfileImage/");
                                string folderPath = System.Web.HttpContext.Current.Server.MapPath("~/upload/");
                                string fileExtension = Path.GetExtension(model.profileImage.FileName);
                                string fileName = Guid.NewGuid().ToString();
                                if (!Directory.Exists(folderPath))
                                    Directory.CreateDirectory(folderPath);
                                var fileSavePath = Path.Combine(folderPath, fileName + fileExtension);
                                model.profileImage.SaveAs(fileSavePath);
                                //filepath = Path.Combine("/Files/ProfileImage/", fileName + fileExtension);
                                //filepath = Path.Combine("/upload/ProfileImage/", fileName + fileExtension);
                                filepath = Path.Combine("/upload/", fileName + fileExtension);
                            }
                            user.Mobile = StringCipher.Encrypt(model.mobileNumber, salt);
                            user.Username = StringCipher.Encrypt(model.username, salt);

                            user.ProfileImage = filepath;
                            //user.Username = model.username;
                            //user.Mobile = model.mobileNumber;
                            user.Country = model.country;
                            user.CountryCode = model.countryCode;

                            returnModel.status = SaveAll();
                            returnModel.message = returnModel.status == true ? "Profile updated" : "Failed";
                            if (returnModel.status)
                            {
                                returnModel.user.country = user.Country ?? string.Empty;
                                returnModel.user.countryCode = user.CountryCode ?? string.Empty;
                                returnModel.user.mobileNumber = StringCipher.Decrypt(user.Mobile, salt);
                                returnModel.user.uniqueIdentifier = user.UserGuid.ToString();
                                returnModel.user.userId = user.UserID;
                                //returnModel.user.userName = user.Username ?? string.Empty;3-12-2018
                                returnModel.user.userName = StringCipher.Decrypt(user.Username, salt);
                                returnModel.user.profileImage = user.ProfileImage ?? string.Empty;
                                returnModel.user.wallet = String.Format("{0:#}", user.Wallet.ToString());
                                returnModel.user.currency = user.Currency ?? string.Empty;
                                returnModel.user.isBlocked = user.IsBlocked;
                                returnModel.user.appVersion = "";
                                var appVersion = _context.SP_FETCH_APPVERSION(model.deviceType).FirstOrDefault();
                                if (appVersion != null)
                                {
                                    returnModel.user.appVersion = appVersion.ToString();
                                }
                            }
                        }
                    }
                    else
                    {
                        returnModel.status = true;
                        returnModel.message = "Profile updated";
                        returnModel.user.country = user.Country ?? string.Empty;
                        returnModel.user.countryCode = user.CountryCode ?? string.Empty;
                        returnModel.user.mobileNumber = StringCipher.Decrypt(user.Mobile, salt);
                        returnModel.user.uniqueIdentifier = user.UserGuid.ToString();
                        returnModel.user.userId = user.UserID;
                        //returnModel.user.userName = user.Username ?? string.Empty;3-12-2018
                        returnModel.user.userName = StringCipher.Decrypt(user.Username, salt);
                        returnModel.user.profileImage = user.ProfileImage ?? string.Empty;
                        returnModel.user.wallet = String.Format("{0:#}", user.Wallet.ToString());
                        returnModel.user.currency = user.Currency ?? string.Empty;
                        returnModel.user.isBlocked = user.IsBlocked;
                        returnModel.user.appVersion = "";
                        var appVersion = _context.SP_FETCH_APPVERSION(model.deviceType).FirstOrDefault();
                        if (appVersion != null)
                        {
                            returnModel.user.appVersion = appVersion.ToString();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                returnModel.status = false;
                returnModel.message = ex.Message;
            }
            return new Tuple<ProfileEditModelReturn>(returnModel);
        }

        public Tuple<RechargeWalletReturnModel> rechargeWallet(RechargeWalletModel model)
        {
            RechargeWalletReturnModel returnModel = new RechargeWalletReturnModel();
            returnModel.postModel = new RechargeWalletModel();
            returnModel.user = new UserData();
            returnModel.postModel = model;

            try
            {
                var cardNumber = model.cardNumber.Replace("-", "");
                var getCardData = _context.tb_RechargeCard.Where(z => z.CardNumber.Replace("-", "") == model.cardNumber).FirstOrDefault();
                if (getCardData != null)
                {

                    if (getCardData.IsUsed)
                    {
                        returnModel.status = false;
                        returnModel.message = "This card has already been used";
                    }
                    else if (getCardData.IsDeactivated)
                    {
                        returnModel.status = false;
                        returnModel.message = "This card has been deactivated";
                    }
                    else if (getCardData.IsDeleted)
                    {
                        returnModel.status = false;
                        returnModel.message = "This card has been deleted";
                    }
                    else
                    {
                        var getUserData = _context.tb_User.FirstOrDefault(z => z.UserID == model.userId);
                        if (getUserData != null)
                        {
                            if (getUserData.Currency == getCardData.Currency)
                            {

                                var currecyRate = _context.tb_AdminCurrencyRate.Where(z => z.ToCurrency == getUserData.Currency && z.FromCurrency == getCardData.Currency).FirstOrDefault();
                                if (currecyRate != null)
                                {
                                    var amount = Convert.ToDecimal(Convert.ToDouble(getCardData.Amount) * currecyRate.CurrencyRate);
                                    getUserData.Wallet = getUserData.Wallet + Convert.ToDecimal(amount);
                                }

                                if (_context.SaveChanges() > 0)
                                {
                                    getCardData.IsUsed = true;

                                    var history = _context.tb_RechargeCardHistory.Create();
                                    history.CardId = getCardData.CardId;
                                    history.UsedTime = currentTime;
                                    history.UserId = model.userId;
                                    _context.tb_RechargeCardHistory.Add(history);
                                    try
                                    {
                                        _context.SaveChanges();
                                    }
                                    catch (Exception exe)
                                    { }

                                    returnModel.user.country = getUserData.Country ?? string.Empty;
                                    returnModel.user.countryCode = getUserData.CountryCode ?? string.Empty;
                                    returnModel.user.mobileNumber = StringCipher.Decrypt(getUserData.Mobile, salt);
                                    returnModel.user.uniqueIdentifier = getUserData.UserGuid.ToString();
                                    returnModel.user.userId = getUserData.UserID;
                                    //returnModel.user.userName = getUserData.Username ?? string.Empty;3-12-2018
                                    returnModel.user.userName = StringCipher.Decrypt(getUserData.Username, salt);
                                    returnModel.user.profileImage = getUserData.ProfileImage ?? string.Empty;
                                    returnModel.user.wallet = String.Format("{0:#}", getUserData.Wallet);
                                    returnModel.user.currency = getUserData.Currency ?? string.Empty;
                                    returnModel.user.isBlocked = getUserData.IsBlocked;
                                    returnModel.user.appVersion = "";
                                    var appVersion = _context.SP_FETCH_APPVERSION(model.deviceType).FirstOrDefault();
                                    if (appVersion != null)
                                    {
                                        returnModel.user.appVersion = appVersion.ToString();
                                    }
                                    returnModel.status = true;
                                    returnModel.message = "Success";
                                }
                                else
                                {
                                    returnModel.status = false;
                                    returnModel.message = "Failed";
                                }
                            }
                            else
                            {
                                returnModel.status = false;
                                returnModel.message = "Invalid Card Currecy";
                            }
                        }
                        else
                        {
                            returnModel.status = false;
                            returnModel.message = "Invalid user account";
                        }
                    }
                }
                else
                {
                    returnModel.status = false;
                    returnModel.message = "Incorrect Card number";
                }
            }
            catch (Exception ex)
            {
                returnModel.status = false;
                returnModel.message = "An error has occured";
            }

            return new Tuple<RechargeWalletReturnModel>(returnModel);
        }

        public Tuple<AssignCardReturnModel> assignCard(AssignCard model)
        {
            AssignCardReturnModel returnModel = new AssignCardReturnModel();
            returnModel.postModel = new AssignCard();
            returnModel.postModel = model;

            try
            {

                string s = model.qrCode;
                string[] values = s.Split('~');
                if (values.Length > 1)
                {
                    for (int i = 0; i < values.Length; i++)
                    {
                        values[i] = values[i].Trim();
                    }

                    var cardId = Convert.ToInt64(values[1].ToString());
                    if (_context.tb_AssignCard.Any(z => z.CardId == cardId))
                    {
                        returnModel.status = false;
                        returnModel.message = "Card already assigned";
                    }
                    else
                    {
                        var assignCard = _context.tb_AssignCard.Create();
                        assignCard.CardId = cardId;
                        assignCard.UserId = model.agentId;
                        assignCard.Timestamp = currentTime;
                        _context.tb_AssignCard.Add(assignCard);
                        if (_context.SaveChanges() > 0)
                        {
                            returnModel.status = true;
                            returnModel.message = "Success";
                        }
                        else
                        {
                            returnModel.status = false;
                            returnModel.message = "Failed";
                        }
                    }
                }
                else
                {
                    returnModel.status = false;
                    returnModel.message = "Failed";
                }

            }
            catch (Exception ex)
            {
                returnModel.status = false;
                returnModel.message = "An error has occured";
            }


            return new Tuple<AssignCardReturnModel>(returnModel);
        }

        public Tuple<ListAgentsReturnModel> listAgents()
        {
            ListAgentsReturnModel returnModel = new ListAgentsReturnModel();
            returnModel.agents = new List<AgentData>();

            try
            {
                var agents = _context.tb_User.Where(z => z.IsActive && z.UserType == (int)ClassLibrary.Enum.userType.Agent).ToList();
                if (agents.Count > 0)
                {
                    foreach (var agent in agents)
                    {
                        var data = new AgentData();
                        data.agentId = agent.UserID;
                        data.userName = StringCipher.Decrypt(agent.Username, salt); ;
                        returnModel.agents.Add(data);
                    }
                    returnModel.status = true;
                    returnModel.message = "Success";
                }
                else
                {
                    returnModel.status = false;
                    returnModel.message = "No agents found";
                }
            }
            catch (Exception ex)
            {
                returnModel.status = false;
                returnModel.message = "An error has occured";
            }
            return new Tuple<ListAgentsReturnModel>(returnModel);
        }

        public object currencyConverter(decimal amount, string fromCurrency, string toCurrency)
        {
            //WebClient web = new WebClient();
            //string url = string.Format("http://www.google.com/ig/calculator?hl=en&q={2}{0}%3D%3F{1}", fromCurrency.ToUpper(), toCurrency.ToUpper(), amount);
            //string response = web.DownloadString(url);
            //Regex regex = new Regex("rhs: \\\"(\\d*.\\d*)");
            //decimal rate = System.Convert.ToDecimal(regex.Match(response).Groups[1].Value);
            //return rate;

            //WebClient web = new WebClient();
            //string url = string.Format("http://finance.yahoo.com/d/quotes.csv?e=.csv&f=sl1d1t1&s={0}{1}=X", fromCurrency.ToUpper(), toCurrency.ToUpper());
            //string response = web.DownloadString(url);
            //string[] values = Regex.Split(response, ",");
            //decimal rate = System.Convert.ToDecimal(values[1]);
            //return rate * amount;

            //string URL = "http://www.google.com/finance/converter?a=" + amount + "&from=" + fromCurrency + "&to=" + toCurrency;
            //byte[] databuffer = Encoding.ASCII.GetBytes("test=postvar&test2=another");
            //HttpWebRequest _webreqquest = (HttpWebRequest)WebRequest.Create(URL);
            //_webreqquest.Method = "POST";
            //_webreqquest.ContentType = "application/x-www-form-urlencoded";
            //_webreqquest.ContentLength = databuffer.Length;
            //Stream PostData = _webreqquest.GetRequestStream();

            //PostData.Write(databuffer, 0, databuffer.Length);
            //PostData.Close();
            //HttpWebResponse WebResp = (HttpWebResponse)_webreqquest.GetResponse();
            //Stream finalanswer = WebResp.GetResponseStream();
            //StreamReader _answer = new StreamReader(finalanswer);
            //string[] value = Regex.Split(_answer.ReadToEnd(), "&nbsp;");

            //return true;


            ////Grab your values and build your Web Request to the API
            //string apiURL = String.Format("https://www.google.com/finance/converter?a={0}&from={1}&to={2}&meta={3}", amount, fromCurrency, toCurrency, Guid.NewGuid().ToString());

            ////Make your Web Request and grab the results
            //var request = WebRequest.Create(apiURL);

            ////Get the Response
            //var streamReader = new StreamReader(request.GetResponse().GetResponseStream(), System.Text.Encoding.ASCII);

            ////Grab your converted value (ie 2.45 USD)
            //var result = Regex.Matches(streamReader.ReadToEnd(), "<span class=\"?bld\"?>([^<]+)</span>")[0].Groups[1].Value;

            ////Get the Result
            //return result;

            decimal currency = 0;
            double convertedAmount = 0;
            //try
            //{
            //    string url = string.Format("https://www.google.com/finance/converter?a={2}&from={0}&to={1}", fromCurrency.ToUpper(), toCurrency.ToUpper(), amount);
            //    WebRequest request = WebRequest.Create(url);
            //    StreamReader streamReader = new StreamReader(request.GetResponse().GetResponseStream(), System.Text.Encoding.ASCII);
            //    string result = Regex.Matches(streamReader.ReadToEnd(), "([^<]+)")[0].Groups[1].Value;
            //    string rs = new Regex(@"^D*?((-?(d+(.d+)?))|(-?.d+)).*").Match(result).Groups[1].Value;
            //    if (decimal.TryParse((new Regex(@"^D*?((-?(d+(.d+)?))|(-?.d+)).*").Match(result).Groups[1].Value), out currency))
            //    {
            //        convertedAmount = currency.ToString("0.00");
            //    }
            //}
            //catch (Exception ex)
            //{
            //    throw ex;
            //}

            WebClient web = new WebClient();
            string url = string.Format("http://free.currencyconverterapi.com/api/v5/convert?q=AED_INR&compact=y&apiKey=" + converterApiKey);
            string response = web.DownloadString(url);
            dynamic d = JObject.Parse(response);
            if (d != null)
            {
                dynamic blogPost = d["AED_INR"];
                dynamic blogPost1 = blogPost["val"];
                if (blogPost1 != null)
                {
                    convertedAmount = blogPost1.Value;
                }
            }
            return convertedAmount;
            ////http://free.currencyconverterapi.com/api/v5/convert?q=AED_INR&compact=y&apiKey=" + converterApiKey
        }

        public Tuple<ResetPasswordModelReturn> resetPassword(ResetPassword model)
        {
            ResetPasswordModelReturn returnModel = new ResetPasswordModelReturn();
            returnModel.postModel = new ResetPassword();
            returnModel.user = new UserData();
            returnModel.postModel = model;
            var oldPassword = StringCipher.Encrypt(model.oldPassword, salt);
            var newPassword = StringCipher.Encrypt(model.newPassword, salt);
            try
            {
                var getData = _context.tb_User.FirstOrDefault(z => z.UserID == model.userId && z.Password == oldPassword && z.IsActive);
                if (getData != null)
                {
                    getData.Password = newPassword;
                    if (_context.SaveChanges() > 0)
                    {
                        returnModel.user.country = getData.Country ?? string.Empty;
                        returnModel.user.countryCode = getData.CountryCode ?? string.Empty;
                        returnModel.user.mobileNumber = StringCipher.Decrypt(getData.Mobile, salt);
                        returnModel.user.uniqueIdentifier = getData.UserGuid.ToString();
                        returnModel.user.userId = getData.UserID;
                        //returnModel.user.userName = getData.Username ?? string.Empty;
                        returnModel.user.userName = StringCipher.Decrypt(getData.Username, salt);
                        returnModel.user.profileImage = getData.ProfileImage ?? string.Empty;
                        //returnModel.user.wallet = String.Format("{0:#}", getData.Wallet);
                        returnModel.user.wallet = getData.Wallet.ToString();
                        returnModel.user.currency = getData.Currency ?? string.Empty;
                        returnModel.user.isBlocked = getData.IsBlocked;
                        returnModel.user.appVersion = "";
                        var appVersion = _context.SP_FETCH_APPVERSION(model.deviceType).FirstOrDefault();
                        if (appVersion != null)
                        {
                            returnModel.user.appVersion = appVersion.ToString();
                        }

                        returnModel.status = true;
                        returnModel.message = "Success";
                    }
                    else
                    {
                        returnModel.user.country = getData.Country ?? string.Empty;
                        returnModel.user.countryCode = getData.CountryCode ?? string.Empty;
                        returnModel.user.mobileNumber = StringCipher.Decrypt(getData.Mobile, salt);
                        returnModel.user.uniqueIdentifier = getData.UserGuid.ToString();
                        returnModel.user.userId = getData.UserID;
                        //returnModel.user.userName = getData.Username ?? string.Empty;
                        returnModel.user.userName = StringCipher.Decrypt(getData.Username, salt);
                        returnModel.user.profileImage = getData.ProfileImage ?? string.Empty;
                        //returnModel.user.wallet = String.Format("{0:#}", getData.Wallet);
                        returnModel.user.wallet = getData.Wallet.ToString();
                        returnModel.user.currency = getData.Currency ?? string.Empty;
                        returnModel.user.isBlocked = getData.IsBlocked;
                        returnModel.user.appVersion = "";
                        var appVersion = _context.SP_FETCH_APPVERSION(model.deviceType).FirstOrDefault();
                        if (appVersion != null)
                        {
                            returnModel.user.appVersion = appVersion.ToString();
                        }

                        returnModel.status = false;
                        returnModel.message = "Failed";
                    }
                }
                else
                {
                    returnModel.status = false;
                    returnModel.message = "Incorrect old password";
                }
            }
            catch (Exception ex)
            {
                returnModel.status = false;
                returnModel.message = "An error has occured";
            }
            return new Tuple<ResetPasswordModelReturn>(returnModel);
        }

        public Tuple<ForgotPasswordModelReturn> forgotPassword(ForgotPassword model)
        {
            ForgotPasswordModelReturn returnModel = new ForgotPasswordModelReturn();
            returnModel.status = false;
            returnModel.message = "failed";
            returnModel.postModel = new ForgotPassword();
            returnModel.postModel = model;
            var mobile = StringCipher.Encrypt(model.mobile, salt);

            var account = _context.tb_User.Where(z => z.CountryCode == model.countryCode && z.Mobile == mobile && z.IsActive).FirstOrDefault();
            if (account != null)
            {
                var password = StringCipher.Decrypt(account.Password, salt);
                var countryCode = model.countryCode.Remove(0, 1);
                string MSG_message = "";
                MSG_message = System.Web.HttpUtility.UrlEncode("Your Touch&Pay account Password is :" + password + "");
                //MSG_message = System.Web.HttpUtility.UrlEncode("Test SMS");
                string url = "https://smsapi.24x7sms.com/api_2.0/SendSMS.aspx?APIKEY=" + SMS_API_Key + "&MobileNo=" + countryCode + model.mobile + "&SenderID=" + SMS_SENDER_ID + "&Message=" + MSG_message + "&ServiceName=" + SMS_SERVICE + "";
                //string url = "https://smsapi.24x7sms.com/api_2.0/SendSMS.aspx?APIKEY=" + SMS_API_Key + "&MobileNo=" + "971" + "507465039" + "&SenderID=" + SMS_SENDER_ID + "&Message=" + MSG_message + "&ServiceName=" + SMS_SERVICE + "";
                string request = url;
                string success = MakeWebRequestGET(request);
                if (success.Contains("success"))
                {
                    returnModel.status = true;
                    returnModel.message = "success";
                }
                else
                {
                    returnModel.status = false;
                    returnModel.message = "failed";
                }
            }
            else
            {
                returnModel.status = false;
                returnModel.message = "Invalid account";
            }
            return new Tuple<ForgotPasswordModelReturn>(returnModel);
        }
        public string MakeWebRequestGET(string url) //url is https API
        {
            string result = "";
            try
            {
                WebRequest WReq = WebRequest.Create(url);
                WebResponse wResp = WReq.GetResponse();
                StreamReader sr = new StreamReader(wResp.GetResponseStream());
                result = sr.ReadToEnd();
                sr.Close();
            }
            catch (Exception e)
            {
                result = "An error has occured";
            }
            finally
            {
            }
            return result; //result will provide you MsgID
        }


        public Tuple<GetUserDataReturn> getUserData(GetUserData model)
        {
            GetUserDataReturn returnModel = new GetUserDataReturn();
            returnModel.postModel = new GetUserData();
            returnModel.user = new UserData();
            returnModel.postModel = model;

            try
            {
                var getData = _context.tb_User.FirstOrDefault(z => z.UserID == model.userId && z.IsActive);
                if (getData != null)
                {
                    returnModel.currentDeviceToken = "";
                    //var prevDeviceToken = _context.tUserDeviceTokens.Where(z => z.UserId == getData.UserID && z.DeviceType == model.deviceType)
                    var prevDeviceToken = _context.tUserDeviceTokens.Where(z => z.UserId == getData.UserID)
                        .ToList().OrderByDescending(z => z.Id).FirstOrDefault();
                    if (prevDeviceToken != null)
                    {
                        returnModel.currentDeviceToken = prevDeviceToken.DeviceToken;
                    }

                    var accessToken = StringCipher.Encrypt(Guid.NewGuid().ToString(), salt);
                    _context.SP_ADD_ACCESSTOKEN(getData.UserID, accessToken);

                    returnModel.user.country = getData.Country ?? string.Empty;
                    returnModel.user.countryCode = getData.CountryCode ?? string.Empty;
                    returnModel.user.mobileNumber = StringCipher.Decrypt(getData.Mobile, salt);
                    returnModel.user.uniqueIdentifier = getData.UserGuid.ToString();
                    returnModel.user.userId = getData.UserID;
                    //returnModel.user.userName = getData.Username ?? string.Empty;
                    returnModel.user.profileImage = getData.ProfileImage ?? string.Empty;
                    returnModel.user.userName = StringCipher.Decrypt(getData.Username, salt);
                    //returnModel.user.wallet = String.Format("{0:#}", getData.Wallet);
                    returnModel.user.wallet = getData.Wallet.ToString();
                    returnModel.user.currency = getData.Currency ?? string.Empty;
                    returnModel.user.isBlocked = getData.IsBlocked;
                    returnModel.user.appVersion = "";
                    var appVersion = _context.SP_FETCH_APPVERSION(model.deviceType).FirstOrDefault();
                    if (appVersion != null)
                    {
                        returnModel.user.appVersion = appVersion.ToString();
                    }

                    returnModel.accessToken = accessToken;

                    returnModel.status = true;
                    returnModel.message = "Success";
                }
                else
                {
                    returnModel.status = false;
                    returnModel.message = "Invalid Account";
                }
            }
            catch (Exception ex)
            {
                returnModel.status = false;
                returnModel.message = "An error has occured";
            }
            return new Tuple<GetUserDataReturn>(returnModel);
        }

        public Tuple<GetTransactionResponse> getTransaction(GetTransaction model)
        {
            GetTransactionResponse returnModel = new GetTransactionResponse();
            returnModel.postModel = new GetTransaction();
            returnModel.data = new List<TransactionData>();
            returnModel.postModel = model;
            returnModel.amountTotal = "0";
            returnModel.deductedAmountTotal = "0";
            try
            {
                var list = new EntityLibrary.Data.AdminService().getUserTransaction(model.id, model.index, model.search, model.fromDate, model.endDate);
                if (list.Count > 0)
                {

                    foreach (var item in list)
                    {
                        var addtolist = new TransactionData();
                        addtolist.amount = item.amount ?? 0;
                        addtolist.apiprovider = item.apiprovider ?? 0;
                        addtolist.rechargenumber = item.rechargenumber;
                        addtolist.transactionid = item.transactionid;
                        addtolist.transactionstate = item.transactionstate;
                        addtolist.transactiontime = item.transactiontime;
                        addtolist.currency = item.currency;
                        addtolist.type = item.type ?? 0;
                        addtolist.amountcurrency = item.amountcurrency ?? string.Empty;
                        addtolist.amountdeducted = item.amountdeducted ?? 0;
                        addtolist.amountdeductedcurrency = item.amountdeductedcurrency ?? string.Empty;
                        returnModel.data.Add(addtolist);
                    }

                    returnModel.status = true;
                    returnModel.message = "Success";


                    var allList = _context.SP_GET_TRANSACTION_SUM(model.id, model.search, model.fromDate, model.endDate).ToList().
                        Select(z => new EntityLibrary.Data.SP_GET_TRANSACTION_SUM(z)).ToList();
                    if (allList.Count > 0)
                    {
                        returnModel.amountTotal = Convert.ToString(allList.Sum(z => z.amount).ToString());
                        returnModel.deductedAmountTotal = Convert.ToString(allList.Sum(z => z.amountdeducted).ToString());
                    }


                }
                else
                {
                    returnModel.status = false;
                    returnModel.message = "No data found";
                }
            }
            catch (Exception ex)
            {
                returnModel.status = false;
                returnModel.message = "An error has occured";
            }

            return new Tuple<GetTransactionResponse>(returnModel);
        }

        public Tuple<SendOtpResponse> sendOtp(SendOtp model)
        {

            SendOtpResponse returnModel = new SendOtpResponse();
            returnModel.postModel = new SendOtp();
            returnModel.postModel = model;

            Random random = new Random();
            var entry = _context.tb_Otp.Create();
            entry.CountryCode = model.countryCode;
            entry.Mobile = model.mobileNumber;
            entry.Otp = random.Next(1111, 9999);
            entry.Timestamp = System.DateTime.UtcNow;
            _context.tb_Otp.Add(entry);

            if (_context.SaveChanges() > 0)
            {
                try
                {
                    var countryCode = model.countryCode.Remove(0, 1);
                    string MSG_message = "";
                    MSG_message = System.Web.HttpUtility.UrlEncode("Your OTP is :" + Convert.ToString(entry.Otp) + "");
                    string url = "https://smsapi.24x7sms.com/api_2.0/SendSMS.aspx?APIKEY=" + SMS_API_Key + "&MobileNo=" + countryCode + model.mobileNumber + "&SenderID=" + SMS_SENDER_ID + "&Message=" + MSG_message + "&ServiceName=" + SMS_SERVICE + "";
                    string request = url;
                    string success = MakeWebRequestGET(request);
                    if (success.Contains("success"))
                    {
                        returnModel.status = true;
                        returnModel.message = "success";
                    }
                    else
                    {
                        returnModel.status = false;
                        returnModel.message = "failed";
                    }
                }
                catch (Exception ex)
                {
                    returnModel.status = false;
                    returnModel.message = "failed";
                }
            }
            return new Tuple<SendOtpResponse>(returnModel);
        }

        public Tuple<ValidateOtpResponse> validateOtp(ValidateOtp model)
        {
            ValidateOtpResponse returnModel = new ValidateOtpResponse();
            returnModel.postModel = new ValidateOtp();
            returnModel.postModel = model;

            try
            {
                if (Convert.ToBoolean(_context.SP_VALIDATE_OTP(model.countryCode, model.mobileNumber, model.otp).FirstOrDefault()))
                {
                    returnModel.status = true;
                    returnModel.message = "success";
                }
                else
                {
                    returnModel.status = false;
                    returnModel.message = "Invalid otp";
                }
            }
            catch (Exception ex)
            {
                returnModel.status = false;
                returnModel.message = "failed";
            }

            return new Tuple<ValidateOtpResponse>(returnModel);
        }

        public Tuple<SendContactMailResponse> sendContactMail(SendContactMail model)
        {
            SendContactMailResponse returnModel = new SendContactMailResponse();
            returnModel.postModel = new SendContactMail();
            returnModel.postModel = model;

            var user = _context.tb_User.FirstOrDefault(z => z.UserID == model.userId);
            if (user != null)
            {

                var contact = _context.tbContactMessages.Create();

                contact.Message = model.message ?? string.Empty;
                contact.Timestamp = currentTime;
                contact.UserId = model.userId;

                try
                {
                    var filePathdelr = System.Web.Hosting.HostingEnvironment.MapPath(@"~/EmailTemplate/ContactUs.html");
                    var emailTemplateCustomer = System.IO.File.ReadAllText(filePathdelr);
                    var mailBodyDoctor = emailTemplateCustomer.Replace("{{phone}}", user.CountryCode + user.Mobile)
                        .Replace("{{username}}", user.Username)
                        .Replace("{{message}}", model.message);
                    var emailstatus = Send(mailBodyDoctor, "Mesage", new System.Collections.ArrayList { "info@touchpay.ae" });
                    //var emailstatus = Send(mailBodyDoctor, "Mesage", new System.Collections.ArrayList { "premjith.srishti@gmail.com" });
                    if (emailstatus)
                    {
                        returnModel.status = true;
                        returnModel.message = "success";
                        contact.EmailSend = true;
                    }
                    else
                    {
                        returnModel.status = false;
                        returnModel.message = "failed";
                        contact.EmailSend = false;
                    }
                }
                catch (Exception ex)
                {
                    returnModel.status = false;
                    returnModel.message = "failed";
                    contact.EmailSend = false;
                }

                _context.tbContactMessages.Add(contact);

                try
                {
                    _context.SaveChanges();
                }
                catch (Exception ex)
                {

                }

            }
            else
            {
                returnModel.status = false;
                returnModel.message = "No user found";
            }
            return new Tuple<SendContactMailResponse>(returnModel);
        }
        internal static bool Send(string mailbody, string receiverName, System.Collections.ArrayList list_emails)
        {
            {
                SmtpClient client = new SmtpClient();
                string userName = "touchandpay2k18@gmail.com";
                string password = "touchandpay";
                string fromName = "Touch&Pay";
                MailAddress address = new MailAddress(userName, fromName);

                foreach (string mailList in list_emails)
                {
                    MailMessage message = new MailMessage();
                    message.To.Add(new MailAddress(mailList, receiverName));
                    message.From = address;
                    message.Subject = "Touch&pay App";
                    message.IsBodyHtml = true;
                    message.Body = mailbody;
                    client.Host = "smtp.gmail.com";//ConfigurationManager.AppSettings["smptpserver"];
                    client.Port = 587;//Convert.ToInt32(ConfigurationManager.AppSettings["port"]);
                    // client.Host = "smtpmailer.hostek.net";
                    // client.Port =25;
                    client.EnableSsl = true;
                    client.UseDefaultCredentials = true;
                    //client.UseDefaultCredentials = true;
                    client.Credentials = new NetworkCredential(userName, password);
                    try
                    {
                        client.Send(message);
                    }
                    catch (Exception e)
                    {
                    }
                }
            }
            return true;
        }

        public bool deleteOtp()
        {
            try
            {
                _context.SP_UPDATE_OTP();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public Tuple<LogoutResponse> logout(Logout model)
        {
            LogoutResponse returnModel = new LogoutResponse();
            try
            {
                _context.SP_REMOVE_DEVICETOKEN(model.deviceToken);
                returnModel.status = true;
                returnModel.message = "success";
            }
            catch (Exception ex)
            {
                returnModel.status = false;
                returnModel.message = ex.Message;
            }
            return new Tuple<LogoutResponse>(returnModel);
        }

        public object removeAccessToken()
        {
            try
            {
                _context.SP_REMOVE_ACCESSTOKEN();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public Tuple<GetAllTotalAmountResponse> getAllTotalAmount(GetAllTotalAmount model)
        {
            GetAllTotalAmountResponse returnModel = new GetAllTotalAmountResponse();
            returnModel.commisionAmount = "0";
            returnModel.creditAmount = "0";
            returnModel.deductedAmount = "0";
            returnModel.wallet = "0";
            try
            {
                returnModel.creditAmount = Convert.ToString(_context.SP_GET_AGENT_CREDITEDAMOUNT_SUM(model.userId).FirstOrDefault());

                var user = _context.tb_User.FirstOrDefault(z => z.UserID == model.userId);
                if (user != null)
                {
                    returnModel.wallet = Convert.ToString(user.Wallet);
                }

                var totalValues = new EntityLibrary.Data.AdminService().getTotalDeductedAmountAndCommission(model.userId);
                var totalDeductedAmount = totalValues.ToList().Select(z => z.TOTALAMOUNTDEDUCTED).Sum();
                var totalCommission = totalValues.ToList().Select(z => z.TOTALCOMMISSION).Sum();
                returnModel.commisionAmount = Convert.ToString(totalCommission);
                returnModel.deductedAmount = Convert.ToString(totalDeductedAmount);

                returnModel.status = true;
                returnModel.message = "success";
            }
            catch (Exception ex)
            {
                returnModel.status = false;
                returnModel.message = ex.Message;
            }
            return new Tuple<GetAllTotalAmountResponse>(returnModel);
        }
    }
}
