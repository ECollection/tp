﻿using ClassLibrary.ServicePostModel;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace EntityLibrary.Repository
{
    public class apiRepository : BaseReference
    {
        public Tuple<ApiPriorityResponse> prioritylist()
        {
            var response = new ApiPriorityResponse();
            response.MobilePriority = new MobilePriorityResponse();
            response.DTHPriority = new DTHPriorityResponse();
            response.UtilityPriority = new UtilityPriorityResponse();
            response.PostpaidPriority = new PostpaidPriorityResponse();
            response.status = false;
            response.message = "failed";
            var priorityList = _context.tb_ApiPriority.Where(z => z.IsActive).ToList();
            if (priorityList.Count > 0)
            {

                //Mobile
                response.MobilePriority.International = new InternationalPriorityResponse();
                response.MobilePriority.International.list = new List<PriorityData>();
                response.MobilePriority.India = new IndiaPriorityResponse();
                response.MobilePriority.India.list = new List<PriorityData>();
                response.MobilePriority.UAE = new UAEPriorityResponse();
                response.MobilePriority.UAE.list = new List<PriorityData>();
                foreach (var item in priorityList.Where(z => z.ServiceType == (int)ClassLibrary.Enum.serviceType.Mobile && z.ServiceArea == (int)ClassLibrary.Enum.serviceArea.International && z.IsActive)
                    .OrderBy(z => z.Priority).ToList())
                {
                    var obj = new PriorityData();
                    obj.ApiName = ((ClassLibrary.Enum.apiProvider)item.ApiProvider).ToString();
                    obj.ApiProviderEnum = item.ApiProvider;
                    obj.Priority = item.Priority;
                    response.MobilePriority.International.list.Add(obj);
                }
                foreach (var item in priorityList.Where(z => z.ServiceType == (int)ClassLibrary.Enum.serviceType.Mobile && z.ServiceArea == (int)ClassLibrary.Enum.serviceArea.India && z.IsActive)
                    .OrderBy(z => z.Priority).ToList())
                {
                    var obj = new PriorityData();
                    obj.ApiName = ((ClassLibrary.Enum.apiProvider)item.ApiProvider).ToString();
                    obj.ApiProviderEnum = item.ApiProvider;
                    obj.Priority = item.Priority;
                    response.MobilePriority.India.list.Add(obj);
                }

                foreach (var item in priorityList.Where(z => z.ServiceType == (int)ClassLibrary.Enum.serviceType.Mobile && z.ServiceArea == (int)ClassLibrary.Enum.serviceArea.UAE && z.IsActive)
               .OrderBy(z => z.Priority).ToList())
                {
                    var obj = new PriorityData();
                    obj.ApiName = ((ClassLibrary.Enum.apiProvider)item.ApiProvider).ToString();
                    obj.ApiProviderEnum = item.ApiProvider;
                    obj.Priority = item.Priority;
                    response.MobilePriority.UAE.list.Add(obj);
                }



                //DTH
                response.DTHPriority.International = new InternationalPriorityResponse();
                response.DTHPriority.International.list = new List<PriorityData>();
                response.DTHPriority.India = new IndiaPriorityResponse();
                response.DTHPriority.India.list = new List<PriorityData>();
                response.DTHPriority.UAE = new UAEPriorityResponse();
                response.DTHPriority.UAE.list = new List<PriorityData>();
                foreach (var item in priorityList.Where(z => z.ServiceType == (int)ClassLibrary.Enum.serviceType.DTH && z.ServiceArea == (int)ClassLibrary.Enum.serviceArea.International && z.IsActive)
                    .OrderBy(z => z.Priority).ToList())
                {
                    var obj = new PriorityData();
                    obj.ApiName = ((ClassLibrary.Enum.apiProvider)item.ApiProvider).ToString();
                    obj.ApiProviderEnum = item.ApiProvider;
                    obj.Priority = item.Priority;
                    response.DTHPriority.International.list.Add(obj);
                }
                foreach (var item in priorityList.Where(z => z.ServiceType == (int)ClassLibrary.Enum.serviceType.DTH && z.ServiceArea == (int)ClassLibrary.Enum.serviceArea.India && z.IsActive)
                    .OrderBy(z => z.Priority).ToList())
                {
                    var obj = new PriorityData();
                    obj.ApiName = ((ClassLibrary.Enum.apiProvider)item.ApiProvider).ToString();
                    obj.ApiProviderEnum = item.ApiProvider;
                    obj.Priority = item.Priority;
                    response.DTHPriority.India.list.Add(obj);
                }
                foreach (var item in priorityList.Where(z => z.ServiceType == (int)ClassLibrary.Enum.serviceType.DTH && z.ServiceArea == (int)ClassLibrary.Enum.serviceArea.UAE && z.IsActive)
                    .OrderBy(z => z.Priority).ToList())
                {
                    var obj = new PriorityData();
                    obj.ApiName = ((ClassLibrary.Enum.apiProvider)item.ApiProvider).ToString();
                    obj.ApiProviderEnum = item.ApiProvider;
                    obj.Priority = item.Priority;
                    response.DTHPriority.UAE.list.Add(obj);
                }


                //Utility
                response.UtilityPriority.International = new InternationalPriorityResponse();
                response.UtilityPriority.International.list = new List<PriorityData>();
                response.UtilityPriority.India = new IndiaPriorityResponse();
                response.UtilityPriority.India.list = new List<PriorityData>();
                response.UtilityPriority.UAE = new UAEPriorityResponse();
                response.UtilityPriority.UAE.list = new List<PriorityData>();
                foreach (var item in priorityList.Where(z => z.ServiceType == (int)ClassLibrary.Enum.serviceType.Utility && z.ServiceArea == (int)ClassLibrary.Enum.serviceArea.International && z.IsActive)
                    .OrderBy(z => z.Priority).ToList())
                {
                    var obj = new PriorityData();
                    obj.ApiName = ((ClassLibrary.Enum.apiProvider)item.ApiProvider).ToString();
                    obj.ApiProviderEnum = item.ApiProvider;
                    obj.Priority = item.Priority;
                    response.UtilityPriority.International.list.Add(obj);
                }
                foreach (var item in priorityList.Where(z => z.ServiceType == (int)ClassLibrary.Enum.serviceType.Utility && z.ServiceArea == (int)ClassLibrary.Enum.serviceArea.India && z.IsActive)
                    .OrderBy(z => z.Priority).ToList())
                {
                    var obj = new PriorityData();
                    obj.ApiName = ((ClassLibrary.Enum.apiProvider)item.ApiProvider).ToString();
                    obj.ApiProviderEnum = item.ApiProvider;
                    obj.Priority = item.Priority;
                    response.UtilityPriority.India.list.Add(obj);
                }
                foreach (var item in priorityList.Where(z => z.ServiceType == (int)ClassLibrary.Enum.serviceType.Utility && z.ServiceArea == (int)ClassLibrary.Enum.serviceArea.UAE && z.IsActive)
               .OrderBy(z => z.Priority).ToList())
                {
                    var obj = new PriorityData();
                    obj.ApiName = ((ClassLibrary.Enum.apiProvider)item.ApiProvider).ToString();
                    obj.ApiProviderEnum = item.ApiProvider;
                    obj.Priority = item.Priority;
                    response.UtilityPriority.UAE.list.Add(obj);
                }

                //postpaid
                response.PostpaidPriority.International = new InternationalPriorityResponse();
                response.PostpaidPriority.International.list = new List<PriorityData>();
                response.PostpaidPriority.India = new IndiaPriorityResponse();
                response.PostpaidPriority.India.list = new List<PriorityData>();
                response.PostpaidPriority.UAE = new UAEPriorityResponse();
                response.PostpaidPriority.UAE.list = new List<PriorityData>();
                foreach (var item in priorityList.Where(z => z.ServiceType == (int)ClassLibrary.Enum.serviceType.Postpaid && z.ServiceArea == (int)ClassLibrary.Enum.serviceArea.International && z.IsActive)
                    .OrderBy(z => z.Priority).ToList())
                {
                    var obj = new PriorityData();
                    obj.ApiName = ((ClassLibrary.Enum.apiProvider)item.ApiProvider).ToString();
                    obj.Priority = item.Priority;
                    obj.ApiProviderEnum = item.ApiProvider;
                    response.PostpaidPriority.International.list.Add(obj);
                }
                foreach (var item in priorityList.Where(z => z.ServiceType == (int)ClassLibrary.Enum.serviceType.Postpaid && z.ServiceArea == (int)ClassLibrary.Enum.serviceArea.India && z.IsActive)
                    .OrderBy(z => z.Priority).ToList())
                {
                    var obj = new PriorityData();
                    obj.ApiName = ((ClassLibrary.Enum.apiProvider)item.ApiProvider).ToString();
                    obj.ApiProviderEnum = item.ApiProvider;
                    obj.Priority = item.Priority;
                    response.PostpaidPriority.India.list.Add(obj);
                }
                foreach (var item in priorityList.Where(z => z.ServiceType == (int)ClassLibrary.Enum.serviceType.Postpaid && z.ServiceArea == (int)ClassLibrary.Enum.serviceArea.UAE && z.IsActive)
                  .OrderBy(z => z.Priority).ToList())
                {
                    var obj = new PriorityData();
                    obj.ApiName = ((ClassLibrary.Enum.apiProvider)item.ApiProvider).ToString();
                    obj.ApiProviderEnum = item.ApiProvider;
                    obj.Priority = item.Priority;
                    response.PostpaidPriority.UAE.list.Add(obj);
                }
                response.status = true;
                response.message = "success";
                return new Tuple<ApiPriorityResponse>(response);
            }
            else
            {
                return new Tuple<ApiPriorityResponse>(response);
            }
        }

        public Tuple<FetchDeductedAmountResponse> fetchdeductedamount(FetchDeductedAmount model)
        {
            var returnModel = new FetchDeductedAmountResponse();
            returnModel.postModel = model;
            var user = _context.tb_User.FirstOrDefault(z => z.UserID == model.userId);
            if (user != null)
            {
                var currecyRate = new tb_AdminCurrencyRate();
                if (model.switchCurrency)
                {
                    currecyRate = _context.tb_AdminCurrencyRate.Where(z => z.FromCurrency == user.Currency && z.ToCurrency == model.currency).FirstOrDefault();//ask rahul
                }
                else
                {
                    currecyRate = _context.tb_AdminCurrencyRate.Where(z => z.ToCurrency == user.Currency && z.FromCurrency == model.currency).FirstOrDefault();//ask rahul
                }
                //var currecyRate = _context.tb_AdminCurrencyRate.Where(z => z.ToCurrency == user.Currency && z.FromCurrency == model.currency).FirstOrDefault();
                if (currecyRate != null)
                {
                    var amount = Convert.ToDecimal(Convert.ToDouble(model.amount) * currecyRate.CurrencyRate);
                    if (model.switchCurrency)
                    {
                        var newCurrecyRate = _context.tb_AdminCurrencyRate.Where(z => z.ToCurrency == user.Currency && z.FromCurrency == model.currency).FirstOrDefault();//ask rahul
                        amount = Convert.ToDecimal(Convert.ToDouble(amount) * newCurrecyRate.CurrencyRate);
                    }
                    var deductamt = amount;
                    var newAmount = Convert.ToDecimal(0);
                    //if (user.UserType == (int)ClassLibrary.Enum.userType.Agent)
                    //{
                    //    //ClassLibrary.Enum.apiProvider pet = (ClassLibrary.Enum.apiProvider)Enum.Parse(typeof(ClassLibrary.Enum.apiProvider), model.provider);
                    //    //int pet1 = (int)pet;
                    //    //var x = Enum.Parse(typeof(ClassLibrary.Enum.apiProvider), model.provider);
                    //    //var number = (int)((ClassLibrary.Enum.apiProvider)x);
                    //    //var apiprovider = model.apiprovider;
                    //    //Enum.TryParse("model.apiprovider", out (ClassLibrary.Enum.apiProvider) model.apiprovider);
                    //    var commision = _context.tb_AgentCommisionRate.Where(z => z.AgentId == user.UserID && z.IsActive && z.ProductType == model.productType && z.ApiProvider == model.apiprovider).FirstOrDefault();
                    //    if (commision != null)
                    //    {
                    //        deductamt = Convert.ToDecimal(amount) - ((Convert.ToDecimal(amount) * Convert.ToDecimal(commision.Commission)) / 100);
                    //        newAmount = Convert.ToDecimal(deductamt);
                    //    }
                    //    else
                    //    {
                    //        newAmount = Convert.ToDecimal(deductamt);
                    //    }
                    //}
                    //else
                    //{
                    //    newAmount = Convert.ToDecimal(deductamt);

                    //}
                    newAmount = Convert.ToDecimal(deductamt);
                    if (user.Wallet >= deductamt)
                    {

                        returnModel.status = true;
                        returnModel.message = "Success";
                        returnModel.wallet = user.Wallet.ToString();
                        returnModel.deductedAmount = newAmount.ToString();
                    }
                    else
                    {
                        returnModel.status = false;
                        returnModel.message = "Insufficient Amount";
                        returnModel.wallet = user.Wallet.ToString();
                        returnModel.deductedAmount = "0";
                    }
                }
                else
                {
                    returnModel.status = false;
                    returnModel.message = "Failed";
                    returnModel.wallet = user.Wallet.ToString();
                    returnModel.deductedAmount = "0";
                }
            }
            else
            {
                returnModel.status = false;
                returnModel.message = "No Account found";
                returnModel.wallet = "0";
                returnModel.deductedAmount = "0";
            }
            return new Tuple<FetchDeductedAmountResponse>(returnModel);
        }

        public Tuple<FetchDeductedAmountResponse> fetchdeductedamountinternational(FetchDeductedAmount model)
        {
            var returnModel = new FetchDeductedAmountResponse();
            returnModel.postModel = model;
            var user = _context.tb_User.FirstOrDefault(z => z.UserID == model.userId);
            if (user != null)
            {

                var currecyRate = Convert.ToDouble(0);

                if (model.switchCurrency)
                {
                    var currencyToConvert = user.Currency + "_" + model.currency;
                    WebClient web = new WebClient();
                    string url = string.Format("http://free.currencyconverterapi.com/api/v5/convert?q=" + currencyToConvert + "&compact=y&apiKey=" + converterApiKey);
                    string responseString = web.DownloadString(url);
                    dynamic d = JObject.Parse(responseString);
                    if (d != null)
                    {
                        dynamic blogPost = d[currencyToConvert];
                        dynamic blogPost1 = blogPost["val"];
                        if (blogPost1 != null)
                        {
                            currecyRate = blogPost1.Value;
                        }
                    }
                }
                else
                {
                    var currencyToConvert = model.currency + "_" + user.Currency;
                    WebClient web = new WebClient();
                    string url = string.Format("http://free.currencyconverterapi.com/api/v5/convert?q=" + currencyToConvert + "&compact=y&apiKey=" + converterApiKey);
                    string responseString = web.DownloadString(url);
                    dynamic d = JObject.Parse(responseString);
                    if (d != null)
                    {
                        dynamic blogPost = d[currencyToConvert];
                        dynamic blogPost1 = blogPost["val"];
                        if (blogPost1 != null)
                        {
                            currecyRate = blogPost1.Value;
                        }
                    }
                }



                if (currecyRate != 0)
                {
                    var amount = Convert.ToDecimal(Convert.ToDouble(model.amount) * currecyRate);
                    if (model.switchCurrency)
                    {
                        var currencyToConvert = model.currency + "_" + user.Currency;
                        WebClient web = new WebClient();
                        string url = string.Format("http://free.currencyconverterapi.com/api/v5/convert?q=" + currencyToConvert + "&compact=y&apiKey=" + converterApiKey);
                        string responseString = web.DownloadString(url);
                        dynamic d = JObject.Parse(responseString);
                        if (d != null)
                        {
                            dynamic blogPost = d[currencyToConvert];
                            dynamic blogPost1 = blogPost["val"];
                            if (blogPost1 != null)
                            {
                                currecyRate = blogPost1.Value;
                            }
                        }
                        amount = Convert.ToDecimal(Convert.ToDouble(amount) * currecyRate);
                    }
                    var deductamt = amount;
                    var newAmount = Convert.ToDecimal(0);
                    //if (user.UserType == (int)ClassLibrary.Enum.userType.Agent)
                    //{
                    //    var commision = _context.tb_AgentCommisionRate.Where(z => z.AgentId == user.UserID && z.IsActive && z.ProductType == model.productType && z.ApiProvider == model.apiprovider).FirstOrDefault();
                    //    if (commision != null)
                    //    {
                    //        deductamt = Convert.ToDecimal(amount) - ((Convert.ToDecimal(amount) * Convert.ToDecimal(commision.Commission)) / 100);
                    //        newAmount = Convert.ToDecimal(deductamt);
                    //    }
                    //    else
                    //    {
                    //        newAmount = Convert.ToDecimal(deductamt);
                    //    }
                    //}
                    //else
                    //{
                    //    newAmount = Convert.ToDecimal(deductamt);
                    //}
                    newAmount = Convert.ToDecimal(deductamt);
                    if (user.Wallet >= deductamt)
                    {

                        returnModel.status = true;
                        returnModel.message = "Success";
                        returnModel.wallet = user.Wallet.ToString();
                        returnModel.deductedAmount = newAmount.ToString();
                    }
                    else
                    {
                        returnModel.status = false;
                        returnModel.message = "Insufficient Amount";
                        returnModel.wallet = user.Wallet.ToString();
                        returnModel.deductedAmount = "0";
                    }
                }
                else
                {
                    returnModel.status = false;
                    returnModel.message = "Failed";
                    returnModel.wallet = user.Wallet.ToString();
                    returnModel.deductedAmount = "0";
                }
            }
            else
            {
                returnModel.status = false;
                returnModel.message = "No Account found";
                returnModel.wallet = "0";
                returnModel.deductedAmount = "0";
            }
            return new Tuple<FetchDeductedAmountResponse>(returnModel);
        }

        public Tuple<GetUiStatusResponse> getUiStatus()
        {
            var returnModel = new GetUiStatusResponse();
            var row = _context.tUIStatus.FirstOrDefault();
            if (row != null)
            {
                returnModel.message = "Success";
                returnModel.status = true;
                returnModel.Uistatus = row.Status;
            }
            else
            {
                returnModel.message = "Success";
                returnModel.status = true;
                returnModel.Uistatus = false;
            }
            return new Tuple<GetUiStatusResponse>(returnModel);
        }
    }
}
