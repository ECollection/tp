﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityLibrary.Repository
{
    public class ResellerRepository : BaseReference
    {
        public Tuple<bool, string> addAgent(ClassLibrary.WebPostModel.addAgentNew model)
        {
            bool status = false;
            string message = "Failed";
            string password = StringCipher.Encrypt(model.password, salt);
            string mobile = StringCipher.Encrypt(model.mobileNumber, salt);
            string userName = StringCipher.Encrypt(model.name, salt);
            string email = StringCipher.Encrypt(model.email, salt);
            var checkList = _context.tAgents.Where(z => (z.Mobile == mobile) && z.IsActive).ToList();
            if (checkList.Count > 0)
            {
                if (checkList.Any(z => z.Mobile == mobile))
                {
                    status = false;
                    message = "Mobile number already taken";
                }
                else if (checkList.Any(z => z.Email == email))
                {
                    status = false;
                    message = "Email already taken";
                }
                else
                {
                    status = false;
                    message = "Failed";
                }
            }
            else if (_context.tb_User.Any(z => z.Mobile == mobile))
            {
                status = false;
                message = "Mobile number already taken";
            }
            else if (_context.tDistributors.Any(z => z.Mobile == mobile))
            {
                status = false;
                message = "Mobile number already taken";
            }
            else if (_context.tAgents.Any(z => z.Mobile == mobile))
            {
                status = false;
                message = "Mobile number already taken";
            }
            else
            {
                var newAgent = _context.tAgents.Create();
                newAgent.Country = model.place ?? string.Empty;
                newAgent.CountryCode = model.countryCode ?? string.Empty;
                newAgent.IsActive = true;
                newAgent.Mobile = mobile ?? string.Empty;
                newAgent.Password = password ?? string.Empty;
                newAgent.Timestamp = currentTime;
                newAgent.LastUpdated = currentTime;
                newAgent.AgentGuid = Guid.NewGuid();
                newAgent.ProfileImage = string.Empty;
                newAgent.Username = userName ?? string.Empty;
                newAgent.Address = model.address ?? string.Empty;
                newAgent.Place = model.place ?? string.Empty;
                newAgent.ShopName = model.shopName ?? string.Empty;
                newAgent.UserType = (int)ClassLibrary.Enum.userTypeNew.Agent;
                newAgent.Wallet = Convert.ToDecimal(0);
                newAgent.IsBlocked = false;
                newAgent.Email = email ?? string.Empty;
                newAgent.DistributorId = model.distributorId;
                newAgent.ResellerId = model.ResellerId;

                string currency = "AED";
                var Fromcurrency = _context.tb_AdminCurrencyRate.Where(z => z.FromCountryCode == newAgent.CountryCode && z.IsActive).FirstOrDefault();
                if (Fromcurrency != null)
                {
                    currency = Fromcurrency.FromCurrency;
                }
                else
                {
                    var Tocurrency = _context.tb_AdminCurrencyRate.Where(z => z.ToCountryCode == newAgent.CountryCode && z.IsActive).FirstOrDefault();
                    if (Tocurrency != null)
                    {
                        currency = Tocurrency.FromCurrency;
                    }
                }
                newAgent.Currency = currency ?? string.Empty;////////////////////
                _context.tAgents.Add(newAgent);
                if (_context.SaveChanges() > 0)
                {

                    if (model.permission_IndiaRegion)
                    {
                        var indiaPerm = _context.tAgentPermissions.Create();//////////
                        indiaPerm.AgentId = newAgent.AgentID;
                        indiaPerm.Permissions = true;
                        indiaPerm.Timestamp = currentTime;
                        indiaPerm.Type = ClassLibrary.Enum.AgentPermissions.India.ToString();
                        _context.tAgentPermissions.Add(indiaPerm);
                    }

                    if (model.permission_International)
                    {
                        var internationalPerm = _context.tAgentPermissions.Create();
                        internationalPerm.AgentId = newAgent.AgentID;
                        internationalPerm.Permissions = true;
                        internationalPerm.Timestamp = currentTime;
                        internationalPerm.Type = ClassLibrary.Enum.AgentPermissions.International.ToString();
                        _context.tAgentPermissions.Add(internationalPerm);
                    }

                    if (model.permission_Uae)
                    {
                        var uaePerm = _context.tAgentPermissions.Create();
                        uaePerm.AgentId = newAgent.AgentID;
                        uaePerm.Permissions = true;
                        uaePerm.Timestamp = currentTime;
                        uaePerm.Type = ClassLibrary.Enum.AgentPermissions.UAE.ToString();
                        _context.tAgentPermissions.Add(uaePerm);
                    }


                    //Du Topup
                    var newEntry_Gulfbox_DuTopup = _context.tAgentCommisionRates.Create();
                    newEntry_Gulfbox_DuTopup.AgentId = newAgent.AgentID;
                    newEntry_Gulfbox_DuTopup.ApiProvider = (int)ClassLibrary.Enum.apiProvider.Gulfbox;
                    newEntry_Gulfbox_DuTopup.Commission = Convert.ToDouble(model.commision_DuTopup);
                    newEntry_Gulfbox_DuTopup.ProductType = (int)ClassLibrary.Enum.GulfboxProductTypes.Du;
                    newEntry_Gulfbox_DuTopup.IsActive = true;
                    newEntry_Gulfbox_DuTopup.Timestamp = currentTime;
                    _context.tAgentCommisionRates.Add(newEntry_Gulfbox_DuTopup);

                    var newEntry_OnePrepay_DuTopup = _context.tAgentCommisionRates.Create();
                    newEntry_OnePrepay_DuTopup.AgentId = newAgent.AgentID;
                    newEntry_OnePrepay_DuTopup.ApiProvider = (int)ClassLibrary.Enum.apiProvider.OnePrepay;
                    newEntry_OnePrepay_DuTopup.Commission = Convert.ToDouble(model.commision_DuTopup);
                    newEntry_OnePrepay_DuTopup.ProductType = (int)ClassLibrary.Enum.GulfboxProductTypes.Du;
                    newEntry_OnePrepay_DuTopup.IsActive = true;
                    newEntry_OnePrepay_DuTopup.Timestamp = currentTime;
                    _context.tAgentCommisionRates.Add(newEntry_OnePrepay_DuTopup);



                    //Du Voucher
                    var newEntry_Gulfbox_DuVoucher = _context.tAgentCommisionRates.Create();
                    newEntry_Gulfbox_DuVoucher.AgentId = newAgent.AgentID;
                    newEntry_Gulfbox_DuVoucher.ApiProvider = (int)ClassLibrary.Enum.apiProvider.Gulfbox;
                    newEntry_Gulfbox_DuVoucher.Commission = Convert.ToDouble(model.commision_DuVoucher);
                    newEntry_Gulfbox_DuVoucher.ProductType = (int)ClassLibrary.Enum.GulfboxProductTypes.DuVoucher;
                    newEntry_Gulfbox_DuVoucher.IsActive = true;
                    newEntry_Gulfbox_DuVoucher.Timestamp = currentTime;
                    _context.tAgentCommisionRates.Add(newEntry_Gulfbox_DuVoucher);

                    var newEntry_OnePrepay_DuVoucher = _context.tAgentCommisionRates.Create();
                    newEntry_OnePrepay_DuVoucher.AgentId = newAgent.AgentID;
                    newEntry_OnePrepay_DuVoucher.ApiProvider = (int)ClassLibrary.Enum.apiProvider.OnePrepay;
                    newEntry_OnePrepay_DuVoucher.Commission = Convert.ToDouble(model.commision_DuVoucher);
                    newEntry_OnePrepay_DuVoucher.ProductType = (int)ClassLibrary.Enum.GulfboxProductTypes.DuVoucher;
                    newEntry_OnePrepay_DuVoucher.IsActive = true;
                    newEntry_OnePrepay_DuVoucher.Timestamp = currentTime;
                    _context.tAgentCommisionRates.Add(newEntry_OnePrepay_DuVoucher);



                    //Etisalat
                    var newEntry_Gulfbox_Etisalat = _context.tAgentCommisionRates.Create();
                    newEntry_Gulfbox_Etisalat.AgentId = newAgent.AgentID;
                    newEntry_Gulfbox_Etisalat.ApiProvider = (int)ClassLibrary.Enum.apiProvider.Gulfbox;
                    newEntry_Gulfbox_Etisalat.Commission = Convert.ToDouble(model.commision_EtisalatTopup);
                    newEntry_Gulfbox_Etisalat.ProductType = (int)ClassLibrary.Enum.GulfboxProductTypes.Etisalat;
                    newEntry_Gulfbox_Etisalat.IsActive = true;
                    newEntry_Gulfbox_Etisalat.Timestamp = currentTime;
                    _context.tAgentCommisionRates.Add(newEntry_Gulfbox_Etisalat);

                    var newEntry_OnePrepay_Etisalat = _context.tAgentCommisionRates.Create();
                    newEntry_OnePrepay_Etisalat.AgentId = newAgent.AgentID;
                    newEntry_OnePrepay_Etisalat.ApiProvider = (int)ClassLibrary.Enum.apiProvider.OnePrepay;
                    newEntry_OnePrepay_Etisalat.Commission = Convert.ToDouble(model.commision_EtisalatTopup);
                    newEntry_OnePrepay_Etisalat.ProductType = (int)ClassLibrary.Enum.GulfboxProductTypes.Etisalat;
                    newEntry_OnePrepay_Etisalat.IsActive = true;
                    newEntry_OnePrepay_Etisalat.Timestamp = currentTime;
                    _context.tAgentCommisionRates.Add(newEntry_OnePrepay_Etisalat);

                    //Etisalat Voucher
                    var newEntry_Gulfbox_EtisalatVoucher = _context.tAgentCommisionRates.Create();
                    newEntry_Gulfbox_EtisalatVoucher.AgentId = newAgent.AgentID;
                    newEntry_Gulfbox_EtisalatVoucher.ApiProvider = (int)ClassLibrary.Enum.apiProvider.Gulfbox;
                    newEntry_Gulfbox_EtisalatVoucher.Commission = Convert.ToDouble(model.commision_EtisalatVoucher);
                    newEntry_Gulfbox_EtisalatVoucher.ProductType = (int)ClassLibrary.Enum.GulfboxProductTypes.EtisalatVoucher;
                    newEntry_Gulfbox_EtisalatVoucher.IsActive = true;
                    newEntry_Gulfbox_EtisalatVoucher.Timestamp = currentTime;
                    _context.tAgentCommisionRates.Add(newEntry_Gulfbox_EtisalatVoucher);

                    var newEntry_OnePrepay_EtisalatVoucher = _context.tAgentCommisionRates.Create();
                    newEntry_OnePrepay_EtisalatVoucher.AgentId = newAgent.AgentID;
                    newEntry_OnePrepay_EtisalatVoucher.ApiProvider = (int)ClassLibrary.Enum.apiProvider.OnePrepay;
                    newEntry_OnePrepay_EtisalatVoucher.Commission = Convert.ToDouble(model.commision_EtisalatVoucher);
                    newEntry_OnePrepay_EtisalatVoucher.ProductType = (int)ClassLibrary.Enum.GulfboxProductTypes.EtisalatVoucher;
                    newEntry_OnePrepay_EtisalatVoucher.IsActive = true;
                    newEntry_OnePrepay_EtisalatVoucher.Timestamp = currentTime;
                    _context.tAgentCommisionRates.Add(newEntry_OnePrepay_EtisalatVoucher);

                    //Five
                    var newEntry_Gulfbox_Five = _context.tAgentCommisionRates.Create();
                    newEntry_Gulfbox_Five.AgentId = newAgent.AgentID;
                    newEntry_Gulfbox_Five.ApiProvider = (int)ClassLibrary.Enum.apiProvider.Gulfbox;
                    newEntry_Gulfbox_Five.Commission = Convert.ToDouble(model.commision_Five);
                    newEntry_Gulfbox_Five.ProductType = (int)ClassLibrary.Enum.GulfboxProductTypes.Five;
                    newEntry_Gulfbox_Five.IsActive = true;
                    newEntry_Gulfbox_Five.Timestamp = currentTime;
                    _context.tAgentCommisionRates.Add(newEntry_Gulfbox_Five);

                    var newEntry_OnePrepay_Five = _context.tAgentCommisionRates.Create();
                    newEntry_OnePrepay_Five.AgentId = newAgent.AgentID;
                    newEntry_OnePrepay_Five.ApiProvider = (int)ClassLibrary.Enum.apiProvider.OnePrepay;
                    newEntry_OnePrepay_Five.Commission = Convert.ToDouble(model.commision_Five);
                    newEntry_OnePrepay_Five.ProductType = (int)ClassLibrary.Enum.GulfboxProductTypes.Five;
                    newEntry_OnePrepay_Five.IsActive = true;
                    newEntry_OnePrepay_Five.Timestamp = currentTime;
                    _context.tAgentCommisionRates.Add(newEntry_OnePrepay_Five);

                    //Hello

                    var newEntry_Gulfbox_Hello = _context.tAgentCommisionRates.Create();
                    newEntry_Gulfbox_Hello.AgentId = newAgent.AgentID;
                    newEntry_Gulfbox_Hello.ApiProvider = (int)ClassLibrary.Enum.apiProvider.Gulfbox;
                    newEntry_Gulfbox_Hello.Commission = Convert.ToDouble(model.commision_Hello);
                    newEntry_Gulfbox_Hello.ProductType = (int)ClassLibrary.Enum.GulfboxProductTypes.Hello;
                    newEntry_Gulfbox_Hello.IsActive = true;
                    newEntry_Gulfbox_Hello.Timestamp = currentTime;
                    _context.tAgentCommisionRates.Add(newEntry_Gulfbox_Hello);

                    var newEntry_OnePrepay_Hello = _context.tAgentCommisionRates.Create();
                    newEntry_OnePrepay_Hello.AgentId = newAgent.AgentID;
                    newEntry_OnePrepay_Hello.ApiProvider = (int)ClassLibrary.Enum.apiProvider.OnePrepay;
                    newEntry_OnePrepay_Hello.Commission = Convert.ToDouble(model.commision_Hello);
                    newEntry_OnePrepay_Hello.ProductType = (int)ClassLibrary.Enum.GulfboxProductTypes.Hello;
                    newEntry_OnePrepay_Hello.IsActive = true;
                    newEntry_OnePrepay_Hello.Timestamp = currentTime;
                    _context.tAgentCommisionRates.Add(newEntry_OnePrepay_Hello);

                    //salik

                    var newEntry_Gulfbox_salik = _context.tAgentCommisionRates.Create();
                    newEntry_Gulfbox_salik.AgentId = newAgent.AgentID;
                    newEntry_Gulfbox_salik.ApiProvider = (int)ClassLibrary.Enum.apiProvider.Gulfbox;
                    newEntry_Gulfbox_salik.Commission = Convert.ToDouble(model.commision_Salik);
                    newEntry_Gulfbox_salik.ProductType = (int)ClassLibrary.Enum.GulfboxProductTypes.Salik;
                    newEntry_Gulfbox_salik.IsActive = true;
                    newEntry_Gulfbox_salik.Timestamp = currentTime;
                    _context.tAgentCommisionRates.Add(newEntry_Gulfbox_salik);

                    var newEntry_OnePrepay_salik = _context.tAgentCommisionRates.Create();
                    newEntry_OnePrepay_salik.AgentId = newAgent.AgentID;
                    newEntry_OnePrepay_salik.ApiProvider = (int)ClassLibrary.Enum.apiProvider.OnePrepay;
                    newEntry_OnePrepay_salik.Commission = Convert.ToDouble(model.commision_Salik);
                    newEntry_OnePrepay_salik.ProductType = (int)ClassLibrary.Enum.GulfboxProductTypes.Salik;
                    newEntry_OnePrepay_salik.IsActive = true;
                    newEntry_OnePrepay_salik.Timestamp = currentTime;
                    _context.tAgentCommisionRates.Add(newEntry_OnePrepay_salik);



                    //India
                    var newEntry_Jolo_India = _context.tAgentCommisionRates.Create();
                    newEntry_Jolo_India.AgentId = newAgent.AgentID;
                    newEntry_Jolo_India.ApiProvider = (int)ClassLibrary.Enum.apiProvider.Jolo;
                    newEntry_Jolo_India.Commission = Convert.ToDouble(model.commision_India);
                    newEntry_Jolo_India.ProductType = (int)ClassLibrary.Enum.GulfboxProductTypes.NoProduct;
                    newEntry_Jolo_India.IsActive = true;
                    newEntry_Jolo_India.Timestamp = currentTime;
                    _context.tAgentCommisionRates.Add(newEntry_Jolo_India);

                    var newEntry_KeralaRecharge_India = _context.tAgentCommisionRates.Create();
                    newEntry_KeralaRecharge_India.AgentId = newAgent.AgentID;
                    newEntry_KeralaRecharge_India.ApiProvider = (int)ClassLibrary.Enum.apiProvider.KeralaRecharge;
                    newEntry_KeralaRecharge_India.Commission = Convert.ToDouble(model.commision_India);
                    newEntry_KeralaRecharge_India.ProductType = (int)ClassLibrary.Enum.GulfboxProductTypes.NoProduct;
                    newEntry_KeralaRecharge_India.IsActive = true;
                    newEntry_KeralaRecharge_India.Timestamp = currentTime;
                    _context.tAgentCommisionRates.Add(newEntry_KeralaRecharge_India);


                    var newEntry_Pay2All_India = _context.tAgentCommisionRates.Create();
                    newEntry_Pay2All_India.AgentId = newAgent.AgentID;
                    newEntry_Pay2All_India.ApiProvider = (int)ClassLibrary.Enum.apiProvider.Pay2All;
                    newEntry_Pay2All_India.Commission = Convert.ToDouble(model.commision_India);
                    newEntry_Pay2All_India.ProductType = (int)ClassLibrary.Enum.GulfboxProductTypes.NoProduct;
                    newEntry_Pay2All_India.IsActive = true;
                    newEntry_Pay2All_India.Timestamp = currentTime;
                    _context.tAgentCommisionRates.Add(newEntry_Pay2All_India);





                    //Other country
                    var newEntry_Gulfbox_Othercountry = _context.tAgentCommisionRates.Create();
                    newEntry_Gulfbox_Othercountry.AgentId = newAgent.AgentID;
                    newEntry_Gulfbox_Othercountry.ApiProvider = (int)ClassLibrary.Enum.apiProvider.Gulfbox;
                    newEntry_Gulfbox_Othercountry.Commission = Convert.ToDouble(model.commision_OtherCountry);
                    newEntry_Gulfbox_Othercountry.ProductType = (int)ClassLibrary.Enum.GulfboxProductTypes.Intl;
                    newEntry_Gulfbox_Othercountry.IsActive = true;
                    newEntry_Gulfbox_Othercountry.Timestamp = currentTime;
                    _context.tAgentCommisionRates.Add(newEntry_Gulfbox_Othercountry);

                    var newEntry_Ding_Othercountry = _context.tAgentCommisionRates.Create();
                    newEntry_Ding_Othercountry.AgentId = newAgent.AgentID;
                    newEntry_Ding_Othercountry.ApiProvider = (int)ClassLibrary.Enum.apiProvider.Ding;
                    newEntry_Ding_Othercountry.Commission = Convert.ToDouble(model.commision_OtherCountry);
                    newEntry_Ding_Othercountry.ProductType = (int)ClassLibrary.Enum.GulfboxProductTypes.Intl;
                    newEntry_Ding_Othercountry.IsActive = true;
                    newEntry_Ding_Othercountry.Timestamp = currentTime;
                    _context.tAgentCommisionRates.Add(newEntry_Ding_Othercountry);



                    //International
                    var newEntry_Ding_Intl = _context.tAgentCommisionRates.Create();
                    newEntry_Ding_Intl.AgentId = newAgent.AgentID;
                    newEntry_Ding_Intl.ApiProvider = (int)ClassLibrary.Enum.apiProvider.Ding;
                    newEntry_Ding_Intl.Commission = Convert.ToDouble(model.commision_International);
                    newEntry_Ding_Intl.ProductType = (int)ClassLibrary.Enum.GulfboxProductTypes.Intl;
                    newEntry_Ding_Intl.IsActive = true;
                    newEntry_Ding_Intl.Timestamp = currentTime;
                    _context.tAgentCommisionRates.Add(newEntry_Ding_Intl);




                    _context.SaveChanges();
                    status = true;
                    message = "Agent added";
                }
            }
            return new Tuple<bool, string>(status, message);
        }


    }
}
