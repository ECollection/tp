﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityLibrary.Repository
{
    public class agentRepository : BaseReference
    {
        public Tuple<bool, string> editprofile(ClassLibrary.WebPostModel.EditAgent model)
        {
            bool status = false;
            string message = "Failed";

            var agent = _context.tb_User.FirstOrDefault(z => z.UserID == model.agentId);
            if (agent != null)
            {
                if (agent.Address == model.address && agent.Place == model.place && agent.ShopName == model.shopName)
                {
                    status = true;
                    message = "Profile edited";
                }
                else
                {
                    agent.Address = model.address ?? string.Empty;
                    agent.Place = model.place ?? string.Empty;
                    agent.ShopName = model.shopName ?? string.Empty;
                    if (_context.SaveChanges() > 0)
                    {
                        status = true;
                        message = "Profile edited";
                    }
                }
            }
            return new Tuple<bool, string>(status, message);
        }
    }
}
