﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityLibrary.Data
{
   public class SP_FETCH_CREDITHISTORY_LIST:BaseReference
    {
       private SP_FETCH_CREDITHISTORY_LIST_Result res;
       public SP_FETCH_CREDITHISTORY_LIST(SP_FETCH_CREDITHISTORY_LIST_Result obj) { res = obj; }
       public long AGENTID { get { return res.AGENTID; } }
       public decimal AMOUNT { get { return res.AMOUNT; } }
       public string TYPE { get { return res.TYPE; } }
       public System.DateTime DATE { get { return res.DATE; } }
       //public string USERNAME { get { return res.USERNAME; } }
       public string USERNAME { get { return StringCipher.Decrypt(res.USERNAME, salt); } }

    }
}
