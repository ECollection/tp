﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityLibrary.Data
{
    public class SP_FETCH_STORED_VOUCHERS:BaseReference
    {
        private SP_FETCH_STORED_VOUCHERS_Result res;
        public SP_FETCH_STORED_VOUCHERS(SP_FETCH_STORED_VOUCHERS_Result obj) { res = obj; }
        public string SerialNumber { get { return res.SerialNumber; } }
        public string PinNumber { get { return res.PinNumber; } }
        public long PID { get { return res.PID; } }
    }
}
