﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityLibrary.Data
{
    public class AgentPermissions : BaseReference
    {
        private tAgentPermission permission;
        public AgentPermissions(tAgentPermission obj) { permission = obj; }
        public AgentPermissions(long id) { permission = _context.tAgentPermissions.FirstOrDefault(z => z.Id == id); }
        public long Id { get { return permission.Id; } }
        public long AgentId { get { return permission.AgentId; } }
        public string Type { get { return permission.Type; } }
        public bool Permissions { get { return permission.Permissions; } }
        public System.DateTime Timestamp { get { return permission.Timestamp; } }

    }
}
