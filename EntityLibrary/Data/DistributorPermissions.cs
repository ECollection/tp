﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityLibrary.Data
{
    public class DistributorPermissions : BaseReference
    {
          private tDistributorPermission permission;
          public DistributorPermissions(tDistributorPermission obj) { permission = obj; }
          public DistributorPermissions(long id) { permission = _context.tDistributorPermissions.FirstOrDefault(z => z.Id == id); }
          public long DistributorID { get { return permission.DistributorId; } }
          public long Id { get { return permission.Id; } }
          public string Type { get { return permission.Type; } }
          public bool Permissions { get { return permission.Permissions; } }
          public System.DateTime Timestamp { get { return permission.Timestamp; } }
    }
}
