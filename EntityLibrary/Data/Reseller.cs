﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityLibrary.Data
{
    public class Reseller : BaseReference
    {
        private tReseller reseller;
        public Reseller(tReseller obj) { reseller = obj; }
        public Reseller(long id) { reseller = _context.tResellers.FirstOrDefault(z => z.ResellerId == id); }
        public long ResellerId { get { return reseller.ResellerId; } }
        public System.Guid ResellerGuid { get { return reseller.ResellerGuid; } }
        public long DistributorId { get { return reseller.DistributorId; } }
        public string Email { get { return StringCipher.Decrypt(reseller.Email, salt); } }
        public string Username { get { return StringCipher.Decrypt(reseller.Username, salt); } }
        public string Country { get { return reseller.Country; } }
        public string CountryCode { get { return reseller.CountryCode; } }
        public string Mobile { get { return StringCipher.Decrypt(reseller.Mobile, salt); } }
        public string Password { get { return StringCipher.Decrypt(reseller.Password, salt); } }
        public bool IsActive { get { return reseller.IsActive; } }
        public System.DateTime Timestamp { get { return reseller.Timestamp; } }
        public string ProfileImage { get { return reseller.ProfileImage; } }
        public string Place { get { return reseller.Place; } }
        public string Address { get { return reseller.Address; } }
        public string ShopName { get { return reseller.ShopName; } }
        public int UserType { get { return reseller.UserType; } }
        public decimal Wallet { get { return reseller.Wallet; } }
        public string Currency { get { return reseller.Currency; } }
        public bool IsBlocked { get { return reseller.IsBlocked; } }
        public System.DateTime LastUpdated { get { return reseller.LastUpdated; } }
        public List<ResellerCommisionRate> commissions
        {
            get
            {
                return reseller.tResellerCommisionRates.Where(z => z.IsActive).ToList().Select(z => new ResellerCommisionRate(z)).ToList();
            }
        }
        public List<ResellerPermissions> permissions
        {
            get
            {
                return reseller.tResellerPermissions.ToList().Select(z => new ResellerPermissions(z)).ToList();
            }
        }

        public decimal CreditTotal
        {
            get
            {
                try
                {
                    return Convert.ToDecimal(_context.SP_RESELLER_CREDIT_SUM(reseller.ResellerId, string.Empty, string.Empty).FirstOrDefault());
                }
                catch (Exception ex)
                {
                    return 0;
                }
            }
        }
    }
}
