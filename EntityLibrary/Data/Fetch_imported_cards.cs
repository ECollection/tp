﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityLibrary.Data
{
    public class Fetch_imported_cards:BaseReference
    {
        private SP_Fetch_Imported_Cards_Result cards;
        public Fetch_imported_cards(SP_Fetch_Imported_Cards_Result obj) { cards = obj; }
        public long Id { get { return cards.Id; } }
        public string SerialNumber { get { return cards.SerialNumber; } }
        public string PinNumber { get { return cards.PinNumber; } }
        public string CardType { get { return cards.CardType; } }
        public string Amount { get { return cards.Amount; } }
        public System.DateTime Timestamp { get { return cards.Timestamp; } }
        public bool IsDuplicate { get { return cards.IsDuplicate; } }
    }
}
