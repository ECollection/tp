﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityLibrary.Data
{
    public class RechargeCardHistory : BaseReference
    {
        private tb_RechargeCardHistory history;
        public RechargeCardHistory(tb_RechargeCardHistory obj) { history = obj; }
        public RechargeCardHistory(long id) { history = _context.tb_RechargeCardHistory.FirstOrDefault(z => z.HistoryId == id); }
        public long HistoryId { get { return history.HistoryId; } }
        public long UserId { get { return history.UserId; } }
        public long CardId { get { return history.CardId; } }
        public System.DateTime UsedTime { get { return history.UsedTime; } }
        public User UserData { get { return new User(history.UserId); } }
    }
}
