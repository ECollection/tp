﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityLibrary.Data
{
   public class sp_fetch_rechargecards_for_export:BaseReference
    {
       private sp_fetch_rechargecards_for_export_Result res;
       public sp_fetch_rechargecards_for_export(sp_fetch_rechargecards_for_export_Result obj) { res = obj; }
       public long CardId { get { return res.CardId; } }
       public System.Guid CardGuid { get { return res.CardGuid; } }
       public string CardNumber { get { return res.CardNumber; } }
       public decimal Amount { get { return res.Amount; } }
       public System.DateTime CreatedAt { get { return res.CreatedAt; } }
       public string BatchId { get { return res.BatchId; } }
       public string Currency { get { return res.Currency; } }
    }
}
