﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityLibrary.Data
{
    public class DistributorCommisionRate : BaseReference
    {
        private tDistributorCommisionRate rate;
        public DistributorCommisionRate(tDistributorCommisionRate obj) { rate = obj; }
        public DistributorCommisionRate(long id) { rate = _context.tDistributorCommisionRates.FirstOrDefault(z => z.Id == id); }
        public long Id { get { return rate.Id; } }
        public long DistributorId { get { return rate.DistributorId; } }
        public int ApiProvider { get { return rate.ApiProvider; } }
        public int ProductType { get { return rate.ProductType; } }
        public double Commission { get { return rate.Commission; } }
        public bool IsActive { get { return rate.IsActive; } }
        public System.DateTime Timestamp { get { return rate.Timestamp; } }
    }
}
