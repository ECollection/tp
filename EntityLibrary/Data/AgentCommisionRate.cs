﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityLibrary.Data
{
    public class AgentCommisionRate : BaseReference
    {
        private tAgentCommisionRate rate;
        public AgentCommisionRate(tAgentCommisionRate obj) { rate = obj; }
        public AgentCommisionRate(long id) { rate = _context.tAgentCommisionRates.FirstOrDefault(z => z.Id == id); }
        public long Id { get { return rate.Id; } }
        public long AgentId { get { return rate.AgentId; } }
        public int ApiProvider { get { return rate.ApiProvider; } }
        public int ProductType { get { return rate.ProductType; } }
        public double Commission { get { return rate.Commission; } }
        public bool IsActive { get { return rate.IsActive; } }
        public System.DateTime Timestamp { get { return rate.Timestamp; } }
    }
}
