﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityLibrary.Data
{
    public class SP_FETCH_ALL_TRANSACTIONS : BaseReference
    {
        private SP_FETCH_ALL_TRANSACTIONS_Result res;
        public SP_FETCH_ALL_TRANSACTIONS(SP_FETCH_ALL_TRANSACTIONS_Result obj) { res = obj; }
        public string transactionid { get { return res.transactionid; } }
        public string transactionstate { get { return res.transactionstate; } }
        public System.DateTime transactiontimeUTC { get { return res.transactiontime; } }
        public System.DateTime transactiontime { get { return uaeTime(transactiontimeUTC); } }
        public string rechargenumber { get { return res.rechargenumber; } }
        public decimal? amount { get { return res.amount; } }
        public Nullable<int> type { get { return res.type; } }
        public Nullable<int> apiprovider { get { return res.apiprovider; } }
        public string currency { get { return res.currency; } }
        public string ProductType { get { return res.ProductType; } }

        public Nullable<decimal> amountdeducted { get { return res.amountdeducted; } }
        public string amountcurrency { get { return res.amountcurrency; } }
        public string amountdeductedcurrency { get { return res.amountdeductedcurrency; } }

        public long USERID { get { return res.USERID; } }
        //public string USERNAME { get { return res.USERNAME; } }
        public string USERNAME { get { return StringCipher.Decrypt(res.USERNAME, salt); } }
        public int USERTYPE { get { return res.USERTYPE; } }

        public string apiProviderString
        {
            get
            {
                ClassLibrary.Enum.apiProvider x = (ClassLibrary.Enum.apiProvider)Convert.ToInt32(apiprovider);
                var value = x.ToString();
                if (value == "NoProvider")
                    return "-";
                else
                    return value;
            }
        }

        public DateTime uaeTime(DateTime utcTime)
        {
            try
            {
                //var timeZones = TimeZoneInfo.GetSystemTimeZones();
                TimeZoneInfo cstZone = TimeZoneInfo.FindSystemTimeZoneById("Arabian Standard Time");
                DateTime cstTime = TimeZoneInfo.ConvertTimeFromUtc(utcTime, cstZone);
                //Console.WriteLine("The date and time are {0} {1}.",
                //                  cstTime,
                //                  cstZone.IsDaylightSavingTime(cstTime) ?
                //                          cstZone.DaylightName : cstZone.StandardName);
                return cstTime;
            }
            catch (TimeZoneNotFoundException)
            {
                return utcTime;
                //Console.WriteLine("The registry does not define the Central Standard Time zone.");
            }
            catch (InvalidTimeZoneException)
            {
                return utcTime;
                //Console.WriteLine("Registry data on the Central Standard Time zone has been corrupted.");
            }
        }

    }
}
