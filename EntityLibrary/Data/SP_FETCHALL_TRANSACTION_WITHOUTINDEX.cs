﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityLibrary.Data
{
    public class SP_FETCHALL_TRANSACTION_WITHOUTINDEX : BaseReference
    {
        private SP_FETCHALL_TRANSACTION_WITHOUTINDEX_Result res;
        public SP_FETCHALL_TRANSACTION_WITHOUTINDEX(SP_FETCHALL_TRANSACTION_WITHOUTINDEX_Result obj) { res = obj; }
        public string transactionid { get { return res.transactionid; } }
        public string transactionstate { get { return res.transactionstate; } }
        public System.DateTime transactiontime { get { return res.transactiontime; } }
        public string rechargenumber { get { return res.rechargenumber; } }
        public decimal? amount { get { return res.amount; } }
        public Nullable<int> type { get { return res.type; } }
        public Nullable<int> apiprovider { get { return res.apiprovider; } }
        public string currency { get { return res.currency; } }
        public string ProductType { get { return res.ProductType; } }
        public Nullable<decimal> amountdeducted { get { return res.amountdeducted; } }
        public string amountcurrency { get { return res.amountcurrency; } }
        public string amountdeductedcurrency { get { return res.amountdeductedcurrency; } }
        public string apiProviderString
        {
            get
            {
                ClassLibrary.Enum.apiProvider x = (ClassLibrary.Enum.apiProvider)Convert.ToInt32(apiprovider);
                var value = x.ToString();
                if (value == "NoProvider")
                    return "-";
                else
                    return value;
            }
        }
    }
}
