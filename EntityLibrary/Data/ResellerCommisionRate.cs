﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityLibrary.Data
{
    public class ResellerCommisionRate : BaseReference
    {
        private tResellerCommisionRate rate;
        public ResellerCommisionRate(tResellerCommisionRate obj) { rate = obj; }
        public ResellerCommisionRate(long id) { rate = _context.tResellerCommisionRates.FirstOrDefault(z => z.Id == id); }
        public long Id { get { return rate.Id; } }
        public long ResellerId { get { return rate.ResellerId; } }
        public int ApiProvider { get { return rate.ApiProvider; } }
        public int ProductType { get { return rate.ProductType; } }
        public double Commission { get { return rate.Commission; } }
        public bool IsActive { get { return rate.IsActive; } }
        public System.DateTime Timestamp { get { return rate.Timestamp; } }
    }
}
