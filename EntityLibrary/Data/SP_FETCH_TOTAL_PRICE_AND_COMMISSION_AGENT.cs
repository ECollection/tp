﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityLibrary.Data
{
    public class SP_FETCH_TOTAL_PRICE_AND_COMMISSION_AGENT:BaseReference
    {
        private SP_FETCH_TOTAL_PRICE_AND_COMMISSION_AGENT_Result res;
        public SP_FETCH_TOTAL_PRICE_AND_COMMISSION_AGENT(SP_FETCH_TOTAL_PRICE_AND_COMMISSION_AGENT_Result obj) { res = obj; }
        public Nullable<decimal> TOTALAMOUNTDEDUCTED { get { return res.TOTALAMOUNTDEDUCTED; } }
        public Nullable<decimal> TOTALCOMMISSION { get { return res.TOTALCOMMISSION; } }
    }
}
