﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityLibrary.Data
{
    public class AgentCommissionRate : BaseReference
    {
        private tb_AgentCommisionRate rate;
        public AgentCommissionRate(tb_AgentCommisionRate obj) { rate = obj; }
        public AgentCommissionRate(long id) { rate = _context.tb_AgentCommisionRate.FirstOrDefault(z => z.Id == id); }
        public long Id { get { return rate.Id; } }
        public long AgentId { get { return rate.AgentId; } }
        public int ApiProvider { get { return rate.ApiProvider; } }
        public int ProductType { get { return rate.ProductType; } }
        public double Commission { get { return rate.Commission; } }
        public bool IsActive { get { return rate.IsActive; } }
        public System.DateTime Timestamp { get { return rate.Timestamp; } }
        public bool IsAdminCommission { get { return rate.IsAdminCommission; } }
    }
}
