﻿using ClassLibrary.WebPostModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace EntityLibrary.Data
{
    public class AdminService : BaseReference
    {
        public List<User> getAgents(int index, string searchtext, string listcount, string userCurrency)
        {
            int count = Convert.ToInt32(listcount);
            if (searchtext != null && searchtext != string.Empty)
            {
                string search = StringCipher.Encrypt(searchtext, salt);
                if (userCurrency == "All")
                {
                    return _context.tb_User.Where(z => z.IsActive && z.UserType == (int)ClassLibrary.Enum.userType.Agent && (z.Username.Contains(search) || z.Mobile.Contains(search)))
                        .ToList().OrderByDescending(z => z.Timestamp).ToList().
                        Skip(count * index).Take(count).ToList().Select(z => new User(z)).ToList();
                }
                else
                {
                    return _context.tb_User.Where(z => z.IsActive && z.Currency == userCurrency && z.UserType == (int)ClassLibrary.Enum.userType.Agent && (z.Username.Contains(search) || z.Mobile.Contains(search)))
                           .ToList().OrderByDescending(z => z.Timestamp).ToList().
                           Skip(count * index).Take(count).ToList().Select(z => new User(z)).ToList();
                }
            }
            else
            {
                if (userCurrency == "All")
                {
                    return _context.tb_User.Where(z => z.IsActive && z.UserType == (int)ClassLibrary.Enum.userType.Agent)
                        .ToList().OrderByDescending(z => z.Timestamp).ToList().
                        Skip(count * index).Take(count).ToList().Select(z => new User(z)).ToList();
                }
                else
                {
                    return _context.tb_User.Where(z => z.IsActive && z.Currency == userCurrency && z.UserType == (int)ClassLibrary.Enum.userType.Agent)
                          .ToList().OrderByDescending(z => z.Timestamp).ToList().
                          Skip(count * index).Take(count).ToList().Select(z => new User(z)).ToList();
                }
            }
        }
        public int getAgentsCount(int index, string searchtext, string listcount, string userCurrency)
        {
            int count = Convert.ToInt32(listcount);
            if (searchtext != null && searchtext != string.Empty)
            {
                string search = StringCipher.Encrypt(searchtext, salt);
                if (userCurrency == "All")
                {
                    return _context.tb_User.Where(z => z.IsActive && z.UserType == (int)ClassLibrary.Enum.userType.Agent && (z.Username.Contains(search) || z.Mobile.Contains(search)))
                    .ToList().OrderByDescending(z => z.Timestamp).ToList().
                    Skip(count * index).Take(count).ToList().Count;
                }
                else
                {
                    return _context.tb_User.Where(z => z.IsActive && z.Currency == userCurrency && z.UserType == (int)ClassLibrary.Enum.userType.Agent && (z.Username.Contains(search) || z.Mobile.Contains(search)))
                        .ToList().OrderByDescending(z => z.Timestamp).ToList().
                        Skip(count * index).Take(count).ToList().Count;
                }
            }
            else
            {
                if (userCurrency == "All")
                {
                    return _context.tb_User.Where(z => z.IsActive && z.UserType == (int)ClassLibrary.Enum.userType.Agent).ToList().OrderByDescending(z => z.Timestamp).ToList().
                 Skip(count * index).Take(count).ToList().Count;
                }
                else
                {
                    return _context.tb_User.Where(z => z.IsActive && z.Currency == userCurrency && z.UserType == (int)ClassLibrary.Enum.userType.Agent).ToList().OrderByDescending(z => z.Timestamp).ToList().
                    Skip(count * index).Take(count).ToList().Count;
                }
            }
        }

        public List<RechargeCard> getRechargeCards(int index, string search, string listcount)
        {
            int count = Convert.ToInt32(listcount);
            var assignedCards = _context.tb_AssignCard.ToList().Select(z => z.CardId);
            if (search != null && search != string.Empty)
            {
                try
                {
                    var decimalsearch = Convert.ToDecimal(search);
                    return _context.tb_RechargeCard.Where(z => !z.IsDeleted && !z.IsUsed && !assignedCards.Contains(z.CardId) && (z.CardNumber.Contains(search) || z.Amount == decimalsearch))
                        .ToList().OrderByDescending(z => z.CreatedAt).ToList().
                           Skip(count * index).Take(count).ToList().Select(z => new RechargeCard(z)).ToList();
                }
                catch (Exception ex)
                {
                    return _context.tb_RechargeCard.Where(z => !z.IsDeleted && !z.IsUsed && !assignedCards.Contains(z.CardId) && (z.CardNumber.Contains(search)))
                          .ToList().OrderByDescending(z => z.CreatedAt).ToList().
                             Skip(count * index).Take(count).ToList().Select(z => new RechargeCard(z)).ToList();
                }
            }
            else
            {
                return _context.tb_RechargeCard.Where(z => !z.IsDeleted && !z.IsUsed && !assignedCards.Contains(z.CardId)).ToList().OrderByDescending(z => z.CreatedAt).ToList().
                           Skip(count * index).Take(count).ToList().Select(z => new RechargeCard(z)).ToList();
            }
        }

        public int getRechargeCardsCount(int index, string search, string listcount)
        {
            int count = Convert.ToInt32(listcount);
            var assignedCards = _context.tb_AssignCard.ToList().Select(z => z.CardId);
            if (search != null && search != string.Empty)
            {
                try
                {
                    var decimalsearch = Convert.ToDecimal(search);
                    return _context.tb_RechargeCard.Where(z => !z.IsDeleted && !z.IsUsed && !assignedCards.Contains(z.CardId) && (z.CardNumber.Contains(search) || z.Amount == decimalsearch))
                        .ToList().OrderByDescending(z => z.CreatedAt).ToList().
                           Skip(count * index).Take(count).ToList().Count;
                }
                catch (Exception ex)
                {
                    return _context.tb_RechargeCard.Where(z => !z.IsDeleted && !z.IsUsed && !assignedCards.Contains(z.CardId) && (z.CardNumber.Contains(search)))
                      .ToList().OrderByDescending(z => z.CreatedAt).ToList().
                         Skip(count * index).Take(count).ToList().Count;
                }
            }
            else
            {
                return _context.tb_RechargeCard.Where(z => !z.IsDeleted && !z.IsUsed && !assignedCards.Contains(z.CardId)).ToList().OrderByDescending(z => z.CreatedAt).ToList().
                 Skip(count * index).Take(count).ToList().Count;
            }
        }




        public List<Fetch_imported_cards> getImportedCards(int index, string search, string listcount)
        {
            int count = Convert.ToInt32(listcount);

            return _context.SP_Fetch_Imported_Cards(search ?? string.Empty, index, count).ToList()
                .Select(z => new Fetch_imported_cards(z)).ToList();

            //29-08-2018 premjith
            //var ids = _context.SP_FETCH_USED_STOREDCARDS_ID().ToList();
            //if (search != null && search != string.Empty)
            //{
            //    try
            //    {
            //        return _context.tb_AdminStoredCard.Where(z => !ids.Contains(z.Id) && !z.IsDeleted && (z.PinNumber.Contains(search) || z.Amount == search || z.CardType==search))
            //            .ToList().OrderByDescending(z => z.Timestamp).ToList().
            //               Skip(count * index).Take(count).ToList().Select(z => new AdminStoredCards(z)).ToList();
            //    }
            //    catch (Exception ex)
            //    {
            //        return _context.tb_AdminStoredCard.Where(z => !ids.Contains(z.Id) && !z.IsDeleted && (z.PinNumber.Contains(search)) || z.CardType == search)
            //              .ToList().OrderByDescending(z => z.Timestamp).ToList().
            //                 Skip(count * index).Take(count).ToList().Select(z => new AdminStoredCards(z)).ToList();
            //    }
            //}
            //else
            //{
            //    return _context.tb_AdminStoredCard.Where(z => !ids.Contains(z.Id) && !z.IsDeleted).ToList().OrderByDescending(z => z.Timestamp).ToList().
            //               Skip(count * index).Take(count).ToList().Select(z => new AdminStoredCards(z)).ToList();
            //}
        }


        public int getImportedCardsCount(int index, string search, string listcount)
        {
            var ids = _context.SP_FETCH_USED_STOREDCARDS_ID().ToList();
            int count = Convert.ToInt32(listcount);
            if (search != null && search != string.Empty)
            {
                try
                {
                    return _context.tb_AdminStoredCard.Where(z => !ids.Contains(z.Id) && !z.IsDeleted && (z.PinNumber.Contains(search) || z.Amount == search || z.CardType == search))
                        .ToList().OrderByDescending(z => z.Timestamp).ToList().
                           Skip(count * index).Take(count).ToList().Count;
                }
                catch (Exception ex)
                {
                    return _context.tb_AdminStoredCard.Where(z => !ids.Contains(z.Id) && !z.IsDeleted && (z.PinNumber.Contains(search)) || z.CardType == search)
                      .ToList().OrderByDescending(z => z.Timestamp).ToList().
                         Skip(count * index).Take(count).ToList().Count;
                }
            }
            else
            {
                return _context.tb_AdminStoredCard.Where(z => !ids.Contains(z.Id) && !z.IsDeleted).ToList().OrderByDescending(z => z.Timestamp).ToList().
                 Skip(count * index).Take(count).ToList().Count;
            }
        }





        public List<User> getUsers(int index, string searchtext, string listcount)
        {
            int count = Convert.ToInt32(listcount);
            if (searchtext != null && searchtext != string.Empty)
            {
                string search = StringCipher.Encrypt(searchtext, salt);
                try
                {
                    var decimalsearch = Convert.ToDecimal(search);
                    var longsearch = Convert.ToInt64(search);
                    return _context.tb_User.Where(z => z.IsActive && z.UserType == (int)ClassLibrary.Enum.userType.User
                        && (z.Username.Contains(search) || z.Mobile.Contains(search) || z.Country.Contains(search)
                        || z.Wallet == decimalsearch || z.UserID == longsearch))
                        .ToList().OrderByDescending(z => z.Timestamp).ToList().
                        Skip(count * index).Take(count).ToList().Select(z => new User(z)).ToList();
                }
                catch (Exception ex)
                {
                    return _context.tb_User.Where(z => z.IsActive && z.UserType == (int)ClassLibrary.Enum.userType.User
                           && (z.Username.Contains(search) || z.Mobile.Contains(search) || z.Country.Contains(search)))
                           .ToList().OrderByDescending(z => z.Timestamp).ToList().
                           Skip(count * index).Take(count).ToList().Select(z => new User(z)).ToList();
                }
            }
            else
            {
                return _context.tb_User.Where(z => z.IsActive && z.UserType == (int)ClassLibrary.Enum.userType.User).ToList().OrderByDescending(z => z.Timestamp).ToList().
                       Skip(count * index).Take(count).ToList().Select(z => new User(z)).ToList();
            }
        }
        public int getUsersCount(int index, string searchtext, string listcount)
        {
            int count = Convert.ToInt32(listcount);
            if (searchtext != null && searchtext != string.Empty)
            {
                string search = StringCipher.Encrypt(searchtext, salt);

                try
                {
                    var decimalsearch = Convert.ToDecimal(search);
                    var longsearch = Convert.ToInt64(search);
                    return _context.tb_User.Where(z => z.IsActive && z.UserType == (int)ClassLibrary.Enum.userType.User
                        && (z.Username.Contains(search) || z.Mobile.Contains(search) || z.Country.Contains(search)
                        || z.Wallet == decimalsearch || z.UserID == longsearch))
                        .ToList().OrderByDescending(z => z.Timestamp).ToList().
                        Skip(count * index).Take(count).ToList().Count;
                }
                catch (Exception ex)
                {
                    return _context.tb_User.Where(z => z.IsActive && z.UserType == (int)ClassLibrary.Enum.userType.User
                        && (z.Username.Contains(search) || z.Mobile.Contains(search) || z.Country.Contains(search)))
                        .ToList().OrderByDescending(z => z.Timestamp).ToList().
                        Skip(count * index).Take(count).ToList().Count;
                }
            }
            else
            {
                return _context.tb_User.Where(z => z.IsActive && z.UserType == (int)ClassLibrary.Enum.userType.User).ToList().OrderByDescending(z => z.Timestamp).ToList().
                          Skip(count * index).Take(count).ToList().Count;
            }
        }

        public List<RechargeCard> getUserUsedCards(int index, long userId, string search, string listcount)
        {
            int count = Convert.ToInt32(listcount);
            if (search != null && search != string.Empty)
            {
                try
                {
                    var decimalsearch = Convert.ToDecimal(search);
                    var longsearch = Convert.ToInt64(search);
                    return _context.tb_RechargeCardHistory.Where(z => z.UserId == userId && (z.tb_RechargeCard.CardNumber.Contains(search) || z.tb_RechargeCard.Amount == decimalsearch)).ToList().OrderByDescending(z => z.UsedTime).ToList().
                        Skip(count * index).Take(count).ToList().Select(z => new RechargeCard(z.tb_RechargeCard)).ToList();
                }
                catch (Exception ex)
                {
                    return _context.tb_RechargeCardHistory.Where(z => z.UserId == userId && (z.tb_RechargeCard.CardNumber.Contains(search))).ToList().OrderByDescending(z => z.UsedTime).ToList().
                        Skip(count * index).Take(count).ToList().Select(z => new RechargeCard(z.tb_RechargeCard)).ToList();
                }
            }
            else
            {
                return _context.tb_RechargeCardHistory.Where(z => z.UserId == userId).ToList().OrderByDescending(z => z.UsedTime).ToList().
                           Skip(count * index).Take(count).ToList().Select(z => new RechargeCard(z.tb_RechargeCard)).ToList();
            }
        }
        public int getUserUsedCardsCount(int index, long userId, string search, string listcount)
        {
            int count = Convert.ToInt32(listcount);
            if (search != null && search != string.Empty)
            {
                try
                {
                    var decimalsearch = Convert.ToDecimal(search);
                    var longsearch = Convert.ToInt64(search);
                    var data = _context.tb_RechargeCardHistory.Where(z => z.UserId == userId && (z.tb_RechargeCard.CardNumber.Contains(search) || z.tb_RechargeCard.Amount == decimalsearch)).ToList().OrderByDescending(z => z.UsedTime).ToList().
                          Skip(count * index).Take(count).ToList().Select(z => new RechargeCard(z.tb_RechargeCard)).ToList();
                    return data.Count;
                }
                catch (Exception ex)
                {
                    var data = _context.tb_RechargeCardHistory.Where(z => z.UserId == userId && (z.tb_RechargeCard.CardNumber.Contains(search))).ToList().OrderByDescending(z => z.UsedTime).ToList().
                          Skip(count * index).Take(count).ToList().Select(z => new RechargeCard(z.tb_RechargeCard)).ToList();
                    return data.Count;
                }
            }
            else
            {
                var data = _context.tb_RechargeCardHistory.Where(z => z.UserId == userId).ToList().OrderByDescending(z => z.UsedTime).ToList().
                       Skip(count * index).Take(count).ToList().Select(z => new RechargeCard(z.tb_RechargeCard)).ToList();
                return data.Count;
            }
        }

        public List<RechargeCard> getAgentAssignedCards(int index, long agentId, string search, string listcount)
        {
            int count = Convert.ToInt32(listcount);
            if (search != null && search != string.Empty)
            {
                try
                {
                    var decimalsearch = Convert.ToDecimal(search);
                    return _context.tb_AssignCard.Where(z => z.UserId == agentId && (z.tb_RechargeCard.CardNumber.Contains(search) || z.tb_RechargeCard.Amount == decimalsearch)).ToList().OrderByDescending(z => z.AssignId).ToList().
                    Skip(count * index).Take(count).ToList().Select(z => new RechargeCard(z.tb_RechargeCard)).ToList();

                }
                catch (Exception ex)
                {
                    return _context.tb_AssignCard.Where(z => z.UserId == agentId && z.tb_RechargeCard.CardNumber.Contains(search)).ToList().OrderByDescending(z => z.AssignId).ToList().
                    Skip(count * index).Take(count).ToList().Select(z => new RechargeCard(z.tb_RechargeCard)).ToList();
                }
            }
            else
            {
                return _context.tb_AssignCard.Where(z => z.UserId == agentId).ToList().OrderByDescending(z => z.AssignId).ToList().
                    Skip(count * index).Take(count).ToList().Select(z => new RechargeCard(z.tb_RechargeCard)).ToList();
            }
        }
        public int getAgentAssignedCardsCount(int index, long agentId, string search, string listcount)
        {
            int count = Convert.ToInt32(listcount);
            if (search != null && search != string.Empty)
            {
                try
                {
                    var decimalsearch = Convert.ToDecimal(search);
                    return _context.tb_AssignCard.Where(z => z.UserId == agentId && (z.tb_RechargeCard.CardNumber.Contains(search) || z.tb_RechargeCard.Amount == decimalsearch)).ToList().OrderByDescending(z => z.AssignId).ToList().
                    Skip(count * index).Take(count).ToList().Select(z => new RechargeCard(z.tb_RechargeCard)).ToList().Count;

                }
                catch (Exception ex)
                {
                    return _context.tb_AssignCard.Where(z => z.UserId == agentId && z.tb_RechargeCard.CardNumber.Contains(search)).ToList().OrderByDescending(z => z.AssignId).ToList().
                    Skip(count * index).Take(count).ToList().Select(z => new RechargeCard(z.tb_RechargeCard)).ToList().Count;
                }
            }
            else
            {
                return _context.tb_AssignCard.Where(z => z.UserId == agentId).ToList().OrderByDescending(z => z.AssignId).ToList().
                    Skip(count * index).Take(count).ToList().Select(z => new RechargeCard(z.tb_RechargeCard)).ToList().Count;
            }
        }

        public List<sp_fetch_rechargecards_for_export> getAllRechargeCards(string type)
        {
            //var assignedCardList = _context.tb_AssignCard.ToList().Select(z => z.CardId).ToList();
            //return _context.tb_RechargeCard.Where(z => !z.IsDeleted && !z.IsUsed && !assignedCardList.Contains(z.CardId)).ToList().OrderByDescending(z => z.CreatedAt).ToList().
            //       ToList().Select(z => new RechargeCard(z)).ToList();
            return _context.sp_fetch_rechargecards_for_export(type).ToList().Select(z => new sp_fetch_rechargecards_for_export(z)).ToList();
        }


        public List<ApiPriority> getAdminPrioritySettings()
        {
            return _context.tb_ApiPriority.Where(z => z.IsActive).ToList().Select(z => new ApiPriority(z)).ToList();
        }

        public List<sp_fetch_transaction> getUserTransaction(long userId, int index, string search, string fromDate, string endDate)
        {
            return _context.sp_fetch_transaction(userId, index, search, fromDate ?? string.Empty, endDate ?? string.Empty).ToList().
                Select(z => new sp_fetch_transaction(z)).ToList();
        }

        public List<sp_fetch_user_transaction> getUserTransactionWeb(long userId, int index, string listcount, string search)
        {
            int count = Convert.ToInt32(listcount);
            return _context.sp_fetch_user_transaction(userId, index, count, search).ToList().
                Select(z => new sp_fetch_user_transaction(z)).ToList();
        }

        public int getUserTransactionWebCount(long userId, int index, string listcount, string search)
        {
            int count = Convert.ToInt32(listcount);
            if (index < 0)
                index = 0;
            return _context.sp_fetch_user_transaction(userId, index, count, search).ToList().Count;
        }

        public List<AdminCurrencyRate> getAdminCurrencyRates()
        {
            return _context.tb_AdminCurrencyRate.Where(z => z.IsActive).ToList().Select(z => new AdminCurrencyRate(z)).ToList();
        }

        public List<string> getAdminCurrency()
        {
            return _context.tb_AdminCurrencyRate.Select(o => o.FromCurrency).Distinct().ToList();
        }

        public List<SP_FETCH_ALL_TRANSACTIONS> getallTransaction(int index, string listcount, string search, string startDate, string endDate, string currency, string filterType)
        {
            int count = Convert.ToInt32(listcount);
            var startDate1 = DateTime.UtcNow;
            var endDate1 = DateTime.UtcNow;
            var list = new List<SP_FETCH_ALL_TRANSACTIONS>();
            if (startDate != null && startDate != string.Empty && endDate != null && endDate != string.Empty)
            {
                var date1 = DateTime.ParseExact(startDate, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture)
                     .ToString("MM/dd/yyyy", System.Globalization.CultureInfo.InvariantCulture);

                var date2 = DateTime.ParseExact(endDate, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture)
                    .ToString("MM/dd/yyyy", System.Globalization.CultureInfo.InvariantCulture);

                startDate1 = Convert.ToDateTime(date1);
                endDate1 = Convert.ToDateTime(date2);

                list = _context.SP_FETCH_ALL_TRANSACTIONS(index, count, search, startDate1, endDate1, true, currency, filterType).ToList().
                Select(z => new SP_FETCH_ALL_TRANSACTIONS(z)).ToList();

            }
            else if (startDate != null && startDate != string.Empty && (endDate == null || endDate == string.Empty))
            {
                var date1 = DateTime.ParseExact(startDate, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture)
                        .ToString("MM/dd/yyyy", System.Globalization.CultureInfo.InvariantCulture);


                startDate1 = Convert.ToDateTime(date1);

                list = _context.SP_FETCH_ALL_TRANSACTIONS(index, count, search, startDate1, endDate1, true, currency, filterType).ToList().
                Select(z => new SP_FETCH_ALL_TRANSACTIONS(z)).ToList();
            }
            else if ((startDate == null || startDate == string.Empty) && endDate != null && endDate != string.Empty)
            {
                var date2 = DateTime.ParseExact(endDate, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture)
                    .ToString("MM/dd/yyyy", System.Globalization.CultureInfo.InvariantCulture);

                endDate1 = Convert.ToDateTime(date2);

                list = _context.SP_FETCH_ALL_TRANSACTIONS(index, count, search, startDate1, endDate1, true, currency, filterType).ToList().
                Select(z => new SP_FETCH_ALL_TRANSACTIONS(z)).ToList();
            }
            else
            {
                list = _context.SP_FETCH_ALL_TRANSACTIONS(index, count, search, startDate1, endDate1, false, currency, filterType).ToList().
                  Select(z => new SP_FETCH_ALL_TRANSACTIONS(z)).ToList();
            }

            return list;
        }

        public int getallTransactionCount(int index, string listcount, string search, string startDate, string endDate, string currency, string filterType)
        {
            int count = Convert.ToInt32(listcount);
            if (index < 0)
                index = 0;
            //return _context.SP_FETCH_ALL_TRANSACTIONS(index, count, search).ToList().Count;
            var startDate1 = DateTime.UtcNow;
            var endDate1 = DateTime.UtcNow;
            var list = new List<SP_FETCH_ALL_TRANSACTIONS>();
            if (startDate != null && startDate != string.Empty && endDate != null && endDate != string.Empty)
            {
                var date1 = DateTime.ParseExact(startDate, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture)
                     .ToString("MM/dd/yyyy", System.Globalization.CultureInfo.InvariantCulture);

                var date2 = DateTime.ParseExact(endDate, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture)
                    .ToString("MM/dd/yyyy", System.Globalization.CultureInfo.InvariantCulture);

                startDate1 = Convert.ToDateTime(date1);
                endDate1 = Convert.ToDateTime(date2);

                list = _context.SP_FETCH_ALL_TRANSACTIONS(index, count, search, startDate1, endDate1, true, currency, filterType).ToList().
                Select(z => new SP_FETCH_ALL_TRANSACTIONS(z)).ToList();

            }
            else if (startDate != null && startDate != string.Empty && (endDate == null || endDate == string.Empty))
            {
                var date1 = DateTime.ParseExact(startDate, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture)
                        .ToString("MM/dd/yyyy", System.Globalization.CultureInfo.InvariantCulture);


                startDate1 = Convert.ToDateTime(date1);

                list = _context.SP_FETCH_ALL_TRANSACTIONS(index, count, search, startDate1, endDate1, true, currency, filterType).ToList().
                Select(z => new SP_FETCH_ALL_TRANSACTIONS(z)).ToList();
            }
            else if ((startDate == null || startDate == string.Empty) && endDate != null && endDate != string.Empty)
            {
                var date2 = DateTime.ParseExact(endDate, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture)
                    .ToString("MM/dd/yyyy", System.Globalization.CultureInfo.InvariantCulture);

                endDate1 = Convert.ToDateTime(date2);

                list = _context.SP_FETCH_ALL_TRANSACTIONS(index, count, search, startDate1, endDate1, true, currency, filterType).ToList().
                Select(z => new SP_FETCH_ALL_TRANSACTIONS(z)).ToList();
            }
            else
            {
                list = _context.SP_FETCH_ALL_TRANSACTIONS(index, count, search, startDate1, endDate1, false, currency, filterType).ToList().
                  Select(z => new SP_FETCH_ALL_TRANSACTIONS(z)).ToList();
            }

            return list.Count;
        }

        public List<SP_FETCHALL_TRANSACTION_WITHOUTINDEX> getallTransactionwithoutIndex(string startDate, string endDate, bool fetchall, string currency, string filterType)
        {
            var startDate1 = DateTime.UtcNow;
            var endDate1 = DateTime.UtcNow;
            var list = new List<SP_FETCHALL_TRANSACTION_WITHOUTINDEX>();
            if (startDate != null && startDate != string.Empty && endDate != null && endDate != string.Empty)
            {
                var date1 = DateTime.ParseExact(startDate, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture)
                     .ToString("MM/dd/yyyy", System.Globalization.CultureInfo.InvariantCulture);

                var date2 = DateTime.ParseExact(endDate, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture)
                    .ToString("MM/dd/yyyy", System.Globalization.CultureInfo.InvariantCulture);

                startDate1 = Convert.ToDateTime(date1);
                endDate1 = Convert.ToDateTime(date2);

                list = _context.SP_FETCHALL_TRANSACTION_WITHOUTINDEX(startDate1, endDate1, fetchall, currency, filterType).ToList().
                Select(z => new SP_FETCHALL_TRANSACTION_WITHOUTINDEX(z)).ToList();

            }
            else if (startDate != null && startDate != string.Empty && (endDate == null || endDate == string.Empty))
            {
                var date1 = DateTime.ParseExact(startDate, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture)
                        .ToString("MM/dd/yyyy", System.Globalization.CultureInfo.InvariantCulture);


                startDate1 = Convert.ToDateTime(date1);

                list = _context.SP_FETCHALL_TRANSACTION_WITHOUTINDEX(startDate1, endDate1, fetchall, currency, filterType).ToList().
                Select(z => new SP_FETCHALL_TRANSACTION_WITHOUTINDEX(z)).ToList();
            }
            else if ((startDate == null || startDate == string.Empty) && endDate != null && endDate != string.Empty)
            {
                var date2 = DateTime.ParseExact(endDate, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture)
                    .ToString("MM/dd/yyyy", System.Globalization.CultureInfo.InvariantCulture);

                endDate1 = Convert.ToDateTime(date2);

                list = _context.SP_FETCHALL_TRANSACTION_WITHOUTINDEX(startDate1, endDate1, fetchall, currency, filterType).ToList().
                Select(z => new SP_FETCHALL_TRANSACTION_WITHOUTINDEX(z)).ToList();
            }
            else
            {
                list = _context.SP_FETCHALL_TRANSACTION_WITHOUTINDEX(startDate1, endDate1, fetchall, currency, filterType).ToList().
                  Select(z => new SP_FETCHALL_TRANSACTION_WITHOUTINDEX(z)).ToList();
            }

            return list;
        }

        public List<string> getAgentCurrency()
        {
            return _context.tb_User.Where(z => z.IsActive && z.UserType == (int)ClassLibrary.Enum.userType.Agent && z.Currency != null && z.Currency != string.Empty).ToList().Select(z => z.Currency).Distinct().ToList();
        }


        public decimal getWalletSum(string currency)
        {
            return _context.tb_User.Where(z => z.IsActive && z.UserType == (int)ClassLibrary.Enum.userType.Agent && z.Currency == currency).ToList()
                .Select(z => z.Wallet).ToList().Sum();
        }
        public List<SP_FETCH_CREDITHISTORY_LIST> GetCreditHistoryData(string index, string listcount, string search)
        {
            var list = _context.SP_FETCH_CREDITHISTORY_LIST(0, Convert.ToInt32(index), Convert.ToInt32(listcount), search).ToList()
                .Select(z => new SP_FETCH_CREDITHISTORY_LIST(z)).ToList();
            return list;
        }
        public int GetCreditHistoryDataCount(string index, string listcount, string search)
        {
            var INDEX = Convert.ToInt32(index);
            if (INDEX < 0)
                INDEX = 0;
            var list = _context.SP_FETCH_CREDITHISTORY_LIST(0, Convert.ToInt32(INDEX), Convert.ToInt32(listcount), search).ToList()
                .Select(z => new SP_FETCH_CREDITHISTORY_LIST(z)).ToList();
            return list.Count;
        }


        public List<SP_FETCH_TOTAL_PRICE_AND_COMMISSION_AGENT> getTotalDeductedAmountAndCommission(long agentId)
        {
            return _context.SP_FETCH_TOTAL_PRICE_AND_COMMISSION_AGENT(agentId).ToList()
                .Select(z => new SP_FETCH_TOTAL_PRICE_AND_COMMISSION_AGENT(z)).ToList();
        }


        public List<SP_FETCH_CREDITHISTORY_LIST> GetAgentCreditHistoryData(long agentId, string index, string listcount, string search)
        {
            var list = _context.SP_FETCH_CREDITHISTORY_LIST(agentId, Convert.ToInt32(index), Convert.ToInt32(listcount), search).ToList()
                .Select(z => new SP_FETCH_CREDITHISTORY_LIST(z)).ToList();
            return list;
        }
        public int GetAgentCreditHistoryDataCount(long agentId, string index, string listcount, string search)
        {
            var INDEX = Convert.ToInt32(index);
            if (INDEX < 0)
                INDEX = 0;
            var list = _context.SP_FETCH_CREDITHISTORY_LIST(agentId, Convert.ToInt32(INDEX), Convert.ToInt32(listcount), search).ToList()
                .Select(z => new SP_FETCH_CREDITHISTORY_LIST(z)).ToList();
            return list.Count;
        }


        public System.Collections.Generic.List<EntityLibrary.tAppVersion> getAppVersions()
        {
            var list = _context.tAppVersions.ToList();
            return list;
        }


        //Distributor



        public List<Distributor> getDistributors(int index, string searchtext, string listcount, string userCurrency)
        {
            int count = Convert.ToInt32(listcount);
            if (searchtext != null && searchtext != string.Empty)
            {
                string search = StringCipher.Encrypt(searchtext, salt);
                if (userCurrency == "All")
                {
                    return _context.tDistributors.Where(z => z.IsActive && z.UserType == (int)ClassLibrary.Enum.userType.Distributor && (z.Username.Contains(search) || z.Mobile.Contains(search)))
                        .ToList().OrderByDescending(z => z.Timestamp).ToList().
                        Skip(count * index).Take(count).ToList().Select(z => new Distributor(z)).ToList();
                }
                else
                {
                    return _context.tDistributors.Where(z => z.IsActive && z.Currency == userCurrency && z.UserType == (int)ClassLibrary.Enum.userType.Distributor && (z.Username.Contains(search) || z.Mobile.Contains(search)))
                           .ToList().OrderByDescending(z => z.Timestamp).ToList().
                           Skip(count * index).Take(count).ToList().Select(z => new Distributor(z)).ToList();
                }
            }
            else
            {
                if (userCurrency == "All")
                {
                    return _context.tDistributors.Where(z => z.IsActive && z.UserType == (int)ClassLibrary.Enum.userType.Distributor)
                        .ToList().OrderByDescending(z => z.Timestamp).ToList().
                        Skip(count * index).Take(count).ToList().Select(z => new Distributor(z)).ToList();
                }
                else
                {
                    return _context.tDistributors.Where(z => z.IsActive && z.Currency == userCurrency && z.UserType == (int)ClassLibrary.Enum.userType.Distributor)
                          .ToList().OrderByDescending(z => z.Timestamp).ToList().
                          Skip(count * index).Take(count).ToList().Select(z => new Distributor(z)).ToList();
                }
            }
        }
        public int getDistributorsCount(int index, string searchtext, string listcount, string userCurrency)
        {
            int count = Convert.ToInt32(listcount);
            if (searchtext != null && searchtext != string.Empty)
            {
                string search = StringCipher.Encrypt(searchtext, salt);
                if (userCurrency == "All")
                {
                    return _context.tDistributors.Where(z => z.IsActive && z.UserType == (int)ClassLibrary.Enum.userType.Distributor && (z.Username.Contains(search) || z.Mobile.Contains(search)))
                    .ToList().OrderByDescending(z => z.Timestamp).ToList().
                    Skip(count * index).Take(count).ToList().Count;
                }
                else
                {
                    return _context.tDistributors.Where(z => z.IsActive && z.Currency == userCurrency && z.UserType == (int)ClassLibrary.Enum.userType.Distributor && (z.Username.Contains(search) || z.Mobile.Contains(search)))
                        .ToList().OrderByDescending(z => z.Timestamp).ToList().
                        Skip(count * index).Take(count).ToList().Count;
                }
            }
            else
            {
                if (userCurrency == "All")
                {
                    return _context.tDistributors.Where(z => z.IsActive && z.UserType == (int)ClassLibrary.Enum.userType.Distributor).ToList().OrderByDescending(z => z.Timestamp).ToList().
                 Skip(count * index).Take(count).ToList().Count;
                }
                else
                {
                    return _context.tDistributors.Where(z => z.IsActive && z.Currency == userCurrency && z.UserType == (int)ClassLibrary.Enum.userType.Distributor).ToList().OrderByDescending(z => z.Timestamp).ToList().
                    Skip(count * index).Take(count).ToList().Count;
                }
            }
        }


        public List<SP_FETCH_DISTRIBUTOR_CREDITHISTORY_LIST> GetDistributorCreditHistoryData(long distributorId, string index, string listcount, string search, string fromdate, string todate)
        {
            var list = _context.SP_FETCH_DISTRIBUTOR_CREDITHISTORY_LIST(distributorId, Convert.ToInt32(index), Convert.ToInt32(listcount), search, fromdate, todate).ToList()
                .Select(z => new SP_FETCH_DISTRIBUTOR_CREDITHISTORY_LIST(z)).ToList();
            return list;
        }
        public int GetDistributorCreditHistoryDataCount(long distributorId, string index, string listcount, string search, string fromdate, string todate)
        {
            var INDEX = Convert.ToInt32(index);
            if (INDEX < 0)
                INDEX = 0;
            var list = _context.SP_FETCH_DISTRIBUTOR_CREDITHISTORY_LIST(distributorId, Convert.ToInt32(INDEX), Convert.ToInt32(listcount), search, fromdate, todate).ToList()
                .Select(z => new SP_FETCH_DISTRIBUTOR_CREDITHISTORY_LIST(z)).ToList();
            return list.Count;
        }


        //Reseller

        public List<Reseller> getResellers(int index, string searchtext, string listcount, string userCurrency ,long DistributerID)
        {
            int count = Convert.ToInt32(listcount);
            if (searchtext != null && searchtext != string.Empty)
            {
                string search = StringCipher.Encrypt(searchtext, salt);
                if (userCurrency == "All")
                {
                    return _context.tResellers.Where(z => z.IsActive && z.DistributorId == DistributerID && z.UserType == (int)ClassLibrary.Enum.userType.Reseller && (z.Username.Contains(search) || z.Mobile.Contains(search)))
                        .ToList().OrderByDescending(z => z.Timestamp).ToList().
                        Skip(count * index).Take(count).ToList().Select(z => new Reseller(z)).ToList();
                }
                else
                {
                    return _context.tResellers.Where(z => z.IsActive && z.DistributorId == DistributerID && z.Currency == userCurrency && z.UserType == (int)ClassLibrary.Enum.userType.Reseller && (z.Username.Contains(search) || z.Mobile.Contains(search)))
                           .ToList().OrderByDescending(z => z.Timestamp).ToList().
                           Skip(count * index).Take(count).ToList().Select(z => new Reseller(z)).ToList();
                }
            }
            else
            {
                if (userCurrency == "All")
                {
                    return _context.tResellers.Where(z => z.IsActive && z.DistributorId == DistributerID && z.UserType == (int)ClassLibrary.Enum.userType.Reseller)
                        .ToList().OrderByDescending(z => z.Timestamp).ToList().
                        Skip(count * index).Take(count).ToList().Select(z => new Reseller(z)).ToList();
                }
                else
                {
                    return _context.tResellers.Where(z => z.IsActive && z.DistributorId == DistributerID && z.Currency == userCurrency && z.UserType == (int)ClassLibrary.Enum.userType.Reseller)
                          .ToList().OrderByDescending(z => z.Timestamp).ToList().
                          Skip(count * index).Take(count).ToList().Select(z => new Reseller(z)).ToList();
                }
            }
        }
        public int getResellersCount(int index, string searchtext, string listcount, string userCurrency)
        {
            int count = Convert.ToInt32(listcount);
            if (searchtext != null && searchtext != string.Empty)
            {
                string search = StringCipher.Encrypt(searchtext, salt);
                if (userCurrency == "All")
                {
                    return _context.tResellers.Where(z => z.IsActive && z.UserType == (int)ClassLibrary.Enum.userType.Reseller && (z.Username.Contains(search) || z.Mobile.Contains(search)))
                    .ToList().OrderByDescending(z => z.Timestamp).ToList().
                    Skip(count * index).Take(count).ToList().Count;
                }
                else
                {
                    return _context.tResellers.Where(z => z.IsActive && z.Currency == userCurrency && z.UserType == (int)ClassLibrary.Enum.userType.Reseller && (z.Username.Contains(search) || z.Mobile.Contains(search)))
                        .ToList().OrderByDescending(z => z.Timestamp).ToList().
                        Skip(count * index).Take(count).ToList().Count;
                }
            }
            else
            {
                if (userCurrency == "All")
                {
                    return _context.tResellers.Where(z => z.IsActive && z.UserType == (int)ClassLibrary.Enum.userType.Reseller).ToList().OrderByDescending(z => z.Timestamp).ToList().
                 Skip(count * index).Take(count).ToList().Count;
                }
                else
                {
                    return _context.tResellers.Where(z => z.IsActive && z.Currency == userCurrency && z.UserType == (int)ClassLibrary.Enum.userType.Reseller).ToList().OrderByDescending(z => z.Timestamp).ToList().
                    Skip(count * index).Take(count).ToList().Count;
                }
            }
        }

        public List<SP_FETCH_RESELLER_CREDITHISTORY_LIST> GetResellerCreditHistoryData(long resellerId, string index, string listcount, string search, string fromdate, string todate)
        {
            var list = _context.SP_FETCH_RESELLER_CREDITHISTORY_LIST(resellerId, Convert.ToInt32(index), Convert.ToInt32(listcount), search, fromdate, todate).ToList()
                .Select(z => new SP_FETCH_RESELLER_CREDITHISTORY_LIST(z)).ToList();
            return list;
        }
        public int GetResellerCreditHistoryDataCount(long resellerId, string index, string listcount, string search, string fromdate, string todate)
        {
            var INDEX = Convert.ToInt32(index);
            if (INDEX < 0)
                INDEX = 0;
            var list = _context.SP_FETCH_RESELLER_CREDITHISTORY_LIST(resellerId, Convert.ToInt32(INDEX), Convert.ToInt32(listcount), search, fromdate, todate).ToList()
                .Select(z => new SP_FETCH_RESELLER_CREDITHISTORY_LIST(z)).ToList();
            return list.Count;
        }
        
        //Agent New 19/04/2019.........Add Sibi
        public List<Agent_New> getAgents_NewAdd(int index, string searchtext, string listcount, string userCurrency , long resellerId)
        {
            int count = Convert.ToInt32(listcount);
            if (searchtext != null && searchtext != string.Empty)
            {
                string search = StringCipher.Encrypt(searchtext, salt);
                if (userCurrency == "All")
                {
                    return _context.tAgents.Where(z => z.IsActive && z.ResellerId == resellerId && z.UserType == (int)ClassLibrary.Enum.userType.Reseller && (z.Username.Contains(search) || z.Mobile.Contains(search)))
                        .ToList().OrderByDescending(z => z.Timestamp).ToList().
                        Skip(count * index).Take(count).ToList().Select(z => new Agent_New(z)).ToList();
                }
                else
                {
                    return _context.tAgents.Where(z => z.IsActive && z.ResellerId == resellerId && z.Currency == userCurrency && z.UserType == (int)ClassLibrary.Enum.userType.Reseller && (z.Username.Contains(search) || z.Mobile.Contains(search)))
                           .ToList().OrderByDescending(z => z.Timestamp).ToList().
                           Skip(count * index).Take(count).ToList().Select(z => new Agent_New(z)).ToList();
                }
            }
            else
            {
                if (userCurrency == "All")
                {
                    return _context.tAgents.Where(z => z.IsActive && z.UserType == (int)ClassLibrary.Enum.userType.Reseller && z.ResellerId == resellerId)
                        .ToList().OrderByDescending(z => z.Timestamp).ToList().
                        Skip(count * index).Take(count).ToList().Select(z => new Agent_New(z)).ToList();
                }
                else
                {
                    return _context.tAgents.Where(z => z.IsActive && z.ResellerId == resellerId && z.Currency == userCurrency && z.UserType == (int)ClassLibrary.Enum.userType.Reseller)
                          .ToList().OrderByDescending(z => z.Timestamp).ToList().
                          Skip(count * index).Take(count).ToList().Select(z => new Agent_New(z)).ToList();
                }
            }
        }

        public int getAgentCount(int index, string searchtext, string listcount, string userCurrency, long resellerId)
        {
            int count = Convert.ToInt32(listcount);
            if (searchtext != null && searchtext != string.Empty)
            {
                string search = StringCipher.Encrypt(searchtext, salt);
                if (userCurrency == "All")
                {
                    return _context.tAgents.Where(z => z.IsActive && z.ResellerId == resellerId && z.UserType == (int)ClassLibrary.Enum.userTypeNew.Agent && (z.Username.Contains(search) || z.Mobile.Contains(search)))
                    .ToList().OrderByDescending(z => z.Timestamp).ToList().
                    Skip(count * index).Take(count).ToList().Count;
                }
                else
                {
                    return _context.tAgents.Where(z => z.IsActive && z.ResellerId == resellerId && z.Currency == userCurrency && z.UserType == (int)ClassLibrary.Enum.userTypeNew.Agent && (z.Username.Contains(search) || z.Mobile.Contains(search)))
                        .ToList().OrderByDescending(z => z.Timestamp).ToList().
                        Skip(count * index).Take(count).ToList().Count;
                }
            }
            else
            {
                if (userCurrency == "All")
                {
                    return _context.tAgents.Where(z => z.IsActive && z.ResellerId == resellerId && z.UserType == (int)ClassLibrary.Enum.userTypeNew.Agent).ToList().OrderByDescending(z => z.Timestamp).ToList().
                 Skip(count * index).Take(count).ToList().Count;
                }
                else
                {
                    return _context.tAgents.Where(z => z.IsActive && z.ResellerId == resellerId && z.Currency == userCurrency && z.UserType == (int)ClassLibrary.Enum.userTypeNew.Agent).ToList().OrderByDescending(z => z.Timestamp).ToList().
                    Skip(count * index).Take(count).ToList().Count;
                }
            }
        }



    }
}
