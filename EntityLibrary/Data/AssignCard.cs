﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityLibrary.Data
{
    public class AssignCard : BaseReference
    {
        private tb_AssignCard card;
        public AssignCard(tb_AssignCard obj) { card = obj; }
        public AssignCard(long id) { card = _context.tb_AssignCard.FirstOrDefault(z => z.AssignId == id); }
        public long AssignId { get { return card.AssignId; } }
        public long UserId { get { return card.UserId; } }
        public long CardId { get { return card.CardId; } }
        public System.DateTime Timestamp { get { return card.Timestamp; } }
        public RechargeCard CardDtails { get { return new RechargeCard(CardId); } }
    }
}
