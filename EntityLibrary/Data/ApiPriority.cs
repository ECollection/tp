﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityLibrary.Data
{
    public class ApiPriority : BaseReference
    {
        private tb_ApiPriority priority;
        public ApiPriority(tb_ApiPriority obj) { priority = obj; }
        public ApiPriority(long id) { priority = _context.tb_ApiPriority.FirstOrDefault(z => z.Id == id); }
        public long Id { get { return priority.Id; } }
        public int ServiceArea { get { return priority.ServiceArea; } }
        public int ServiceType { get { return priority.ServiceType; } }
        public int ApiProvider { get { return priority.ApiProvider; } }
        public int Priority { get { return priority.Priority; } }
        public bool IsActive { get { return priority.IsActive; } }
    }
}
