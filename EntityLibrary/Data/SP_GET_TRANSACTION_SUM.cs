﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityLibrary.Data
{
    public class SP_GET_TRANSACTION_SUM
    {
        private SP_GET_TRANSACTION_SUM_Result res;
        public SP_GET_TRANSACTION_SUM(SP_GET_TRANSACTION_SUM_Result obj) { res = obj; }
        public string transactionid { get { return res.transactionid; } }
        public string transactionstate { get { return res.transactionstate; } }
        public System.DateTime transactiontimeUTC { get { return res.transactiontime; } }
        public System.DateTime transactiontime { get { return uaeTime(transactiontimeUTC); } }
        public string rechargenumber { get { return res.rechargenumber; } }
        public decimal amount { get { return res.amount ?? 0; } }
        public Nullable<int> type { get { return res.type; } }
        public Nullable<int> apiprovider { get { return res.apiprovider; } }
        public string currency { get { return res.currency; } }
        public string ProductType { get { return res.ProductType; } }
        public decimal amountdeducted { get { return res.amountdeducted ?? 0; } }
        public string amountcurrency { get { return res.amountcurrency; } }
        public string amountdeductedcurrency { get { return res.amountdeductedcurrency; } }
        public DateTime uaeTime(DateTime utcTime)
        {
            try
            {
                TimeZoneInfo cstZone = TimeZoneInfo.FindSystemTimeZoneById("Gulf Standard Time");
                DateTime cstTime = TimeZoneInfo.ConvertTimeFromUtc(utcTime, cstZone);
                //Console.WriteLine("The date and time are {0} {1}.",
                //                  cstTime,
                //                  cstZone.IsDaylightSavingTime(cstTime) ?
                //                          cstZone.DaylightName : cstZone.StandardName);
                return cstTime;
            }
            catch (TimeZoneNotFoundException)
            {
                return utcTime;
                //Console.WriteLine("The registry does not define the Central Standard Time zone.");
            }
            catch (InvalidTimeZoneException)
            {
                return utcTime;
                //Console.WriteLine("Registry data on the Central Standard Time zone has been corrupted.");
            }
        }
    }
}
