﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityLibrary.Data
{
    public class Distributor : BaseReference
    {
        private tDistributor distributor;
        public Distributor(tDistributor obj) { distributor = obj; }
        public Distributor(long id) { distributor = _context.tDistributors.FirstOrDefault(z => z.DistributorID == id); }
        public long DistributorID { get { return distributor.DistributorID; } }
        public System.Guid DistributorGuid { get { return distributor.DistributorGuid; } }
        public string Username { get { return StringCipher.Decrypt(distributor.Username, salt); } }
        public string Country { get { return distributor.Country; } }
        public string CountryCode { get { return distributor.CountryCode; } }
        public string Mobile { get { return StringCipher.Decrypt(distributor.Mobile, salt); } }
        public string Password { get { return StringCipher.Decrypt(distributor.Password, salt); } }
        public bool IsActive { get { return distributor.IsActive; } }
        public System.DateTime Timestamp { get { return distributor.Timestamp; } }
        public string ProfileImage { get { return distributor.ProfileImage; } }
        public string Place { get { return distributor.Place; } }
        public string Address { get { return distributor.Address; } }
        public string ShopName { get { return distributor.ShopName; } }
        public int UserType { get { return distributor.UserType; } }
        public decimal Wallet { get { return distributor.Wallet; } }
        public string Currency { get { return distributor.Currency; } }
        public bool IsBlocked { get { return distributor.IsBlocked; } }
        public string Email { get { return StringCipher.Decrypt(distributor.Email, salt); } }
        public System.DateTime LastUpdated { get { return distributor.LastUpdated; } }
        public List<DistributorCommisionRate> commissions
        {
            get
            {
                return distributor.tDistributorCommisionRates.Where(z => z.IsActive).ToList().Select(z => new DistributorCommisionRate(z)).ToList();
            }
        }
        public List<DistributorPermissions> permissions
        {
            get
            {
                return distributor.tDistributorPermissions.ToList().Select(z => new DistributorPermissions(z)).ToList();
            }
        }

        public decimal CreditTotal
        {
            get
            {
                try
                {
                    return Convert.ToDecimal(_context.SP_DISTRIBUTOR_CREDIT_SUM(distributor.DistributorID, string.Empty, string.Empty).FirstOrDefault());
                }
                catch (Exception ex)
                {
                    return 0;
                }
            }
        }


    }
}
