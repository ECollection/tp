﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityLibrary.Data
{
    public class ResellerPermissions : BaseReference
    {
        private tResellerPermission permission;
        public ResellerPermissions(tResellerPermission obj) { permission = obj; }
        public ResellerPermissions(long id) { permission = _context.tResellerPermissions.FirstOrDefault(z => z.Id == id); }
        public long Id { get { return permission.Id; } }
        public long ResellerId { get { return permission.ResellerId; } }
        public string Type { get { return permission.Type; } }
        public bool Permissions { get { return permission.Permissions; } }
        public System.DateTime Timestamp { get { return permission.Timestamp; } }
    }
}
