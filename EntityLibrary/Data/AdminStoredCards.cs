﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityLibrary.Data
{
    public class AdminStoredCards : BaseReference
    {
        private tb_AdminStoredCard cards;
        public AdminStoredCards(tb_AdminStoredCard card) { cards = card; }
        public AdminStoredCards(long id) { cards = _context.tb_AdminStoredCard.FirstOrDefault(z => z.Id == id); }
        public long Id { get { return cards.Id; } }
        public string SerialNumber { get { return cards.SerialNumber; } }
        public string PinNumber { get { return cards.PinNumber; } }
        public string CardType { get { return cards.CardType; } }
        public string Amount { get { return cards.Amount; } }
        public bool IsDeleted { get { return cards.IsDeleted; } }
        public System.DateTime Timestamp { get { return cards.Timestamp; } }
        public bool IsDuplicate { get { return cards.IsDuplicate; } }
        public bool IsActive { get { return cards.IsActive; } }
    }
}
