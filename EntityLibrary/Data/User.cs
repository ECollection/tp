﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityLibrary.Data
{
    public class User : BaseReference
    {
        private tb_User user;
        public User(tb_User obj) { user = obj; }
        public User(long id) { user = _context.tb_User.FirstOrDefault(z => z.UserID == id); }
        public long UserID { get { return user.UserID; } }
        public System.Guid UserGuid { get { return user.UserGuid; } }
        public string Username { get { return StringCipher.Decrypt(user.Username, salt); } }
        //public string Username { get { return user.Username; } }
        public string Country { get { return user.Country; } }
        //public string Mobile { get { return user.Mobile; } }
        public string Mobile { get { return StringCipher.Decrypt(user.Mobile, salt); } }
        public string Password { get { return StringCipher.Decrypt(user.Password, salt); } }
        //public string Password { get { return user.Password; } }
        public bool IsActive { get { return user.IsActive; } }
        public System.DateTime Timestamp { get { return user.Timestamp; } }
        public string ProfileImage { get { return user.ProfileImage; } }
        public string CountryCode { get { return user.CountryCode; } }
        public string Place { get { return user.Place; } }
        public string Address { get { return user.Address; } }
        public string ShopName { get { return user.ShopName; } }
        public int UserType { get { return user.UserType; } }
        public decimal Wallet { get { return user.Wallet; } }
        public string Currency { get { return user.Currency; } }
        public bool IsBlocked { get { return user.IsBlocked; } }
        public List<AgentCommissionRate> commissions
        {
            get
            {
                return user.tb_AgentCommisionRate.Where(z => z.IsActive).ToList().Select(z => new AgentCommissionRate(z)).ToList();
            }
        }

    }
}
