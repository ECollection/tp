﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityLibrary.Data
{
    public class Agent_New : BaseReference
    {
        private tAgent agent;
        public Agent_New(tAgent obj) { agent = obj; }
        public Agent_New(long id) { agent = _context.tAgents.FirstOrDefault(z => z.AgentID == id); }
        public long AgentID { get {return agent.AgentID ;} }
        public long ResellerId { get { return agent.ResellerId; } }
        public long DistributorId { get { return agent.DistributorId; } }
        public System.Guid AgentGuid { get { return agent.AgentGuid; } }
        public string Email { get { return StringCipher.Decrypt(agent.Email, salt); } }
        public string Username { get { return StringCipher.Decrypt(agent.Username, salt); } }
        public string Country { get { return agent.Country; } }
        public string CountryCode { get { return agent.CountryCode; } }
        public string Mobile { get { return StringCipher.Decrypt(agent.Mobile, salt); } }
        public string Password { get { return StringCipher.Decrypt(agent.Password, salt); } }
        public bool IsActive { get { return agent.IsActive; } }
        public System.DateTime Timestamp { get { return agent.Timestamp; } }
        public string ProfileImage { get { return agent.ProfileImage; } }
        public string Place { get { return agent.Place; } }
        public string Address { get { return agent.Address; } }
        public string ShopName { get { return agent.ShopName; } }
        public int UserType { get { return agent.UserType; } }
        public decimal Wallet { get { return agent.Wallet; } }
        public string Currency { get { return agent.Currency; } }
        public bool IsBlocked { get { return agent.IsBlocked; } }
        public System.DateTime LastUpdated { get { return agent.LastUpdated; } }

        public List<AgentCommisionRate> commissions
        {
            get
            {
                return agent.tAgentCommisionRates.Where(z => z.IsActive).ToList().Select(z => new AgentCommisionRate(z)).ToList();
            }
        }
        public List<AgentPermissions> permissions
        {
            get
            {
                return agent.tAgentPermissions.ToList().Select(z => new AgentPermissions(z)).ToList();
            }
        }

        public decimal CreditTotal
        {
            get
            {
                try
                {
                    return Convert.ToDecimal(_context.SP_AGENT_CREDIT_SUM(agent.AgentID, string.Empty, string.Empty).FirstOrDefault());
                }
                catch (Exception ex)
                {
                    return 0;
                }
            }
        }
    }
}
