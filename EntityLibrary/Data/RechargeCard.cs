﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityLibrary.Data
{
    public class RechargeCard : BaseReference
    {
        private tb_RechargeCard card;
        public RechargeCard(tb_RechargeCard obj) { card = obj; }
        public RechargeCard(long id) { card = _context.tb_RechargeCard.FirstOrDefault(z => z.CardId == id); }
        public long CardId { get { return card.CardId; } }
        public System.Guid CardGuid { get { return card.CardGuid; } }
        public string CardNumber { get { return card.CardNumber; } }
        public decimal Amount { get { return card.Amount; } }
        public bool IsDeleted { get { return card.IsDeleted; } }
        public bool IsUsed { get { return card.IsUsed; } }
        public bool IsDeactivated { get { return card.IsDeactivated; } }
        public System.DateTime CreatedAt { get { return card.CreatedAt; } }
        public string Currency { get { return card.Currency; } }
        public RechargeCardHistory History
        {
            get
            {
                var data = _context.tb_RechargeCardHistory.Where(z => z.CardId == CardId).FirstOrDefault();
                if (data != null)
                {
                    return new RechargeCardHistory(data);
                }
                else
                {
                    return null;
                }
            }
        }
    }
}
