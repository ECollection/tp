﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityLibrary.Data
{
    public class AdminCurrencyRate : BaseReference
    {
        private tb_AdminCurrencyRate rate;
        public AdminCurrencyRate(tb_AdminCurrencyRate obj) { rate = obj; }
        public AdminCurrencyRate(long id) { rate = _context.tb_AdminCurrencyRate.FirstOrDefault(z => z.Id == id); }
        public long Id { get { return rate.Id; } }
        public string FromCountry { get { return rate.FromCountry; } }
        public string FromCountryCode { get { return rate.FromCountryCode; } }
        public string FromCurrency { get { return rate.FromCurrency; } }
        public string ToCountry { get { return rate.ToCountry; } }
        public string ToCountryCode { get { return rate.ToCountryCode; } }
        public string ToCurrency { get { return rate.ToCurrency; } }
        public double CurrencyRate { get { return rate.CurrencyRate; } }
        public bool IsActive { get { return rate.IsActive; } }
    }
}
