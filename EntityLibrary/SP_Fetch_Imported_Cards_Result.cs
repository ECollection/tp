//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace EntityLibrary
{
    using System;
    
    public partial class SP_Fetch_Imported_Cards_Result
    {
        public long Id { get; set; }
        public string SerialNumber { get; set; }
        public string PinNumber { get; set; }
        public string CardType { get; set; }
        public string Amount { get; set; }
        public System.DateTime Timestamp { get; set; }
        public bool IsDuplicate { get; set; }
    }
}
