//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace EntityLibrary
{
    using System;
    
    public partial class SP_FETCH_ALL_TRANSACTIONS_Result
    {
        public string transactionid { get; set; }
        public string transactionstate { get; set; }
        public System.DateTime transactiontime { get; set; }
        public string rechargenumber { get; set; }
        public Nullable<decimal> amount { get; set; }
        public Nullable<int> type { get; set; }
        public Nullable<int> apiprovider { get; set; }
        public string currency { get; set; }
        public string ProductType { get; set; }
        public Nullable<decimal> amountdeducted { get; set; }
        public string amountcurrency { get; set; }
        public string amountdeductedcurrency { get; set; }
        public long USERID { get; set; }
        public string USERNAME { get; set; }
        public int USERTYPE { get; set; }
    }
}
