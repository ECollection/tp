﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary.ServicePostModel
{
    public class MagicEasyPostModel
    {
        public class MagicEasy_Get_Country_Response
        {
            public bool status { get; set; }
            public string message { get; set; }
            public dynamic data { get; set; }
        }
        public class MagicEasy_AuthData
        {
            public string clientid { get; set; }
            public string apikey { get; set; }
        }
        public class MagicEasy_Response
        {
            public bool status { get; set; }
            public string message { get; set; }
            public dynamic data { get; set; }
        }
        public class Get_Opt_list
        {
            public string ccode { get; set; }
        }

        public class Get_Oprator
        {
            public string ccode { get; set; }
            public string mobile { get; set; }
        }

        public class Get_Opt_offer
        {
            public string ccode { get; set; }
            public string mobile { get; set; }
            public string optr { get; set; }
        }
    }
}
