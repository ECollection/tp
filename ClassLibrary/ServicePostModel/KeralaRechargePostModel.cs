﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary.ServicePostModel
{
    public class KeralaRechargePostModel
    {
        public class FindOperatorAndCircle
        {
            public string mobileNumber { get; set; }
            public long userId { get; set; }
        }
        public class FindOperatorAndCircleReturnModel
        {
            public bool status { get; set; }
            public string message { get; set; }
            public dynamic data { get; set; }
        }


        public class RechargePlans
        {
            public string Operator { get; set; }
            public int circle { get; set; }
            public long userId { get; set; }
        }

        public class RechargePlansResponse
        {
            public bool status { get; set; }
            public string message { get; set; }
            public dynamic data { get; set; }
        }

        public class Recharge
        {
            public string mobileNumber { get; set; }
            public int amount { get; set; }
            public string Operator { get; set; }
            public int circle { get; set; }
            public int std { get; set; }
            public int ca { get; set; }
            public int type { get; set; }
            public long userId { get; set; }
            public int productType { get; set; }
            public string amountCurrency { get; set; }
            public string ip { get; set; }
        }

        public class RechargeResponse
        {
            public bool status { get; set; }
            public string message { get; set; }
            public dynamic data { get; set; }
            public string wallet { get; set; }
        }


        public class KsebBillPayment
        {
            public string section { get; set; }
            public int consumerNumber { get; set; }
            public int billNumber { get; set; }
            public int amount { get; set; }
            public int customerMobile { get; set; }
            public string customerName { get; set; }
            public long userId { get; set; }
            public int productType { get; set; }
            public string amountCurrency { get; set; }
            public string ip { get; set; }
        }
        public class KsebBillPaymentResponse
        {
            public bool status { get; set; }
            public string message { get; set; }
            public dynamic data { get; set; }
            public string wallet { get; set; }
        }
    }
}
