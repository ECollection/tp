﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary.ServicePostModel
{
    public class MobileApPostModel
    {
        public class AccessTokenPostModel
        {
            public string username { get; set; }
            public string password { get; set; }
        }

        public class AccessTokenResponse
        {
            public string token_type { get; set; }
            public int expires_in { get; set; }
            public string access_token { get; set; }
            public string refresh_token { get; set; }
        }

        public class ProviderResponse
        {
            public bool status { get; set; }
            public string message { get; set; }
            public dynamic data { get; set; }
        }
        public class CircleResponse
        {
            public bool status { get; set; }
            public string message { get; set; }
            public dynamic data { get; set; }
        }
        public class PlansPostModel
        {
            public int provider_id { get; set; }
            public int circle_id { get; set; }
        }
        public class PlansResponseModel
        {
            public bool status { get; set; }
            public string message { get; set; }
            public dynamic data { get; set; }
        }

        public class PlansDthModel
        {
            public int provider_id { get; set; }
            public string plan_type { get; set; }
        }
        public class PlansDthResponseModel
        {
            public bool status { get; set; }
            public string message { get; set; }
            public dynamic data { get; set; }
        }

        public class Roffer
        {
            public int provider_id { get; set; }
            public int number { get; set; }
        
        }
        public class RofferResponseModel
        {
            public bool status { get; set; }
            public string message { get; set; }
            public dynamic data { get; set; }
        }
    }
}
