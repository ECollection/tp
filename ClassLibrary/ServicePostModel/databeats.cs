﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary.ServicePostModel
{
    public class databeats
    {
        public class MNP
        {
            public string mobile { get; set; }
        }
        public class MNPReturnModel
        {
            public bool status { get; set; }
            public string message { get; set; }
            public MNPResponse.Success Success { get; set; }
            public MNPResponse.Failure Failure { get; set; }
        }
        public class MNPResponse
        {
            public class Success
            {
                public int OperatorId { get; set; }
                public string Operator { get; set; }
                public string Circle { get; set; }
                public int CircleId { get; set; }
            }

            public class Failure
            {
                public string status { get; set; }
            }
        }


        public class MobileOffers
        {
            public class MobileOffersRequest
            {
                public string operators { get; set; }
                public string circle { get; set; }
                public string type { get; set; }
            }


            public class MobileOffersReturnModel
            {
                public bool status { get; set; }
                public string message { get; set; }
                public List<MobileOfferApiResponse.Success> Success { get; set; }
                public MobileOfferApiResponse.Failure Failure { get; set; }
            }

            public class MobileOfferApiResponse
            {
                public class Success
                {
                    public string ID { get; set; }
                    public string Operator { get; set; }
                    public string Circle { get; set; }
                    public string Value { get; set; }
                    public string TalkTime { get; set; }
                    public string Validity { get; set; }
                    public string Type { get; set; }
                    public string Recommended { get; set; }
                    public string Special { get; set; }
                    public string Date { get; set; }
                    public string Description { get; set; }
                }
                public class Failure
                {
                    public int errorCode { get; set; }
                    public string error { get; set; }
                }
            }


        }


    }
}
