﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace ClassLibrary.ServicePostModel
{
    public class ProfileEditModel
    {
        public long userId { get; set; }
        public HttpPostedFile profileImage { get; set; }
        public string mobileNumber { get; set; }
        public string username { get; set; }
        public string country { get; set; }
        public string countryCode { get; set; }
        public int deviceType { get; set; }
    }
    public class ProfileEditModelReturn
    {
        public bool status { get; set; }
        public string message { get; set; }
        public UserData user { get; set; }
    }
}
