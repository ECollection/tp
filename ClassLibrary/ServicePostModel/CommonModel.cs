﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary.ServicePostModel
{
    class CommonModel
    {
    }
    public class Test
    {
        public testClass auth { get; set; }
    }
    public class testClass
    {
        public int id { get; set; }
        public long Key { get; set; }
        public string hash { get; set; }
    }

    public class RechargeWalletModel
    {
        public long userId { get; set; }
        public string cardNumber { get; set; }
        public int deviceType { get; set; }
    }
    public class RechargeWalletReturnModel
    {
        public bool status { get; set; }
        public string message { get; set; }
        public RechargeWalletModel postModel { get; set; }
        public UserData user { get; set; }
    }

    public class AssignCard
    {
        public long agentId { get; set; }
        public string qrCode { get; set; }
    }

    public class AssignCardReturnModel
    {
        public bool status { get; set; }
        public string message { get; set; }
        public AssignCard postModel { get; set; }
    }
    public class ListAgentsReturnModel
    {
        public bool status { get; set; }
        public string message { get; set; }
        public List<AgentData> agents { get; set; }
    }
    public class AgentData
    {
        public long agentId { get; set; }
        public string userName { get; set; }
    }

    public class ApiPriority
    {
        public string Data { get; set; }
    }
    public class ApiPriorityData
    {
        public int ServiceArea { get; set; }
        public int ServiceType { get; set; }
        public int ApiProvider { get; set; }
        public string Priority { get; set; }
    }

    public class PriorityData
    {
        public string ApiName { get; set; }
        public int ApiProviderEnum { get; set; }
        public int Priority { get; set; }
    }

    public class ApiPriorityResponse
    {
        public bool status { get; set; }
        public string message { get; set; }
        public MobilePriorityResponse MobilePriority { get; set; }
        public DTHPriorityResponse DTHPriority { get; set; }
        public UtilityPriorityResponse UtilityPriority { get; set; }
        public PostpaidPriorityResponse PostpaidPriority { get; set; }
    }

    public class MobilePriorityResponse
    {
        public InternationalPriorityResponse International { get; set; }
        public IndiaPriorityResponse India { get; set; }
        public UAEPriorityResponse UAE { get; set; }

    }
    public class DTHPriorityResponse
    {
        public InternationalPriorityResponse International { get; set; }
        public IndiaPriorityResponse India { get; set; }
        public UAEPriorityResponse UAE { get; set; }
    }

    public class UtilityPriorityResponse
    {
        public InternationalPriorityResponse International { get; set; }
        public IndiaPriorityResponse India { get; set; }
        public UAEPriorityResponse UAE { get; set; }
    }
    public class PostpaidPriorityResponse
    {
        public InternationalPriorityResponse International { get; set; }
        public IndiaPriorityResponse India { get; set; }
        public UAEPriorityResponse UAE { get; set; }
    }

    public class InternationalPriorityResponse
    {
        public List<PriorityData> list { get; set; }
    }
    public class IndiaPriorityResponse
    {
        public List<PriorityData> list { get; set; }
    }
    public class UAEPriorityResponse
    {
        public List<PriorityData> list { get; set; }
    }
    public class ResetPassword
    {
        public long userId { get; set; }
        public string oldPassword { get; set; }
        public string newPassword { get; set; }
        public int deviceType { get; set; }
    }
    public class ResetPasswordModelReturn
    {
        public bool status { get; set; }
        public string message { get; set; }
        public ResetPassword postModel { get; set; }
        public UserData user { get; set; }

    }
    public class ForgotPassword
    {
        public string countryCode { get; set; }
        public string mobile { get; set; }
    }
    public class ForgotPasswordModelReturn
    {
        public bool status { get; set; }
        public string message { get; set; }
        public ForgotPassword postModel { get; set; }

    }

    public class GetUserData
    {
        public long userId { get; set; }
        public int deviceType { get; set; }
    }
    public class GetUserDataReturn
    {
        public bool status { get; set; }
        public string message { get; set; }
        public string accessToken { get; set; }
        public string currentDeviceToken { get; set; }
        public GetUserData postModel { get; set; }
        public UserData user { get; set; }
    }

    public class GetTransaction
    {
        public long id { get; set; }
        public int index { get; set; }
        public string search { get; set; }
        public string fromDate { get; set; }
        public string endDate { get; set; }
    }

    public class GetTransactionResponse
    {
        public bool status { get; set; }
        public string message { get; set; }
        public string amountTotal { get; set; }
        public string deductedAmountTotal { get; set; }
        public GetTransaction postModel { get; set; }
        public List<TransactionData> data { get; set; }
    }
    public class TransactionData
    {
        public string transactionid { get; set; }
        public string transactionstate { get; set; }
        public DateTime transactiontime { get; set; }
        public string rechargenumber { get; set; }
        public decimal amount { get; set; }
        public int type { get; set; }
        public int apiprovider { get; set; }
        public string currency { get; set; }
        public decimal amountdeducted { get; set; }
        public string amountcurrency { get; set; }
        public string amountdeductedcurrency { get; set; }
    }


    public class SendOtp
    {
        public string countryCode { get; set; }
        public string mobileNumber { get; set; }
    }
    public class SendOtpResponse
    {
        public bool status { get; set; }
        public string message { get; set; }
        public SendOtp postModel { get; set; }
    }

    public class ValidateOtp
    {
        public string countryCode { get; set; }
        public string mobileNumber { get; set; }
        public int otp { get; set; }
    }
    public class ValidateOtpResponse
    {
        public bool status { get; set; }
        public string message { get; set; }
        public ValidateOtp postModel { get; set; }
    }

    public class SendContactMail
    {
        public long userId { get; set; }
        public string message { get; set; }
    }

    public class SendContactMailResponse
    {
        public bool status { get; set; }
        public string message { get; set; }
        public SendContactMail postModel { get; set; }
    }

    public class FetchDeductedAmount
    {
        public long userId { get; set; }
        public decimal amount { get; set; }
        public string currency { get; set; }
        //public string apiprovider { get; set; }
        //public ClassLibrary.Enum.apiProvider apiprovider { get; set; }
        public int apiprovider { get; set; }
        //public int apiprovider1 { get; set; }
        public int productType { get; set; }
        public bool switchCurrency { get; set; }
    }

    public class FetchDeductedAmountResponse
    {
        public bool status { get; set; }
        public string message { get; set; }
        public FetchDeductedAmount postModel { get; set; }
        public string wallet { get; set; }
        public string deductedAmount { get; set; }
    }

    public class GetUiStatusResponse
    {
        public bool status { get; set; }
        public bool Uistatus { get; set; }
        public string message { get; set; }
    }

    public class Logout
    {
        public string deviceToken { get; set; }
    }
    public class LogoutResponse
    {
        public bool status { get; set; }
        public string message { get; set; }
    }


    public class GetAllTotalAmount
    {
        public long userId { get; set; }
    }
    public class GetAllTotalAmountResponse
    {
        public bool status { get; set; }
        public string message { get; set; }
        public string creditAmount { get; set; }
        public string deductedAmount { get; set; }
        public string commisionAmount { get; set; }
        public string wallet { get; set; }
    }
}
