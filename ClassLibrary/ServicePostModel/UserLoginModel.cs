﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary.ServicePostModel
{
    public class UserLoginModel
    {
        public string mobileNumber { get; set; }
        public string countryCode { get; set; }
        public string password { get; set; }
        public string versionCode { get; set; }
        public string deviceToken { get; set; }
        public string ip { get; set; }
        public int deviceType { get; set; }
    }
    public class UserLoginModelReturn
    {
        public bool status { get; set; }
        public string message { get; set; }
        public string ip { get; set; }
        public string accessToken { get; set; }
        public string prevDeviceToken { get; set; }
        public UserLoginModel postModel { get; set; }
        public UserData user { get; set; }

    }
}
