﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary.ServicePostModel
{
    public class OnePrepayPostModel
    {
        public class PinlessRequest
        {
            public long userId { get; set; }
            public string account { get; set; }
            public string amount { get; set; }
            public string prodCode { get; set; }
            public int productType { get; set; }
            public string amountCurrency { get; set; }
            public string ip { get; set; }
        }
        public class PinlessRequestReturnModel
        {
            public bool status { get; set; }
            public string message { get; set; }
            public string wallet { get; set; }
            public dynamic data { get; set; }
        }

        public class PinRequest
        {
            public long userId { get; set; }
            public string ValueCode { get; set; }
            public string prodCode { get; set; }
            public int productType { get; set; }
            public string type { get; set; }
            public string amountCurrency { get; set; }
            public string ip { get; set; }
        }
        public class PinRequestReturnModel
        {
            public bool status { get; set; }
            public string message { get; set; }
            public string wallet { get; set; }
            public dynamic data { get; set; }
            public string pinNumber { get; set; }
            public string serialNumber { get; set; }
            public bool hasImportedCards { get; set; }
        }

        public class GetbalancestReturnModel
        {
            public bool status { get; set; }
            public string message { get; set; }
            public dynamic data { get; set; }
        }

    }
}
