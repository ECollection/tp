﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary.ServicePostModel
{
    public class JoloPostModel
    {
        public class operatorANDcircleFinder
        {
            public string mobileNumber { get; set; }
        }

        public class operatorANDcircleFinderReturnModel
        {
            public bool status { get; set; }
            public string message { get; set; }
            public dynamic data { get; set; }
        }

        public class operatorplanANDofferFinder
        {
            public string mobileNumber { get; set; }
            public string amount { get; set; }
            public string operator_code { get; set; }
            public string circle_code { get; set; }
            public string category_code { get; set; }
            public string rowCount { get; set; }
        }

        public class operatorplanANDofferFinderReturnModel
        {
            public bool status { get; set; }
            public string message { get; set; }
            public dynamic data { get; set; }
        }

        public class mobileAndDthRecharge
        {
            public long userId { get; set; }
            public int mode { get; set; }
            public string mobileNumber { get; set; }
            public string amount { get; set; }
            public string operator_code { get; set; }
            public int type { get; set; }
            public int productType { get; set; }
            public string amountCurrency { get; set; }
            public string ip { get; set; }
        }
        public class mobileAndDthRechargeReturnModel
        {
            public bool status { get; set; }
            public string message { get; set; }
            public string wallet { get; set; }
            public dynamic data { get; set; }
        }
        public class FindDthOperator
        {
            public long userId { get; set; }
            public string subscriberid { get; set; }
        }
        public class FindDthOperatorReturnModel
        {
            public bool status { get; set; }
            public string message { get; set; }
            public dynamic data { get; set; }
        }
        public class FindPlanDTH
        {
            public string amount { get; set; }
            public string operator_code { get; set; }
            public string rowCount { get; set; }
        }
        public class FindPlanDTHReturnModel
        {
            public bool status { get; set; }
            public string message { get; set; }
            public dynamic data { get; set; }
        }

        public class MobilePostpaidBill
        {
            public long userId { get; set; }
            public int mode { get; set; }
            public string mobileNumber { get; set; }
            public string amount { get; set; }
            public string operator_code { get; set; }
            public int type { get; set; }
            public int productType { get; set; }
            public string amountCurrency { get; set; }
            public string ip { get; set; }

        }

        public class MobilePostpaidBillReturnModel
        {
            public bool status { get; set; }
            public string message { get; set; }
            public string wallet { get; set; }
            public dynamic data { get; set; }
        }

    }
}
