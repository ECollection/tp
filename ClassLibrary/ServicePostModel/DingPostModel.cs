﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary.ServicePostModel
{
    public class DingPostModel
    {
        public class GetAccountLookup
        {
            public string accountNumber { get; set; }
        }
        public class GetAccountLookupResponse
        {
            public bool status { get; set; }
            public string message { get; set; }
            public dynamic data { get; set; }
        }
        public class GetProviders
        {
            public string providerCodes { get; set; }
            public string countryIsos { get; set; }
            public string regionCodes { get; set; }
            public string accountNumber { get; set; }
        }
        public class GetProvidersResponse
        {
            public bool status { get; set; }
            public string message { get; set; }
            public dynamic data { get; set; }
        }

        public class GetProducts
        {
            public string providerCodes { get; set; }
            public string countryIsos { get; set; }
            public string regionCodes { get; set; }
            public string skuCodes { get; set; }
            public string benefits { get; set; }
            public string accountNumber { get; set; }
        }
        public class GetProductsResponse
        {
            public bool status { get; set; }
            public string message { get; set; }
            public dynamic data { get; set; }
        }

        public class SendTransfer
        {
            public string SkuCode { get; set; }
            public string SendValue { get; set; }
            public string SendCurrencyIso { get; set; }
            public string AccountNumber { get; set; }
            public bool ValidateOnly { get; set; }
            public long userId { get; set; }
            public int productType { get; set; }
            public string amount { get; set; }
            public string amountCurrency { get; set; }
            public int dingProductType { get; set; }
            public bool switchCurrency { get; set; }

            public string ip { get; set; }
        }
        public class SendTransferResponse
        {
            public bool status { get; set; }
            public string message { get; set; }
            public dynamic data { get; set; }
            public string wallet { get; set; }
        }
        public class PostModel
        {
            public Request request { get; set; }
        }
        public class Request
        {
            public string SkuCode { get; set; }
            public double SendValue { get; set; }
            public string SendCurrencyIso { get; set; }
            public string AccountNumber { get; set; }
            public string DistributorRef { get; set; }
            public bool ValidateOnly { get; set; }
        }

    }
}
