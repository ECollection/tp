﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary.ServicePostModel
{
    public class UserData
    {
        public long userId { get; set; }
        public string uniqueIdentifier { get; set; }
        public string userName { get; set; }
        public string country { get; set; }
        public string countryCode { get; set; }
        public string mobileNumber { get; set; }
        public string profileImage { get; set; }
        public string wallet { get; set; }
        public string currency { get; set; }
        public bool isBlocked { get; set; }
        public string appVersion { get; set; }
    }
}
