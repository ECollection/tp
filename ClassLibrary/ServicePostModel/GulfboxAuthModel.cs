﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary.ServicePostModel
{
    public class GulfboxAuthModel
    {
        public string id { get; set; }
        public string key { get; set; }
        public string hash { get; set; }
    }
    public class GulfboxAuthPostModel
    {
        public GulfboxAuthModel auth { get; set; }
    }
}
