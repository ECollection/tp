﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary.ServicePostModel
{
    public class GulfboxPostModel
    {
        public class GulfboxServiceApi
        {
            public GulfboxAuthModel auth { get; set; }
        }
        public class GulfboxServiceApiReturnModel
        {
            public bool status { get; set; }
            public string message { get; set; }
            public dynamic data { get; set; }
        }
        public class GulfboxProviderApi
        {
            public GulfboxAuthModel auth { get; set; }
            public string country_id { get; set; }
            public string provider_id { get; set; }
        }
        public class GulfboxProviderApiReturnModel
        {
            public bool status { get; set; }
            public string message { get; set; }
            public dynamic data { get; set; }
        }

        public class GulfboxTransactionApi
        {
            public GulfboxAuthModel auth { get; set; }
            public bool simulation { get; set; }
            public string external_transaction_id { get; set; }
            public string transaction_id { get; set; }
            public string product { get; set; }
            public Fields fields { get; set; }
            public long userId { get; set; }

            public string ip { get; set; }

        }
        public class GulfboxVoucherTransactionApi
        {
            public GulfboxAuthModel auth { get; set; }
            public bool simulation { get; set; }
            public string external_transaction_id { get; set; }
            public string transaction_id { get; set; }
            public string product { get; set; }
            public string amount { get; set; }
            public string type { get; set; }
            //public Fields fields { get; set; }
            public long userId { get; set; }
            public int productType { get; set; }

            public string ip { get; set; }
        }

        public class Fields
        {
            public string customer { get; set; }
            public string account { get; set; }
            public string amount { get; set; }
        }
        public class GulfboxTransactionApiReturnModel
        {
            public bool status { get; set; }
            public string message { get; set; }
            public dynamic data { get; set; }
            public string external_transaction_id { get; set; }
            public string transaction_id { get; set; }
        }

        public class GulfboxVoucherTransactionApiReturnModel
        {
            public bool status { get; set; }
            public string message { get; set; }
            public dynamic data { get; set; }
            public string external_transaction_id { get; set; }
            public string transaction_id { get; set; }
            public string pinNumber { get; set; }
            public string serialNumber { get; set; }
            public bool hasImportedCards { get; set; }
            public string wallet { get; set; }
        }

        public class GulfboxTransactionConfirmApi
        {
            public GulfboxAuthModel auth { get; set; }
            public bool simulation { get; set; }
            public string external_transaction_id { get; set; }
            public string transaction_id { get; set; }
            public Fields fields { get; set; }
            public long userId { get; set; }
            public string amountAED { get; set; }
            public string currency { get; set; }
            public int productType { get; set; }
            public string amountCurrency { get; set; }
            public string ip { get; set; }
        }
        public class GulfboxTransactionConfirmApiReturnModel
        {
            public bool status { get; set; }
            public string message { get; set; }
            public string wallet { get; set; }
            public dynamic data { get; set; }
        }

        public class GulfboxVoucherTransactionConfirmApi
        {
            public GulfboxAuthModel auth { get; set; }
            public bool simulation { get; set; }
            public string external_transaction_id { get; set; }
            public string transaction_id { get; set; }
            //public Fields fields { get; set; }
            public long userId { get; set; }
            public string amountAED { get; set; }
            public string currency { get; set; }
            public int productType { get; set; }
            public string amountCurrency { get; set; }
            public string ip { get; set; }
        }
        public class GulfboxVoucherTransactionConfirmApiReturnModel
        {
            public bool status { get; set; }
            public string message { get; set; }
            public string wallet { get; set; }
            public dynamic data { get; set; }
        }


        public class GulfboxBalanceApi
        {
            public GulfboxAuthModel auth { get; set; }
        }
        public class GulfboxBalanceReturnModel
        {
            public bool status { get; set; }
            public string message { get; set; }
            public dynamic data { get; set; }
        }
        public class GulfboxServiceTransactionApi
        {
            public GulfboxAuthModel auth { get; set; }
            public bool simulation { get; set; }
            public string external_transaction_id { get; set; }
            public string transaction_id { get; set; }
            public string product { get; set; }
            public Fields fields { get; set; }
            public long userId { get; set; }
            public string amountAED { get; set; }
            public string currency { get; set; }
            public string ip { get; set; }
        }
        public class GulfboxServiceTransactionApiReturnModel
        {
            public bool status { get; set; }
            public string message { get; set; }
            public dynamic data { get; set; }
            public string external_transaction_id { get; set; }
            public string transaction_id { get; set; }
        }
        public class GulfboxServiceTransactionConfirmApi
        {
            public GulfboxAuthModel auth { get; set; }
            public bool simulation { get; set; }
            public string external_transaction_id { get; set; }
            public string transaction_id { get; set; }
            public Fields fields { get; set; }
            public long userId { get; set; }
            public string ip { get; set; }
        }
        public class GulfboxServiceTransactionConfirmApiReturnModel
        {
            public bool status { get; set; }
            public string message { get; set; }
            public dynamic data { get; set; }
        }

        public class GulfboxFindApi
        {
            public GulfboxAuthModel auth { get; set; }
            public string transaction_id { get; set; }
        }

    }
}
