﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary.ServicePostModel
{
    public class Pay2All
    {
        public class prepaidrechargeANDmobilepostpaidbill
        {
            public long userId { get; set; }
            public string number { get; set; }
            public string providerId { get; set; }
            public string amount { get; set; }
            public int productType { get; set; }
            public string amountCurrency { get; set; }
            public string ip { get; set; }
        }

        public class prepaidrechargeANDmobilepostpaidbillApiResponse
        {
            public string payid { get; set; }
            public string operator_ref { get; set; }
            public string status { get; set; }
            public string txstatus_desc { get; set; }
            public string message { get; set; }
        }

        public class prepaidrechargeANDmobilepostpaidbillReturnModel
        {
            public bool status { get; set; }
            public string message { get; set; }
            public string wallet { get; set; }
            public prepaidrechargeANDmobilepostpaidbillApiResponse data { get; set; }
        }


    }
}
