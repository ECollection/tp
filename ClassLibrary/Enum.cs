﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary
{
    public class Enum
    {
        public enum userType
        {
            User = 1,
            Agent = 2,
            Distributor = 3,
            Reseller = 4
        }

        public enum serviceType
        {
            Mobile = 1,
            DTH = 2,
            Utility = 3,
            Postpaid = 4
        }
        public enum apiProvider
        {
            NoProvider = 0,
            Gulfbox = 1,
            Magiceasy = 2,
            Jolo = 3,
            KeralaRecharge = 4,
            //Pay2All = 4,18-03-2019 premjith
            OnePrepay = 5,
            Ding = 6,
            Pay2All = 7
        }
        public enum serviceArea
        {
            International = 1,
            India = 2,
            UAE = 3
        }

        public enum joloServices
        {
            MobileRecharge = 1,
            DthRecharge = 2,
            DatacardRecharge = 3,
            MobilePostpaidBill = 4
        }

        public enum gulfboxServices
        {
            MobileRecharge = 1,
            Voucher = 2
        }

        public enum transactionListing
        {
            GulfboxMobileRecharge = 1,
            GulfboxVoucher = 2,
            JoloRecharge = 3
        }
        public enum keralaRechargeApiServices
        {
            PrepaidMobile = 1,
            PostpaidMobile = 2,
            DTHMobile = 3,
            KSEBBill = 4
        }
        public enum GulfboxProductTypes
        {
            NoProduct = 0,
            Du = 1,
            Etisalat = 2,
            Hello = 3,
            Five = 4,
            Salik = 5,
            Intl = 6,
            DuVoucher = 7,
            EtisalatVoucher = 8
        }
        public enum OnePrepayType
        {
            PinlessRequest = 1,
            PinRequest = 2
        }

        public enum DingProductType
        {
            Mobile = 1,
            DTH = 2
        }

        public enum DeviceType
        {
            Ios = 1,
            Android = 2
        }

        public enum AppType
        {
            Ios = 1,
            Android = 2
        }

        public enum DistributorPermissions
        {
            India = 1,
            International = 2,
            UAE = 3
        }
        public enum ResellerPermissions
        {
            India = 1,
            International = 2,
            UAE = 3
        }
        public enum AgentPermissions
        {
            India = 1,
            International = 2,
            UAE = 3
        }
        public enum userTypeNew
        {
            User = 1,            
            Distributor = 2,
            Reseller = 3,
            Agent = 4
        }

    }
}
