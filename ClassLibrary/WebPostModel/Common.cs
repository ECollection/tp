﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace ClassLibrary.WebPostModel
{
    public class Common
    {
    }
    public class LoginModel
    {
        [Required(ErrorMessage = "Required")]
        [DataType(DataType.PhoneNumber)]
        //[RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Not a valid phone number")]
        //[RegularExpression(@"^(?:\+971|00971|0)?(?:50|51|52|55|56|2|3|4|6|7|9)\d{7}$", ErrorMessage = "Not a valid phone number")]
        public string mobileNumber { get; set; }
        [Required(ErrorMessage = "Required")]
        public string password { get; set; }
        public bool isRemember { get; set; }
    }
    public class AgentDetails
    {
        public long agentId { get; set; }

    }
    public class AddAgent
    {
        [Required(ErrorMessage = "Required")]
        [DataType(DataType.PhoneNumber)]
        //[RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Not a valid phone number")]
        public string mobileNumber { get; set; }
        [Required(ErrorMessage = "Required")]
        public string name { get; set; }
        [Required(ErrorMessage = "Required")]
        public string place { get; set; }
        [Required(ErrorMessage = "Required")]
        public string address { get; set; }
        [Required(ErrorMessage = "Required")]
        public string shopName { get; set; }
        [Required(ErrorMessage = "Required")]
        public string password { get; set; }
        [Required(ErrorMessage = "Required")]
        public string countryCode { get; set; }
    }

    public class EditAgent
    {
        [Required(ErrorMessage = "Required")]
        [DataType(DataType.PhoneNumber)]
        //[RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Not a valid phone number")]
        public string mobileNumber { get; set; }
        [Required(ErrorMessage = "Required")]
        public string name { get; set; }
        [Required(ErrorMessage = "Required")]
        public string place { get; set; }
        [Required(ErrorMessage = "Required")]
        public string address { get; set; }
        [Required(ErrorMessage = "Required")]
        public string shopName { get; set; }
        [Required(ErrorMessage = "Required")]
        public string password { get; set; }
        [Required(ErrorMessage = "Required")]
        public string countryCode { get; set; }
        public long agentId { get; set; }
    }

    public class Addrechargecard
    {
        public string amount { get; set; }
        public string count { get; set; }
        public string currency { get; set; }
    }
    public class RechargeCardlistModel
    {
        public int index { get; set; }
        public string search { get; set; }
        public string listcount { get; set; }

    }

    public class ImportedCardlistModel
    {
        public int index { get; set; }
        public string search { get; set; }
        public string listcount { get; set; }

    }
    public class ImportedCardlistPaginationModel
    {
        public int currentIndex { get; set; }
        public int nextIndex { get; set; }
        public string search { get; set; }
        public string listcount { get; set; }

    }

    public class RechargeCardlistPaginationModel
    {
        public int currentIndex { get; set; }
        public int nextIndex { get; set; }
        public string search { get; set; }
        public string listcount { get; set; }

    }

    public class ListAgentModel
    {
        //public int index { get; set; }
        //public string search { get; set; }
        //public string listcount { get; set; }
        //public string userCurrency { get; set; }
        public int index { get; set; }
        public string search { get; set; }
        public string listcount { get; set; }
        public string userCurrency { get; set; }
        public long ResellerId { get; set; }

    }
    public class ListAgentPaginationModel
    {
        public int currentIndex { get; set; }
        public int nextIndex { get; set; }
        public string search { get; set; }
        public string listcount { get; set; }
        public string userCurrency { get; set; }
    }

    public class ListUserModel
    {
        public int index { get; set; }
        public string search { get; set; }
        public string listcount { get; set; }
    }
    public class ListUserPaginationModel
    {
        public int currentIndex { get; set; }
        public int nextIndex { get; set; }
        public string search { get; set; }
        public string listcount { get; set; }
    }
    public class UserDetails
    {
        public long userId { get; set; }

    }
    public class ListUserUsedCards
    {
        public long userId { get; set; }
        public int index { get; set; }
        public string search { get; set; }
        public string listcount { get; set; }
    }
    public class ListUserUsedCardsPaginationModel
    {
        public long userId { get; set; }
        public int currentIndex { get; set; }
        public int nextIndex { get; set; }
        public string search { get; set; }
        public string listcount { get; set; }
    }

    public class AgentAssignedCards
    {
        public long agentId { get; set; }
        public int index { get; set; }
        public string search { get; set; }
        public string listcount { get; set; }
    }
    public class AgentAssignedCardsPaginationModel
    {
        public long agentId { get; set; }
        public int currentIndex { get; set; }
        public int nextIndex { get; set; }
        public string search { get; set; }
        public string listcount { get; set; }
    }

    public class AgentTransactions
    {
        public long agentId { get; set; }
        public int index { get; set; }
        public string search { get; set; }
        public string listcount { get; set; }
    }
    public class AgentTransactionsPaginationModel
    {
        public long agentId { get; set; }
        public int currentIndex { get; set; }
        public int nextIndex { get; set; }
        public string search { get; set; }
        public string listcount { get; set; }
    }

    public class UserTransactions
    {
        public long userId { get; set; }
        public int index { get; set; }
        public string search { get; set; }
        public string listcount { get; set; }
    }
    public class UserTransactionsPaginationModel
    {
        public long userId { get; set; }
        public int currentIndex { get; set; }
        public int nextIndex { get; set; }
        public string search { get; set; }
        public string listcount { get; set; }
    }

    public class Transactions
    {
        public int index { get; set; }
        public string search { get; set; }
        public string listcount { get; set; }
        public string startDate { get; set; }
        public string endDate { get; set; }
        public string currency { get; set; }
        public string filterType { get; set; }
    }
    public class TransactionsPaginationModel
    {
        public int currentIndex { get; set; }
        public int nextIndex { get; set; }
        public string search { get; set; }
        public string listcount { get; set; }
        public string startDate { get; set; }
        public string endDate { get; set; }
        public string currency { get; set; }
        public string filterType { get; set; }
    }


    public class AdminLoginModel
    {
        public string mobileNumber { get; set; }
        public string password { get; set; }
        //[Required(ErrorMessage = "Required")]
        public string pin { get; set; }
        public bool isRemember { get; set; }
    }
    public class UploadProfileImage
    {
        public string adminId { get; set; }
        public string imageFilepath { get; set; }
        public HttpPostedFileBase profileImage { get; set; }
    }
    public class EditName
    {
        public string adminId { get; set; }
        public string name { get; set; }
    }
    public class UpdateAndroidversion
    {
        public string adminId { get; set; }
        public string version { get; set; }
    }
    public class UpdateIosversion
    {
        public string adminId { get; set; }
        public string version { get; set; }
    }
    public class EditCountry
    {
        public string adminId { get; set; }
        public string country { get; set; }
    }
    public class EditPassword
    {
        public string adminId { get; set; }
        public string currentPassword { get; set; }
        public string newPassword { get; set; }
    }

    public class EditMobile
    {
        public string adminId { get; set; }
        public string currentMobile { get; set; }
        public string newMobile { get; set; }
    }

    public class UpdateCurrencyRate
    {
        public string data { get; set; }
    }

    public class CurrencyData
    {
        public string fromcountry { get; set; }
        public string fromcountrycode { get; set; }
        public string fromcurrency { get; set; }
        public string tocountry { get; set; }
        public string tocountrycode { get; set; }
        public string tocurrency { get; set; }
        public string rate { get; set; }
    }

    public class AddCommission
    {
        public int apiprovider { get; set; }
        public int producttype { get; set; }
        public float commission { get; set; }
        public long agentId { get; set; }
        public string isadmincommission { get; set; }
    }

    public class UpdateCommission
    {
        public string data { get; set; }
        public long agentId { get; set; }
    }
    public class ImportDataFromExcel
    {
        public HttpPostedFileBase file { get; set; }
    }

    public class AddAgentBalance
    {
        public long agentId { get; set; }
        public string amount { get; set; }
    }
    public class SubtractAgentBalance
    {
        public long agentId { get; set; }
        public string amount { get; set; }
    }

    public class SendContactUsMail
    {
        [Required(ErrorMessage = "Required")]
        public string name { get; set; }
        [Required(ErrorMessage = "Required")]
        public string message { get; set; }
        [Required(ErrorMessage = "Required")]
        [EmailAddress(ErrorMessage = "Invalid email")]
        public string email { get; set; }
        [Required(ErrorMessage = "Required")]
        public string subject { get; set; }
    }

    public class CreditHistoryModel
    {
        public long Id { get; set; }
        public long AgentId { get; set; }
        public decimal Amount { get; set; }
        public string Type { get; set; }
        public DateTime Date { get; set; }
        public string AgentName { get; set; }
    }

    public class CreditHistoryViewModel
    {
        public long AgentId { get; set; }
        public int index { get; set; }
        public string search { get; set; }
        public string listcount { get; set; }
    }
    public class CreditHistoryPaginationViewModel
    {
        public long AgentId { get; set; }
        public int currentIndex { get; set; }
        public int nextIndex { get; set; }
        public string search { get; set; }
        public string listcount { get; set; }
    }
    public class Distributor
    {
        public long DistributorID { get; set; }
        public System.Guid DistributorGuid { get; set; }
        public string Email { get; set; }
        public string Username { get; set; }
        public string Country { get; set; }
        public string CountryCode { get; set; }
        public string Mobile { get; set; }
        public string Password { get; set; }
        public bool IsActive { get; set; }
        public System.DateTime Timestamp { get; set; }
        public string ProfileImage { get; set; }
        public string Place { get; set; }
        public string Address { get; set; }
        public string ShopName { get; set; }
        public int UserType { get; set; }
        public decimal Wallet { get; set; }
        public string Currency { get; set; }
        public bool IsBlocked { get; set; }
        public System.DateTime LastUpdated { get; set; }
        public List<Distributor> DistributorLists { get; set; }
    }

    public class AddDistributor
    {
        public long DistributorID { get; set; }

        [Required(ErrorMessage = "Required")]
        [DataType(DataType.PhoneNumber)]
        //[RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Not a valid phone number")]
        public string mobileNumber { get; set; }
        [Required(ErrorMessage = "Required")]
        public string name { get; set; }
        [Required(ErrorMessage = "Required")]
        public string place { get; set; }
        [Required(ErrorMessage = "Required")]
        public string address { get; set; }
        [Required(ErrorMessage = "Required")]
        public string shopName { get; set; }
        [Required(ErrorMessage = "Required")]
        public string password { get; set; }
        [Required(ErrorMessage = "Required")]
        public string countryCode { get; set; }
        [Required(ErrorMessage = "Required")]
        [EmailAddress(ErrorMessage = "Invalid Email Address.")]
        public string email { get; set; }

        public bool permission_IndiaRegion { get; set; }
        public bool permission_International { get; set; }
        public bool permission_Uae { get; set; }

        [Required(ErrorMessage = "Required")]
        public string commision_DuVoucher { get; set; }
        [Required(ErrorMessage = "Required")]
        public string commision_DuTopup { get; set; }
        [Required(ErrorMessage = "Required")]
        public string commision_EtisalatTopup { get; set; }
        [Required(ErrorMessage = "Required")]
        public string commision_EtisalatVoucher { get; set; }
        [Required(ErrorMessage = "Required")]
        public string commision_Five { get; set; }
        [Required(ErrorMessage = "Required")]
        public string commision_Hello { get; set; }
        [Required(ErrorMessage = "Required")]
        public string commision_Salik { get; set; }
        [Required(ErrorMessage = "Required")]
        public string commision_India { get; set; }
        [Required(ErrorMessage = "Required")]
        public string commision_OtherCountry { get; set; }
        [Required(ErrorMessage = "Required")]
        public string commision_International { get; set; }
        

    }

    public class EditDistributor
    {
        [Required(ErrorMessage = "Required")]
        [DataType(DataType.PhoneNumber)]
        //[RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Not a valid phone number")]
        public string mobileNumber { get; set; }
        [Required(ErrorMessage = "Required")]
        public string name { get; set; }
        [Required(ErrorMessage = "Required")]
        public string place { get; set; }
        [Required(ErrorMessage = "Required")]
        public string address { get; set; }
        [Required(ErrorMessage = "Required")]
        public string shopName { get; set; }
        [Required(ErrorMessage = "Required")]
        public string password { get; set; }
        [Required(ErrorMessage = "Required")]
        public string countryCode { get; set; }
        public long distributorId { get; set; }


        [Required(ErrorMessage = "Required")]
        [EmailAddress(ErrorMessage = "Invalid Email Address.")]
        public string email { get; set; }

        public bool permission_IndiaRegion { get; set; }
        public bool permission_International { get; set; }
        public bool permission_Uae { get; set; }

        [Required(ErrorMessage = "Required")]
        public string commision_DuVoucher { get; set; }
        [Required(ErrorMessage = "Required")]
        public string commision_DuTopup { get; set; }
        [Required(ErrorMessage = "Required")]
        public string commision_EtisalatTopup { get; set; }
        [Required(ErrorMessage = "Required")]
        public string commision_EtisalatVoucher { get; set; }
        [Required(ErrorMessage = "Required")]
        public string commision_Five { get; set; }
        [Required(ErrorMessage = "Required")]
        public string commision_Hello { get; set; }
        [Required(ErrorMessage = "Required")]
        public string commision_Salik { get; set; }
        [Required(ErrorMessage = "Required")]
        public string commision_India { get; set; }
        [Required(ErrorMessage = "Required")]
        public string commision_OtherCountry { get; set; }
        [Required(ErrorMessage = "Required")]
        public string commision_International { get; set; }

    }
    public class ListDistributorModel
    {
        public int index { get; set; }
        public string search { get; set; }
        public string listcount { get; set; }
        public string userCurrency { get; set; }
    }
    public class ListDistributorPaginationModel
    {
        public int currentIndex { get; set; }
        public int nextIndex { get; set; }
        public string search { get; set; }
        public string listcount { get; set; }
        public string userCurrency { get; set; }
    }

    public class DistributorCredit
    {
        public long distributorId { get; set; }
        [Required(ErrorMessage = "Required")]
        public string amount { get; set; }
    }
        
    public class DistributorCreditHistoryListModel
    {
        public long distributorId { get; set; }
        public int index { get; set; }
        public string search { get; set; }
        public string listcount { get; set; }
        public string fromdate { get; set; }
        public string todate { get; set; }
    }
    public class DistributorCreditHistoryListPaginationModel
    {
        public long distributorId { get; set; }
        public int currentIndex { get; set; }
        public int nextIndex { get; set; }
        public string search { get; set; }
        public string listcount { get; set; }
        public string fromdate { get; set; }
        public string todate { get; set; }
    }

    public class RefreshCreditSum
    {
        public long distributorId { get; set; }
        public string fromdate { get; set; }
        public string todate { get; set; }
    }

    public class Reseller
    {
        public long ResellerId { get; set; }
        public System.Guid ResellerGuid { get; set; }
        public long DistributorId { get; set; }
        public string Email { get; set; }
        public string Username { get; set; }
        public string Country { get; set; }
        public string CountryCode { get; set; }
        public string Mobile { get; set; }
        public string Password { get; set; }
        public bool IsActive { get; set; }
        public System.DateTime Timestamp { get; set; }
        public string ProfileImage { get; set; }
        public string Place { get; set; }
        public string Address { get; set; }
        public string ShopName { get; set; }
        public int UserType { get; set; }
        public decimal Wallet { get; set; }
        public string Currency { get; set; }
        public bool IsBlocked { get; set; }
        public System.DateTime LastUpdated { get; set; }
        public List<Reseller> ResellerLists { get; set; }
    }

    public class AddReseller
    {
        [Required(ErrorMessage = "Required")]
        [DataType(DataType.PhoneNumber)]
        //[RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Not a valid phone number")]
        public string mobileNumber { get; set; }
        [Required(ErrorMessage = "Required")]
        public string name { get; set; }
        [Required(ErrorMessage = "Required")]
        public string place { get; set; }
        [Required(ErrorMessage = "Required")]
        public string address { get; set; }
        [Required(ErrorMessage = "Required")]
        public string shopName { get; set; }
        [Required(ErrorMessage = "Required")]
        public string password { get; set; }
        [Required(ErrorMessage = "Required")]
        public string countryCode { get; set; }
        [Required(ErrorMessage = "Required")]
        [EmailAddress(ErrorMessage = "Invalid Email Address.")]
        public string email { get; set; }

        public bool permission_IndiaRegion { get; set; }
        public bool permission_International { get; set; }
        public bool permission_Uae { get; set; }

        [Required(ErrorMessage = "Required")]
        public string commision_DuVoucher { get; set; }
        [Required(ErrorMessage = "Required")]
        public string commision_DuTopup { get; set; }
        [Required(ErrorMessage = "Required")]
        public string commision_EtisalatTopup { get; set; }
        [Required(ErrorMessage = "Required")]
        public string commision_EtisalatVoucher { get; set; }
        [Required(ErrorMessage = "Required")]
        public string commision_Five { get; set; }
        [Required(ErrorMessage = "Required")]
        public string commision_Hello { get; set; }
        [Required(ErrorMessage = "Required")]
        public string commision_Salik { get; set; }
        [Required(ErrorMessage = "Required")]
        public string commision_India { get; set; }
        [Required(ErrorMessage = "Required")]
        public string commision_OtherCountry { get; set; }
        [Required(ErrorMessage = "Required")]
        public string commision_International { get; set; }
        public long distributorId { get; set; }
        public long ResellerId { get; set; }

    }

    public class ListResellerModel
    {
        public int index { get; set; }
        public string search { get; set; }
        public string listcount { get; set; }
        public string userCurrency { get; set; }
        public long DistributorId { get; set; }
        public long ResellerId { get; set; }
    }
    public class ListResellerPaginationModel
    {
        public int currentIndex { get; set; }
        public int nextIndex { get; set; }
        public string search { get; set; }
        public string listcount { get; set; }
        public string userCurrency { get; set; }
        public long DistributorId { get; set; }
        public long ResellerId { get; set; }
    }
    public class EditReseller
    {
        [Required(ErrorMessage = "Required")]
        [DataType(DataType.PhoneNumber)]
        //[RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Not a valid phone number")]
        public string mobileNumber { get; set; }
        [Required(ErrorMessage = "Required")]
        public string name { get; set; }
        [Required(ErrorMessage = "Required")]
        public string place { get; set; }
        [Required(ErrorMessage = "Required")]
        public string address { get; set; }
        [Required(ErrorMessage = "Required")]
        public string shopName { get; set; }
        [Required(ErrorMessage = "Required")]
        public string password { get; set; }
        [Required(ErrorMessage = "Required")]
        public string countryCode { get; set; }
        public long resellerId { get; set; }
        public long distributorId { get; set; }


        [Required(ErrorMessage = "Required")]
        [EmailAddress(ErrorMessage = "Invalid Email Address.")]
        public string email { get; set; }

        public bool permission_IndiaRegion { get; set; }
        public bool permission_International { get; set; }
        public bool permission_Uae { get; set; }

        [Required(ErrorMessage = "Required")]
        public string commision_DuVoucher { get; set; }
        [Required(ErrorMessage = "Required")]
        public string commision_DuTopup { get; set; }
        [Required(ErrorMessage = "Required")]
        public string commision_EtisalatTopup { get; set; }
        [Required(ErrorMessage = "Required")]
        public string commision_EtisalatVoucher { get; set; }
        [Required(ErrorMessage = "Required")]
        public string commision_Five { get; set; }
        [Required(ErrorMessage = "Required")]
        public string commision_Hello { get; set; }
        [Required(ErrorMessage = "Required")]
        public string commision_Salik { get; set; }
        [Required(ErrorMessage = "Required")]
        public string commision_India { get; set; }
        [Required(ErrorMessage = "Required")]
        public string commision_OtherCountry { get; set; }
        [Required(ErrorMessage = "Required")]
        public string commision_International { get; set; }

    }

    //reseller

    public class ResellerCredit
    {
        public long resellerId { get; set; }
        [Required(ErrorMessage = "Required")]
        public string amount { get; set; }
    }

    public class ResellerCreditHistoryListModel
    {
        public long resellerId { get; set; }
        public int index { get; set; }
        public string search { get; set; }
        public string listcount { get; set; }
        public string fromdate { get; set; }
        public string todate { get; set; }
    }
    public class ResellerCreditHistoryListPaginationModel
    {
        public long resellerId { get; set; }
        public int currentIndex { get; set; }
        public int nextIndex { get; set; }
        public string search { get; set; }
        public string listcount { get; set; }
        public string fromdate { get; set; }
        public string todate { get; set; }
    }

    public class RefreshCreditSumReseller
    {
        public long resellerId { get; set; }
        public string fromdate { get; set; }
        public string todate { get; set; }
    }

    public class Agent{
        public long AgentID { get; set; }
        public long ResellerId { get; set; }
        public long DistributorId { get; set; }
        public System.Guid AgentGuid { get; set; }
        public string Email { get; set; }
        public string Username { get; set; }
        public string Country { get; set; }
        public string CountryCode { get; set; }
        public string Mobile { get; set; }
        public string Password { get; set; }
        public bool IsActive { get; set; }
        public System.DateTime Timestamp { get; set; }
        public string ProfileImage { get; set; }
        public string Place { get; set; }
        public string Address { get; set; }
        public string ShopName { get; set; }
        public int UserType { get; set; }
        public decimal Wallet { get; set; }
        public string Currency { get; set; }
        public bool IsBlocked { get; set; }
        public System.DateTime LastUpdated { get; set; }
        public List<Agent> AgentLists { get; set; }
    }

    public class addAgent
    {
        [Required(ErrorMessage = "Required")]
        [DataType(DataType.PhoneNumber)]
        //[RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Not a valid phone number")]
        public string mobileNumber { get; set; }
        [Required(ErrorMessage = "Required")]
        public string name { get; set; }
        [Required(ErrorMessage = "Required")]
        public string place { get; set; }
        [Required(ErrorMessage = "Required")]
        public string address { get; set; }
        [Required(ErrorMessage = "Required")]
        public string shopName { get; set; }
        [Required(ErrorMessage = "Required")]
        public string password { get; set; }
        [Required(ErrorMessage = "Required")]
        public string countryCode { get; set; }
        [Required(ErrorMessage = "Required")]
        [EmailAddress(ErrorMessage = "Invalid Email Address.")]
        public string email { get; set; }

        public bool permission_IndiaRegion { get; set; }
        public bool permission_International { get; set; }
        public bool permission_Uae { get; set; }

        [Required(ErrorMessage = "Required")]
        public string commision_DuVoucher { get; set; }
        [Required(ErrorMessage = "Required")]
        public string commision_DuTopup { get; set; }
        [Required(ErrorMessage = "Required")]
        public string commision_EtisalatTopup { get; set; }
        [Required(ErrorMessage = "Required")]
        public string commision_EtisalatVoucher { get; set; }
        [Required(ErrorMessage = "Required")]
        public string commision_Five { get; set; }
        [Required(ErrorMessage = "Required")]
        public string commision_Hello { get; set; }
        [Required(ErrorMessage = "Required")]
        public string commision_Salik { get; set; }
        [Required(ErrorMessage = "Required")]
        public string commision_India { get; set; }
        [Required(ErrorMessage = "Required")]
        public string commision_OtherCountry { get; set; }
        [Required(ErrorMessage = "Required")]
        public string commision_International { get; set; }
        public long distributorId { get; set; }

    }
        
    //added by sibi
    public class addAgentNew 
    {
        [Required(ErrorMessage = "Required")]
        [DataType(DataType.PhoneNumber)]
        //[RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Not a valid phone number")]
        public string mobileNumber { get; set; }
        [Required(ErrorMessage = "Required")]
        public string name { get; set; }
        [Required(ErrorMessage = "Required")]
        public string place { get; set; }
        [Required(ErrorMessage = "Required")]
        public string address { get; set; }
        [Required(ErrorMessage = "Required")]
        public string shopName { get; set; }
        [Required(ErrorMessage = "Required")]
        public string password { get; set; }
        [Required(ErrorMessage = "Required")]
        public string countryCode { get; set; }
        [Required(ErrorMessage = "Required")]
        [EmailAddress(ErrorMessage = "Invalid Email Address.")]
        public string email { get; set; }

        public bool permission_IndiaRegion { get; set; }
        public bool permission_International { get; set; }
        public bool permission_Uae { get; set; }

        [Required(ErrorMessage = "Required")]
        public string commision_DuVoucher { get; set; }
        [Required(ErrorMessage = "Required")]
        public string commision_DuTopup { get; set; }
        [Required(ErrorMessage = "Required")]
        public string commision_EtisalatTopup { get; set; }
        [Required(ErrorMessage = "Required")]
        public string commision_EtisalatVoucher { get; set; }
        [Required(ErrorMessage = "Required")]
        public string commision_Five { get; set; }
        [Required(ErrorMessage = "Required")]
        public string commision_Hello { get; set; }
        [Required(ErrorMessage = "Required")]
        public string commision_Salik { get; set; }
        [Required(ErrorMessage = "Required")]
        public string commision_India { get; set; }
        [Required(ErrorMessage = "Required")]
        public string commision_OtherCountry { get; set; }
        [Required(ErrorMessage = "Required")]
        public string commision_International { get; set; }
        public long distributorId { get; set; }
        public long ResellerId { get; set; }
        

    }

}
