﻿using ClassLibrary.WebPostModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace TouchAndPayWeb.Controllers
{
    public class DistributorController : BaseController
    {
        public ActionResult home()
        {
            return View();
        }

        [HttpGet]
        public ActionResult logout()
        {
            Session.Clear();
            Session.RemoveAll();
            Session.Abandon();
            FormsAuthentication.SignOut();
            return RedirectToAction("login", "Account");
        }

        public ActionResult resellers()
        {
            var model = new Reseller();
            model.DistributorId =  Convert.ToInt64(Session["DistributorId"]);
            return View(model);
        }
        public ActionResult addReseller()
        {
            var model = new AddReseller();
            model.distributorId = _distributor.DistributorID;
            return View(model);
        }

        [HttpPost]
        public object addReseller(AddReseller model)
        {
            bool status = false;
            string message = "failed";
            Tuple<bool, string> t = _distributorRepository.addReseller(model);
            status = t.Item1;
            message = t.Item2;
            return Json(new { status = status, message = message }, JsonRequestBehavior.AllowGet);
        }
        public PartialViewResult refreshResellerlist(ListResellerModel model)
        {
            return PartialView("~/Views/Distributor/_pv_ListResellers.cshtml", model);
        }
        public PartialViewResult refreshResellerlistPagination(ListResellerPaginationModel model)
        {
            return PartialView("~/Views/Distributor/_pv_ListReseller_Pagination.cshtml", model);
        }

        [HttpGet]
        public object deactivateReseller(string id)
        {
            bool status = false;
            string message = "failed";
            Tuple<bool, string> t = _distributorRepository.deactivateReseller(id);
            status = t.Item1;
            message = t.Item2;
            return Json(new { status = status, message = message }, JsonRequestBehavior.AllowGet);
        }


        public ActionResult editreseller(string id)
        {
            var model = new ClassLibrary.WebPostModel.EditReseller();
            model.resellerId = Convert.ToInt64(id);

            var reseller = _context.tResellers.FirstOrDefault(z => z.ResellerId == model.resellerId);
            if (reseller != null)
            {
                var resellerData = new EntityLibrary.Data.Reseller(reseller);

                if (resellerData != null)
                {
                    model.address = resellerData.Address ?? string.Empty;
                    model.countryCode = resellerData.CountryCode ?? string.Empty;
                    model.mobileNumber = resellerData.Mobile ?? string.Empty;
                    model.name = resellerData.Username ?? string.Empty;
                    model.password = resellerData.Password ?? string.Empty;
                    model.place = resellerData.Place ?? string.Empty;
                    model.shopName = resellerData.ShopName ?? string.Empty;
                    model.email = resellerData.Email ?? string.Empty;
                    model.distributorId = resellerData.DistributorId;

                    if (resellerData.commissions.Count > 0)
                    {
                        model.commision_DuTopup = Convert.ToString(resellerData.commissions.Where(z => z.ProductType == (int)ClassLibrary.Enum.GulfboxProductTypes.Du).FirstOrDefault().Commission);
                        model.commision_DuVoucher = Convert.ToString(resellerData.commissions.Where(z => z.ProductType == (int)ClassLibrary.Enum.GulfboxProductTypes.DuVoucher).FirstOrDefault().Commission);
                        model.commision_EtisalatTopup = Convert.ToString(resellerData.commissions.Where(z => z.ProductType == (int)ClassLibrary.Enum.GulfboxProductTypes.Etisalat).FirstOrDefault().Commission);
                        model.commision_EtisalatVoucher = Convert.ToString(resellerData.commissions.Where(z => z.ProductType == (int)ClassLibrary.Enum.GulfboxProductTypes.EtisalatVoucher).FirstOrDefault().Commission);
                        model.commision_Five = Convert.ToString(resellerData.commissions.Where(z => z.ProductType == (int)ClassLibrary.Enum.GulfboxProductTypes.Five).FirstOrDefault().Commission);
                        model.commision_Hello = Convert.ToString(resellerData.commissions.Where(z => z.ProductType == (int)ClassLibrary.Enum.GulfboxProductTypes.Hello).FirstOrDefault().Commission);
                        model.commision_Salik = Convert.ToString(resellerData.commissions.Where(z => z.ProductType == (int)ClassLibrary.Enum.GulfboxProductTypes.Salik).FirstOrDefault().Commission);
                        model.commision_India = Convert.ToString(resellerData.commissions.Where(z => z.ProductType == (int)ClassLibrary.Enum.GulfboxProductTypes.NoProduct).FirstOrDefault().Commission);
                        model.commision_OtherCountry = Convert.ToString(resellerData.commissions.Where(z => z.ApiProvider == (int)ClassLibrary.Enum.apiProvider.Gulfbox && z.ProductType == (int)ClassLibrary.Enum.GulfboxProductTypes.Intl).FirstOrDefault().Commission);
                        model.commision_International = Convert.ToString(resellerData.commissions.Where(z => z.ApiProvider == (int)ClassLibrary.Enum.apiProvider.Ding && z.ProductType == (int)ClassLibrary.Enum.GulfboxProductTypes.Intl).LastOrDefault().Commission);
                    }

                    if (resellerData.permissions.Count > 0)
                    {
                        model.permission_IndiaRegion = resellerData.permissions.Any(z => z.Type == ClassLibrary.Enum.ResellerPermissions.India.ToString());
                        model.permission_International = resellerData.permissions.Any(z => z.Type == ClassLibrary.Enum.ResellerPermissions.International.ToString());
                        model.permission_Uae = resellerData.permissions.Any(z => z.Type == ClassLibrary.Enum.ResellerPermissions.UAE.ToString());
                    }

                }
            }

            return View(model);
        }

        [HttpPost]
        public object editReseller(ClassLibrary.WebPostModel.EditReseller model)
        {
            bool status = false;
            string message = "failed";
            Tuple<bool, string> t = _distributorRepository.editReseller(model);
            status = t.Item1;
            message = t.Item2;
            return Json(new { status = status, message = message }, JsonRequestBehavior.AllowGet);
        }


        public ActionResult ResellerTransactions()
        {
            var model = new Reseller();
            long id = Convert.ToInt64(Session["DistributorId"]);
            model.ResellerLists = _adminRepository.ResellerLists(id);
            return View(model);
        }


        public ActionResult resellercredit(string id)
        {
            var model = new ClassLibrary.WebPostModel.ResellerCredit();
            model.resellerId = Convert.ToInt64(id);
            return View(model);
        }

        [HttpPost]
        public object AddResellerCredit(ResellerCredit model)
        {
            bool status = false;
            string message = "failed";
            Tuple<bool, string> t = _distributorRepository.AddResellerCredit(model);
            status = t.Item1;
            message = t.Item2;
            return Json(new { status = status, message = message }, JsonRequestBehavior.AllowGet);
        }

        public PartialViewResult refreshResellerCreditlist(ClassLibrary.WebPostModel.ResellerCreditHistoryListModel model)
        {
            return PartialView("~/Views/Distributor/_pv_distributor_Credit_HistoryList.cshtml", model);
        }
        public PartialViewResult refreshResellerCreditlistPagination(ClassLibrary.WebPostModel.ResellerCreditHistoryListPaginationModel model)
        {
            return PartialView("~/Views/Distributor/_pv_distributor_Credit_History_Pagination.cshtml", model);
        }

        [HttpPost]
        public object RefreshCreditSum(RefreshCreditSumReseller model)
        {
            bool status = false;
            string message = "failed";
            Tuple<bool, string, string> t = _distributorRepository.RefreshCreditSum(model);
            status = t.Item1;
            message = t.Item2;
            return Json(new { status = status, message = message, sum = t.Item3 }, JsonRequestBehavior.AllowGet);
        }

        //Added sibi
        ////
        [HttpGet]
        public ActionResult RegularCardReseller()
        {
            return null;
        }

        [HttpGet]
        public ActionResult Settings()
        {
            return null;
        }



	}
}