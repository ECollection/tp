﻿using ClassLibrary.WebPostModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace TouchAndPayWeb.Controllers
{
    public class ResellerController : BaseController
    {
        //
        // GET: /Reseller/
        public ActionResult Home()
        {
            return View();
        }
        public ActionResult Agents()
        {
            //var model = new ListAgentModel();
            //model.ResellerId = 
            return View();
        }
        public ActionResult addAgent()
        {
            var model = new addAgentNew();
            //Lon a1 = TempData["ResellerId"];
            model.ResellerId = Convert.ToInt64(Session["ResellerId"]);//tReseller.ResellerId; /// no Idea This Line Temp Hard code 22/04/2019....           
            var re = _context.tResellers.Where(z => z.ResellerId == model.ResellerId).FirstOrDefault();           
            model.distributorId = re.DistributorId;
            return View(model);
        }

        [HttpPost]
        public object addAgent(addAgentNew model)
        {
            bool status = false;
            string message = "failed";
            Tuple<bool, string> t = _ResellerRepository.addAgent(model);
            status = t.Item1;
            message = t.Item2;
            return Json(new { status = status, message = message }, JsonRequestBehavior.AllowGet);
        }

        public PartialViewResult refreshAgentlist(ListAgentModel model)
        {
            return PartialView("~/Views/Reseller/_pv_ListAgents.cshtml", model);
        }
        public PartialViewResult refreshAgentlistPagination(ListResellerPaginationModel model)/////// this Item to start...lastUpdate 19/04/2019
        {
            return PartialView("~/Views/Reseller/_pv_ListAgent_Pagination.cshtml", model);
        }


        public ActionResult AgentTransactions()
        {
            var model = new Agent();
            long id = Convert.ToInt64(Session["ResellerId"]);
            model.AgentLists = _adminRepository.AgentLists(id);
            return View(model);
        }
        //ResellerTransactions
        [HttpGet]
        public ActionResult logout()
        {
            Session.Clear();
            Session.RemoveAll();
            Session.Abandon();
            FormsAuthentication.SignOut();
            return RedirectToAction("login", "Account");
        }
	}
}