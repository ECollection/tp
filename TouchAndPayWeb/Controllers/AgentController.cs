﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace TouchAndPayWeb.Controllers
{
    public class AgentController : BaseController
    {
        public ActionResult home()
        {
            return View();
        }

        [HttpGet]
        public ActionResult logout()
        {
            Session.Clear();
            Session.RemoveAll();
            Session.Abandon();
            FormsAuthentication.SignOut();
            return RedirectToAction("login", "Account");
        }

        public ActionResult cardlist()
        {
            if (_agent != null)
            {
                var model = new ClassLibrary.WebPostModel.AgentAssignedCards();
                model.agentId = Convert.ToInt64(_agent.UserID);
                model.index = 0;
                return View(model);
            }
            else
            {
                Session.Clear();
                Session.RemoveAll();
                Session.Abandon();
                FormsAuthentication.SignOut();
                return RedirectToAction("login", "Account");
            }
        }

        public PartialViewResult refreshAgentCardlist(ClassLibrary.WebPostModel.AgentAssignedCards model)
        {
            return PartialView("~/Views/Agent/_pv_AgentCardlist.cshtml", model);
        }
        public PartialViewResult refreshAgentCardlistPagination(ClassLibrary.WebPostModel.AgentAssignedCardsPaginationModel model)
        {
            return PartialView("~/Views/Agent/_pv_AgentCardlist_Pagination.cshtml", model);
        }

        public ActionResult transactions(string id)
        {

            var model = new ClassLibrary.WebPostModel.AgentTransactions();
            model.agentId = Convert.ToInt64(id);
            model.index = 0;
            return View(model);
        }

        public ActionResult editprofile(string id)
        {
            var model = new ClassLibrary.WebPostModel.EditAgent();
            model.agentId = Convert.ToInt64(id);

            var agent = _context.tb_User.FirstOrDefault(z => z.UserID == model.agentId);
            if (agent != null)
            {

                var agentData = new EntityLibrary.Data.User(agent);

                model.address = agentData.Address ?? string.Empty;
                model.countryCode = agentData.CountryCode ?? string.Empty;
                model.mobileNumber = agentData.Mobile ?? string.Empty;
                model.name = agentData.Username ?? string.Empty;
                model.password = agentData.Password ?? string.Empty;
                model.place = agentData.Place ?? string.Empty;
                model.shopName = agentData.ShopName ?? string.Empty;
            }

            return View(model);
        }
        [HttpPost]
        public object editprofile(ClassLibrary.WebPostModel.EditAgent model)
        {
            bool status = false;
            string message = "failed";
            Tuple<bool, string> t = _agentRepository.editprofile(model);
            status = t.Item1;
            message = t.Item2;
            return Json(new { status = status, message = message }, JsonRequestBehavior.AllowGet);
        }

        public PartialViewResult refreshAgentTransactionlist(ClassLibrary.WebPostModel.AgentTransactions model)
        {
            return PartialView("~/Views/Agent/_pv_agent_transactionlist.cshtml", model);
        }
        public PartialViewResult refreshAgentTransactionlistPagination(ClassLibrary.WebPostModel.AgentTransactionsPaginationModel model)
        {
            return PartialView("~/Views/Agent/_pv_agent_transactionlist_pagination.cshtml", model);
        }

        public ActionResult credithistory()
        {
            ClassLibrary.WebPostModel.CreditHistoryViewModel model = new ClassLibrary.WebPostModel.CreditHistoryViewModel();
            if (_agent != null)
            {
                model.AgentId = Convert.ToInt64(_agent.UserID);
            }
            model.search = "";
            model.listcount = "10";
            model.index = 0;
            return View(model);
        }

        public PartialViewResult refreshCreditlist(ClassLibrary.WebPostModel.CreditHistoryViewModel model)
        {
            return PartialView("~/Views/Agent/_pv_credithistory_list.cshtml", model);
        }
        public PartialViewResult refreshCreditlistPagination(ClassLibrary.WebPostModel.CreditHistoryPaginationViewModel model)
        {
            return PartialView("~/Views/Agent/_pv_credithistory_list_pagination.cshtml", model);
        }

    }
}