﻿using EntityLibrary;
using EntityLibrary.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TouchAndPayWeb.Controllers
{
    public class AgentBaseController : Controller
    {
        //
        protected TouchAndPayEntities _context = new TouchAndPayEntities();
        protected UserRepository _userRepository = new UserRepository();
        protected gulfboxRepository _gulfboxRepository = new gulfboxRepository();
        protected adminRepository _adminRepository = new adminRepository();
        public DateTime currentTime = System.DateTime.UtcNow;

        public tb_User _agent;
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            //check whether the user is logged in or else
            if (User != null && User.Identity.IsAuthenticated)
            {

                if (Session["Agent"] != null)
                {
                    var userId = long.Parse(User.Identity.Name);
                    var user = getAgentById(userId);
                    Session["Agent"] = user;
                    _agent = (tb_User)Session["Agent"];
                }
                else if (Session["Admin"] != null)
                {

                }
            }
            else
            {
                filterContext.Result = new RedirectResult("/Account/login");
            }
        }

        public object UpdateAgentSession()
        {
            if (User != null && User.Identity.IsAuthenticated)
            {
                var agentId = long.Parse(User.Identity.Name);
                var agent = getAgentById(agentId);
                Session["Agent"] = agent;
                _agent = (tb_User)Session["Agent"];
            }
            return true;
        }

        public tb_User getAgentById(long Id)
        {
            return _context.tb_User.Where(z => z.UserID == Id).FirstOrDefault();
        }

	}
}