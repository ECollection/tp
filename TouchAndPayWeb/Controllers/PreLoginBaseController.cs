﻿using EntityLibrary;
using EntityLibrary.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TouchAndPayWeb.Controllers
{
    public class PreLoginBaseController : Controller
    {
        protected TouchAndPayEntities _context = new TouchAndPayEntities();
        protected UserRepository _userRepository = new UserRepository();
        protected gulfboxRepository _gulfboxRepository = new gulfboxRepository();
        protected adminRepository _adminRepository = new adminRepository();
        public DateTime currentTime = System.DateTime.UtcNow;

        public tb_Admin _admin;
        public tb_User _agent;
        public tDistributor _distributor;
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            //check whether the user is logged in or else
            if (User != null && User.Identity.IsAuthenticated)
            {

                if (Session["Admin"] != null)
                {
                    var userId = long.Parse(User.Identity.Name);
                    var user = getAdminById(userId);
                    Session["Admin"] = user;
                    _admin = (tb_Admin)Session["Admin"];
                    filterContext.Result = new RedirectResult("/Admin/home");
                }
                else if (Session["Agent"] != null)
                {
                    var userId = long.Parse(User.Identity.Name);
                    var user = getAgentById(userId);
                    Session["Agent"] = user;
                    _agent = (tb_User)Session["Agent"];
                    filterContext.Result = new RedirectResult("/Agent/home");
                }
                else if (Session["Distributor"] != null)
                {
                    var distributorId = long.Parse(User.Identity.Name);
                    var distributor = getDistributorById(distributorId);
                    Session["Distributor"] = distributor;
                    _distributor = (tDistributor)Session["Distributor"];
                    filterContext.Result = new RedirectResult("/Distributor/home");
                }
            }
        }

       

        public object UpdateSession()
        {
            if (User != null && User.Identity.IsAuthenticated)
            {
                var adminId = long.Parse(User.Identity.Name);
                var admin = getAdminById(adminId);
                Session["Admin"] = admin;
                _admin = (tb_Admin)Session["Admin"];
            }
            return true;
        }

        public tb_Admin getAdminById(long Id)
        {
            return _context.tb_Admin.Where(z => z.AdminId == Id).FirstOrDefault();
        }

        public object UpdateAgentSession()
        {
            if (User != null && User.Identity.IsAuthenticated)
            {
                var agentId = long.Parse(User.Identity.Name);
                var agent = getAgentById(agentId);
                Session["Agent"] = agent;
                _agent = (tb_User)Session["Agent"];
            }
            return true;
        }

        public tb_User getAgentById(long Id)
        {
            return _context.tb_User.Where(z => z.UserID == Id).FirstOrDefault();
        }
        public tDistributor getDistributorById(long Id)
        {
            return _context.tDistributors.Where(z => z.DistributorID == Id).FirstOrDefault();
        }
    }
}