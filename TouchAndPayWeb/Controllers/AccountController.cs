﻿using ClassLibrary.WebPostModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TouchAndPayWeb.Controllers
{
    public class AccountController : PreLoginBaseController
    {
        public ActionResult login()
        {
            var model = new LoginModel();
            if (Request.Cookies["LoginData"] != null)
            {

                model.isRemember = Convert.ToBoolean(Request.Cookies["LoginData"].Values["RememberMe"]);
                if (model.isRemember)
                {
                    model.mobileNumber = Request.Cookies["LoginData"].Values["MobileNumer"];
                    model.password = Request.Cookies["LoginData"].Values["Password"];
                }

            }
            return View(model);
        }
        [HttpPost]
        public object login(LoginModel model)
        {
            bool status = false;
            string message = "failed";
            Tuple<bool, string, EntityLibrary.tb_Admin, EntityLibrary.tb_User, EntityLibrary.tDistributor, EntityLibrary.tReseller, EntityLibrary.tAgent> t = _adminRepository.login(model);
            status = t.Item1;
            message = t.Item2;
            bool isadmin = false;
            string userType = "";
            if (status)
            {
                if (t.Item3 != null)
                {
                    isadmin = true;
                    Session["Admin"] = t.Item3;
                    Session["isRemember"] = model.isRemember;
                    //if (model.isRemember)
                    //{
                    //    HttpCookie cookie = new HttpCookie("LoginData");
                    //    cookie.Values.Add("Email", model.mobileNumber);
                    //    cookie.Values.Add("Password", model.password);
                    //    cookie.Values.Add("RememberMe", model.isRemember ? "True" : "False");

                    //    cookie.Expires = DateTime.Now.AddDays(15);
                    //    Response.Cookies.Add(cookie);
                    //}
                    //else
                    //{
                    //    HttpCookie cookie = new HttpCookie("LoginData");
                    //    cookie.Expires = DateTime.Now.AddDays(-1);
                    //    Response.Cookies.Add(cookie);
                    //}
                    userType = "Admin";
                }
                else if (t.Item4 != null)
                {
                    Session["Agent"] = t.Item4;
                    Session["Admin"] = null;
                    Session["Distributor"] = null;
                    Session["Reseller"] = null;
                    Session["AgentNew"] = null;
                    if (model.isRemember)
                    {
                        HttpCookie cookie = new HttpCookie("LoginData");
                        cookie.Values.Add("Email", model.mobileNumber);
                        cookie.Values.Add("Password", model.password);
                        cookie.Values.Add("RememberMe", model.isRemember ? "True" : "False");

                        cookie.Expires = DateTime.Now.AddDays(15);
                        Response.Cookies.Add(cookie);
                    }
                    else
                    {
                        HttpCookie cookie = new HttpCookie("LoginData");
                        cookie.Expires = DateTime.Now.AddDays(-1);
                        Response.Cookies.Add(cookie);
                    }
                    userType = "Agent";
                }
                else if (t.Item5 != null)
                {
                    Session["Agent"] = null;
                    Session["Admin"] = null;
                    Session["Distributor"] = t.Item5;
                    Session["DistributorId"] = t.Item5.DistributorID.ToString();
                    Session["Reseller"] = null;
                    Session["AgentNew"] = null;
                    if (model.isRemember)
                    {
                        HttpCookie cookie = new HttpCookie("LoginData");
                        cookie.Values.Add("Email", model.mobileNumber);
                        cookie.Values.Add("Password", model.password);
                        cookie.Values.Add("RememberMe", model.isRemember ? "True" : "False");

                        cookie.Expires = DateTime.Now.AddDays(15);
                        Response.Cookies.Add(cookie);
                    }
                    else
                    {
                        HttpCookie cookie = new HttpCookie("LoginData");
                        cookie.Expires = DateTime.Now.AddDays(-1);
                        Response.Cookies.Add(cookie);
                    }
                    userType = "Distributor";
                }
                else if (t.Item6 != null)
                {
                    Session["Agent"] = null;
                    Session["Admin"] = null;
                    Session["Distributor"] = null;
                    Session["Reseller"] = t.Item6;
                    Session["ResellerId"] = t.Item6.ResellerId.ToString();
                    Session["AgentNew"] = null;
                    if (model.isRemember)
                    {
                        HttpCookie cookie = new HttpCookie("LoginData");
                        cookie.Values.Add("Email", model.mobileNumber);
                        cookie.Values.Add("Password", model.password);                       
                        cookie.Values.Add("RememberMe", model.isRemember ? "True" : "False");

                        cookie.Expires = DateTime.Now.AddDays(15);
                        Response.Cookies.Add(cookie);
                    }
                    else
                    {
                        HttpCookie cookie = new HttpCookie("LoginData");
                        cookie.Expires = DateTime.Now.AddDays(-1);
                        Response.Cookies.Add(cookie);
                    }
                    userType = "Reseller";
                }
                else if (t.Item7 != null)
                {
                    Session["Agent"] = null;
                    Session["Admin"] = null;
                    Session["Distributor"] = null;
                    Session["Reseller"] = null;
                    Session["AgentNew"] = t.Item7;
                    Session["AgentId"] = t.Item7.AgentID.ToString();
                    if (model.isRemember)
                    {
                        HttpCookie cookie = new HttpCookie("LoginData");
                        cookie.Values.Add("Email", model.mobileNumber);
                        cookie.Values.Add("Password", model.password);
                        cookie.Values.Add("RememberMe", model.isRemember ? "True" : "False");

                        cookie.Expires = DateTime.Now.AddDays(15);
                        Response.Cookies.Add(cookie);
                    }
                    else
                    {
                        HttpCookie cookie = new HttpCookie("LoginData");
                        cookie.Expires = DateTime.Now.AddDays(-1);
                        Response.Cookies.Add(cookie);
                    }
                    userType = "AgentNew";
                }
                return Json(new { status = status, message = message, isAdmin = isadmin, userType = userType }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { status = status, message = message, isAdmin = isadmin, userType = userType }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult index()
        {
            return View();
        }

        [HttpPost]
        public object sendContactUsMail(SendContactUsMail model)
        {
            bool status = false;
            string message = "failed";
            Tuple<bool, string> t = _adminRepository.sendContactUsMail(model);
            status = t.Item1;
            message = t.Item2;
            return Json(new { status = status, message = message }, JsonRequestBehavior.AllowGet);
        }


        public ActionResult Distributorlogin()
        {
            var model = new LoginModel();
            if (Request.Cookies["LoginData"] != null)
            {

                model.isRemember = Convert.ToBoolean(Request.Cookies["LoginData"].Values["RememberMe"]);
                if (model.isRemember)
                {
                    model.mobileNumber = Request.Cookies["LoginData"].Values["MobileNumer"];
                    model.password = Request.Cookies["LoginData"].Values["Password"];
                }

            }
            return View(model);
        }


        [HttpPost]
        public object Distributorlogin(LoginModel model)
        {
            bool status = false;
            string message = "failed";
            Tuple<bool, string, EntityLibrary.tDistributor> t = _adminRepository.Distributorlogin(model);
            status = t.Item1;
            message = t.Item2;
            bool isadmin = false;
            if (status)
            {
                Session["Distributor"] = t.Item3;
                Session["Admin"] = null;
                Session["Agent"] = null;
                if (model.isRemember)
                {
                    HttpCookie cookie = new HttpCookie("LoginData");
                    cookie.Values.Add("Email", model.mobileNumber);
                    cookie.Values.Add("Password", model.password);
                    cookie.Values.Add("RememberMe", model.isRemember ? "True" : "False");

                    cookie.Expires = DateTime.Now.AddDays(15);
                    Response.Cookies.Add(cookie);
                }
                else
                {
                    HttpCookie cookie = new HttpCookie("LoginData");
                    cookie.Expires = DateTime.Now.AddDays(-1);
                    Response.Cookies.Add(cookie);
                }
                return Json(new { status = status, message = message, isAdmin = isadmin }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { status = status, message = message, isAdmin = isadmin }, JsonRequestBehavior.AllowGet);
            }
        }

        ///Added Sibi
        ///
        public ActionResult Resellerlogin()
        {
            var model = new LoginModel();
            if (Request.Cookies["LoginData"] != null)
            {

                model.isRemember = Convert.ToBoolean(Request.Cookies["LoginData"].Values["RememberMe"]);
                if (model.isRemember)
                {
                    model.mobileNumber = Request.Cookies["LoginData"].Values["MobileNumer"];
                    model.password = Request.Cookies["LoginData"].Values["Password"];
                }

            }
            return View(model);
        }


        [HttpPost]
        public object Resellerlogin(LoginModel model)
        {
            bool status = false;
            string message = "failed";
            Tuple<bool, string, EntityLibrary.tDistributor> t = _adminRepository.Distributorlogin(model);
            status = t.Item1;
            message = t.Item2;
            bool isadmin = false;
            if (status)
            {
                Session["Distributor"] = t.Item3;
                Session["Admin"] = null;
                Session["Agent"] = null;
                if (model.isRemember)
                {
                    HttpCookie cookie = new HttpCookie("LoginData");
                    cookie.Values.Add("Email", model.mobileNumber);
                    cookie.Values.Add("Password", model.password);
                    cookie.Values.Add("RememberMe", model.isRemember ? "True" : "False");

                    cookie.Expires = DateTime.Now.AddDays(15);
                    Response.Cookies.Add(cookie);
                }
                else
                {
                    HttpCookie cookie = new HttpCookie("LoginData");
                    cookie.Expires = DateTime.Now.AddDays(-1);
                    Response.Cookies.Add(cookie);
                }
                return Json(new { status = status, message = message, isAdmin = isadmin }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { status = status, message = message, isAdmin = isadmin }, JsonRequestBehavior.AllowGet);
            }
        }

    }
}