﻿using EntityLibrary;
using EntityLibrary.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TouchAndPayWeb.Controllers
{
    public class BaseController : Controller
    {
        protected TouchAndPayEntities _context = new TouchAndPayEntities();
        protected UserRepository _userRepository = new UserRepository();
        protected gulfboxRepository _gulfboxRepository = new gulfboxRepository();
        protected adminRepository _adminRepository = new adminRepository();
        protected agentRepository _agentRepository = new agentRepository();
        protected distributorRepository _distributorRepository = new distributorRepository();
        protected ResellerRepository _ResellerRepository = new ResellerRepository();
        public DateTime currentTime = System.DateTime.UtcNow;

        public tb_Admin _admin;
        public tb_User _agent;
        public tDistributor _distributor;
        public tReseller tReseller;
        public tAgent tAgent;
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            //check whether the user is logged in or else
            if (User != null && User.Identity.IsAuthenticated)
            {

                if (Session["Admin"] != null)
                {
                    var userId = long.Parse(User.Identity.Name);
                    var user = getAdminById(userId);
                    Session["Admin"] = user;
                    _admin = (tb_Admin)Session["Admin"];
                    string actionName = this.ControllerContext.RouteData.Values["action"].ToString();
                    string controllerName = this.ControllerContext.RouteData.Values["controller"].ToString();
                    if (actionName == "adminpin")
                    {
                        filterContext.Result = new RedirectResult("/Admin/home");
                    }
                }
                else if (Session["Agent"] != null)
                {
                    var userId = long.Parse(User.Identity.Name);
                    var user = getAgentById(userId);
                    Session["Agent"] = user;
                    _agent = (tb_User)Session["Agent"];
                }
                else if (Session["Distributor"] != null)
                {
                    var distributorId = long.Parse(User.Identity.Name);
                    var distributor = getDistributorById(distributorId);
                    Session["Distributor"] = distributor;
                    _distributor = (tDistributor)Session["Distributor"];
                }
                else if (Session["Reseller"] != null)
                {
                    var resellerId = long.Parse(User.Identity.Name);
                    var reseller = getResellerById(resellerId);
                    Session["Reseller"] = reseller;
                    tReseller = (tReseller)Session["Reseller"];
                }
                else if (Session["AgentNew"] != null)
                {
                    var resellerId = long.Parse(User.Identity.Name);
                    var reseller = getAgentNewById(resellerId);
                    Session["AgentNew"] = reseller;
                    tAgent = (tAgent)Session["AgentNew"];
                }
            }
            else
            {
                if (Session["Admin"] != null)
                {
                    string actionName = this.ControllerContext.RouteData.Values["action"].ToString();
                    string controllerName = this.ControllerContext.RouteData.Values["controller"].ToString();
                    if (actionName != "adminpin" && actionName != "Adminlogin")
                        filterContext.Result = new RedirectResult("/Admin/adminpin");
                }
                else
                {
                    filterContext.Result = new RedirectResult("/Account/login");
                }
            }
        }

        public object UpdateSession()
        {
            if (User != null && User.Identity.IsAuthenticated)
            {
                var adminId = long.Parse(User.Identity.Name);
                var admin = getAdminById(adminId);
                Session["Admin"] = admin;
                _admin = (tb_Admin)Session["Admin"];
            }
            return true;
        }

        public tb_Admin getAdminById(long Id)
        {
            return _context.tb_Admin.Where(z => z.AdminId == Id).FirstOrDefault();
        }

        public object UpdateAgentSession()
        {
            if (User != null && User.Identity.IsAuthenticated)
            {
                var agentId = long.Parse(User.Identity.Name);
                var agent = getAgentById(agentId);
                Session["Agent"] = agent;
                _agent = (tb_User)Session["Agent"];
            }
            return true;
        }

        public tb_User getAgentById(long Id)
        {
            return _context.tb_User.Where(z => z.UserID == Id).FirstOrDefault();
        }
        public tDistributor getDistributorById(long Id)
        {
            return _context.tDistributors.Where(z => z.DistributorID == Id).FirstOrDefault();
        }
        public tReseller getResellerById(long Id)
        {
            return _context.tResellers.Where(z => z.ResellerId == Id).FirstOrDefault();
        }
        public tAgent getAgentNewById(long Id)
        {
            return _context.tAgents.Where(z => z.AgentID == Id).FirstOrDefault();
        }

    }
    //public static class UrlHelperExtensions
    //{
    //    public static string ContentVersioned(this UrlHelper self, string contentPath)
    //    {
    //        string versionedContentPath = contentPath + "?v=" + System.Reflection.Assembly.GetAssembly(typeof(UrlHelperExtensions)).GetName().Version.ToString();
    //        return self.Content(versionedContentPath);
    //    }
    //}
}