﻿using ClassLibrary.WebPostModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace TouchAndPayWeb.Controllers
{
    public class AdminController : BaseController
    {
        //
        // GET: /Admin/
        public ActionResult agents()
        {
            return View();
        }
        [HttpGet]
        public ActionResult logout()
        {
            Session.Clear();
            Session.RemoveAll();
            Session.Abandon();
            FormsAuthentication.SignOut();
            return RedirectToAction("login", "Account");
        }
        public ActionResult agentdetails(string id)
        {
            var model = new AgentDetails();
            model.agentId = Convert.ToInt64(id);
            return View(model);
        }
        public ActionResult addagent()
        {
            var model = new AddAgent();
            return View(model);
        }

        [HttpPost]
        public object addagent(AddAgent model)
        {
            bool status = false;
            string message = "failed";
            Tuple<bool, string> t = _adminRepository.addagent(model);
            status = t.Item1;
            message = t.Item2;
            return Json(new { status = status, message = message }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult home()
        {
            return View();
        }

        [HttpPost]
        public object addrechargecard(Addrechargecard model)
        {
            bool status = false;
            string message = "failed";
            Tuple<bool, string> t = _adminRepository.addrechargecard(model);
            status = t.Item1;
            message = t.Item2;
            return Json(new { status = status, message = message }, JsonRequestBehavior.AllowGet);
        }
        public PartialViewResult refreshRechargeCardlist(RechargeCardlistModel model)
        {
            return PartialView("~/Views/Admin/_pv_ListRechargeCards.cshtml", model);
        }
        public PartialViewResult refreshRechargeCardlistPagination(RechargeCardlistPaginationModel model)
        {
            return PartialView("~/Views/Admin/_pv_ListRechargeCards_Pagination.cshtml", model);
        }

        public PartialViewResult refreshAgentlist(ListAgentModel model)
        {
            return PartialView("~/Views/Admin/_pv_ListAgents.cshtml", model);
        }
        public PartialViewResult refreshAgentlistPagination(ListAgentPaginationModel model)
        {
            return PartialView("~/Views/Admin/_pv_ListAgtents_Pagination.cshtml", model);
        }
        public ActionResult userlist()
        {
            return View();
        }
        public PartialViewResult refreshUserlist(ListUserModel model)
        {
            return PartialView("~/Views/Admin/_pv_listUsers.cshtml", model);
        }
        public PartialViewResult refreshUserlistPagination(ListUserPaginationModel model)
        {
            return PartialView("~/Views/Admin/_pv_listUser_Pagination.cshtml", model);
        }
        public ActionResult userdetails(string id)
        {
            var model = new UserDetails();
            model.userId = Convert.ToInt64(id);
            return View(model);
        }

        public ActionResult usercardlist(string id)
        {
            var model = new ListUserUsedCards();
            model.userId = Convert.ToInt64(id);
            model.index = 0;
            return View(model);
        }
        public PartialViewResult refreshUserUsedCardlist(ListUserUsedCards model)
        {
            return PartialView("~/Views/Admin/_pv_listUserUsedCards.cshtml", model);
        }
        public PartialViewResult refreshUserUsedCardlistPagination(ListUserUsedCardsPaginationModel model)
        {
            return PartialView("~/Views/Admin/_pv_listUserUsedCards_Pagination.cshtml", model);
        }

        public ActionResult agentassignedcards(string id)
        {
            var model = new AgentAssignedCards();
            model.agentId = Convert.ToInt64(id);
            model.index = 0;
            return View(model);
        }

        public PartialViewResult refreshAgentAssignedCardlist(AgentAssignedCards model)
        {
            return PartialView("~/Views/Admin/_pv_list_AgentAssignedCards.cshtml", model);
        }
        public PartialViewResult refreshAgentAssignedCardlistPagination(AgentAssignedCardsPaginationModel model)
        {
            return PartialView("~/Views/Admin/_pv_list_AgentAssignedCards_Pagination.cshtml", model);
        }
        public ActionResult agenttransactions(string id)
        {
            var model = new AgentTransactions();
            model.agentId = Convert.ToInt64(id);
            model.index = 0;
            return View(model);
        }

        public ActionResult usertransactions(string id)
        {
            var model = new UserTransactions();
            model.userId = Convert.ToInt64(id);
            model.index = 0;
            return View(model);
        }
        public ActionResult cards()
        {
            return View();
        }

        public object updateCard(string id)
        {
            bool status = false;
            string message = "failed";
            Tuple<bool, string> t = _adminRepository.updateCard(id);
            status = t.Item1;
            message = t.Item2;
            return Json(new { status = status, message = message }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult adminpin()
        {
            var _admin = (EntityLibrary.tb_Admin)Session["Admin"];
            bool isRemember = (bool)Session["isRemember"];
            var model = new AdminLoginModel();
            if (_admin != null)
            {
                model.mobileNumber = _admin.Mobilenumber;
                model.password = _admin.Password;
                model.isRemember = isRemember;
            }
            return View(model);
        }
        [HttpPost]
        public ActionResult Adminlogin(AdminLoginModel model)
        {
            bool status = false;
            string message = "failed";
            Tuple<bool, string, EntityLibrary.tb_Admin> t = _adminRepository.adminlogin(model);
            status = t.Item1;
            message = t.Item2;
            if (status)
            {
                if (t.Item3 != null)
                {
                    Session["Admin"] = t.Item3;
                    Session["Agent"] = null;
                    Session["Distributor"] = null;
                    if (model.isRemember)
                    {
                        HttpCookie cookie = new HttpCookie("LoginData");
                        cookie.Values.Add("Email", model.mobileNumber);
                        cookie.Values.Add("Password", model.password);
                        cookie.Values.Add("RememberMe", model.isRemember ? "True" : "False");

                        cookie.Expires = DateTime.Now.AddDays(15);
                        Response.Cookies.Add(cookie);
                    }
                    else
                    {
                        HttpCookie cookie = new HttpCookie("LoginData");
                        cookie.Expires = DateTime.Now.AddDays(-1);
                        Response.Cookies.Add(cookie);
                    }
                }
                return Json(new { status = status, message = message }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { status = status, message = message }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult settings()
        {
            return View();
        }

        [HttpPost]
        public object UploadProfileImage(UploadProfileImage model)
        {
            bool status = false;
            string message = "failed";
            Tuple<bool, string> t = _adminRepository.uploadprofileimage(model);
            status = t.Item1;
            message = t.Item2;
            return Json(new { status = status, message = message }, JsonRequestBehavior.AllowGet);
        }
        public object EditName(EditName model)
        {
            bool status = false;
            string message = "failed";
            Tuple<bool, string> t = _adminRepository.editname(model);
            status = t.Item1;
            message = t.Item2;
            return Json(new { status = status, message = message }, JsonRequestBehavior.AllowGet);
        }
        public object EditCountry(EditCountry model)
        {
            bool status = false;
            string message = "failed";
            Tuple<bool, string> t = _adminRepository.editcountry(model);
            status = t.Item1;
            message = t.Item2;
            return Json(new { status = status, message = message }, JsonRequestBehavior.AllowGet);
        }
        public object EditPassword(EditPassword model)
        {
            bool status = false;
            string message = "failed";
            Tuple<bool, string> t = _adminRepository.editpassword(model);
            status = t.Item1;
            message = t.Item2;
            return Json(new { status = status, message = message }, JsonRequestBehavior.AllowGet);
        }

        public object EditMobile(EditMobile model)
        {
            bool status = false;
            string message = "failed";
            Tuple<bool, string> t = _adminRepository.editmobile(model);
            status = t.Item1;
            message = t.Item2;
            return Json(new { status = status, message = message }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public object exportDataToExcel(string type)
        {
            bool status = false;
            string message = "failed";
            Tuple<bool, string, string, string> t = _adminRepository.exportCardDataToExcel(type);
            status = t.Item1;
            message = t.Item2;
            return Json(new { status = status, message = message, filepath = t.Item3, filename = t.Item4 }, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public virtual ActionResult Download(string filename, string filepath)
        {
            return File(filepath, "application/vnd.ms-excel", filename);
        }

        //public object EditPriority(ClassLibrary.ServicePostModel.ApiPriority model)
        public object EditPriority(string data)
        {
            bool status = false;
            string message = "failed";
            Tuple<bool, string> t = _adminRepository.editpriority(data);
            status = t.Item1;
            message = t.Item2;
            return Json(new { status = status, message = message }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public object deactivateagent(string id)
        {
            bool status = false;
            string message = "failed";
            Tuple<bool, string> t = _adminRepository.deactivateagent(id);
            status = t.Item1;
            message = t.Item2;
            return Json(new { status = status, message = message }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult editagent(string id)
        {
            var model = new ClassLibrary.WebPostModel.EditAgent();
            model.agentId = Convert.ToInt64(id);

            var agent = _context.tb_User.FirstOrDefault(z => z.UserID == model.agentId);
            if (agent != null)
            {
                var agentData = new EntityLibrary.Data.User(agent);

                model.address = agentData.Address ?? string.Empty;
                model.countryCode = agentData.CountryCode ?? string.Empty;
                model.mobileNumber = agentData.Mobile ?? string.Empty;
                model.name = agentData.Username ?? string.Empty;
                model.password = agentData.Password ?? string.Empty;
                model.place = agentData.Place ?? string.Empty;
                model.shopName = agentData.ShopName ?? string.Empty;
            }

            return View(model);
        }
        [HttpPost]
        public object editagent(ClassLibrary.WebPostModel.EditAgent model)
        {
            bool status = false;
            string message = "failed";
            Tuple<bool, string> t = _adminRepository.editagent(model);
            status = t.Item1;
            message = t.Item2;
            return Json(new { status = status, message = message }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public object updatecurrencyrate(UpdateCurrencyRate model)
        {
            bool status = false;
            string message = "failed";
            Tuple<bool, string> t = _adminRepository.updatecurrencyrate(model);
            status = t.Item1;
            message = t.Item2;
            return Json(new { status = status, message = message }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public object deleteuser(string id)
        {
            bool status = false;
            string message = "failed";
            Tuple<bool, string> t = _adminRepository.deleteuser(id);
            status = t.Item1;
            message = t.Item2;
            return Json(new { status = status, message = message }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public object updateagentcommissionrates(UpdateCommission model)
        {
            bool status = false;
            string message = "failed";
            Tuple<bool, string> t = _adminRepository.updateagentcommissionrates(model);
            status = t.Item1;
            message = t.Item2;
            return Json(new { status = status, message = message }, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public object importdatafromexcel(ImportDataFromExcel model)
        {
            bool status = false;
            string message = "failed";
            Tuple<bool, string> t = _adminRepository.importdatafromexcel(model);
            status = t.Item1;
            message = t.Item2;
            return Json(new { status = status, message = message }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult importedcards()
        {
            return View();
        }
        public PartialViewResult refreshImportedCardlist(ImportedCardlistModel model)
        {
            return PartialView("~/Views/Admin/_pv_ListImportedCards.cshtml", model);
        }
        public PartialViewResult refreshImportedCardlistPagination(ImportedCardlistPaginationModel model)
        {
            return PartialView("~/Views/Admin/_pv_ListImportedCards_Pagination.cshtml", model);
        }

        public PartialViewResult refreshAgentTransactionlist(AgentTransactions model)
        {
            return PartialView("~/Views/Admin/_pv_agent_transactionlist.cshtml", model);
        }
        public PartialViewResult refreshAgentTransactionlistPagination(AgentTransactionsPaginationModel model)
        {
            return PartialView("~/Views/Admin/_pv_agent_transactionlist_pagination.cshtml", model);
        }

        public PartialViewResult refreshUserTransactionlist(UserTransactions model)
        {
            return PartialView("~/Views/Admin/_pv_user_transactionlist.cshtml", model);
        }
        public PartialViewResult refreshUserTransactionlistPagination(UserTransactionsPaginationModel model)
        {
            return PartialView("~/Views/Admin/_pv_user_transactionlist_pagination.cshtml", model);
        }

        public ActionResult transactions()
        {
            return View();
        }

        public PartialViewResult refreshAllTransactionlist(Transactions model)
        {
            return PartialView("~/Views/Admin/_pv_list_all_transactions.cshtml", model);
        }
        public PartialViewResult refreshAllTransactionlistPagination(TransactionsPaginationModel model)
        {
            return PartialView("~/Views/Admin/_pv_list_all_transactions_pagination.cshtml", model);
        }

        public object DeleteImportedCards(string id)
        {
            bool status = false;
            string message = "failed";
            Tuple<bool, string> t = _adminRepository.deleteimportedcards(id);
            status = t.Item1;
            message = t.Item2;
            return Json(new { status = status, message = message }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public object AddAgentBalance(AddAgentBalance model)
        {
            bool status = false;
            string message = "failed";
            Tuple<bool, string> t = _adminRepository.AddAgentBalance(model);
            status = t.Item1;
            message = t.Item2;
            return Json(new { status = status, message = message }, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public object SubtractAgentBalance(SubtractAgentBalance model)
        {
            bool status = false;
            string message = "failed";
            Tuple<bool, string> t = _adminRepository.SubtractAgentBalance(model);
            status = t.Item1;
            message = t.Item2;
            return Json(new { status = status, message = message }, JsonRequestBehavior.AllowGet);
        }


        [HttpGet]
        public object RefreshTotalSale(string startDate, string endDate, bool fetchall, string currency, string filterType)
        {
            bool status = false;
            string message = "failed";
            Tuple<bool, string, List<EntityLibrary.Data.SP_FETCHALL_TRANSACTION_WITHOUTINDEX>> t = _adminRepository.RefreshTotalSale(startDate, endDate, fetchall, currency, filterType);
            status = t.Item1;
            message = t.Item2;
            string totalAED = "";
            string totalINR = "";
            if (t.Item3.Count > 0)
            {
                var inr = t.Item3.Where(z => z.currency == "INR").ToList().Select(x => x.amount).Sum();
                totalINR = Convert.ToString(inr);

                var aed = t.Item3.Where(z => z.currency == "AED").ToList().Select(x => x.amount).Sum();
                totalAED = Convert.ToString(aed);

            }
            return Json(new { status = status, message = message, AED = totalAED, INR = totalINR }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public object getAllAngentsWalletByCurrency(string currency)
        {
            var sum = new EntityLibrary.Data.AdminService().getWalletSum(currency);
            return Json(new { status = true, message = "success", total = sum }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public object RefreshActualAmountTotalSale(string startDate, string endDate, bool fetchall, string currency, string filterType)
        {
            bool status = false;
            string message = "failed";
            Tuple<bool, string, List<EntityLibrary.Data.SP_FETCHALL_TRANSACTION_WITHOUTINDEX>> t = _adminRepository.RefreshActualAmountTotalSale(startDate, endDate, fetchall, currency, filterType);
            status = t.Item1;
            message = t.Item2;
            string totalsale = "";
            if (t.Item3.Count > 0)
            {
                var res = t.Item3.ToList().Select(x => x.amount).Sum();
                totalsale = Convert.ToString(res);

            }
            return Json(new { status = status, message = message, totalsale = totalsale }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public object RefreshWalletDeductionTotalSale(string startDate, string endDate, bool fetchall, string currency, string filterType)
        {
            bool status = false;
            string message = "failed";
            Tuple<bool, string, List<EntityLibrary.Data.SP_FETCHALL_TRANSACTION_WITHOUTINDEX>> t = _adminRepository.RefreshWalletDeductionTotalSale(startDate, endDate, fetchall, currency, filterType);
            status = t.Item1;
            message = t.Item2;
            string totalsale = "";
            if (t.Item3.Count > 0)
            {
                var res = t.Item3.ToList().Select(x => x.amountdeducted).Sum();
                totalsale = Convert.ToString(res);
            }
            return Json(new { status = status, message = message, totalsale = totalsale }, JsonRequestBehavior.AllowGet);
        }


        public ActionResult credithistory()
        {
            ClassLibrary.WebPostModel.CreditHistoryViewModel model = new CreditHistoryViewModel();
            model.search = "";
            model.listcount = "10";
            model.index = 0;
            return View(model);
        }


        public PartialViewResult refreshCreditlist(CreditHistoryViewModel model)
        {
            return PartialView("~/Views/Admin/_pv_credithistory_list.cshtml", model);
        }
        public PartialViewResult refreshCreditlistPagination(CreditHistoryPaginationViewModel model)
        {
            return PartialView("~/Views/Admin/_pv_credithistory_list_pagination.cshtml", model);
        }


        public ActionResult agentcredithistory(string id)
        {
            ClassLibrary.WebPostModel.CreditHistoryViewModel model = new ClassLibrary.WebPostModel.CreditHistoryViewModel();
            model.AgentId = Convert.ToInt64(id);
            model.search = "";
            model.listcount = "10";
            model.index = 0;
            return View(model);
        }

        public PartialViewResult refreshAgentCreditlist(ClassLibrary.WebPostModel.CreditHistoryViewModel model)
        {
            return PartialView("~/Views/Admin/_pv_agent_credithistory_list.cshtml", model);
        }
        public PartialViewResult refreshAgentCreditlistPagination(ClassLibrary.WebPostModel.CreditHistoryPaginationViewModel model)
        {
            return PartialView("~/Views/Admin/_pv_agent_credithistory_list_pagination.cshtml", model);
        }

        [HttpGet]
        public object forgotPasswordAgent(string id)
        {
            bool status = false;
            string message = "failed";
            Tuple<bool, string> t = _adminRepository.forgotPasswordAgent(id);
            status = t.Item1;
            message = t.Item2;
            return Json(new { status = status, message = message }, JsonRequestBehavior.AllowGet);
        }


        [HttpGet]
        public object blockagent(string id)
        {
            bool status = false;
            string message = "failed";
            Tuple<bool, string> t = _adminRepository.blockagent(id);
            status = t.Item1;
            message = t.Item2;
            return Json(new { status = status, message = message }, JsonRequestBehavior.AllowGet);
        }

        public object UpdateAndroidversion(UpdateAndroidversion model)
        {
            bool status = false;
            string message = "failed";
            Tuple<bool, string> t = _adminRepository.UpdateAndroidversion(model);
            status = t.Item1;
            message = t.Item2;
            return Json(new { status = status, message = message }, JsonRequestBehavior.AllowGet);
        }
        public object UpdateIosversion(UpdateIosversion model)
        {
            bool status = false;
            string message = "failed";
            Tuple<bool, string> t = _adminRepository.UpdateIosversion(model);
            status = t.Item1;
            message = t.Item2;
            return Json(new { status = status, message = message }, JsonRequestBehavior.AllowGet);
        }


        public ActionResult distributors()
        {
            return View();
        }
        public ActionResult addDistributor()
        {
            var model = new AddDistributor();
            return View(model);
        }

        [HttpPost]
        public object addDistributor(AddDistributor model)
        {
            bool status = false;
            string message = "failed";
            Tuple<bool, string> t = _adminRepository.addDistributor(model);
            status = t.Item1;
            message = t.Item2;
            return Json(new { status = status, message = message }, JsonRequestBehavior.AllowGet);
        }


        public PartialViewResult refreshDistributorlist(ListDistributorModel model)
        {
            return PartialView("~/Views/Admin/_pv_ListDistributors.cshtml", model);
        }
        public PartialViewResult refreshDistributorlistPagination(ListDistributorPaginationModel model)
        {
            return PartialView("~/Views/Admin/_pv_ListDistributors_Pagination.cshtml", model);
        }


        public ActionResult editdistributor(string id)
        {
            var model = new ClassLibrary.WebPostModel.EditDistributor();
            model.distributorId = Convert.ToInt64(id);

            var agent = _context.tDistributors.FirstOrDefault(z => z.DistributorID == model.distributorId);
            if (agent != null)
            {
                var distributorData = new EntityLibrary.Data.Distributor(agent);

                if (distributorData != null)
                {
                    model.address = distributorData.Address ?? string.Empty;
                    model.countryCode = distributorData.CountryCode ?? string.Empty;
                    model.mobileNumber = distributorData.Mobile ?? string.Empty;
                    model.name = distributorData.Username ?? string.Empty;
                    model.password = distributorData.Password ?? string.Empty;
                    model.place = distributorData.Place ?? string.Empty;
                    model.shopName = distributorData.ShopName ?? string.Empty;
                    model.email = distributorData.Email ?? string.Empty;

                    if (distributorData.commissions.Count > 0)
                    {
                        model.commision_DuTopup = Convert.ToString(distributorData.commissions.Where(z => z.ProductType == (int)ClassLibrary.Enum.GulfboxProductTypes.Du).FirstOrDefault().Commission);
                        model.commision_DuVoucher = Convert.ToString(distributorData.commissions.Where(z => z.ProductType == (int)ClassLibrary.Enum.GulfboxProductTypes.DuVoucher).FirstOrDefault().Commission);
                        model.commision_EtisalatTopup = Convert.ToString(distributorData.commissions.Where(z => z.ProductType == (int)ClassLibrary.Enum.GulfboxProductTypes.Etisalat).FirstOrDefault().Commission);
                        model.commision_EtisalatVoucher = Convert.ToString(distributorData.commissions.Where(z => z.ProductType == (int)ClassLibrary.Enum.GulfboxProductTypes.EtisalatVoucher).FirstOrDefault().Commission);
                        model.commision_Five = Convert.ToString(distributorData.commissions.Where(z => z.ProductType == (int)ClassLibrary.Enum.GulfboxProductTypes.Five).FirstOrDefault().Commission);
                        model.commision_Hello = Convert.ToString(distributorData.commissions.Where(z => z.ProductType == (int)ClassLibrary.Enum.GulfboxProductTypes.Hello).FirstOrDefault().Commission);
                        model.commision_Salik = Convert.ToString(distributorData.commissions.Where(z => z.ProductType == (int)ClassLibrary.Enum.GulfboxProductTypes.Salik).FirstOrDefault().Commission);
                        model.commision_India = Convert.ToString(distributorData.commissions.Where(z => z.ProductType == (int)ClassLibrary.Enum.GulfboxProductTypes.NoProduct).FirstOrDefault().Commission);
                        model.commision_OtherCountry = Convert.ToString(distributorData.commissions.Where(z => z.ApiProvider == (int)ClassLibrary.Enum.apiProvider.Gulfbox && z.ProductType == (int)ClassLibrary.Enum.GulfboxProductTypes.Intl).FirstOrDefault().Commission);
                        model.commision_International = Convert.ToString(distributorData.commissions.Where(z => z.ApiProvider == (int)ClassLibrary.Enum.apiProvider.Ding && z.ProductType == (int)ClassLibrary.Enum.GulfboxProductTypes.Intl).LastOrDefault().Commission);
                    }

                    if (distributorData.permissions.Count > 0)
                    {
                        model.permission_IndiaRegion = distributorData.permissions.Any(z => z.Type == ClassLibrary.Enum.DistributorPermissions.India.ToString());
                        model.permission_International = distributorData.permissions.Any(z => z.Type == ClassLibrary.Enum.DistributorPermissions.International.ToString());
                        model.permission_Uae = distributorData.permissions.Any(z => z.Type == ClassLibrary.Enum.DistributorPermissions.UAE.ToString());
                    }

                }
            }

            return View(model);
        }


        [HttpPost]
        public object editDistributor(EditDistributor model)
        {
            bool status = false;
            string message = "failed";
            Tuple<bool, string> t = _adminRepository.editDistributor(model);
            status = t.Item1;
            message = t.Item2;
            return Json(new { status = status, message = message }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public object deactivateDistributor(string id)
        {
            bool status = false;
            string message = "failed";
            Tuple<bool, string> t = _adminRepository.deactivateDistributor(id);
            status = t.Item1;
            message = t.Item2;
            return Json(new { status = status, message = message }, JsonRequestBehavior.AllowGet);
        }


        public ActionResult distributorcredit(string id)
        {
            var model = new ClassLibrary.WebPostModel.DistributorCredit();
            model.distributorId = Convert.ToInt64(id);
            return View(model);
        }

        [HttpPost]
        public object AddDistributorCredit(DistributorCredit model)
        {
            bool status = false;
            string message = "failed";
            Tuple<bool, string> t = _adminRepository.AddDistributorCredit(model);
            status = t.Item1;
            message = t.Item2;
            return Json(new { status = status, message = message }, JsonRequestBehavior.AllowGet);
        }

        public PartialViewResult refreshDistributorCreditlist(ClassLibrary.WebPostModel.DistributorCreditHistoryListModel model)
        {
            return PartialView("~/Views/Admin/_pv_distributor_Credit_HistoryList.cshtml", model);
        }
        public PartialViewResult refreshDistributorCreditlistPagination(ClassLibrary.WebPostModel.DistributorCreditHistoryListPaginationModel model)
        {
            return PartialView("~/Views/Admin/_pv_distributor_Credit_History_Pagination.cshtml", model);
        }

        [HttpPost]
        public object RefreshCreditSum(RefreshCreditSum model)
        {
            bool status = false;
            string message = "failed";
            Tuple<bool, string, string> t = _adminRepository.RefreshCreditSum(model);
            status = t.Item1;
            message = t.Item2;
            return Json(new { status = status, message = message, sum = t.Item3 }, JsonRequestBehavior.AllowGet);
        }


        public ActionResult DistributorTransactions()
        {
            var model = new Distributor();
            model.DistributorLists = _adminRepository.DistributorLists();
            
            return View(model);
        }
        [HttpPost]
        public JsonResult ResellerLists(long id) 
        {
            var model = _adminRepository.ResellerLists(id);
            return Json(model,JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult AgentLists(long id)
        {
            var model = _adminRepository.AgentLists(id);
            return Json(model, JsonRequestBehavior.AllowGet);
        }

    }
}