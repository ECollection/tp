﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace TouchAndPayWeb.Controllers
{
    public class AgentNewController : BaseController
    {
        //
        // GET: /AgentNew/
        public ActionResult home()
        {
            return View();
        }

        public ActionResult editprofile(string id)
        {
            var model = new ClassLibrary.WebPostModel.EditAgent();
            model.agentId = Convert.ToInt64(id);

            var agent = _context.tAgents.FirstOrDefault(z => z.AgentID == model.agentId);
            if (agent != null)
            {

                var agentData = new EntityLibrary.Data.Agent_New(agent);

                model.address = agentData.Address ?? string.Empty;
                model.countryCode = agentData.CountryCode ?? string.Empty;
                model.mobileNumber = agentData.Mobile ?? string.Empty;
                model.name = agentData.Username ?? string.Empty;
                model.password = agentData.Password ?? string.Empty;
                model.place = agentData.Place ?? string.Empty;
                model.shopName = agentData.ShopName ?? string.Empty;
            }

            return View(model);
        }

        public ActionResult credithistory()
        {
            ClassLibrary.WebPostModel.CreditHistoryViewModel model = new ClassLibrary.WebPostModel.CreditHistoryViewModel();
            if (_agent != null)
            {
                model.AgentId = Convert.ToInt64(_agent.UserID);
            }
            model.search = "";
            model.listcount = "10";
            model.index = 0;
            return View(model);
        }

        public ActionResult transactions(string id)
        {

            var model = new ClassLibrary.WebPostModel.AgentTransactions();
            model.agentId = Convert.ToInt64(id);
            model.index = 0;
            return View(model);
        }

        public ActionResult cardlist()
        {
            if (_agent != null)
            {
                var model = new ClassLibrary.WebPostModel.AgentAssignedCards();
                model.agentId = Convert.ToInt64(tAgent.AgentID);
                model.index = 0;
                return View(model);
            }
            else
            {
                Session.Clear();
                Session.RemoveAll();
                Session.Abandon();
                FormsAuthentication.SignOut();
                return RedirectToAction("login", "Account");
            }
        }
        [HttpGet]
        public ActionResult logout()
        {
            Session.Clear();
            Session.RemoveAll();
            Session.Abandon();
            FormsAuthentication.SignOut();
            return RedirectToAction("login", "Account");
        }
	}
}