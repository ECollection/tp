﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TouchAndPayWeb.Models
{
    public class CreditHistoryModel
    {
      public long  Id{get;set;}
public long AgentId{get;set;}
public decimal Amount{get;set;}
public string Type{get;set;}
public DateTime Date { get; set; }
public string AgentName { get; set; }
    }
}