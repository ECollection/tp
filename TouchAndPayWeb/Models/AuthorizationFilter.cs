﻿using EntityLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http.Controllers;
using System.Net.Http;

namespace TouchAndPayWeb.Models
{
    public class AuthorizationFilter : System.Web.Http.Filters.ActionFilterAttribute
    {
        protected TouchAndPayEntities _context = new TouchAndPayEntities();
        protected string salt = System.Configuration.ConfigurationManager.AppSettings["salt"];
        public override void OnActionExecuting(HttpActionContext actionContext)
        {

            string actionName = actionContext.ActionDescriptor.ActionName;
            string controllerName = actionContext.ControllerContext.ControllerDescriptor.ControllerName;

            var re = actionContext.Request;
            var headers = re.Headers;

            if (headers.Contains("x-token"))
            {
                string token = headers.GetValues("x-token").First();
                try
                {
                    var validToken = _context.tAccessTokens.Where(z => z.Token == token).FirstOrDefault();
                    if (validToken == null)
                    {
                        actionContext.Response = actionContext.Request.CreateResponse(
              HttpStatusCode.Unauthorized,
              new { status = false, message = "Invalid Token" },
              actionContext.ControllerContext.Configuration.Formatters.JsonFormatter);
                    }
                }
                catch (Exception ex)
                {
                    actionContext.Response = actionContext.Request.CreateResponse(
             HttpStatusCode.Unauthorized,
             new { status = false, message = ex.Message },
             actionContext.ControllerContext.Configuration.Formatters.JsonFormatter);
                }
            }

        }
    }
}