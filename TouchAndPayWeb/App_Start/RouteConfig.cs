﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace TouchAndPayWeb
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");


            // Admin/
            routes.MapRoute(
                name: "Admin",
                //url: "Admin/{action}/{id}",
                url: "Admin",
                defaults: new
                {
                    controller = "Account",
                    action = "login",
                    id = UrlParameter.Optional
                }
            );

            // Agent/
            routes.MapRoute(
                name: "Agent",
                //url: "Admin/{action}/{id}",
                url: "Agent",
                defaults: new
                {
                    controller = "Account",
                    action = "login",
                    id = UrlParameter.Optional
                }
            );

            // Distributor/
            routes.MapRoute(
                name: "Distributor",
                //url: "Admin/{action}/{id}",
                url: "Distributor",
                defaults: new
                {
                    controller = "Account",
                    action = "login",
                    id = UrlParameter.Optional
                }
            );





            //// Admin/
            //routes.MapRoute(
            //    name: "Admin Dynamic route",
            //    url: "Admin/{action}/{id}",
            //    //url: "Admin",
            //    defaults: new
            //    {
            //        controller = "Account",
            //        action = "login",
            //        id = UrlParameter.Optional
            //    }
            //);


            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                //defaults: new { controller = "Account", action = "login", id = UrlParameter.Optional }
                defaults: new { controller = "Account", action = "index", id = UrlParameter.Optional }
            );
        }
    }
}
